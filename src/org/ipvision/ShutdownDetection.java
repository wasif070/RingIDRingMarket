package org.ipvision;

import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.ipvision.ringmarket.cache.ProductCategoryCacheReloader;
import org.ipvision.ringmarket.cache.redis.CacheStatLogger;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.io.MarketAcceptor;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShutdownDetection extends Thread {

    private static ShutdownDetection instance;
    private volatile boolean running;
    private final long fileDeleteInterval = 1000L;
    private static final File file = new File("shutdown.dat");
    Logger logger = LoggerFactory.getLogger(ShutdownDetection.class.getSimpleName());

    private Thread productViewSycnScheduler = new Thread(() -> {
        try {
            MarketRepository.INSTANCE.getProductViewRepository().syncProductView();
            TimeUnit.HOURS.sleep(2);
        } catch (InterruptedException ex) {
        }
    });

    public static ShutdownDetection getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new ShutdownDetection();
            instance.productViewSycnScheduler.start();
        }
    }

    public ShutdownDetection() {
        running = true;
        while (file.exists()) {
            try {
                file.delete();
                TimeUnit.MILLISECONDS.sleep(fileDeleteInterval);
            } catch (InterruptedException ex) {
            }
        }
    }

    public void stopService() {
        try {
            running = false;
            ProductCategoryCacheReloader.getInstance().stopReloader();
            CacheStatLogger.getInstance().stopService();
            instance.productViewSycnScheduler.join(3000);
            MarketAcceptor.getInstance().stop();
            System.out.println("ringMarket server  has been stopped at: " + new Date() + "\n");
            logger.info("ringMarket server has been stopped at: " + new Date());

            LogManager.shutdown();

            System.exit(0);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void run() {
        this.setName("ShutdownDetection");
        System.out.println("ShutdownDetection is running...");
        logger.info("ShutdownDetection is running");
        while (running) {
            if (file.exists()) {
                if (file.delete()) {
                    instance.stopService();
                    logger.info("Closing shutdown detection..");

                    DBConnection.getInstance().closeFactory();
                    logger.info("Closed EntityManager Factory..");

                }
            }
            try {
                Thread.sleep(fileDeleteInterval);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
