package org.ipvision.ringmarket.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alamgir
 */
import org.ipvision.ShutdownDetection;
import org.ipvision.ringmarket.cache.ProductCategoryCacheReloader;
import org.ipvision.ringmarket.cache.redis.CacheStatLogger;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.io.MarketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MarketController {

    private static final RequestController controller = new RequestController();
    private static final Logger logger = LoggerFactory.getLogger(MarketController.class);

    public static void main(String[] args) {
        try {
            DBConnection.getInstance();
            logger.info("database connection ok.");
            
            CacheStatLogger.getInstance().start();
            System.out.println("Product Category cache loading.");
            ProductCategoryCacheReloader.getInstance().start();
            
            ShutdownDetection.getInstance().start();
            MarketAcceptor.getInstance().startProcess(controller);
            System.out.println("Market TCP server started");

        } catch (Exception ex) {
            logger.error("Server running failed.", ex);
        }

    }
}
