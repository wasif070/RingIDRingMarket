/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.controller.annotation;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.ipvision.ringmarket.controller.action.Action;

/**
 *
 * @author saikat
 */
@Repeatable(Request.List.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface Request {

    Action action();

    int timeOut() default 1000 * 60 * 2;

    @Retention(RetentionPolicy.RUNTIME)
    @interface List {
        Request[] value();
    }

}
