/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author alamgir
 */
public class StatProvider {

    public static void main(String[] args) {
        Set<Long> users = new HashSet<>();
        Map<String, Integer> actnFreq = new HashMap<>();

        try {
            Scanner input = new Scanner(new File("log/ringmarket.log"));
            int requestCount = 0;

            while (input.hasNextLine()) {
                String line = input.nextLine();
                if (line.contains("USER") && line.contains("ACTION")) {
                    requestCount++;
                    Pattern userIdPattern = Pattern.compile("(USER\\s*:\\s*)([0-9]*)");
                    Pattern actionPattern = Pattern.compile("(ACTION\\s*:\\s*)([A-Z_]*)([()0-9]*)");

                    Matcher userIdMatcher = userIdPattern.matcher(line);

                    if (userIdMatcher.find()) {
                        users.add(Long.parseLong(userIdMatcher.group(2)));
                    }
                    Matcher actionMatcher = actionPattern.matcher(line);
                    if (actionMatcher.find()) {
                        String actn = actionMatcher.group(2);
                        if (actnFreq.containsKey(actn)) {
                            actnFreq.put(actn, actnFreq.get(actn) + 1);
                        } else {
                            actnFreq.put(actn, 1);
                        }
                    }

                }
            }
            System.out.println("Total Request Count: " + requestCount);
            System.out.println("Total Unique users: " + users.size());
            users.stream().forEach((user) -> {
                System.out.print(user + ", ");
            });
            System.err.println("");
            actnFreq.keySet().stream().forEach((key) -> {
                System.out.println("ACTION : " + key + ", FREQ : " + actnFreq.get(key));
            });
        } catch (FileNotFoundException | NumberFormatException ex) {
            ex.printStackTrace();
        } finally {

        }
    }
}
