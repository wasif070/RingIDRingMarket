/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.controller;

import com.google.gson.JsonObject;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.controller.annotation.Request;
import org.ipvision.ringmarket.service.ClientRequestHandler;
import org.ipvision.ringmarket.sender.ISender;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class RequestController {
    
    private final Set<String> classNames;
    private final Map<Integer, MethodHandle> actionMap;
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(RequestController.class);
    
    private final ClientRequestHandler requestHandler;
    
    public RequestController() {
        classNames = new HashSet<>();
        actionMap = new HashMap<>();
        this.requestHandler = new ClientRequestHandler();
        buildDelegateMap();
    }
    
    private void importRequestDelegates() {
        classNames.add("org.ipvision.ringmarket.handler.shop.ShopHandler");
        classNames.add("org.ipvision.ringmarket.handler.category.CategoryHandler");
        classNames.add("org.ipvision.ringmarket.handler.product.ProductHandler");
        classNames.add("org.ipvision.ringmarket.handler.category.MenuHandler");
        classNames.add("org.ipvision.ringmarket.handler.user.UserHandler");
        classNames.add("org.ipvision.ringmarket.handler.user.BuyerHandler");
        classNames.add("org.ipvision.ringmarket.handler.user.SellerHandler");
        classNames.add("org.ipvision.ringmarket.handler.user.AgentHandler");
    }
    
    private void buildDelegateMap() {
        
        importRequestDelegates();
        
        for (String className : classNames) {
            try {
                Class delegate = Class.forName(className);
                MethodHandles.Lookup lookup = MethodHandles.lookup();
                
                for (Method method : delegate.getDeclaredMethods()) {
                    if (method.isAnnotationPresent(Request.List.class)) {
                        Request.List requests = (Request.List) method.getAnnotation(Request.List.class);
                        
                        Request[] value = requests.value();
                        for (Request clientRequest : value) {
                            findAndMapRequest(clientRequest, lookup, method, delegate);
                        }
                    } else if (method.isAnnotationPresent(Request.class)) {
                        Request clientRequest = (Request) method.getAnnotation(Request.class);
                        findAndMapRequest(clientRequest, lookup, method, delegate);
                    }
                }
            } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | SecurityException ex) {
                LOGGER.error("Exception in buildDelegateMap() ", ex);
            }
        }
        
    }
    
    private void findAndMapRequest(Request clientRequest, MethodHandles.Lookup lookup, Method method, Class delegate) throws NoSuchMethodException, IllegalAccessException {
        Action action = clientRequest.action();
        MethodType methodType = MethodType.methodType(void.class, JsonObject.class, ISender.class);
        mapRequest(action, lookup.findStatic(delegate, method.getName(), methodType));
    }
    
    private void mapRequest(Action action, MethodHandle method) {
        if (!actionMap.containsKey(action.getValue())) {
            actionMap.put(action.getValue(), method);
        }
    }
    
    private MethodHandle getMethod(int action) {
        if (actionMap.containsKey(action)) {
            return actionMap.get(action);
        }
        
        return null;
    }
    
    public void getResponse(JsonObject request, boolean isTCPRequest, ISender sender) {
        if (request.has(MarketAttribute.ACTION)) {
            int action = request.get(MarketAttribute.ACTION).getAsInt();
            if (action == 200) {
                sender.sendToClient(request);
                return;
            }
            MethodHandle method = getMethod(action);
            if (method == null) {
                LOGGER.error("no method found for {action : " + action + " }");
                JsonObject response = new JsonObject();
                response.addProperty(MarketAttribute.SUCCESS, Boolean.FALSE);
                response.addProperty(MarketAttribute.MESSAGE, "Bad Request");
                response.addProperty(MarketAttribute.REASON_CODE, ReasonCode.INVALID_REQUEST);
                response.add(MarketAttribute.PACKET_ID, request.get(MarketAttribute.PACKET_ID));
                response.add(MarketAttribute.ACTION, request.get(MarketAttribute.ACTION));
                sender.sendToClient(response);
            } else {
                LOGGER.debug("actn: " + action + " ,request: " + request.toString());
                if (isTCPRequest) {
                    requestHandler.handleTCP(method, request, sender);
                } else {
                    requestHandler.handleHTTP(method, request, sender);
                }
            }
        } else {
            LOGGER.error("action key not found in request");
            JsonObject response = new JsonObject();
            response.addProperty(MarketAttribute.SUCCESS, Boolean.FALSE);
            response.addProperty(MarketAttribute.MESSAGE, "Please provide valid request");
            response.add(MarketAttribute.PACKET_ID, request.get(MarketAttribute.PACKET_ID));
            sender.sendToClient(response);
        }
        
    }
}
