package org.ipvision.ringmarket.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alamgir
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.ipvision.ShutdownDetection;
import org.ipvision.ringmarket.cache.ProductCategoryCacheReloader;
import org.ipvision.ringmarket.cache.redis.CacheStatLogger;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.io.MarketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@Controller
@EnableAutoConfiguration
public class MarketControllerAll {

    private static RequestController controller;
    private String responseString = null;
    private static final Logger logger = LoggerFactory.getLogger(MarketControllerAll.class);
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public MarketControllerAll() {
    }

    @RequestMapping("/market")
    @ResponseBody
    String home(@RequestParam("request") String request) {
        logger.trace("request : " + request);
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(request).getAsJsonObject();
        logger.info("http request : " + request);
        controller.getResponse(json, false, (JsonObject response) -> {
            responseString = response.toString();
            logger.info("http response : " + responseString);
            JsonObject fromJson = gson.fromJson(responseString, JsonObject.class);
            responseString = gson.toJson(fromJson);
        });

        return responseString;
    }

    public static void main(String[] args) {
        try {
            controller = new RequestController();
            DBConnection.getInstance();
            logger.info("database connection ok.");

            ShutdownDetection.getInstance().start();
            MarketAcceptor.getInstance().startProcess(controller);
            System.out.println("Market TCP server started");

            CacheStatLogger.getInstance().start();

            System.out.println("Product Category cache loading.");
            ProductCategoryCacheReloader.getInstance().start();

            SpringApplication.run(MarketControllerAll.class, args);
            System.out.println("Market WEB server started");
            
        } catch (Exception ex) {
            logger.error("Server running failed.", ex);
        }

    }
}
