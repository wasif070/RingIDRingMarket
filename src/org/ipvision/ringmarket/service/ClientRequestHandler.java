/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.service;

import com.google.gson.JsonObject;
import java.lang.invoke.MethodHandle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.ipvision.ringmarket.sender.ISender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class ClientRequestHandler {

    private final ExecutorService executorService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientRequestHandler.class);

    public ClientRequestHandler() {
        this.executorService = Executors.newFixedThreadPool(100);
        ThreadPoolExecutor executor = (ThreadPoolExecutor) executorService;
        executor.setKeepAliveTime(200, TimeUnit.MILLISECONDS);
    }

    public void handleHTTP(MethodHandle handle, JsonObject jsonObject, ISender sender) {
        try {
            handle.invoke(jsonObject, sender);
        } catch (Throwable ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
    }

    public void handleTCP(MethodHandle handle, JsonObject jsonObject, ISender sender) {
        try {
            executorService.execute(new MethodInvoker(handle, jsonObject, sender));
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
    }

    public void stop() {
        executorService.shutdownNow();
    }

}
