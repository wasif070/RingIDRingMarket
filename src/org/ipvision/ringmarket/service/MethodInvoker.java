/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.service;

import com.google.gson.JsonObject;
import java.lang.invoke.MethodHandle;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.sender.ISender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class MethodInvoker implements Runnable {

    private final JsonObject jsonObject;
    private final MethodHandle methodHandle;
    private final ISender iSender;
    private final long TIMEOUT_TIME = 1000 * 60;
    private final long processingStartTime;
    private final Logger LOGGER = LoggerFactory.getLogger(MethodInvoker.class);

    public MethodInvoker(MethodHandle methodHandle, JsonObject jsonObject, ISender sender) {
        this.methodHandle = methodHandle;
        this.jsonObject = jsonObject;
        this.iSender = sender;
        this.processingStartTime = System.currentTimeMillis();
    }

    @Override
    public void run() {
        long elapsedTime = System.currentTimeMillis() - processingStartTime;
        if (elapsedTime < TIMEOUT_TIME) {
            try {
                methodHandle.invoke(jsonObject, iSender);
            } catch (Exception  ex) {
                LOGGER.error("Exception in Method Handling", ex);
                sendExceptionPacket();
            } catch (Throwable ex) {
                LOGGER.error("Exception in Method Handling", ex);
                sendExceptionPacket();
            }
        } else {
            LOGGER.debug("Timeout for invoking.");
        }
    }

    private void sendExceptionPacket() {
        JsonObject response = new JsonObject();
        response.addProperty(MarketAttribute.SUCCESS, Boolean.FALSE);
        response.addProperty(MarketAttribute.MESSAGE, "Something went wrong. Please try again!");
        response.addProperty(MarketAttribute.REASON_CODE, ReasonCode.EXCEPTION_OCCURED);
        response.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        
        iSender.sendToClient(response);
    }

}
