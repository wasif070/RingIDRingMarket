/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.dtos.orders;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.math.BigDecimal;
import org.ipvision.ringmarket.constants.MarketAttribute;

/**
 *
 * @author saikat
 */
public class InvoiceDTO implements Serializable{
    
    @SerializedName(MarketAttribute.INVOICE_ID)
    private String invoiceId;
    @SerializedName(MarketAttribute.USER_ID)
    private Long userId;
    @SerializedName(MarketAttribute.COUNT)
    private Long count;
    @SerializedName(MarketAttribute.PRICE)
    private BigDecimal price;
    @SerializedName(MarketAttribute.ADDED_TIME)
    private Long time;

    public InvoiceDTO(String invoiceId, Long userId, Long count, BigDecimal price, Long time) {
        this.invoiceId = invoiceId;
        this.userId = userId;
        this.count = count;
        this.price = price;
        this.time = time;
    }
    
    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
    
    
}
