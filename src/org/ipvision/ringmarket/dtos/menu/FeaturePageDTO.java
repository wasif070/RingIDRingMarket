/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.dtos.menu;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import org.ipvision.ringmarket.constants.MarketAttribute;

/**
 *
 * @author saikat
 */
public class FeaturePageDTO  implements Serializable{
    @SerializedName(MarketAttribute.ID)
    private int id;
    @SerializedName(MarketAttribute.NAME)
    private String name;
    @SerializedName(MarketAttribute.TYPE)
    private int type;
    
    private List<?> items;

    public void setItems(List<?> items) {
        this.items = items;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
