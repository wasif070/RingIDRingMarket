/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.retryutil;

import org.ipvision.ringmarket.retryutil.Retry;

/**
 *
 * @author alamgir
 */
public class Main {
    @Retry(retryAttempts = 2,sleepInterval = 1500, ignoreExceptions = {})
    public void m1(String param) throws Exception{
        System.out.println(param);
        throw new Exception("Ex............");
    }
    
    public static void main(String[] args) {
        Main m = new Main();
        try {
            m.m1("Hi");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}
