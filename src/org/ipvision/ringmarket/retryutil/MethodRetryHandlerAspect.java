/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.retryutil;

/**
 *
 * @author alamgir
 */
import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class MethodRetryHandlerAspect {

    private static Logger logger = LoggerFactory.getLogger(MethodRetryHandlerAspect.class);

    @Around("@annotation(org.ipvision.ringmarket.retryutil.Retry)")
    public Object audit(ProceedingJoinPoint pjp) {
        Object result = null;
        result = retryableExecute(pjp);
        return result;
    }

    protected Object retryableExecute(final ProceedingJoinPoint pjp) {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();

        Retry retry = method.getDeclaredAnnotation(Retry.class);
        int retryAttempts = retry.retryAttempts();
        long sleepInterval = retry.sleepInterval();
        Class<? extends Throwable>[] ignoreExceptions = retry.ignoreExceptions();

        Task<Object> task = new Task<Object>() {
            @Override
            public Object execute() {
                try {
                    return pjp.proceed();
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }
            }
        };
        return TaskExecutionUtil.execute(task, retryAttempts, sleepInterval, ignoreExceptions);
    }
}
