/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.retryutil;

/**
 *
 * @author alamgir
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
 
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public  @interface Retry {
   
  public int retryAttempts() default 3;
   
  public long sleepInterval() default 1000L; //milliseconds
   
  Class<? extends Throwable>[] ignoreExceptions() default { RuntimeException.class };
   
}