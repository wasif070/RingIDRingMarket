/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.handler.product;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import java.util.Map;
import org.ipvision.ringmarket.constants.DiscountUnit;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.Messages;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.SortOrderType;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.SearchType;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.controller.annotation.Request;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.EntityHistoryProduct;
import org.ipvision.ringmarket.entities.EntityOfferProduct;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.product.EntityProductImage;
import org.ipvision.ringmarket.entities.EntityProductVideo;
import org.ipvision.ringmarket.entities.EntityRingMarketUser;
import org.ipvision.ringmarket.entities.EntitySellerProduct;
import org.ipvision.ringmarket.entities.discount.EntityProductDiscount;
import org.ipvision.ringmarket.entities.product.EntityProductDetail;
import org.ipvision.ringmarket.entities.product.EntityProductRating;
import org.ipvision.ringmarket.entities.product.EntityProductStock;
import org.ipvision.ringmarket.sender.ISender;
import org.ipvision.ringmarket.repositories.product.HistoryProduct;
import org.ipvision.ringmarket.repositories.user.MarketUserRepository;
import org.ipvision.ringmarket.repositories.buyer.OfferProductRepository;
import org.ipvision.ringmarket.repositories.product.ProductRepository;
import org.ipvision.ringmarket.repositories.buyer.ProductViewRepository;
import org.ipvision.ringmarket.repositories.buyer.ReportProductRepository;
import org.ipvision.ringmarket.repositories.seller.SellerProductRepository;
import org.ipvision.ringmarket.utils.JsonValidator;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;

/**
 *
 * @author saikat
 */
public class ProductHandler {

    private static final Gson gson = new Gson();
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductHandler.class);

    @Request(action = Action.GET_FEATURED_PRODUCTS)
    public static void getFeaturedProducts(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String productID = null;
        int scrollValue = 0;
        int limit = 20;
        JsonElement element;
        element = jsonObject.get("productId");
        if (JsonValidator.hasValidString(element)) {
            productID = element.getAsString();
        }

        element = jsonObject.get("scrl");
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue > 0) ? Scroll.UP : Scroll.DOWN;

        element = jsonObject.get("lmt");
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        ProductRepository product = MarketRepository.INSTANCE.getProductRepository();
        ListReturnDTO<EntityProduct> exclusiveProducts = product.getExclusiveProducts(productID, scroll, limit);
        List<EntityProduct> featuredProudcts = exclusiveProducts.getList();
        if (featuredProudcts != null && !featuredProudcts.isEmpty()) {
            responseJsonObject.addProperty("sucs", Boolean.TRUE);
            responseJsonObject.add("featuredProducts", gson.toJsonTree(featuredProudcts));
        } else {
            responseJsonObject.addProperty("sucs", Boolean.FALSE);
            responseJsonObject.addProperty("mg", "No Data found");
        }
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_MOST_VIEWED_PRODUCTS)
    public static void getMostViewedProducts(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String productID = null;
        int scrollValue = 0;
        int limit = 20;
        JsonElement element;
        element = jsonObject.get("productId");
        if (JsonValidator.hasValidString(element)) {
            productID = element.getAsString();
        }

        element = jsonObject.get("scrl");
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }

        element = jsonObject.get("lmt");
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        Scroll scroll = (scrollValue > 0) ? Scroll.UP : Scroll.DOWN;

        ProductRepository product = MarketRepository.INSTANCE.getProductRepository();
        List<EntityProduct> mostViewedProducts = product.getMostViewedProducts(productID, scroll, limit);
        if (mostViewedProducts != null && !mostViewedProducts.isEmpty()) {
            responseJsonObject.addProperty("sucs", Boolean.TRUE);
            responseJsonObject.add("mostViewedProducts", gson.toJsonTree(mostViewedProducts));
        } else {
            responseJsonObject.addProperty("sucs", Boolean.FALSE);
            responseJsonObject.addProperty("mg", "No Data Found");
        }

        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.NEAREST_PRODUCTS)
    public static void getNearestProducts(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String productID = null;
        int scrollValue = 0;
        int limit = 20;
        JsonElement element;
        float latitute = 0;
        float longitude = 0;
        int radiusInKm = 50;

        element = jsonObject.get("productId");
        if (JsonValidator.hasValidString(element)) {
            productID = element.getAsString();
        }

        element = jsonObject.get("scrl");
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }

        Scroll scroll = (scrollValue > 0) ? Scroll.UP : Scroll.DOWN;

        element = jsonObject.get("lmt");
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        element = jsonObject.get("lat");
        if (JsonValidator.hasElement(element)) {
            latitute = element.getAsFloat();
        }

        element = jsonObject.get("lon");
        if (JsonValidator.hasElement(element)) {
            longitude = element.getAsFloat();
        }

        element = jsonObject.get("rad");
        if (JsonValidator.hasPositiveInteger(element)) {
            radiusInKm = element.getAsInt();
        }

        ProductRepository product = MarketRepository.INSTANCE.getProductRepository();
        List<EntityProduct> nearestProducts = product.getNearestLocaitonProducts(latitute, longitude, radiusInKm, productID, scroll, limit);
        if (nearestProducts != null && !nearestProducts.isEmpty()) {
            responseJsonObject.addProperty("sucs", Boolean.TRUE);
            responseJsonObject.add("nearestProducts", gson.toJsonTree(nearestProducts));
        } else {
            responseJsonObject.addProperty("sucs", Boolean.FALSE);
            responseJsonObject.addProperty("mg", "No Data Found");
        }
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_FILTERED_PRODUCTS)
    public static void getFilteredProducts(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        int scrollValue = 2;
        int limit = 20;
        JsonElement element;
        String message = null;
        boolean success = false;
        Integer categoryId = null;
        int orderType = 0;
        Long shopId = null;
        String productId = null;

        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        element = jsonObject.get(MarketAttribute.CATEGORY_ID);
        if (JsonValidator.hasPositiveInteger(element)) {
            categoryId = element.getAsInt();
        }

        element = jsonObject.get(MarketAttribute.ORDER_TYPE);
        if (JsonValidator.hasPositiveInteger(element)) {
            orderType = element.getAsInt();
        }

        element = jsonObject.get(MarketAttribute.SHOP_ID);
        if (JsonValidator.hasPositiveLong(element)) {
            shopId = element.getAsLong();
        }

        element = jsonObject.get(MarketAttribute.PRODUCT_ID);
        if (JsonValidator.hasValidString(element)) {
            productId = element.getAsString();
        }

        element = jsonObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = jsonObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        ProductStatus status = ProductStatus.AVAILABLE;
        element = jsonObject.get(MarketAttribute.STATUS);
        if (JsonValidator.hasPositiveInteger(element)) {
            Byte statusValue = element.getAsByte();
            ProductStatus lookupStatus = ProductStatus.getStatus(statusValue);
            if (lookupStatus != null) {
                status = lookupStatus;
            }
        }

        List<EntityProduct> products;
        SortOrderType orderTypeEnum = SortOrderType.getOrderType(orderType);
        int reasonCode;

        if(shopId == null && categoryId == null){
            responseJsonObject.addProperty(MarketAttribute.SUCCESS, false);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, ReasonCode.INSUFFICIENT_INFORMATION);
            
            sender.sendToClient(responseJsonObject);
            return ;
        }
        ListReturnDTO<EntityProduct> filtererProducts = MarketRepository.INSTANCE.getProductRepository().getFiltererProducts(categoryId, shopId, productId, scroll, limit, status);
        products = filtererProducts.getList();
        reasonCode = filtererProducts.getReasonCode();

        if (products != null && !products.isEmpty()) {
            responseJsonObject.addProperty(MarketAttribute.SUCCESS, true);
            responseJsonObject.add(MarketAttribute.PRODUCT_LIST, gson.toJsonTree(products));
        } else {
            responseJsonObject.addProperty(MarketAttribute.SUCCESS, false);
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_DATA);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        }
        responseJsonObject.add(MarketAttribute.PRODUCT_LIST, gson.toJsonTree(products));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_BASKET_PRODUCTS)
    public static void getBasketProducts(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        int scrollValue = 2;
        int limit = 20;
        JsonElement element;
        String message = null;
        boolean success = false;
        Integer categoryId = null;
        String productId = null;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        element = jsonObject.get(MarketAttribute.CATEGORY_ID);
        if (JsonValidator.hasPositiveInteger(element)) {
            categoryId = element.getAsInt();
        }

        element = jsonObject.get(MarketAttribute.PRODUCT_ID);
        if (JsonValidator.hasValidString(element)) {
            productId = element.getAsString();
        }

        element = jsonObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = jsonObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        String basketId = null;
        element = jsonObject.get(MarketAttribute.BASKET_ID);
        if (JsonValidator.hasValidString(element)) {
            basketId = element.getAsString();
        }
        Long pageId = null;
        element = jsonObject.get(MarketAttribute.PAGE_ID);
        if (JsonValidator.hasPositiveLong(element)) {
            pageId = element.getAsLong();
        }

        ListReturnDTO<EntityProduct> products = null;
        if (basketId == null) {
            if (pageId != null) {
                products = MarketRepository.INSTANCE.getBasketProductCategoryRepository().getProductsOfBasket(pageId, productId, scroll, limit);
            } else {
                message = "Please provide basket Id";
            }

        } else {
            products = MarketRepository.INSTANCE.getBasketProductCategoryRepository().getProductsOfBasket(basketId, categoryId, productId, scroll, limit);
        }
        if (products != null) {
            reasonCode = products.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                responseJsonObject.add(MarketAttribute.PRODUCT_LIST, gson.toJsonTree(products.getList()));
            }
        }

        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);

        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_PRODUCT_DETAILS)
    public static void getProductDetails(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();
        String message = null;
        boolean success = false;
        JsonElement productElement = jsonObject.get(MarketAttribute.PRODUCT_ID);
        JsonElement userIdElement = jsonObject.get(MarketAttribute.USER_ID);
        if (!JsonValidator.hasValidString(productElement)) {
            message = "Please provide product ID";
        } else if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user Id";
        } else {
            String productId = productElement.getAsString();
            Long userId = userIdElement.getAsLong();

            long referrerId;
            if (JsonValidator.hasPositiveLong(jsonObject.get(MarketAttribute.REFERRER_ID))) {
                referrerId = jsonObject.get(MarketAttribute.REFERRER_ID).getAsLong();
            }

            EntityProductDetail productDetail = MarketRepository.INSTANCE.getProductDetailsRepository().getProductDetail(productId);

            if (productDetail == null) {
                productDetail = new EntityProductDetail();
                productDetail.setDescription("Description is not available");
            }
//            if (productDetail != null) {
            List<EntityProductImage> imagesOfProduct = MarketRepository.INSTANCE.getProductImageRepository().getImagesOfProduct(productId);
            productDetail.setImageUrls(imagesOfProduct);

            responseObject = (JsonObject) gson.toJsonTree(productDetail);
            responseObject.remove(MarketAttribute.ID);

            EntityProductStock stock = MarketRepository.INSTANCE.getProductStockRepository().getStock(productDetail.getId());
            if (stock != null) {
                responseObject.addProperty(MarketAttribute.STOCK_QUANTITY, stock.getQuantity());
            }

            float rating = 0;
            EntityProductRating ratingEntity = MarketRepository.INSTANCE.getProductRatingRepository().getRating(productId);
            if (ratingEntity != null) {
                rating = (float) ratingEntity.getTotalRating() / ratingEntity.getNumberOfRaters();
            }
            responseObject.addProperty(MarketAttribute.RATING, rating);
            responseObject.addProperty(MarketAttribute.BASKET_ID, MarketRepository.INSTANCE.getBasketProductCategoryRepository().getBasketIdByUserIdAndProductId(userId, productId));
            success = true;
            Thread thread = new Thread(() -> {
//                    int incrementViewReasonCode = MarketRepository.INSTANCE.getProductViewRepository().incrementViewCount(userId, productId);
//                    if (incrementViewReasonCode == ReasonCode.NO_DATA_FOUND) {
//                        MarketRepository.INSTANCE.getProductViewRepository().addToViewList(userId, productId);
//                    }
                MarketRepository.INSTANCE.getProductViewRepository().addToProductViewCache(userId, productId);
            });

            DBConnection.getInstance().submitAsyncJob(thread);

//            } else {
//                message = Messages.NO_DATA_FOUND;
//            }
        }
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        sender.sendToClient(responseObject);
    }

    @Request(action = Action.GET_PRODUCT_INFO)
    public static void getProductInfo(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();
        String message = null;
        boolean success = false;
        JsonElement productElement = jsonObject.get(MarketAttribute.PRODUCT_ID);
        JsonElement codeElement = jsonObject.get(MarketAttribute.CODE);
//        JsonElement userIdElement = jsonObject.get(MarketAttribute.USER_ID);
        int reasonCode = ReasonCode.NONE;

        String productId = null;
        String code = null;
        if (JsonValidator.hasValidString(productElement)) {
            productId = productElement.getAsString();
        }

        if (JsonValidator.hasValidString(codeElement)) {
            code = codeElement.getAsString();
        }

        if (productId == null && code == null) {
            reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        } else {
            EntityProduct product;

            if (productId != null) {
                product = MarketRepository.INSTANCE.getProductRepository().getProductById(productId);
            } else {
                product = MarketRepository.INSTANCE.getProductRepository().getProductByCode(code);
            }
            if (product != null) {
                success = true;
                responseObject = gson.toJsonTree(product).getAsJsonObject();
                EntityProductDetail productDetail = MarketRepository.INSTANCE.getProductDetailsRepository().getProductDetail(productId);
                if (productDetail == null) {
                    productDetail = new EntityProductDetail();
                    productDetail.setDescription("Description is not available");
                }
//                if (productDetail != null) {
                List<EntityProductImage> imagesOfProduct = MarketRepository.INSTANCE.getProductImageRepository().getImagesOfProduct(productId);
                productDetail.setImageUrls(imagesOfProduct);

                JsonObject detailsJson = gson.toJsonTree(productDetail).getAsJsonObject();
                detailsJson.remove(MarketAttribute.ID);

                EntityProductStock stock = MarketRepository.INSTANCE.getProductStockRepository().getStock(productDetail.getId());
                if (stock != null) {
                    detailsJson.addProperty(MarketAttribute.STOCK_QUANTITY, stock.getQuantity());
                }

                responseObject.add(MarketAttribute.DETAILS, detailsJson);
                reasonCode = ReasonCode.NONE;

//            }
            } else {
                message = Messages.NO_DATA_FOUND;
                reasonCode = ReasonCode.NO_DATA_FOUND;
            }
        }
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        sender.sendToClient(responseObject);
    }

    @Request(action = Action.GET_PRODUCT_IMAGES)
    public static void getProductImages(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();

        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        JsonElement element = jsonObject.get("productId");
        if (!JsonValidator.hasValidString(element)) {
            responseObject.addProperty("sucs", Boolean.FALSE);
            responseObject.addProperty("mg", "Please provide product ID");
        } else {
            String productId = element.getAsString();
            List<EntityProductImage> imagesOfProduct = MarketRepository.INSTANCE.getProductImageRepository().getImagesOfProduct(productId);
            if (imagesOfProduct != null && !imagesOfProduct.isEmpty()) {
//                JsonElement imagesJson = new GsonBuilder().registerTypeAdapter(EntityProductImage.class, new ProductImageAdapter()).create().toJsonTree(imagesOfProduct.toArray(), EntityProductImage[].class);
                JsonElement imagesJson = new GsonBuilder().create().toJsonTree(imagesOfProduct.toArray(), EntityProductImage[].class);
                responseObject.add("imgList", imagesJson);
                responseObject.addProperty("sucs", Boolean.TRUE);
            } else {
                responseObject.addProperty("sucs", Boolean.FALSE);
                responseObject.addProperty("mg", "No data found");
            }

        }

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.GET_PRODUCT_VIDEOS)
    public static void getProductVideos(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();

        JsonElement element = jsonObject.get("productId");
        if (!JsonValidator.hasValidString(element)) {
            responseObject.addProperty("sucs", Boolean.FALSE);
            responseObject.addProperty("mg", "Please provide product ID");
        } else {
            String productId = element.getAsString();
            List<EntityProductVideo> videosOfProduct = MarketRepository.INSTANCE.getProductVideoRepository().getVideosOfProduct(productId);
            if (videosOfProduct != null && !videosOfProduct.isEmpty()) {
//                JsonElement videosJson = new GsonBuilder().registerTypeAdapter(EntityProductVideo.class, new ProductVideoAdapter()).create().toJsonTree(videosOfProduct.toArray(), EntityProductVideo[].class);
                JsonElement videosJson = new GsonBuilder().create().toJsonTree(videosOfProduct.toArray(), EntityProductVideo[].class);
                responseObject.add("vidList", videosJson);
                responseObject.addProperty("sucs", Boolean.TRUE);
            } else {
                responseObject.addProperty("sucs", Boolean.FALSE);
                responseObject.addProperty("mg", "No data found");
            }
        }
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.INCREASE_VIEW_COUNT)
    public static void increaseViewCount(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();
        String message = null;
        String productId;
        long userId;
        boolean success = false;
        JsonElement element = jsonObject.get("productId");
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        if (!JsonValidator.hasValidString(element)) {
            message = "Please provide product Id";
            responseObject.addProperty("sucs", Boolean.FALSE);
            responseObject.addProperty("mg", message);

            sender.sendToClient(responseObject);
            return;
        } else {
            productId = element.getAsString();
        }

        element = jsonObject.get("userId");
        if (!JsonValidator.hasPositiveLong(element)) {
            message = "Please provide user Id";
            responseObject.addProperty("sucs", Boolean.FALSE);
            responseObject.addProperty("mg", message);

            sender.sendToClient(responseObject);
            return;
        } else {
            userId = element.getAsLong();
        }

        SellerProductRepository sellerProduct = MarketRepository.INSTANCE.getSellerProductRepository();
        EntitySellerProduct product = sellerProduct.getProduct(productId);
        if (product != null) {
            if (product.getSellerId() != userId) {
                ProductViewRepository productView = MarketRepository.INSTANCE.getProductViewRepository();
                boolean susc = productView.findViewedByBuyer(userId, productId);
                if (!susc) {
                    productView.addToViewList(userId, productId);
                    success = true;
                } else {
                    message = "Already viewed";
                }
            } else {
                message = "Can't increase view count for own product";
            }
        } else {
            message = "Please provide valid product id";
        }

        responseObject.addProperty("sucs", success);
        responseObject.addProperty("mg", message);

        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.SOLD_PRODUCT_INFO)
    public static void soldProductInfo(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();
        String message = null;
        String productId;
        boolean success = false;
        JsonElement element = jsonObject.get("productId");
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        if (!JsonValidator.hasValidString(element)) {
            message = "Please provide product Id";
            responseObject.addProperty("sucs", Boolean.FALSE);
            responseObject.addProperty("mg", message);

            sender.sendToClient(responseObject);
            return;
        } else {
            productId = element.getAsString();
            EntityHistoryProduct productById = new HistoryProduct().getProductFromHistory(productId);
            if (productById != null) {
                GsonBuilder builder = new GsonBuilder();
//                builder.registerTypeAdapter(EntityProduct.class, new ProductAdapter());
                JsonElement productJson = builder.create().toJsonTree(productById, EntityHistoryProduct.class);
                responseObject.add("productInfo", productJson);
                success = true;
            } else {
                message = "No Product found";
            }
        }

        responseObject.addProperty("sucs", success);
        responseObject.addProperty("mg", message);

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.GET_OFFERER_LIST)
    public static void getOffererList(JsonObject jsonObject, ISender sender) {
        JsonObject responseJson = new JsonObject();
        JsonElement element;
        boolean success = false;
        String productId;
        long time;
        int scrollValue = 0;
        int limit = 10;
        String message = null;

        element = jsonObject.get("productId");
        if (!JsonValidator.hasValidString(element)) {
            message = "Please provide productId";
        } else {
            productId = element.getAsString();
            element = jsonObject.get("tm");
            if (element != null && !element.isJsonNull()) {
                time = element.getAsLong();
            } else {
                time = System.currentTimeMillis();
            }

            element = jsonObject.get("scrl");
            if (JsonValidator.hasPositiveInteger(element)) {
                scrollValue = element.getAsInt();
            }

            element = jsonObject.get("lmt");
            if (JsonValidator.hasPositiveInteger(element)) {
                limit = element.getAsInt();
            }

            Scroll scroll = (scrollValue > 0) ? Scroll.UP : Scroll.DOWN;

            OfferProductRepository offerProduct = MarketRepository.INSTANCE.getOfferProductRepository();
            List<EntityOfferProduct> allOffersForProduct = offerProduct.getAllOffersForProduct(productId, time, limit, scroll);

            if (allOffersForProduct == null || allOffersForProduct.isEmpty()) {
                message = "No Offers Found";
            } else {
                GsonBuilder builder = new GsonBuilder();
//                builder.registerTypeAdapter(EntityOfferProduct.class, new OfferProductAdapter());
                JsonElement offerListJson = builder.create().toJsonTree(allOffersForProduct.toArray(), EntityOfferProduct[].class);
                responseJson.add("offerList", offerListJson);
                success = true;
            }
        }
        responseJson.addProperty("sucs", success);
        responseJson.addProperty("mg", message);
        responseJson.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.REPORT_PRODUCT)
    public static void reportProduct(JsonObject jsonObject, ISender sender) {
        JsonObject responseJson = new JsonObject();
        boolean success = false;
        String message;
        JsonElement productIdElement = jsonObject.get("prodId");
        JsonElement userIdElement = jsonObject.get("userId");
        JsonElement sellerIdElement = jsonObject.get("sellerId");
        JsonElement reasonElement = jsonObject.get("rsn");

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product Id";
        } else if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasPositiveLong(sellerIdElement)) {
            message = "Please provide seller Id";
        } else if (!JsonValidator.hasPositiveInteger(reasonElement)) {
            message = "Please provide reason";
        } else {
            String productId = productIdElement.getAsString();
            long userId = userIdElement.getAsLong();
            long sellerId = sellerIdElement.getAsLong();
            MarketUserRepository ringmarketUser = MarketRepository.INSTANCE.getMarketUserRepository();
            EntityRingMarketUser user = ringmarketUser.getUser(userId);
            if (user == null) {
                message = "Please provide valid user";
            } else {
                user = ringmarketUser.getUser(sellerId);
                if (user == null) {
                    message = "Please provide valid seller";
                } else {
                    SellerProductRepository sellerProduct = MarketRepository.INSTANCE.getSellerProductRepository();
                    EntitySellerProduct product = sellerProduct.getProduct(productId);
                    if (product == null) {
                        message = "Please provide valid product id";
                    } else {
                        ReportProductRepository reportProduct = MarketRepository.INSTANCE.getReportProductRepository();
                        String reportID = TimeUUID.timeBased().toString();
                        int reason = reasonElement.getAsInt();
                        String description = null;
                        if (jsonObject.has("desc")) {
                            description = jsonObject.get("desc").getAsString();
                        }
                        boolean susc = reportProduct.isAlreadyReported(userId, productId);
                        if (!susc) {
                            success = reportProduct.addReportProduct(reportID, userId, sellerId, productId, reason, description);
                            if (success) {
                                message = "Successfully submitted report";
                            } else {
                                message = "Failed to submit report";
                            }
                        } else {
                            message = "Already Reported. Can't Report Again.";
                        }
                    }
                }
            }

        }
        responseJson.addProperty("sucs", success);
        responseJson.addProperty("mg", message);
        responseJson.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.SEARCH_TYPE)
    public static void getSearchType(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        boolean success = true;
        JsonArray searchTypesJson = new JsonArray();
        for (SearchType searchType : SearchType.values()) {
            JsonObject searchTypeJson = new JsonObject();
            searchTypeJson.addProperty("key", searchType.getSearchKey());
            searchTypeJson.addProperty("value", searchType.getSearchValue());
            searchTypesJson.add(searchTypeJson);
        }
        responseJson.add("srchTypes", searchTypesJson);
        responseJson.addProperty("sucs", success);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.GET_RECOMMENDED_PRODUCTS)
    public static void getRecommendations(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        String message = null;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();

        element = requestObject.get(MarketAttribute.PRODUCT_ID);
        String productId = null;
        if (JsonValidator.hasValidString(element)) {
            productId = element.getAsString();
        }

        element = requestObject.get(MarketAttribute.PIVOT_STRING);
        String pivotId = null;
        if (JsonValidator.hasValidString(element)) {
            pivotId = element.getAsString();
        }

        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        PivotedListReturnDTO<EntityProduct, String> recommendedProducts = MarketRepository.INSTANCE.getProductRepository()
                .getRecommendedProducts(productId, pivotId, scroll, limit);

        List<EntityProduct> items = recommendedProducts.getList();

        if (!items.isEmpty()) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_DATA);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, recommendedProducts.getReasonCode());
        }

        responseJsonObject.addProperty(MarketAttribute.PIVOT_STRING, recommendedProducts.getPivot());
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.RATE_PRODUCT)
    public static void rateProduct(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        String message;
        boolean success = false;
        JsonElement userIdElement = requestJson.get(MarketAttribute.USER_ID);
        JsonElement productIdElement = requestJson.get(MarketAttribute.PRODUCT_ID);
        JsonElement ratingElement = requestJson.get(MarketAttribute.RATING);
        if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product Id";
        } else if (!JsonValidator.hasElement(ratingElement)) {
            message = "Please provide rating value";
        } else {
            long fromUserId = userIdElement.getAsLong();
            String productId = productIdElement.getAsString();
            byte rating = ratingElement.getAsByte();

            ProductRepository productRepository = MarketRepository.INSTANCE.getProductRepository();
            if (productRepository.getProductById(productId) == null) {
                message = "Product not found";
            } else {
                success = MarketRepository.INSTANCE.getProductRatingRepository().updateProductRating(productId, fromUserId, rating);
                if (success) {
                    message = "Your rating has been accepted";
                    EntityProductRating productRating = MarketRepository.INSTANCE.getProductRatingRepository().getRating(productId);
                    if (productRating != null) {
                        float averageRating = (float) (productRating.getTotalRating()) / productRating.getNumberOfRaters();
                        responseJson.addProperty(MarketAttribute.RATING, averageRating);
                    }
                } else {
                    message = "Unable to rate";
                }

            }
        }
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.PRODUCT_RATING_DETAILS)
    public static void productRatingDetails(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        String message = null;
        boolean success = false;
        JsonElement productIdElement = requestJson.get(MarketAttribute.PRODUCT_ID);
        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product Id";
        } else {
            String productId = productIdElement.getAsString();

            Map<Byte, Long> ratingDetails = MarketRepository.INSTANCE.getProductRatingLogRepository().getRatingDetails(productId);
            if (!ratingDetails.isEmpty()) {
                success = true;
                responseJson.add(MarketAttribute.DETAILS, gson.toJsonTree(ratingDetails));
            }
        }
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.SEARCH_PRODUCT)
    public static void searchProduct(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        int scrollValue = 2;
        int limit = 20;
        JsonElement element;
        String message = null;
        boolean success = false;
        String productId = null;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));

        element = jsonObject.get(MarketAttribute.PIVOT_STRING);
        if (JsonValidator.hasValidString(element)) {
            productId = element.getAsString();
        }

        element = jsonObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = jsonObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        String searchParam;
        element = jsonObject.get(MarketAttribute.SEARCH_PARAM);
        if (!JsonValidator.hasValidString(element)) {
            message = "Please provide search params";
        } else {
            searchParam = element.getAsString();
            PivotedListReturnDTO<EntityProduct, String> searchProductByName = MarketRepository.INSTANCE.getProductRepository().searchProductByName(productId, scroll, limit, searchParam);
            reasonCode = searchProductByName.getReasonCode();
            productId = searchProductByName.getPivot();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                responseJsonObject.add(MarketAttribute.PRODUCT_LIST, gson.toJsonTree(searchProductByName.getList()));
            } else {
                if (reasonCode == ReasonCode.NO_MORE_DATA) {
                    message = Messages.NO_MORE_DATA;
                } else {
                    message = Messages.NO_DATA_FOUND;
                }
            }
        }
        responseJsonObject.addProperty(MarketAttribute.PIVOT_STRING, productId);
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.ADD_PRODUCT_DISCOUNT)
    public static void addProductDiscount(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String message;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));

        EntityProductDiscount discount = gson.fromJson(jsonObject, EntityProductDiscount.class);
        if (discount.getProductId() == null || discount.getProductId().trim().isEmpty()) {
            message = "Please provide product id";
        } else if (discount.getDiscountValue() == null || discount.getDiscountValue() <= 0) {
            message = "Please provide discount amount";
        } else if (discount.getDiscountUnit() == 0) {
            message = "Please specify discount unit";
        } else if (discount.getActiveTime() == null || discount.getActiveTime() <= 0) {
            message = "Please specify active time";
        } else if (discount.getValidityTime() != null && discount.getValidityTime() > 0 && discount.getActiveTime() > discount.getValidityTime()) {
            message = "Invalid validity time";
        } else {
            discount.setCouponCode("");
            discount.setCreationTime(System.currentTimeMillis());
            discount.setMaximumDiscountAmount(0d);
            discount.setMinimumOrderValue(0);
            discount.setIsRedeemAllowed(false);
            reasonCode = MarketRepository.INSTANCE.getProductDiscounRepository().add(discount);
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                message = "Discount added";
            } else {
                message = Messages.OPERATION_FAILED;
            }
        }
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.UPDATE_PRODUCT_DISCOUNT)
    public static void updateProductDiscount(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String message = null;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));

        JsonElement productIdElement = jsonObject.get(MarketAttribute.PRODUCT_ID);
        JsonElement discountValueElement = jsonObject.get(MarketAttribute.DISCOUNT);

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product id";
        } else if (!JsonValidator.hasPositiveDouble(discountValueElement)) {
            message = "Please provide discount amount";
        } else {

            reasonCode = MarketRepository.INSTANCE.getProductDiscounRepository().updateDiscountValue(productIdElement.getAsString(), discountValueElement.getAsFloat());
            if (reasonCode == ReasonCode.NONE) {
                success = true;
            } else {
                message = Messages.UPDATE_FAILED;
            }
        }
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.UPDATE_PRODUCT_STATUS)
    public static void updateProductStatus(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String message = null;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));

        JsonElement productIdElement = jsonObject.get(MarketAttribute.PRODUCT_ID);
        JsonElement statusElement = jsonObject.get(MarketAttribute.STATUS);

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product id";
        } else if (statusElement == null || statusElement.getAsInt() < 0) {
            message = "Please provide product status";
        } else {

            String productId = productIdElement.getAsString();
            Integer sts = statusElement.getAsInt();
            ProductStatus status = ProductStatus.getStatus(sts.byteValue());

            reasonCode = MarketRepository.INSTANCE.getProductRepository().updateProductStatus(productId, status);
            if (reasonCode == ReasonCode.NONE) {
                success = true;
            } else {
                message = Messages.UPDATE_FAILED;
            }
        }
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.UPDATE_PRODUCT_DISCOUNT_VALIDITY)
    public static void updateProductDiscountValidity(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String message = null;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));

        JsonElement productIdElement = jsonObject.get(MarketAttribute.PRODUCT_ID);
        JsonElement discountActiveFromElement = jsonObject.get(MarketAttribute.ACTIVE_TIME);
        JsonElement discountValidityElement = jsonObject.get(MarketAttribute.VALIDITY_TIME);

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product id";
        } else if (!JsonValidator.hasElement(discountValidityElement)) {
            message = "Please provide discount validity";
        } else if (!JsonValidator.hasPositiveLong(discountActiveFromElement)) {
            message = "Please provide discount active time";
        } else {
            Long activeTime = discountActiveFromElement.getAsLong();
            Long time = discountValidityElement.getAsLong();
            if (time >= 0) {
                reasonCode = MarketRepository.INSTANCE.getProductDiscounRepository().updateDiscountValidityTime(productIdElement.getAsString(), activeTime, time);
                if (reasonCode == ReasonCode.NONE) {
                    success = true;
                } else {
                    message = Messages.UPDATE_FAILED;
                }
            } else {
                reasonCode = ReasonCode.INVALID_PARAMETER;
                message = "Invalid time";
            }
        }
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.CHANGE_DISCOUNT_UNIT)
    public static void changeDiscountUnit(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String message = null;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));

        JsonElement productIdElement = jsonObject.get(MarketAttribute.PRODUCT_ID);
        JsonElement discountUnitElement = jsonObject.get(MarketAttribute.DISCOUNT);

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product id";
        } else if (!JsonValidator.hasElement(discountUnitElement)) {
            message = "Please provide discount unit";
        } else {
            byte discountUnitValue = discountUnitElement.getAsByte();
            DiscountUnit discountUnit = DiscountUnit.getDiscountUnit(discountUnitValue);
            if (discountUnit != null) {
                reasonCode = MarketRepository.INSTANCE.getProductDiscounRepository().changeDiscountUnit(productIdElement.getAsString(), discountUnit);
                if (reasonCode == ReasonCode.NONE) {
                    success = true;
                } else {
                    message = Messages.UPDATE_FAILED;
                }
            } else {
                reasonCode = ReasonCode.INVALID_PARAMETER;
                message = "Invalid unit";
            }
        }
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_PRODUCT_DISCOUNT_DETAILS)
    public static void getProductDiscountDetails(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String message = null;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        JsonElement productIdElement = jsonObject.get(MarketAttribute.PRODUCT_ID);

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product id";
        } else {
            ObjectReturnDTO<EntityProductDiscount> findDiscountById = MarketRepository.INSTANCE.getProductDiscounRepository().findDiscountById(productIdElement.getAsString());
            reasonCode = findDiscountById.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                responseJsonObject = (JsonObject) gson.toJsonTree(findDiscountById.getValue());
            } else {
                message = Messages.NO_DATA_FOUND;
            }
        }
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.REMOVE_PRODUCT_DISCOUNT)
    public static void removeDiscount(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String message = null;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        JsonElement productIdElement = jsonObject.get(MarketAttribute.PRODUCT_ID);

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product id";
        } else {
            reasonCode = MarketRepository.INSTANCE.getProductDiscounRepository().deleteFromDb(productIdElement.getAsString());
            if (reasonCode == ReasonCode.NONE) {
                success = true;
            } else {
                message = Messages.OPERATION_FAILED;
            }
        }
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }
}
