/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.handler.user;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.MarketOrderStatus;
import org.ipvision.ringmarket.constants.Messages;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.RingmarketRole;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.controller.annotation.Request;
import org.ipvision.ringmarket.dtos.orders.InvoiceDTO;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.EntityRingMarketUser;
import org.ipvision.ringmarket.entities.EntityUserRole;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.entities.user.basket.EntityUserBasket;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.repositories.user.MarketUserRepository;
import org.ipvision.ringmarket.repositories.seller.SellerRatingRepository;
import org.ipvision.ringmarket.repositories.user.UserRoleRepository;
import org.ipvision.ringmarket.sender.ISender;
import org.ipvision.ringmarket.utils.JsonValidator;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.ipvision.ringmarket.utils.TimeUUID;

/**
 *
 * @author saikat
 */
public class UserHandler {

    private static Gson gson = new Gson();

    @Request(action = Action.GET_ALL_ROLEWISE_USERS)
    public static void getUsersWithRole(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        UserRoleRepository userRole = MarketRepository.INSTANCE.getUserRoleRepository();
        JsonArray roleList = new JsonArray();
        JsonArray usersJson;
        MarketUserRepository ringmarketUser = MarketRepository.INSTANCE.getMarketUserRepository();
        for (RingmarketRole value : RingmarketRole.values()) {
            List<EntityUserRole> usersByRole = (List<EntityUserRole>) userRole.getUsersByRole(value);
            usersJson = new JsonArray();
            for (EntityUserRole iUserRole : usersByRole) {
                EntityRingMarketUser user = ringmarketUser.getUser(iUserRole.getUserId());
                if (user != null) {
                    JsonObject userJson = new JsonObject();
                    userJson.addProperty("userName", user.getName());
                    userJson.addProperty("userId", user.getId());
                    usersJson.add(userJson);
                }
            }
            JsonObject roleJson = new JsonObject();
            roleJson.addProperty("roleName", value.toString());
            roleJson.addProperty("roleValue", value.ordinal());
            roleJson.add("users", usersJson);
            roleList.add(roleJson);
        }
        if (roleList.size() > 0) {
            responseJson.addProperty("sucs", Boolean.TRUE);
            responseJson.add("roles", roleList);
        } else {
            responseJson.addProperty("sucs", Boolean.FALSE);
            responseJson.addProperty("mg", "No data found");
        }
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    /**
     *
     * @param requestJson
     * @param sender
     */
    @Request(action = Action.RATE_SHOP)
    public static void rateShop(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        String message;
        boolean success = false;
        JsonElement userIdElement = requestJson.get(MarketAttribute.USER_ID);
        JsonElement shopIdElemet = requestJson.get(MarketAttribute.SHOP_ID);
        JsonElement ratingElement = requestJson.get(MarketAttribute.RATING);
        if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user id";
        } else if (!JsonValidator.hasPositiveLong(shopIdElemet)) {
            message = "Please provide shop Id";
        } else if (!JsonValidator.hasElement(ratingElement)) {
            message = "Please provide rating value";
        } else {
            long fromUserId = userIdElement.getAsLong();
            long shopId = shopIdElemet.getAsLong();
            byte rating = ratingElement.getAsByte();

            EntityShop shopById = MarketRepository.INSTANCE.getShopRepository().getShopById(shopId);
            if (shopById == null) {
                message = "Shop not found";
            } else {
                SellerRatingRepository sellerRating = MarketRepository.INSTANCE.getSellerRatingRepository();
                success = sellerRating.updateSellerRating(shopId, fromUserId, rating);
                if (success) {
                    message = "Rating has been updated successfully";
                } else {
                    message = "Unsuccessful while updating rating";
                }
            }
        }
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.CREATE_BASKET)
    public static void createBasket(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        String message;
        boolean success = false;
        EntityUserBasket basket = new Gson().fromJson(requestJson, EntityUserBasket.class);
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (JsonValidator.hasPositiveLong(requestJson.get(MarketAttribute.FRIEND_USER_ID))) {
            basket.setUserId(requestJson.get(MarketAttribute.FRIEND_USER_ID).getAsLong());
        }

        if (basket.getUserId() == null || basket.getUserId() <= 0) {
            message = "Please provide userId";
        } else if (basket.getOwnerType() == null || basket.getOwnerType() < 0) {
            message = "Please provide user Type";
        } //        else if (basket.getImageUrl() == null || basket.getImageUrl().isEmpty()) {
        //            message = "Please provide image";
        //        } 
        else if (basket.getName() == null || basket.getName().isEmpty()) {
            message = "Please provide basket name";
        } else {
            basket.setId(TimeUUID.timeBased().toString());
            basket.setCreationTime(System.currentTimeMillis());
            reasonCode = MarketRepository.INSTANCE.getBasketRepository().addBasket(basket);
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                message = "Successfully created basket";
                responseJson = (JsonObject) new Gson().toJsonTree(basket);
            } else {
                message = "Basket creation failed";
            }
        }
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.ADD_PRODUCT_TO_BASKET)
    public static void addProductToBasket(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        String message;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        if (!JsonValidator.hasValidString(requestJson.get(MarketAttribute.BASKET_ID))) {
            message = "Please provide basket Id";
        } else if (!JsonValidator.hasValidString(requestJson.get(MarketAttribute.PRODUCT_ID))) {
            message = "Please provide product Id";
        } else if (!JsonValidator.hasPositiveLong(requestJson.get(MarketAttribute.USER_ID))) {
            message = "Please provide user Id";
        } else {
            String basketId = requestJson.get(MarketAttribute.BASKET_ID).getAsString();
            String productId = requestJson.get(MarketAttribute.PRODUCT_ID).getAsString();
            Long userId = requestJson.get(MarketAttribute.USER_ID).getAsLong();
            reasonCode = MarketRepository.INSTANCE.getBasketProductCategoryRepository().addBasketProductCategory(basketId, productId, userId);
            if (reasonCode == ReasonCode.NONE) {
                message = "Successfully added into basket";
                success = true;
            } else {
                message = "Failed";
            }
        }
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.DELETE_PRODUCT_FROM_BASKET)
    public static void deleteProductFromBasket(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        String message;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        if (!JsonValidator.hasValidString(requestJson.get(MarketAttribute.BASKET_ID))) {
            message = "Please provide basket Id";
        } else if (!JsonValidator.hasValidString(requestJson.get(MarketAttribute.PRODUCT_ID))) {
            message = "Please provide product Id";
        } else if (!JsonValidator.hasPositiveLong(requestJson.get(MarketAttribute.USER_ID))) {
            message = "Please provide user Id";
        } else {
            String basketId = requestJson.get(MarketAttribute.BASKET_ID).getAsString();
            String productId = requestJson.get(MarketAttribute.PRODUCT_ID).getAsString();
            Long userId = requestJson.get(MarketAttribute.USER_ID).getAsLong();
            reasonCode = MarketRepository.INSTANCE.getBasketProductCategoryRepository().deleteProductFromBasket(basketId, productId, userId);
            if (reasonCode == ReasonCode.NONE) {
                message = "Successfull removed from basket";
                success = true;
            } else {
                message = "Failed to remove";
            }
        }
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.GET_USER_BASKETS)
    public static void getUserBaskets(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        String message = null;
        boolean success = false;

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        Long userId = null;
        if (JsonValidator.hasPositiveLong(requestJson.get(MarketAttribute.FRIEND_USER_ID))) {
            userId = requestJson.get(MarketAttribute.FRIEND_USER_ID).getAsLong();
        } else if (JsonValidator.hasPositiveLong(requestJson.get(MarketAttribute.USER_ID))) {
            userId = requestJson.get(MarketAttribute.USER_ID).getAsLong();
        }
        if (userId == null) {
            message = "Please provide user Id";
        } else {
            ListReturnDTO<EntityUserBasket> userBaskets = MarketRepository.INSTANCE.getBasketRepository().getUserBaskets(userId);
            reasonCode = userBaskets.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                responseJson.add(MarketAttribute.LIST, gson.toJsonTree(userBaskets.getList()));
            } else {
                message = Messages.NO_DATA_FOUND;
            }
        }
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    public static void orderItemStatusUpdate(JsonObject requestObject, ISender sender, Byte status) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ORDER_ID))) {
            message = "Please provide order Id";
        } else {
            String orderId = requestObject.get(MarketAttribute.ORDER_ID).getAsString();
            Long userId = requestObject.get(MarketAttribute.USER_ID).getAsLong();

            Type listType = new TypeToken<List<String>>() {
            }.getType();

            List<String> productIdList = null;
            if (requestObject.has(MarketAttribute.IDS)) {
                productIdList = gson.fromJson(requestObject.get(MarketAttribute.IDS), listType);
            }
            if (productIdList == null || productIdList.isEmpty()) {
                message = "Please provide product ids";
            } else {
                reasonCode = MarketRepository.INSTANCE.getOrderDetailsRepository().updateOrderDetailStatus(userId, orderId, productIdList, status);
                if (reasonCode == ReasonCode.NONE) {
                    success = true;
                    message = "You have successfully received products.";
                } else {
                    message = Messages.OPERATION_FAILED;
                }
            }
        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.WISH_LIST)
    public static void wishList(JsonObject requestObject, ISender sender) {
        boolean success = false;
        JsonObject response = new JsonObject();

        Long buyerId = null;
        Long shopId = null;
        JsonElement element = requestObject.get(MarketAttribute.BUYER_ID);

        if (JsonValidator.hasPositiveLong(element)) {
            buyerId = element.getAsLong();
        }

        element = requestObject.get(MarketAttribute.SHOP_ID);

        if (JsonValidator.hasPositiveLong(element)) {
            shopId = element.getAsLong();
        }

        String pivot = null;
        element = requestObject.get(MarketAttribute.PIVOT_STRING);
        if (JsonValidator.hasValidString(element)) {
            pivot = element.getAsString();
        }

        element = requestObject.get(MarketAttribute.SCROLL);
        int scrollValue = 2;
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;
        int limit = 10;
        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        int reasonCode = 0;
        PivotedListReturnDTO<EntityCartItem, String> cartItems = MarketRepository.INSTANCE.getCartRepository().getCartItems(buyerId, shopId, pivot, scroll, limit);
        if (cartItems.getReasonCode() == ReasonCode.NONE) {
            success = true;
            response.add(MarketAttribute.ITEMS, gson.toJsonTree(cartItems.getList()));
        } else {
            reasonCode = cartItems.getReasonCode();
        }

        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.PIVOT_STRING, cartItems.getPivot());
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.WISH_LIST_COUNT)
    public static void wishListCount(JsonObject requestObject, ISender sender) {
        boolean success = false;
        JsonObject response = new JsonObject();
        ObjectReturnDTO<Long> userCount = MarketRepository.INSTANCE.getCartRepository().userCount();
        int reasonCode = userCount.getReasonCode();
        if (userCount.getReasonCode() == ReasonCode.NONE) {
            success = true;
            response.addProperty(MarketAttribute.COUNT, userCount.getValue());
        }

        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }    
}
