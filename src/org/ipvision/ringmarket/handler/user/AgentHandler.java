/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.handler.user;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.LinkedList;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.MarketOrderStatus;
import org.ipvision.ringmarket.constants.Messages;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.ShopType;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.controller.annotation.Request;
import org.ipvision.ringmarket.dtos.orders.InvoiceDTO;
import org.ipvision.ringmarket.entities.order.EntityOrder;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.sender.ISender;
import org.ipvision.ringmarket.utils.JsonValidator;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;

/**
 *
 * @author saikat
 */
public class AgentHandler {

    private static Gson gson = new Gson();

    @Request(action = Action.INVOICE_LIST)
    public static void getInvoiceList(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        int start = 0;
        element = requestObject.get(MarketAttribute.START);
        if (JsonValidator.hasPositiveInteger(element)) {
            start = element.getAsInt();
        }
//        element = requestObject.get(MarketAttribute.SCROLL);
//        if (JsonValidator.hasPositiveInteger(element)) {
//            scrollValue = element.getAsInt();
//        }
//        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        ListReturnDTO<InvoiceDTO> items = MarketRepository.INSTANCE.getOrderRepository().getInvoiceList(start, limit);

        if (items.getReasonCode() == ReasonCode.NONE) {
            responseJsonObject.add(MarketAttribute.LIST, gson.toJsonTree(items.getList()));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_DATA);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, items.getReasonCode());
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_SHOP_LIST)
    public static void getShopList(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        long pivotId = 0;
        element = requestObject.get(MarketAttribute.PIVOT_NUMBER);
        if (JsonValidator.hasPositiveLong(element)) {
            pivotId = element.getAsLong();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }
        Boolean enabled = null;
        element = requestObject.get(MarketAttribute.IS_ENABLE);
        if (JsonValidator.hasPositiveInteger(element)) {
            enabled = element.getAsBoolean();
        }

        PivotedListReturnDTO<EntityShop, Long> items = MarketRepository.INSTANCE.getShopRepository().getAllStores(pivotId, limit, scroll, enabled);

        if (items.getReasonCode() == ReasonCode.NONE) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items.getList()));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_STORE);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, items.getReasonCode());
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.PIVOT_NUMBER, pivotId);

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.TOGGLE_SHOP_STATUS)
    public static void toggleShopStatus(JsonObject requestObject, ISender sender) {
        boolean success = false;
        JsonObject response = new JsonObject();
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        JsonElement shopIdElement = requestObject.get(MarketAttribute.SHOP_ID);
        JsonElement userIdElement = requestObject.get(MarketAttribute.USER_ID);
        JsonElement enabledElement = requestObject.get(MarketAttribute.IS_ENABLE);
        String message = null;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        if (!JsonValidator.hasPositiveLong(shopIdElement)) {
            message = "Please provide shop Id";
        } else if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasElement(enabledElement)) {
            message = "Please provide toggle value";
        } else {
            long shopId = shopIdElement.getAsLong();
            boolean enabled = enabledElement.getAsBoolean();
            Long userId = userIdElement.getAsLong();

            reasonCode = MarketRepository.INSTANCE.getShopRepository().toggleShopStatus(shopId, enabled, userId);

            if (reasonCode == ReasonCode.NONE) {
                success = true;
            }
        }

        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.MESSAGE, message);

        sender.sendToClient(response);
    }

    @Request(action = Action.TOGGLE_BRAND)
    public static void toggleBrand(JsonObject requestObject, ISender sender) {
        boolean success = false;
        JsonObject response = new JsonObject();
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        JsonElement shopIdElement = requestObject.get(MarketAttribute.SHOP_ID);
        JsonElement userIdElement = requestObject.get(MarketAttribute.USER_ID);
        JsonElement enabledElement = requestObject.get(MarketAttribute.IS_ENABLE);
        String message = null;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        if (!JsonValidator.hasPositiveLong(shopIdElement)) {
            message = "Please provide shop Id";
        } else if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasElement(enabledElement)) {
            message = "Please provide toggle value";
        } else {
            long shopId = shopIdElement.getAsLong();
            boolean enabled = enabledElement.getAsBoolean();
            Long userId = userIdElement.getAsLong();
            int type = ShopType.GENERAL_STORE;
            if (enabled) {
                type = ShopType.BRANDED_STORE;
            }
            reasonCode = MarketRepository.INSTANCE.getShopRepository().toggleStoreType(shopId, type, userId);

            if (reasonCode == ReasonCode.NONE) {
                success = true;
            }
        }

        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.MESSAGE, message);

        sender.sendToClient(response);
    }

    @Request(action = Action.TOGGLE_CELEBRITY_STORE)
    public static void toggleCelebrityStore(JsonObject requestObject, ISender sender) {
        boolean success = false;
        JsonObject response = new JsonObject();
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        JsonElement shopIdElement = requestObject.get(MarketAttribute.SHOP_ID);
        JsonElement userIdElement = requestObject.get(MarketAttribute.USER_ID);
        JsonElement enabledElement = requestObject.get(MarketAttribute.IS_ENABLE);
        String message = null;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        if (!JsonValidator.hasPositiveLong(shopIdElement)) {
            message = "Please provide shop Id";
        } else if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasElement(enabledElement)) {
            message = "Please provide toggle value";
        } else {
            long shopId = shopIdElement.getAsLong();
            boolean enabled = enabledElement.getAsBoolean();
            Long userId = userIdElement.getAsLong();

//            reasonCode = MarketRepository.INSTANCE.getCelebrityStoreRepository().toggleCelebrityStore(shopId, userId, enabled);
            reasonCode = MarketRepository.INSTANCE.getShopRepository().toggleCelebrityStore(shopId, userId, enabled);

            if (reasonCode == ReasonCode.NONE) {
                success = true;
            }
        }

        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.MESSAGE, message);

        sender.sendToClient(response);
    }

    @Request(action = Action.CHANGE_INVOICE_STATUS)
    public static void changeInvoiceStatus(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        Collection<String> orderIds = null;

        if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.INVOICE_ID))) {
            message = "Please provide invoice Id";
        } else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.STATUS))) {

        } else {
            Long userId = requestObject.get(MarketAttribute.USER_ID).getAsLong();
            Byte statusValue = requestObject.get(MarketAttribute.STATUS).getAsByte();

            MarketOrderStatus status = MarketOrderStatus.getStatus(statusValue);
            if (status == null || status == MarketOrderStatus.PAYMENT_INCOMPLETE) {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            } else {

                if (JsonValidator.hasElement(requestObject.get(MarketAttribute.IDS))) {
                    Type listType = new TypeToken<Collection<String>>() {
                    }.getType();
                    orderIds = gson.fromJson(requestObject.get(MarketAttribute.IDS), listType);
                }

                if (orderIds == null) {
                    String invoiceId = requestObject.get(MarketAttribute.INVOICE_ID).getAsString();
                    ListReturnDTO<EntityOrder> orderListByInvoiceId = MarketRepository.INSTANCE.getOrderRepository().getOrderListByInvoiceId(invoiceId);
                    reasonCode = orderListByInvoiceId.getReasonCode();
                    if (reasonCode == ReasonCode.NONE) {
                        orderIds = new LinkedList<>();
                        for (EntityOrder entityOrder : orderListByInvoiceId.getList()) {
                            orderIds.add(entityOrder.getId());
                        }
                    }
                }
                if (orderIds != null && !orderIds.isEmpty()) {
                    reasonCode = MarketRepository.INSTANCE.getOrderRepository().updateOrderStatus(userId, orderIds, statusValue);
                    if (reasonCode == ReasonCode.NONE) {
                        success = true;
                    } else {
                        message = Messages.OPERATION_FAILED;
                    }
                } else {
                    reasonCode = ReasonCode.INVALID_INFORMATION;
                }
            }

        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        if (message != null) {
            response.addProperty(MarketAttribute.MESSAGE, message);
        }
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.CHANGE_FULL_ORDER_STATUS)
    public static void changeFullOrderStatus(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ORDER_ID))) {
            message = "Please provide order Id";
        } else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.STATUS))) {

        } else {
            String orderId = requestObject.get(MarketAttribute.ORDER_ID).getAsString();
            Long userId = requestObject.get(MarketAttribute.USER_ID).getAsLong();
            Byte statusValue = requestObject.get(MarketAttribute.STATUS).getAsByte();

            MarketOrderStatus status = MarketOrderStatus.getStatus(statusValue);
            if (status == null || status == MarketOrderStatus.PAYMENT_INCOMPLETE) {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            } else {
                reasonCode = MarketRepository.INSTANCE.getOrderRepository().updateOrderStatus(userId, orderId, statusValue);
                if (reasonCode == ReasonCode.NONE) {
                    success = true;
                } else {
                    message = Messages.OPERATION_FAILED;
                }
            }

        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        if (message != null) {
            response.addProperty(MarketAttribute.MESSAGE, message);
        }
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }
}
