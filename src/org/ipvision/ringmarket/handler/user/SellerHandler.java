/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.handler.user;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javafx.util.Pair;
import org.ipvision.ringmarket.cache.ProductCategoryCache;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.MarketOrderStatus;
import org.ipvision.ringmarket.constants.Messages;
import org.ipvision.ringmarket.constants.OfferStatus;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.ShopType;
import org.ipvision.ringmarket.constants.UserTypeConstants;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.controller.annotation.Request;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.EntitySellerProduct;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.entities.order.EntityOrder;
import org.ipvision.ringmarket.entities.order.EntityOrderDetails;
import org.ipvision.ringmarket.entities.product.EntityProductDetail;
import org.ipvision.ringmarket.entities.product.EntityProductImage;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.product.order.OrderProduct;
import org.ipvision.ringmarket.repositories.buyer.OfferProductRepository;
import org.ipvision.ringmarket.repositories.product.ProductRepository;
import org.ipvision.ringmarket.repositories.seller.SellerProductRepository;
import org.ipvision.ringmarket.sender.ISender;
import org.ipvision.ringmarket.utils.JpaConverterJson;
import org.ipvision.ringmarket.utils.JsonValidator;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;

/**
 *
 * @author saikat
 */
public class SellerHandler {

    private static Gson gson = new Gson();

    @Request(action = Action.ADD_PRODUCT)
    public static void addProduct(JsonObject jsonObject, ISender sender) {

        EntityProduct entityProduct = gson.fromJson(jsonObject.toString(), EntityProduct.class);
        JsonObject responseObject = new JsonObject();

        boolean success = false;
        String message = null;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (entityProduct.getShopId() == null || entityProduct.getShopId() <= 0) {
            message = "Please provide store Id";
        } else if (entityProduct.getName() == null || entityProduct.getName().isEmpty()) {
            message = "Please provide name";
        } else if (entityProduct.getPrice() == null || entityProduct.getPrice() <= 0) {
            message = "Please provide product price";
        } else if (entityProduct.getCurrencyCode() == null || entityProduct.getCurrencyCode().isEmpty()) {
            message = "Please provide currency code";
        } else if (entityProduct.getImageUrl() == null || entityProduct.getImageUrl().isEmpty()) {
            message = "Please provide image url";
        } else if (entityProduct.getImageHeight() <= 0 || entityProduct.getImageWidth() <= 0) {
            message = "Please provide product image height width";
            reasonCode = ReasonCode.INVALID_IMAGE_PROPERTY;
        } else if (entityProduct.getLat() == null || entityProduct.getLon() == null) {
            message = "Please provide Lat/Lon";
        } else if (!JsonValidator.hasElement(jsonObject.get(MarketAttribute.CATEGORY_IDS)) || jsonObject.get(MarketAttribute.CATEGORY_IDS).getAsJsonArray().size() == 0) {
            message = "Please provide category ids";
        } //        else if (!JsonValidator.hasPositiveInteger(jsonObject.get(MarketAttribute.QUANTITY))) {
        //            message = "Please provide quantity";
        //        } 
        else {
            EntityShop shopById = MarketRepository.INSTANCE.getShopRepository().getShopById(entityProduct.getShopId());
            if (shopById != null) {
                JsonElement categoryJson = jsonObject.get(MarketAttribute.CATEGORY_IDS);

                Type listType = new TypeToken<Set<Integer>>() {
                }.getType();
                Set<Integer> categoryList = gson.fromJson(categoryJson, listType);

                boolean hasLeaf = false;
                Map<Integer , Integer> positionToIdMap = new HashMap<>();
                
                for (Integer categoryId : categoryList) {
                    EntityCategory categoryInfo = ProductCategoryCache.getInstance().getCategoryInfo(categoryId);
                    if (!categoryInfo.getHasNext()) {
                        hasLeaf = true;
                    }
                    if(positionToIdMap.containsKey(categoryInfo.getPosition())){
                        reasonCode = ReasonCode.INVALID_INFORMATION;
                        message = "Invalid category information";
                        break;
                    }else{
                        positionToIdMap.put(categoryInfo.getPosition(), categoryId);
                    }
                }
                if(reasonCode == ReasonCode.INVALID_INFORMATION){
                    
                }
                else if (hasLeaf) {
//                    entityProduct.setStatus(ProductStatus.AWAITING_FOR_APPROVAL);
                    entityProduct.setStatus(ProductStatus.AVAILABLE);
                    Long addedTime = System.currentTimeMillis();
                    entityProduct.setShopName(shopById.getName());
                    entityProduct.setCreationTime(addedTime);
                    Integer quantity = jsonObject.get(MarketAttribute.QUANTITY).getAsInt();

                    ObjectReturnDTO<String> addProduct = MarketRepository.INSTANCE.getProductRepository()
                            .addProduct(entityProduct, categoryList, quantity);
                    reasonCode = addProduct.getReasonCode();
                    if (reasonCode == ReasonCode.NONE) {
                        success = true;
                        message = "Successfully added product";
                        responseObject.addProperty(MarketAttribute.PRODUCT_ID, addProduct.getValue());
                    } else {
                        message = "Failed to add product";
                    }
                } else {
                    reasonCode = ReasonCode.INVALID_INFORMATION;
                    message = "Leaf category not found";
                }

            } else {
                reasonCode = ReasonCode.INVALID_INFORMATION;
                message = "Store not found";
            }
        }

        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.ADD_PRODUCT_DETAILS)
    public static void addProductDetails(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();

        boolean success = false;
        String message;

        JsonElement productIdElement = jsonObject.get(MarketAttribute.PRODUCT_ID);
        JsonElement descrptionElement = jsonObject.get(MarketAttribute.DESCRIPTION);

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide productId";
        } else if (!JsonValidator.hasValidString(descrptionElement)) {
            message = "Please provide product description";
        } else {
            LinkedHashMap featureElement = null;
            String productId = productIdElement.getAsString();
            if (JsonValidator.hasElement(jsonObject.get(MarketAttribute.FEATURES))) {
//                JSONObject object = new JSONObject(jsonObject.get(MarketAttribute.FEATURES).getAsJsonObject().toString());
//                featureElement = object.toMap();
                featureElement = gson.fromJson(jsonObject.get(MarketAttribute.FEATURES), JpaConverterJson.MAP_TYPE);
            }
            success = MarketRepository.INSTANCE.getProductDetailsRepository().addProductDetail(productIdElement.getAsString(), descrptionElement.getAsString(), featureElement);
            if (success) {
                message = "Successfully added product details";

//                MarketRepository.INSTANCE.getProductRepository().updateProductStatus(productId, ProductStatus.AVAILABLE);
            } else {
                message = "Failed to add details";
            }
        }

        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        sender.sendToClient(responseObject);
    }

    @Request(action = Action.ADD_PRODUCT_IMAGE_ALBUM)
    public static void addProductImageAlbum(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();

        boolean success = false;
        String message;

        JsonElement productIdElement = jsonObject.get(MarketAttribute.PRODUCT_ID);
        Type listType = new TypeToken<List<EntityProductImage>>() {
        }.getType();
        List<EntityProductImage> entityProductImages = gson.fromJson(jsonObject.get(MarketAttribute.LIST), listType);

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide productId";
        } else if (entityProductImages == null || entityProductImages.isEmpty()) {
            message = "Please provide product image album";
        } else {
            String productId = productIdElement.getAsString();
            reasonCode = MarketRepository.INSTANCE.getProductImageRepository().addImageList(productId, entityProductImages);
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                message = "Successfully added to album";
            } else {
                message = "Failed to add image";
            }
        }

        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        sender.sendToClient(responseObject);
    }

    @Request(action = Action.DELETE_PRODUCT)
    public static void deleteProduct(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();

        boolean success = false;
        String message = null;

        JsonElement sellerIdElement = jsonObject.get("sellerId");
        JsonElement productIdElement = jsonObject.get("id");

        if (!JsonValidator.hasPositiveLong(sellerIdElement)) {
            message = "Please provide seller Id";
        } else if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide productId";
        } else {
            String productId = productIdElement.getAsString();
            SellerProductRepository sellerProduct = MarketRepository.INSTANCE.getSellerProductRepository();
            EntitySellerProduct product = sellerProduct.getProduct(productId);
            if (product == null) {
                message = "Product not found";
            } else if (product.getSellerId() != sellerIdElement.getAsLong()) {
                message = "You aren't owner";
            } else {
                OrderProduct orderProduct = new OrderProduct();
                boolean productOrdered = orderProduct.isProductOrdered(productId);
                if (productOrdered) {
                    message = "Product can't be removed after order";
                } else {
                    ProductRepository productModel = MarketRepository.INSTANCE.getProductRepository();
                    success = productModel.deleteProduct(productId);
                    if (success) {
                        message = "Successfully deleted product";
                    } else {
                        message = "Failed to delete product";
                    }
                }
            }
        }
        responseObject.addProperty("sucs", success);
        responseObject.addProperty("mg", message);
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.UPDATE_PRODUCT)
    public static void updateProduct(JsonObject requestJson, ISender sender) {
        JsonObject responseObject = new JsonObject();

        boolean success = false;
        String message;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        JsonElement sellerIdElement = requestJson.get(MarketAttribute.SHOP_ID);
        JsonElement productIdElement = requestJson.get(MarketAttribute.PRODUCT_ID);
        if (!JsonValidator.hasPositiveLong(sellerIdElement)) {
            message = "Must provide shop Id";
        } else if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Must provide product Id";
        } else {
            ProductRepository productModule = MarketRepository.INSTANCE.getProductRepository();
            EntityProduct product = productModule.getProductById(productIdElement.getAsString());
            if (product != null) {
                boolean hasUpdateParameter = false;
                EntityProduct updateEntityProduct = gson.fromJson(requestJson, EntityProduct.class);

                if (updateEntityProduct.getName() != null && !updateEntityProduct.getName().isEmpty()
                        && !updateEntityProduct.getName().trim().equals(product.getName())) {
                    product.setName(updateEntityProduct.getName());
                    hasUpdateParameter = true;
                }

                if (updateEntityProduct.getPrice() != null && updateEntityProduct.getPrice() > 0
                        && updateEntityProduct.getPrice().doubleValue() != product.getPrice().doubleValue()) {
                    product.setPrice(updateEntityProduct.getPrice());
                    hasUpdateParameter = true;
                }
                if (updateEntityProduct.isExclusive() != null && updateEntityProduct.isExclusive() != product.isExclusive()) {
                    product.setIsExclusive(updateEntityProduct.isExclusive());
                    hasUpdateParameter = true;
                }
                if (updateEntityProduct.getCurrencyCode() != null && !updateEntityProduct.getCurrencyCode().isEmpty()
                        && !updateEntityProduct.getCurrencyCode().trim().equals(product.getCurrencyCode())) {
                    product.setCurrencyCode(updateEntityProduct.getCurrencyCode());
                    hasUpdateParameter = true;
                }

                if (updateEntityProduct.getLat() != null && updateEntityProduct.getLon() != null
                        && (updateEntityProduct.getLat().doubleValue() != product.getLat().doubleValue()
                        || updateEntityProduct.getLon().doubleValue() != product.getLon().doubleValue())) {
                    product.setLat(updateEntityProduct.getLat());
                    product.setLon(updateEntityProduct.getLon());
                    hasUpdateParameter = true;
                }

                if (hasUpdateParameter) {
                    product.setUpdateTime(System.currentTimeMillis());
                    reasonCode = productModule.updateProduct(product);
                    if (reasonCode == ReasonCode.NONE) {
                        message = "Successfully updated";
                        success = true;
                    } else {
                        message = "Failed to update";
                    }
                } else {
                    message = "Nothing to update";
                }
            } else {
                reasonCode = ReasonCode.INVALID_INFORMATION;
                message = "Product is unavailable";
            }
        }

        responseObject.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));
        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));

        sender.sendToClient(responseObject);
    }

    /**
     *
     * @param requestJson
     * @param sender
     */
    @Request(action = Action.UPDATE_PRODUCT_DETAILS)
    public static void updateProductDetails(JsonObject requestJson, ISender sender) {
        JsonObject responseObject = new JsonObject();

        boolean success = false;
        String message;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        JsonElement sellerIdElement = requestJson.get(MarketAttribute.SHOP_ID);

        if (!JsonValidator.hasPositiveLong(sellerIdElement)) {
            message = "Must provide shop Id";
        } else if (!JsonValidator.hasValidString(requestJson.get(MarketAttribute.ID))) {
            message = "Must provide product Id";
        } else {
            EntityProductDetail detail = new EntityProductDetail();
            detail.setId(requestJson.get(MarketAttribute.ID).getAsString());
            detail.setDescription(requestJson.get(MarketAttribute.DESCRIPTION).getAsString());

            LinkedHashMap featureElement;
            if (JsonValidator.hasElement(requestJson.get(MarketAttribute.FEATURES))) {
//                JSONObject object = new JSONObject(requestJson.get(MarketAttribute.FEATURES).getAsJsonObject().toString());
//                featureElement = object.toMap();
                featureElement = gson.fromJson(requestJson.get(MarketAttribute.FEATURES), JpaConverterJson.MAP_TYPE);
                detail.setAttributes(featureElement);
            }

            if ((detail.getDescription() != null && !detail.getDescription().isEmpty())
                    || (detail.getAttributes() != null && ((Map) detail.getAttributes()).isEmpty())) {
                reasonCode = MarketRepository.INSTANCE.getProductDetailsRepository().updateProductDetail(detail);
            }

            if (reasonCode == ReasonCode.NONE) {
                message = "Successfully updated";
                success = true;
            } else {
                message = "Unable to update";
            }

        }

        responseObject.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));
        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.UPDATE_PRODUCT_IMAGE)
    public static void updateProductImage(JsonObject requestJson, ISender sender) {
        JsonObject responseObject = new JsonObject();

        boolean success = false;
        String message;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        JsonElement sellerIdElement = requestJson.get(MarketAttribute.SHOP_ID);
        JsonElement productIdElement = requestJson.get(MarketAttribute.PRODUCT_ID);
        if (!JsonValidator.hasPositiveLong(sellerIdElement)) {
            message = "Must provide shop Id";
        } else if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Must provide product Id";
        } else if (!JsonValidator.hasValidString(requestJson.get(MarketAttribute.IMAGE_URL))) {
            message = "Please provide image url";
        } else if (!JsonValidator.hasElement(requestJson.get(MarketAttribute.DTO))) {
            message = "Please provide updated image property";
        } else {
            String priviousImageUrl = requestJson.get(MarketAttribute.IMAGE_URL).getAsString();

            EntityProductImage updateEntityImage = gson.fromJson(requestJson.get(MarketAttribute.DTO), EntityProductImage.class);

            if (updateEntityImage.getImageUrl() != null && !updateEntityImage.getImageUrl().isEmpty()
                    && updateEntityImage.getImageWidth() != null && updateEntityImage.getImageWidth() > 0
                    && updateEntityImage.getImageHeight() != null && updateEntityImage.getImageHeight() > 0) {

                updateEntityImage.setImageUrl(productIdElement.getAsString());
                reasonCode = MarketRepository.INSTANCE.getProductImageRepository().updateImage(priviousImageUrl, updateEntityImage);
            } else {
                reasonCode = ReasonCode.INVALID_IMAGE_PROPERTY;
            }

            if (reasonCode == ReasonCode.NONE) {
                message = "Successfully updated";
                success = true;
            } else {
                message = Messages.UPDATE_FAILED;
            }

        }

        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.DELETE_PRODUCT_IMAGE)
    public static void deleteProductImage(JsonObject requestJson, ISender sender) {
        JsonObject responseObject = new JsonObject();

        boolean success = false;
        String message = null;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        JsonElement productIdElement = requestJson.get(MarketAttribute.PRODUCT_ID);
        JsonElement imageUrlElement = requestJson.get(MarketAttribute.IMAGE_URL);
        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Must provide product Id";
        } else if (!JsonValidator.hasValidString(imageUrlElement)) {
            message = "Please provide image url";
        } else {
            String imageUrl = requestJson.get(MarketAttribute.IMAGE_URL).getAsString();
            String productId = requestJson.get(MarketAttribute.PRODUCT_ID).getAsString();

            reasonCode = MarketRepository.INSTANCE.getProductImageRepository().deleteImage(productId, imageUrl);
            switch (reasonCode) {
                case ReasonCode.NONE:
                    success = true;
                    break;
                case ReasonCode.PERMISSION_DENIED:
                    message = "Can't remove main image";
                    break;
                default:
                    message = Messages.OPERATION_FAILED;
                    break;
            }

        }

        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.SET_DEFAULT_PRODUCT_IMAGE)
    public static void setDefaultImage(JsonObject requestJson, ISender sender) {
        JsonObject responseObject = new JsonObject();

        boolean success = false;
        String message = null;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        JsonElement productIdElement = requestJson.get(MarketAttribute.PRODUCT_ID);
        JsonElement newImageUrlElement = requestJson.get(MarketAttribute.IMAGE_URL);
        JsonElement oldImageUrlElement = requestJson.get(MarketAttribute.OLD_IMAGE_URL);
        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Must provide product Id";
        } else if (!JsonValidator.hasValidString(newImageUrlElement)) {
            message = "Please provide image url";
        } else if (!JsonValidator.hasValidString(oldImageUrlElement)) {
            message = "Please provide old image url";
        } else {
            String newImageUrl = requestJson.get(MarketAttribute.IMAGE_URL).getAsString();
            String oldImageUrl = requestJson.get(MarketAttribute.OLD_IMAGE_URL).getAsString();
            String productId = requestJson.get(MarketAttribute.PRODUCT_ID).getAsString();

            reasonCode = MarketRepository.INSTANCE.getProductImageRepository().changeDefaultImage(oldImageUrl, newImageUrl, productId);
            switch (reasonCode) {
                case ReasonCode.NONE:
                    success = true;
                    break;
                default:
                    message = Messages.OPERATION_FAILED;
                    break;
            }

        }

        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

//    @Request(action = Action.GET_ORDER_DETAILS)
//    public static void getOrderDetails(JsonObject jsonObject, ISender sender) {
//        JsonObject responseJson = new JsonObject();
//        boolean success = false;
//        long buyerId;
//        String productId = null;
//        int scrollValue = 0;
//        int limit = 10;
//        String message = null;
//
//        JsonElement productIdElement = jsonObject.get("productId");
//        JsonElement buyerIdElement = jsonObject.get("userId");
//        if (productIdElement == null || productIdElement.isJsonNull() || productIdElement.getAsString().isEmpty()) {
//            message = "Please provide productId";
//        } else if (buyerIdElement == null || buyerIdElement.isJsonNull() || buyerIdElement.getAsLong() <= 0) {
//            message = "Please provide buyerId";
//        } else {
//            productId = productIdElement.getAsString();
//            buyerId = buyerIdElement.getAsLong();
// 
//            OrderProduct orderProduct = new OrderProduct();
//            IOrderProduct orderInfo = orderProduct.getOrderInfo(productId);
//            if (orderInfo != null) {
//                GsonBuilder builder = new GsonBuilder();
//                builder.registerTypeAdapter(IOrderProduct.class, new OrderProductAdapter());
//                JsonElement orderInfoJson = builder.create().toJsonTree(orderInfo, IOrderProduct.class);
//                success = true;
//                responseJson.add("orderInfo", orderInfoJson);
//                
//            } else {
//                message = "No order details found for this product";
//            }
//        }
//        responseJson.addProperty("sucs", success);
//        if (message != null) {
//            responseJson.addProperty("mg", message);
//        }
//
//        sender.sendToClient(responseJson);
//    }
    /**
     *
     * @param jsonObject
     * @param sender
     */
    @Request(action = Action.MY_PRODUCTS)
    public static void getMyProducts(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String productID = null;
        int scrollValue = 0;
        int limit = 20;
        JsonElement element;

        String message = null;
        boolean success = false;

        element = jsonObject.get("userId");
        if (!JsonValidator.hasPositiveLong(element)) {
            message = "Please provide user id";
        } else {
            long sellerId = element.getAsLong();

            element = jsonObject.get("productId");
            if (JsonValidator.hasValidString(element)) {
                productID = element.getAsString();
            }

            element = jsonObject.get("scrl");
            if (JsonValidator.hasPositiveInteger(element)) {
                scrollValue = element.getAsInt();
            }

            element = jsonObject.get("lmt");
            if (JsonValidator.hasPositiveInteger(element)) {
                limit = element.getAsInt();
            }

            Scroll scroll = (scrollValue > 0) ? Scroll.UP : Scroll.DOWN;

            SellerProductRepository sellerProduct = MarketRepository.INSTANCE.getSellerProductRepository();
            List<EntitySellerProduct> productList = sellerProduct.getProductList(sellerId, productID, scroll, limit);
            if (productList == null || productList.isEmpty()) {
                message = "No products found";
            } else {
                GsonBuilder builder = new GsonBuilder();
//                builder.registerTypeAdapter(ISellerProduct.class, new SellerProductAdapter());
                JsonElement sellerProductsJson = builder.create().toJsonTree(productList.toArray(), EntitySellerProduct[].class);
                success = true;
                responseJsonObject.add("sellerProducts", sellerProductsJson);
            }
        }

        responseJsonObject.addProperty("sucs", success);
        responseJsonObject.addProperty("mg", message);
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.ACCEPT_OFFER)
    public static void acceptOffer(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        JsonElement userIdElement = jsonObject.get("userId");
        JsonElement productIdElement = jsonObject.get("productId");
        JsonElement offerIdElement = jsonObject.get("offerId");

        String message;
        boolean success = false;

        if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user id";
        } else if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product id";
        } else if (!JsonValidator.hasValidString(offerIdElement)) {
            message = "Please provide offer id";
        } else {
            long userId = userIdElement.getAsLong();
            String offerId = offerIdElement.getAsString();
            String productId = productIdElement.getAsString();

            SellerProductRepository sellerProduct = MarketRepository.INSTANCE.getSellerProductRepository();
            EntitySellerProduct product = sellerProduct.getProduct(productId);
            if (product != null && product.getSellerId() != userId) {
                message = "You are not owner of this product";
            } else {
                OfferProductRepository offerProduct = MarketRepository.INSTANCE.getOfferProductRepository();
                boolean accepted = offerProduct.updateOfferStatus(offerId, OfferStatus.ACCEPTED);
                if (accepted) {
                    success = true;
                    message = "Successfully accepted";
                } else {
                    message = "Unsuccessful";
                }
            }
            responseJsonObject.addProperty("sucs", success);
            responseJsonObject.addProperty("mg", message);
            responseJsonObject.addProperty(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID).getAsString());

            sender.sendToClient(responseJsonObject);
        }

        responseJsonObject.addProperty("sucs", success);
        responseJsonObject.addProperty("mg", message);
        responseJsonObject.addProperty(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID).getAsString());

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.REJECT_OFFER)
    public static void rejectOffer(JsonObject jsonObject, ISender sender
    ) {
        JsonObject responseJsonObject = new JsonObject();
        JsonElement userIdElement = jsonObject.get("userId");
        JsonElement productIdElement = jsonObject.get("productId");
        JsonElement offerIdElement = jsonObject.get("offerId");

        String message;
        boolean success = false;

        if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user id";
        } else if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide product id";
        } else if (!JsonValidator.hasValidString(offerIdElement)) {
            message = "Please provide offer id";
        } else {
            long userId = userIdElement.getAsLong();
            String offerId = offerIdElement.getAsString();
            String productId = productIdElement.getAsString();

            SellerProductRepository sellerProduct = MarketRepository.INSTANCE.getSellerProductRepository();
            boolean isProductOwner = sellerProduct.isProductOwner(userId, productId);
            if (!isProductOwner) {
                message = "You are not owner of this product";
            } else {
                OfferProductRepository offerProduct = MarketRepository.INSTANCE.getOfferProductRepository();
                boolean accepted = offerProduct.updateOfferStatus(offerId, OfferStatus.REJECTED);
                if (accepted) {
                    success = true;
                    message = "Successfully rejected";
                } else {
                    message = "Unsuccessful";
                }
            }
            responseJsonObject.addProperty("sucs", success);
            responseJsonObject.addProperty("mg", message);
            responseJsonObject.addProperty(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID).getAsString());

            sender.sendToClient(responseJsonObject);
        }

        responseJsonObject.addProperty("sucs", success);
        responseJsonObject.addProperty("mg", message);
        responseJsonObject.addProperty(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID).getAsString());

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.PAYMENT_COMPLETED)
    public static void completePayment(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String message = null;
        boolean success = false;
        int reasonCode;
        JsonElement invoiceIdElement = jsonObject.get(MarketAttribute.INVOICE_ID);
        JsonElement userIdElement = jsonObject.get(MarketAttribute.USER_ID);
        JsonElement paymentMethodElement = jsonObject.get(MarketAttribute.PAYMENT_METHOD);

        if (!JsonValidator.hasValidString(invoiceIdElement)) {
            message = "Please provide invoice id";
        } else if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide user id";
        } else if (!JsonValidator.hasPositiveLong(paymentMethodElement)) {
            message = "Please provide payment method";
        } else {
            String invoiceId = invoiceIdElement.getAsString();
            Long userId = userIdElement.getAsLong();
            Byte paymentMode = paymentMethodElement.getAsByte();

            reasonCode = MarketRepository.INSTANCE.getOrderRepository().completePayment(userId, invoiceId, paymentMode);
            if (reasonCode == ReasonCode.NONE) {
//                ListReturnDTO<EntityOrder> orderListByInvoiceId = MarketRepository.INSTANCE.getOrderRepository().getOrderListByInvoiceId(invoiceId);
//                reasonCode = orderListByInvoiceId.getReasonCode();
//                if (reasonCode == ReasonCode.NONE) {
                success = true;
//                    responseJsonObject.add(MarketAttribute.LIST, gson.toJsonTree(orderListByInvoiceId.getList()));
//                }
            }

            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_ORDERS_BY_INVOICE_FOR_AUTH)
    @Request(action = Action.GET_ORDERS_BY_INVOICE)
    public static void findOrdersByInvoice(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        String message = null;
        boolean success = false;
        int reasonCode;
        JsonElement invoiceIdElement = jsonObject.get(MarketAttribute.INVOICE_ID);

        if (!JsonValidator.hasValidString(invoiceIdElement)) {
            message = "Please provide invoice id";
        } else {
            String invoiceId = invoiceIdElement.getAsString();

            ListReturnDTO<EntityOrder> orderListByInvoiceId = MarketRepository.INSTANCE.getOrderRepository().getOrderListByInvoiceId(invoiceId);
            reasonCode = orderListByInvoiceId.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                responseJsonObject.add(MarketAttribute.LIST, gson.toJsonTree(orderListByInvoiceId.getList()));
            }
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        if (jsonObject.has(MarketAttribute.CREDITED_AMOUNT)) {
            responseJsonObject.add(MarketAttribute.CREDITED_AMOUNT, jsonObject.get(MarketAttribute.CREDITED_AMOUNT));
        }
        if (jsonObject.has(MarketAttribute.CURRENCY_ISO)) {
            responseJsonObject.add(MarketAttribute.CURRENCY_ISO, jsonObject.get(MarketAttribute.CURRENCY_ISO));
        }

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.SELLER_RATING_DETAILS)
    public static void sellerRatingDetails(JsonObject requestJson, ISender sender
    ) {
        JsonObject responseJson = new JsonObject();
        String message = null;
        boolean success = false;
        JsonElement productIdElement = requestJson.get(MarketAttribute.SHOP_ID);
        if (!JsonValidator.hasPositiveLong(productIdElement)) {
            message = "Please provide seller Id";
        } else {
            Long sellerId = productIdElement.getAsLong();

            Map<Byte, Long> ratingDetails = MarketRepository.INSTANCE.getSellerRatingLogRepository().getRatingDetails(sellerId);
            if (!ratingDetails.isEmpty()) {
                success = true;
                responseJson.add(MarketAttribute.DETAILS, gson.toJsonTree(ratingDetails));
            }
        }
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.CREATE_STORE)
    public static void createStore(JsonObject requestJson, ISender sender
    ) {
        JsonObject responseJson = new JsonObject();
        String message;
        boolean success = false;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        EntityShop entityShop = gson.fromJson(requestJson, EntityShop.class);
        Integer userType = null;
        if (JsonValidator.hasElement(requestJson.get(MarketAttribute.USER_TYPE))) {
            userType = requestJson.get(MarketAttribute.USER_TYPE).getAsInt();
        }
        if (userType == null || userType < 0) {
            message = "Please provide user type";
        } else if (entityShop.getId() == 0) {
            message = "Please provide store id";
        } else if (entityShop.getLat() == null || entityShop.getLon() == null) {
            message = "Please provide lat/lon";
        } else if (entityShop.getType() == null) {
            message = "Please provide store type";
        } else if (entityShop.getOwnerId() == null || entityShop.getOwnerId() <= 0) {
            message = "Please provide store owner id";
        } else {
            entityShop.setCreationTime(System.currentTimeMillis());
            if (entityShop.getCancelDuration() == null) {
                entityShop.setCancelDuration(TimeUnit.DAYS.toMillis(19));
            }

            if (entityShop.getProtectionDuration() == null) {
                entityShop.setProtectionDuration(TimeUnit.DAYS.toMillis(30));
            }

            if (entityShop.getFeedbackDuration() == null) {
                entityShop.setFeedbackDuration(TimeUnit.DAYS.toMillis(15));
            }

            if (userType == UserTypeConstants.CELEBRITY) {
                entityShop.setType(ShopType.CELEBRITY_STORE);
            }

            reasonCode = MarketRepository.INSTANCE.getShopRepository().addShop(entityShop, userType);
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                message = "Successfully Created Store";
            } else {
                message = "Store creation failed";
            }
        }
        responseJson.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

//    @Request(action = Action.SELLER_ORDER_LIST)
//    public static void sellerOrderList(JsonObject requestJson, ISender sender
//    ) {
//        JsonObject responseJson = new JsonObject();
//        String message = null;
//        boolean success = false;
//        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
//
//        JsonElement element = requestJson.get(MarketAttribute.SHOP_ID);
//
//        if (!JsonValidator.hasPositiveLong(element)) {
//            message = "Please provide shop Id";
//        } else {
//            Long sellerId = requestJson.get(MarketAttribute.SHOP_ID).getAsLong();
//            String pivotId = null;
//
//            Long pivotTime = System.currentTimeMillis();
//
//            element = requestJson.get(MarketAttribute.PIVOT_STRING);
//            if (JsonValidator.hasValidString(element)) {
//                pivotId = element.getAsString();
//            }
//
//            element = requestJson.get(MarketAttribute.SCROLL);
//            int scrollValue = 2;
//
//            if (JsonValidator.hasPositiveInteger(element)) {
//                scrollValue = element.getAsInt();
//            }
//            Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;
//
//            element = requestJson.get(MarketAttribute.PIVOT_NUMBER);
//            if (JsonValidator.hasPositiveLong(element)) {
//                pivotTime = element.getAsLong();
//            }
//
//            int limit = 10;
//            element = requestJson.get(MarketAttribute.LIMIT);
//            if (JsonValidator.hasPositiveInteger(element)) {
//                limit = element.getAsInt();
//            }
//
//            Byte shipmentMethod = null;
//            Set<Byte> status = null;
//            Byte paymentType = null;
//
//            element = requestJson.get(MarketAttribute.SHIPMENT_METHOD);
//            if (JsonValidator.hasElement(element)) {
//                shipmentMethod = element.getAsByte();
//            }
//            element = requestJson.get(MarketAttribute.LIST);
//            if (JsonValidator.hasElement(element)) {
//                Type setType = new TypeToken<HashSet<Byte>>() {
//                }.getType();
//                status = gson.fromJson(element, setType);
//            }
//            element = requestJson.get(MarketAttribute.PAYMENT_METHOD);
//            if (JsonValidator.hasElement(element)) {
//                paymentType = element.getAsByte();
//            }
//
//            PivotedListReturnDTO<EntityOrder, String> filteredSellerOrders = MarketRepository.INSTANCE.getOrderRepository().getFilteredOrders(pivotId, pivotTime, sellerId, shipmentMethod, status, paymentType, scroll, limit);
//            pivotId = filteredSellerOrders.getPivot();
//            reasonCode = filteredSellerOrders.getReasonCode();
//            if (reasonCode == ReasonCode.NONE) {
//                success = true;
//                responseJson.add(MarketAttribute.LIST, gson.toJsonTree(filteredSellerOrders.getList()));
//                responseJson.addProperty(MarketAttribute.PIVOT_STRING, pivotId);
//            }
//        }
//        responseJson.addProperty(MarketAttribute.REASON_CODE, reasonCode);
//        responseJson.addProperty(MarketAttribute.SUCCESS, success);
//        responseJson.addProperty(MarketAttribute.MESSAGE, message);
//        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
//        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));
//
//        sender.sendToClient(responseJson);
//    }
    @Request(action = Action.SELLER_ORDER_LIST)
    @Request(action = Action.ORDER_LIST)
    public static void filteredOrderList(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        String message = null;
        boolean success = false;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        JsonElement element = requestJson.get(MarketAttribute.SHOP_ID);

        Long sellerId = null;
        if (JsonValidator.hasPositiveLong(element)) {
            sellerId = element.getAsLong();
        }
        if (requestJson.get(MarketAttribute.ACTION).getAsInt() == Action.SELLER_ORDER_LIST.getValue() && sellerId == null) {
            message = "Please provide shop Id";
        } else {
            String pivotId = null;

            Long pivotTime = 0L;

            element = requestJson.get(MarketAttribute.PIVOT_STRING);
            if (JsonValidator.hasValidString(element)) {
                pivotId = element.getAsString();
            }

            element = requestJson.get(MarketAttribute.SCROLL);
            int scrollValue = 2;

            if (JsonValidator.hasPositiveInteger(element)) {
                scrollValue = element.getAsInt();
            }
            Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

            element = requestJson.get(MarketAttribute.PIVOT_NUMBER);
            if (JsonValidator.hasPositiveLong(element)) {
                pivotTime = element.getAsLong();
            }

            int limit = 10;
            element = requestJson.get(MarketAttribute.LIMIT);
            if (JsonValidator.hasPositiveInteger(element)) {
                limit = element.getAsInt();
            }

            Byte shipmentMethod = null;
            Set<Byte> status = null;
            Byte paymentType = null;

            element = requestJson.get(MarketAttribute.SHIPMENT_METHOD);
            if (JsonValidator.hasElement(element)) {
                shipmentMethod = element.getAsByte();
            }
            element = requestJson.get(MarketAttribute.LIST);
            if (JsonValidator.hasElement(element)) {
                Type setType = new TypeToken<HashSet<Byte>>() {
                }.getType();
                status = gson.fromJson(element, setType);
            }
            element = requestJson.get(MarketAttribute.PAYMENT_METHOD);
            if (JsonValidator.hasElement(element)) {
                paymentType = element.getAsByte();
            }

            PivotedListReturnDTO<EntityOrder, Pair<String, Long>> filteredOrders = MarketRepository.INSTANCE.getOrderRepository().getFilteredOrders(new Pair<>(pivotId, pivotTime), sellerId,
                    shipmentMethod, status, paymentType, scroll, limit);
            pivotId = filteredOrders.getPivot().getKey();
            pivotTime = filteredOrders.getPivot().getValue();
            reasonCode = filteredOrders.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                responseJson.add(MarketAttribute.LIST, gson.toJsonTree(filteredOrders.getList()));
            } else if (reasonCode == ReasonCode.NO_DATA_FOUND) {
                message = Messages.NO_DATA_FOUND;
            } else if (reasonCode == ReasonCode.NO_MORE_DATA) {
                message = Messages.NO_MORE_DATA;
            }
            responseJson.addProperty(MarketAttribute.PIVOT_STRING, pivotId);
            responseJson.addProperty(MarketAttribute.PIVOT_NUMBER, pivotTime);

        }

        responseJson.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.ON_SHIPMENT)
    public static void orderItemsOnShipment(JsonObject requestObject, ISender sender) {
        UserHandler.orderItemStatusUpdate(requestObject, sender, MarketOrderStatus.ON_SHIPMENT.getValue());
    }

    @Request(action = Action.SHIPMENT_COMPLETE)
    public static void orderItemsShipmentComplete(JsonObject requestObject, ISender sender
    ) {
        UserHandler.orderItemStatusUpdate(requestObject, sender, MarketOrderStatus.SHIPMENT_COMPLETED.getValue());
    }

    @Request(action = Action.ORDER_DETAILS)
    public static void orderDetails(JsonObject requestJson, ISender sender) {
        JsonObject responseJson = new JsonObject();
        String message = null;
        boolean success = false;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        JsonElement orderIdElement = requestJson.get(MarketAttribute.ORDER_ID);

        if (!JsonValidator.hasValidString(orderIdElement)) {
            message = "Please provide order Id";
        } else {
            String orderId = orderIdElement.getAsString();
            ObjectReturnDTO<EntityOrder> orderInfo = MarketRepository.INSTANCE.getOrderRepository().findOrder(orderId);

            reasonCode = orderInfo.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                ListReturnDTO<EntityOrderDetails> orderDetails = MarketRepository.INSTANCE.getOrderDetailsRepository().getOrderDetails(orderId);
                reasonCode = orderDetails.getReasonCode();
                if (reasonCode == ReasonCode.NONE) {
                    orderInfo.getValue().setStatusMessage(
                            MarketOrderStatus.getStatus(orderInfo.getValue().getStatus())
                                    .getMessage()
                    );
                    for (EntityOrderDetails entityOrderDetails : orderDetails.getList()) {
                        entityOrderDetails.setStatusMessage(MarketOrderStatus.getStatus(entityOrderDetails.getItemStatus()).getMessage("Product"));
                    }
                    responseJson = gson.toJsonTree(orderInfo.getValue()).getAsJsonObject();
                    success = true;
                    responseJson.add(MarketAttribute.DETAILS, gson.toJsonTree(orderDetails.getList()));
                }
            }
        }

        responseJson.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseJson.addProperty(MarketAttribute.SUCCESS, success);
        responseJson.addProperty(MarketAttribute.MESSAGE, message);
        responseJson.add(MarketAttribute.PACKET_ID, requestJson.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, requestJson.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.GET_USER_STORES)
    public static void getUserStores(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        String message = null;
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        if (!JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.OWNER_ID))) {
            message = "Please provide owner id";
        } else {
            Long ownerId = requestObject.get(MarketAttribute.OWNER_ID).getAsLong();
            long pivotId = 0;
            element = requestObject.get(MarketAttribute.PIVOT_NUMBER);
            if (JsonValidator.hasPositiveLong(element)) {
                pivotId = element.getAsLong();
            }
            element = requestObject.get(MarketAttribute.SCROLL);
            if (JsonValidator.hasPositiveInteger(element)) {
                scrollValue = element.getAsInt();
            }
            Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

            element = requestObject.get(MarketAttribute.LIMIT);
            if (JsonValidator.hasPositiveInteger(element)) {
                limit = element.getAsInt();
            }

            PivotedListReturnDTO<EntityShop, Long> items = MarketRepository.INSTANCE.getShopRepository().getShopsOfOwner(pivotId, ownerId, limit, scroll);
            pivotId = items.getPivot();
            reasonCode = items.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items.getList()));
                success = true;
            } else {
                message = Messages.NO_MORE_STORE;
                reasonCode = items.getReasonCode();
            }
            responseJsonObject.addProperty(MarketAttribute.PIVOT_NUMBER, pivotId);
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);

        sender.sendToClient(responseJsonObject);
    }
}
