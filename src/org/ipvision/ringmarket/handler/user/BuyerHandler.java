/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.handler.user;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.MarketOrderStatus;
import org.ipvision.ringmarket.constants.Messages;
import org.ipvision.ringmarket.constants.OfferStatus;
import org.ipvision.ringmarket.constants.OrderStatus;
import org.ipvision.ringmarket.constants.PaymentMode;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.controller.annotation.Request;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.EntityOfferProduct;
import org.ipvision.ringmarket.entities.EntityOrderProduct;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.EntitySavedProduct;
import org.ipvision.ringmarket.entities.EntitySellerProduct;
import org.ipvision.ringmarket.entities.order.Address;
import org.ipvision.ringmarket.entities.order.EntityOrder;
import org.ipvision.ringmarket.entities.order.EntityOrderDetails;
import org.ipvision.ringmarket.entities.order.EntityShippingAddress;
import org.ipvision.ringmarket.entities.product.EntityProductStock;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.product.order.OrderProduct;
import org.ipvision.ringmarket.repositories.user.MarketUserRepository;
import org.ipvision.ringmarket.repositories.buyer.OfferProductRepository;
import org.ipvision.ringmarket.repositories.product.ProductRepository;
import org.ipvision.ringmarket.repositories.buyer.SavedProductRepository;
import org.ipvision.ringmarket.repositories.seller.SellerProductRepository;
import org.ipvision.ringmarket.sender.ISender;
import org.ipvision.ringmarket.utils.EmailValidator;
import org.ipvision.ringmarket.utils.JsonValidator;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;

/**
 *
 * @author saikat
 */
public class BuyerHandler {

    private static Gson gson = new Gson();

    @Request(action = Action.ADD_TO_SAVE_LIST)
    public static void addToSaveList(JsonObject jsonObject, ISender sender) {
        JsonObject responseJson = new JsonObject();
        JsonElement productIdElement = jsonObject.get("productId");
        JsonElement userIdIdElement = jsonObject.get("userId");
        boolean success = false;
        String message;

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide Product Id";
        } else if (!JsonValidator.hasPositiveLong(userIdIdElement)) {
            message = "Please provide User Id";
        } else {
            String productId = productIdElement.getAsString();
            long buyerId = userIdIdElement.getAsLong();
            MarketUserRepository ringmarketUser = MarketRepository.INSTANCE.getMarketUserRepository();
            if (ringmarketUser.getUser(buyerId) != null) {
                SavedProductRepository savedProduct = MarketRepository.INSTANCE.getSavedProductRepository();
                success = savedProduct.isAlreadyOnSavedList(buyerId, productId);
                if (success) {
                    message = "Already in saved list";
                    success = false;
                } else {
                    ProductRepository product = MarketRepository.INSTANCE.getProductRepository();
                    EntityProduct entityProduct = product.getProductById(productId);
                    if (entityProduct != null) {
                        success = savedProduct.addToSavedList(buyerId, entityProduct);
                        if (success) {
                            message = "Successfully added to saved list";
                        } else {
                            message = "Failed to add saved list";
                        }
                    } else {
                        message = "No Such Product Found";
                        success = false;
                    }
                }
            } else {
                message = "User not found";
            }
        }

        responseJson.addProperty("sucs", success);
        responseJson.addProperty("mg", message);
        responseJson.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.REMOVE_FROM_SAVE_LIST)
    public static void removeFromSaveList(JsonObject jsonObject, ISender sender) {
        JsonObject responseJson = new JsonObject();
        JsonElement productIdElement = jsonObject.get("productId");
        JsonElement userIdIdElement = jsonObject.get("userId");
        boolean success = false;
        String message;

        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide Product Id";
        } else if (!JsonValidator.hasPositiveLong(userIdIdElement)) {
            message = "Please provide User Id";
        } else {
            String productId = productIdElement.getAsString();
            long buyerId = userIdIdElement.getAsLong();
            MarketUserRepository ringmarketUser = MarketRepository.INSTANCE.getMarketUserRepository();
            if (ringmarketUser.getUser(buyerId) != null) {
                SavedProductRepository savedProduct = MarketRepository.INSTANCE.getSavedProductRepository();
                success = savedProduct.deleteSavedProduct(buyerId, productId);
                if (success) {
                    message = "Successfully removed from saved list";
                } else {
                    message = "Failed to remove";
                }
            } else {
                message = "User not found";
            }
        }
        responseJson.addProperty("sucs", success);
        responseJson.addProperty("mg", message);
        responseJson.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJson.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.GET_SAVE_LIST)
    public static void getSaveList(JsonObject jsonObject, ISender sender) {
        JsonObject responseJson = new JsonObject();
        JsonElement element;
        boolean success = false;
        long buyerId;
        String productId = null;
        int scrollValue = 0;
        int limit = 10;
        String message = null;

        element = jsonObject.get("userId");
        if (!JsonValidator.hasPositiveLong(element)) {
            message = "Please provide userId";
        } else {
            buyerId = element.getAsLong();
            if (MarketRepository.INSTANCE.getMarketUserRepository().getUser(buyerId) != null) {
                element = jsonObject.get("productId");
                if (JsonValidator.hasValidString(element)) {
                    productId = element.getAsString();
                }

                element = jsonObject.get("scrl");
                if (JsonValidator.hasPositiveInteger(element)) {
                    scrollValue = element.getAsInt();
                }

                element = jsonObject.get("lmt");
                if (JsonValidator.hasPositiveInteger(element)) {
                    limit = element.getAsInt();
                }

                Scroll scroll = (scrollValue > 0) ? Scroll.UP : Scroll.DOWN;

                SavedProductRepository savedProduct = MarketRepository.INSTANCE.getSavedProductRepository();
                List<EntitySavedProduct> savedProductList = savedProduct.getSavedProductList(buyerId, productId, scroll, limit);

                if (savedProductList == null || savedProductList.isEmpty()) {
                    message = "No products in saved list";
                } else {
                    GsonBuilder builder = new GsonBuilder();
//                builder.registerTypeAdapter(ISavedProduct.class, new SavedProductAdapter());
                    JsonElement savedProductsJson = builder.create().toJsonTree(savedProductList.toArray(), EntitySavedProduct[].class);
                    responseJson.add("savedProduct", savedProductsJson);
                    success = true;
                }
            } else {
                message = "User not found";
            }
        }
        responseJson.addProperty("sucs", success);
        responseJson.addProperty("mg", message);
        responseJson.addProperty(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID).getAsString());

        sender.sendToClient(responseJson);
    }

    /**
     *
     * @param jsonObject
     * @param sender
     */
    @Request(action = Action.GET_MY_OFFER_LIST)
    public static void getMyOfferList(JsonObject jsonObject, ISender sender) {
        JsonObject responseJson = new JsonObject();
        JsonElement element;
        boolean success = false;
        long buyerId;
        long time;
        int scrollValue = 0;
        int limit = 10;
        String message = null;

        element = jsonObject.get("userId");
        if (!JsonValidator.hasPositiveLong(element)) {
            message = "Please provide userId";
        } else {
            buyerId = element.getAsLong();
            if (MarketRepository.INSTANCE.getMarketUserRepository().getUser(buyerId) != null) {
                element = jsonObject.get("tm");
                if (JsonValidator.hasPositiveLong(element)) {
                    time = element.getAsLong();
                } else {
                    time = System.currentTimeMillis();
                }

                element = jsonObject.get("scrl");
                if (JsonValidator.hasPositiveInteger(element)) {
                    scrollValue = element.getAsInt();
                }

                element = jsonObject.get("lmt");
                if (JsonValidator.hasPositiveInteger(element)) {
                    limit = element.getAsInt();
                }

                Scroll scroll = (scrollValue > 0) ? Scroll.UP : Scroll.DOWN;

                OfferProductRepository offerProduct = MarketRepository.INSTANCE.getOfferProductRepository();
                List<EntityOfferProduct> offersByBuyer = offerProduct.getOffersByBuyer(buyerId, time, limit, scroll);

                if (offersByBuyer == null || offersByBuyer.isEmpty()) {
                    message = "No Offers Found";
                } else {
                    GsonBuilder builder = new GsonBuilder();
//                builder.registerTypeAdapter(IOfferProduct.class, new OfferProductAdapter());
                    JsonElement offerListJson = builder.create().toJsonTree(offersByBuyer.toArray(), EntityOfferProduct[].class);
                    responseJson.add("offerList", offerListJson);
                    success = true;
                }
            } else {
                message = "User not found";
            }
        }
        responseJson.addProperty("sucs", success);
        responseJson.addProperty("mg", message);
        responseJson.addProperty(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID).getAsString());

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.COMPLETE_ORDER)
    public static void completeOrder(JsonObject jsonObject, ISender sender) {
        JsonObject responseJson = new JsonObject();
        boolean success = false;
        long buyerId;
        String orderId;
        String message;

        JsonElement userIdelement = jsonObject.get("userId");
        JsonElement orderIdelement = jsonObject.get("ordrId");
        if (!JsonValidator.hasPositiveLong(userIdelement)) {
            message = "Please provide userId";
        } else if (!JsonValidator.hasValidString(orderIdelement)) {
            message = "Please provide order Id";
        } else {
            buyerId = userIdelement.getAsLong();
            if (MarketRepository.INSTANCE.getMarketUserRepository().getUser(buyerId) != null) {
                orderId = orderIdelement.getAsString();
                OrderProduct orderProduct = new OrderProduct();
                success = orderProduct.updateOrderStatusCompleted(orderId, System.currentTimeMillis());
                if (success) {
                    message = "Successfully completed order";
                } else {
                    message = "Failed to complete order";
                }
            } else {
                message = "User not found";
            }
        }
        responseJson.addProperty("sucs", success);
        responseJson.addProperty("mg", message);
        responseJson.addProperty(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID).getAsString());

        sender.sendToClient(responseJson);
    }

    /**
     * Offer product
     *
     * @param jsonObject
     * @param sender
     */
//    @Request(action = Action.OFFER_PRODUCT)
//    public static void offerProduct(JsonObject jsonObject, ISender sender) {
//        JsonObject responseJson = new JsonObject();
//        boolean success = false;
//        long buyerId;
//        String productId;
//        String message;
//
//        JsonElement productIdElement = jsonObject.get("productId");
//        JsonElement buyerIdElement = jsonObject.get("userId");
//        JsonElement buyerNameElement = jsonObject.get("userName");
//        JsonElement bidPriceElement = jsonObject.get("bitPrice");
//
//        if (!JsonValidator.hasValidString(productIdElement)) {
//            message = "Please provide productId";
//        } else if (!JsonValidator.hasPositiveLong(buyerIdElement)) {
//            message = "Please provide buyerId";
//        } else if (!JsonValidator.hasPositiveDouble(bidPriceElement)) {
//            message = "Please provide valid price";
//        } else if (!JsonValidator.hasValidString(buyerNameElement)) {
//            message = "Please provide buyer name";
//        } else {
//            productId = productIdElement.getAsString();
//            buyerId = buyerIdElement.getAsLong();
//            if (MarketRepository.INSTANCE.getMarketUserRepository().getUser(buyerId) != null) {
//                String buyerName = buyerNameElement.getAsString();
//                double bidPrice = bidPriceElement.getAsDouble();
//
//                EntitySellerProduct sellerProduct = MarketRepository.INSTANCE.getSellerProductRepository().getProduct(productId);
//                if (sellerProduct != null) {
//                    if (sellerProduct.getSellerId() == buyerId) {
//                        message = "Can't bit own product";
//                    } else {
//                        EntityProduct productById = MarketRepository.INSTANCE.getProductRepository().getProductById(productId);
//                        if (productById.getStatus() == ProductStatus.AVAILABLE.ordinal()) {
//                            if (productById.getIsFixedPrice() && productById.getBasePrice() != bidPrice) {
//                                message = "Fixed Price";
//                            } else {
//                                OfferProductRepository offerProduct = MarketRepository.INSTANCE.getOfferProductRepository();
//                                EntityOfferProduct offerInfoOfProduct = offerProduct.getOfferInfoOfProduct(buyerId, productId);
//                                if (offerInfoOfProduct != null) {
//                                    if (offerInfoOfProduct.getStatus() != OfferStatus.ACCEPTED.ordinal()
//                                            && offerInfoOfProduct.getStatus() != OfferStatus.ORDERED.ordinal()) {
//                                        if (offerInfoOfProduct.getOfferedPrice() == bidPrice) {
//                                            message = "Already received this offer";
//                                        } else {
//                                            long offeredTime = System.currentTimeMillis();
//                                            boolean addNewOffer = offerProduct.updateOfferBid(offerInfoOfProduct, bidPrice, offeredTime);
//                                            if (addNewOffer) {
//                                                success = true;
//                                                message = "Offer has been submitted";
//                                            } else {
//                                                message = "Offer failed";
//                                            }
//                                        }
//                                    } else {
//                                        message = "Can't offer this product now";
//                                    }
//                                } else {
//                                    String offerId = TimeUUID.timeBased().toString();
//                                    long offeredTime = System.currentTimeMillis();
//                                    boolean addNewOffer = offerProduct.addNewOffer(offerId, buyerId, buyerName, productId, productById.getName(), productById.getImageUrl(), bidPrice, offeredTime, OfferStatus.PROCESSING);
//                                    if (addNewOffer) {
//                                        success = true;
//                                        message = "Offer has been submitted";
//                                    } else {
//                                        message = "Offer failed";
//                                    }
//                                }
//                            }
//                        } else {
//                            message = "Product is not available";
//                        }
//                    }
//                } else {
//                    message = "Please provide valid product Id";
//                }
//            } else {
//                message = "User not found";
//            }
//        }
//
//        responseJson.addProperty("sucs", success);
//        responseJson.addProperty("mg", message);
//        sender.sendToClient(responseJson);
//    }
    /**
     *
     * @param jsonObject
     * @param sender
     */
    @Request(action = Action.GET_PRODUCT_OFFER_DETAILS)
    public static void getOfferDetails(JsonObject jsonObject, ISender sender) {
        JsonObject responseJson = new JsonObject();
        boolean success = false;
        long buyerId;
        String productId;
        String message;

        JsonElement productIdElement = jsonObject.get("productId");
        JsonElement buyerIdElement = jsonObject.get("userId");
        if (!JsonValidator.hasValidString(productIdElement)) {
            message = "Please provide productId";
        } else if (!JsonValidator.hasPositiveLong(buyerIdElement)) {
            message = "Please provide buyerId";
        } else {
            productId = productIdElement.getAsString();
            buyerId = buyerIdElement.getAsLong();
            if (MarketRepository.INSTANCE.getMarketUserRepository().getUser(buyerId) != null) {
                SellerProductRepository sellerProduct = MarketRepository.INSTANCE.getSellerProductRepository();
                EntitySellerProduct product = sellerProduct.getProduct(productId);
                if (product != null) {
                    if (product.getSellerId() == buyerId) {
                        responseJson.addProperty("isOwner", true);
                    }
                    OfferProductRepository offerProduct = MarketRepository.INSTANCE.getOfferProductRepository();
                    EntityOfferProduct offerInfo = offerProduct.getOfferInfoOfProduct(buyerId, productId);
                    if (offerInfo != null) {
                        GsonBuilder builder = new GsonBuilder();
//                builder.registerTypeAdapter(IOfferProduct.class, new OfferProductAdapter());
                        JsonElement offerInfoJson = builder.create().toJsonTree(offerInfo, EntityOfferProduct.class);
                        success = true;
                        responseJson.add("offerInfo", offerInfoJson);
                        message = "Offer Info has been provided";
                    } else {
                        message = "Not offered yet for this product";
                    }
                } else {
                    message = "Product not found";
                }
            } else {
                message = "User not found";
            }
        }
        responseJson.addProperty("sucs", success);
        responseJson.addProperty("mg", message);
        responseJson.addProperty(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID).getAsString());

        sender.sendToClient(responseJson);
    }

    @Request(action = Action.CANCEL_OFFER)
    public static void cancelOffer(JsonObject request, ISender sender) {
        boolean success = false;
        String message;
        JsonElement offerIdElement = request.get("offerId");
        JsonElement userIdElement = request.get("userId");
        if (!JsonValidator.hasValidString(offerIdElement)) {
            message = "Please provide offerId";
        } else if (!JsonValidator.hasPositiveLong(userIdElement)) {
            message = "Please provide userId";
        } else {
            String offerId = offerIdElement.getAsString();
            long userId = userIdElement.getAsLong();
            if (MarketRepository.INSTANCE.getMarketUserRepository().getUser(userId) != null) {
                OfferProductRepository offerProduct = MarketRepository.INSTANCE.getOfferProductRepository();
                EntityOfferProduct offerInfo = offerProduct.getOfferInfo(offerId);
                if (offerInfo != null) {
                    if (offerInfo.getBuyerId() == userId && offerInfo.getStatus() != OfferStatus.ORDERED.ordinal()) {
                        boolean removeOffer = offerProduct.removeOffer(offerId);
                        if (removeOffer) {
                            success = true;
                            message = "Successfully cancled";
                        } else {
                            message = "Unsuccessful";
                        }
                    } else {
                        message = "Can't no cancel offer now";
                    }
                } else {
                    message = "Offer not found";
                }
            } else {
                message = "User not found";
            }
        }
        JsonObject response = new JsonObject();
        response.addProperty("sucs", success);
        response.addProperty("mg", message);
        response.addProperty(MarketAttribute.PACKET_ID, request.get(MarketAttribute.PACKET_ID).getAsString());

        sender.sendToClient(response);
    }

    @Request(action = Action.CANCEL_ORDER_BY_BUYER)
    public static void cancelOrder(JsonObject request, ISender sender) {
        boolean success = false;
        String message;
        JsonElement orderIdElement = request.get("orderId");
        JsonElement buyerIdElement = request.get("byrId");
        if (!JsonValidator.hasValidString(orderIdElement)) {
            message = "Please provide orderId";
        } else if (!JsonValidator.hasPositiveLong(buyerIdElement)) {
            message = "Please provide buyer Id";
        } else {
            String orderId = orderIdElement.getAsString();
            OrderProduct orderProduct = new OrderProduct();
            EntityOrderProduct orderInfo = orderProduct.getOrderInfo(orderId);
            if (orderInfo == null) {
                message = "Please provide valid order Id";
            } else if (orderInfo.getBuyerId() != buyerIdElement.getAsLong() && orderInfo.getStatus() != OrderStatus.PROCESSING.ordinal()) {
                message = "You can't cancel this order now";
            } else {
                boolean removeOrder = orderProduct.removeOrder(orderId);
                if (removeOrder) {
                    OfferProductRepository offerProduct = MarketRepository.INSTANCE.getOfferProductRepository();
                    success = offerProduct.updateOfferStatus(orderId, OfferStatus.CANCELLED);
                    if (success) {
                        message = "Successfully cacelled the order";
                    } else {
                        message = "Can't cancel order";
                    }
                } else {
                    message = "Can't cancel order";
                }
            }

        }
        JsonObject response = new JsonObject();
        response.addProperty("sucs", success);
        response.addProperty("mg", message);
        response.addProperty(MarketAttribute.PACKET_ID, request.get(MarketAttribute.PACKET_ID).getAsString());

        sender.sendToClient(response);
    }

    @Request(action = Action.GET_USER_CART)
    public static void getUserCart(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();
        Long buyerId;
        JsonElement element = requestObject.get(MarketAttribute.USER_ID);
        int reasonCode = 0;
        if (!JsonValidator.hasPositiveLong(element)) {
            message = "Please provide buyer Id";
            reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        } else {
            buyerId = element.getAsLong();
            String pivotId = null;
            element = requestObject.get(MarketAttribute.PIVOT_STRING);
            if (JsonValidator.hasValidString(element)) {
                pivotId = element.getAsString();
            }

            element = requestObject.get(MarketAttribute.SCROLL);
            int scrollValue = 2;
            if (JsonValidator.hasPositiveInteger(element)) {
                scrollValue = element.getAsInt();
            }
            Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;
            int limit = 10;
            element = requestObject.get(MarketAttribute.LIMIT);
            if (JsonValidator.hasPositiveInteger(element)) {
                limit = element.getAsInt();
            }

            PivotedListReturnDTO<EntityCartItem, String> cartItemDTOs = MarketRepository.INSTANCE.getCartRepository().getUserCartItems(buyerId, pivotId, scroll, limit);
            pivotId = cartItemDTOs.getPivot();
            if (cartItemDTOs.getReasonCode() == ReasonCode.NONE) {
                success = true;

                List<EntityCartItem> itemList = cartItemDTOs.getList();

                Map<String, JsonObject> cartProductJsonMap = new HashMap<>();

                JsonArray cartItemJsonArray = new JsonArray();
                for (EntityCartItem entityCartItem : itemList) {
                    JsonObject itemJson = gson.toJsonTree(entityCartItem).getAsJsonObject();
                    cartProductJsonMap.put(entityCartItem.getProductId(), itemJson);
                    cartItemJsonArray.add(itemJson);
                }

                ListReturnDTO<EntityProductStock> productStockByIds = MarketRepository.INSTANCE.getProductStockRepository().getProductStockByIds(cartProductJsonMap.keySet());
                if (productStockByIds.getReasonCode() == ReasonCode.NONE) {

                    for (EntityProductStock stock : productStockByIds.getList()) {
                        cartProductJsonMap.get(stock.getId()).addProperty(MarketAttribute.STOCK_QUANTITY, stock.getQuantity());
                    }
                }
//                response.add(MarketAttribute.ITEMS, gson.toJsonTree(cartItemDTOs.getList()));
                response.add(MarketAttribute.ITEMS, cartItemJsonArray);

            } else {
                reasonCode = cartItemDTOs.getReasonCode();
            }
            response.addProperty(MarketAttribute.PIVOT_STRING, pivotId);
        }
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.BUYER_ORDER_LIST)
    public static void getOrderList(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();
        Long buyerId;
        JsonElement element = requestObject.get(MarketAttribute.USER_ID);
        int reasonCode = 0;
        if (!JsonValidator.hasPositiveLong(element)) {
            message = "Please provide buyer Id";
            reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        } else {
            buyerId = element.getAsLong();
            String pivotId = null;
            Long pivotTime = 0l;
            element = requestObject.get(MarketAttribute.PIVOT_STRING);
            if (JsonValidator.hasValidString(element)) {
                pivotId = element.getAsString();
            }

            element = requestObject.get(MarketAttribute.PIVOT_NUMBER);
            if (JsonValidator.hasPositiveLong(element)) {
                pivotTime = element.getAsLong();
            }

            element = requestObject.get(MarketAttribute.SCROLL);
            int scrollValue = 2;
            if (JsonValidator.hasPositiveInteger(element)) {
                scrollValue = element.getAsInt();
            }

            Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;
            int limit = 10;
            element = requestObject.get(MarketAttribute.LIMIT);
            if (JsonValidator.hasPositiveInteger(element)) {
                limit = element.getAsInt();
            }

            Pair<String, Long> pivot = new Pair<>(pivotId, pivotTime);
            PivotedListReturnDTO<EntityOrder, Pair<String, Long>> buyerOrders = MarketRepository.INSTANCE.getOrderRepository().getBuyerOrders(buyerId, pivot, scroll, limit);
            if (buyerOrders.getReasonCode() == ReasonCode.NONE) {
                success = true;
                List<EntityOrder> orders = buyerOrders.getList();
                for (EntityOrder order : orders) {
                    order.setStatusMessage(MarketOrderStatus.getStatus(order.getStatus()).getMessage());

                    for (EntityOrderDetails detail : order.getDetails()) {
                        detail.setStatusMessage(MarketOrderStatus.getStatus(detail.getItemStatus()).getMessage("product"));
                    }

                }
                response.add(MarketAttribute.LIST, gson.toJsonTree(buyerOrders.getList()));
                response.addProperty(MarketAttribute.PIVOT_STRING, buyerOrders.getPivot().getKey());
                response.addProperty(MarketAttribute.PIVOT_NUMBER, buyerOrders.getPivot().getValue());
            } else {
                reasonCode = buyerOrders.getReasonCode();
                message = Messages.NO_MORE_DATA;
            }
        }
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.ADD_TO_CART)
    public static void addToCart(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        if (!JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide buyer Id";
        } else if (!JsonValidator.hasPositiveInteger(requestObject.get(MarketAttribute.QUANTITY))) {
            message = "Please provide quantity";
        } else {
            Long buyerId = requestObject.get(MarketAttribute.USER_ID).getAsLong();
            Integer quantity = requestObject.get(MarketAttribute.QUANTITY).getAsInt();

            long referrerId;
            if (JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.REFERRER_ID))) {
                referrerId = requestObject.get(MarketAttribute.REFERRER_ID).getAsLong();
            }

            if (JsonValidator.hasValidString(requestObject.get(MarketAttribute.ID))) {
                String cartItemId = requestObject.get(MarketAttribute.ID).getAsString();
                reasonCode = MarketRepository.INSTANCE.getCartRepository().updateCartItemQuantity(cartItemId, quantity);
                if (reasonCode == ReasonCode.NONE) {
                    message = "Cart has been updated successfully";
                    success = true;
                }
            } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.PRODUCT_ID))) {
                message = "Please provide product Id";
            } else {
                String productId = requestObject.get(MarketAttribute.PRODUCT_ID).getAsString();
                reasonCode = MarketRepository.INSTANCE.getCartRepository().addCartItem(productId, buyerId, quantity);
                if (reasonCode == ReasonCode.NONE) {
                    message = "Product is added to your cart";
                    success = true;
                } else {
                    message = "Unable to add to cart";
                }
            }
        }
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    /**
     *
     * @param requestObject
     * @param sender
     */
    @Request(action = Action.DELETE_CART_ITEMS)
    public static void deleteCartItems(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        if (!JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide buyer Id";
        } else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.IDS))) {
            message = "Please provide cart Ids";
        } else {
            Long buyerId = requestObject.get(MarketAttribute.USER_ID).getAsLong();

            Type listType = new TypeToken<List<String>>() {
            }.getType();

            List<String> cartItemIds = new Gson().fromJson(requestObject.get(MarketAttribute.IDS), listType);
            if (cartItemIds != null && !cartItemIds.isEmpty()) {
                success = MarketRepository.INSTANCE.getCartRepository().deleteCartItems(buyerId, cartItemIds);
                if (!success) {
                    message = "Unable to delete items";
                }
            }
        }
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    /**
     *
     * @param requestObject
     * @param sender
     */
    @Request(action = Action.BUY_FROM_CART)
    public static void buyFromCart(JsonObject requestObject, ISender sender) {
        boolean success = false;

        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        byte paymentMethod = PaymentMode.HAND_CASH;
        if (requestObject.has(MarketAttribute.PAYMENT_METHOD)) {
            paymentMethod = requestObject.get(MarketAttribute.PAYMENT_METHOD).getAsByte();
        }
        String message = "";
        if (paymentMethod != PaymentMode.HAND_CASH && paymentMethod != PaymentMode.SSL_WIRELESS_BD && paymentMethod != PaymentMode.CASH_WALLET) {
            message = "Please provide payment type";
        } else if (!JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide buyer Id";
        } else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.IDS))) {
            message = "Please provide cart Ids";
        } else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.SHIPMENT_METHOD))) {
            message = "Please provide shipment method";
        } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ADDRESS_ID))) {
            message = "Please provide address Id";
        } else {
            Long buyerId = requestObject.get(MarketAttribute.USER_ID).getAsLong();

            Type listType = new TypeToken<List<String>>() {
            }.getType();

            List<String> cartItemIds = new Gson().fromJson(requestObject.get(MarketAttribute.IDS), listType);
            if (cartItemIds != null && !cartItemIds.isEmpty()) {

                Shipment shipment = Shipment.getMethod(requestObject.get(MarketAttribute.SHIPMENT_METHOD).getAsByte());
//                if (shipment == Shipment.HOME_DELIVERY && !JsonValidator.hasValidString(requestObject.get(MarketAttribute.ADDRESS_ID))) {
//                    message = "Please provide address Id";
//                } else {
                String addressId = requestObject.get(MarketAttribute.ADDRESS_ID).getAsString();
                ObjectReturnDTO<String> orderProductsFromCart = MarketRepository.INSTANCE.getCartRepository().orderProductsFromCart(buyerId, buyerId, cartItemIds, shipment, addressId, paymentMethod);
                reasonCode = orderProductsFromCart.getReasonCode();
                if (reasonCode == ReasonCode.NONE) {
                    success = true;
                    response.addProperty(MarketAttribute.INVOICE_ID, orderProductsFromCart.getValue());
                }
//                }
            }
        }

        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);

        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.BUY_PRODUCT)
    public static void buyProduct(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = "";
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        byte paymentMethod = PaymentMode.HAND_CASH;
        if (requestObject.has(MarketAttribute.PAYMENT_METHOD)) {
            paymentMethod = requestObject.get(MarketAttribute.PAYMENT_METHOD).getAsByte();
        }
        if (paymentMethod != PaymentMode.HAND_CASH && paymentMethod != PaymentMode.SSL_WIRELESS_BD && paymentMethod != PaymentMode.CASH_WALLET) {
            message = "Please provide payment type";
        } else if (!JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide buyer Id";
        } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.PRODUCT_ID))) {
            message = "Please provide product id";
        } else if (!JsonValidator.hasPositiveInteger(requestObject.get(MarketAttribute.QUANTITY))) {
            message = "Please provide quantity";
        } else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.SHIPMENT_METHOD))) {
            message = "Please provide shipment method";
        } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ADDRESS_ID))) {
            message = "Please provide address Id";
        } else {
            Long buyerId = requestObject.get(MarketAttribute.USER_ID).getAsLong();
            String productId = requestObject.get(MarketAttribute.PRODUCT_ID).getAsString();
            Integer quantity = requestObject.get(MarketAttribute.QUANTITY).getAsInt();
            Shipment shipment = Shipment.getMethod(requestObject.get(MarketAttribute.SHIPMENT_METHOD).getAsByte());
//            if (shipment == Shipment.HOME_DELIVERY && !JsonValidator.hasValidString(requestObject.get(MarketAttribute.ADDRESS_ID))) {
//                message = "Please provide address Id";
//            } else {
            String addressId = requestObject.get(MarketAttribute.ADDRESS_ID).getAsString();

            long referrerId;
            if (JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.REFERRER_ID))) {
                referrerId = requestObject.get(MarketAttribute.REFERRER_ID).getAsLong();
            }

            EntityProduct productById = MarketRepository.INSTANCE.getProductRepository().getProductById(productId);
            if (productById != null) {
                ObjectReturnDTO<String> placeOrder = MarketRepository.INSTANCE.getOrderRepository().placeOrder(buyerId, buyerId, quantity, productById, shipment, addressId, paymentMethod);
                reasonCode = placeOrder.getReasonCode();
                if (reasonCode == ReasonCode.NONE) {
                    success = true;
                    response.addProperty(MarketAttribute.INVOICE_ID, placeOrder.getValue());
                }
            } else {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            }
//            }
        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.BUY_FROM_STORE)
    public static void buyFromStore(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = "";
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        byte paymentMethod = PaymentMode.HAND_CASH;
        if (requestObject.has(MarketAttribute.PAYMENT_METHOD)) {
            paymentMethod = requestObject.get(MarketAttribute.PAYMENT_METHOD).getAsByte();
        }
        if (paymentMethod != PaymentMode.HAND_CASH && paymentMethod != PaymentMode.SSL_WIRELESS_BD && paymentMethod != PaymentMode.CASH_WALLET) {
            message = "Please provide payment type";
        } else if (!JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.BUYER_ID))) {
            message = "Please provide buyer Id";
        } else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.ITEMS))) {
            message = "Please provide items";
        } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ADDRESS_ID))) {
            message = "Please provide address Id";
        } else {
            Long buyerId = requestObject.get(MarketAttribute.BUYER_ID).getAsLong();
            Long userId = requestObject.get(MarketAttribute.USER_ID).getAsLong();

            Type listType = new TypeToken<List<EntityCartItem>>() {
            }.getType();

            List<EntityCartItem> orders = gson.fromJson(requestObject.get(MarketAttribute.ITEMS), listType);
            String addressId = requestObject.get(MarketAttribute.ADDRESS_ID).getAsString();

            Shipment shipment = Shipment.PICKUP_FROM_STORE;

            if (orders != null && !orders.isEmpty()) {
                ObjectReturnDTO<String> placeOrder = MarketRepository.INSTANCE.getOrderRepository().orderItemsAndGetInvoiceId(userId, buyerId, orders, shipment, addressId, paymentMethod);
                reasonCode = placeOrder.getReasonCode();
                if (reasonCode == ReasonCode.NONE) {
                    success = true;
                    response.addProperty(MarketAttribute.INVOICE_ID, placeOrder.getValue());
                    message = "User successfully buy from store";
                }
            } else {
                reasonCode = ReasonCode.INVALID_INFORMATION;
                message = "Invalid information";
            }
//            }
        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.ADD_SHIPPING_ADDRESS)
    public static void addShippingAddress(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide buyer Id";
        } else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.ADDRESS))) {
            message = "Please provide address";
        } else {
            Long buyerId = requestObject.get(MarketAttribute.USER_ID).getAsLong();
            Address address = gson.fromJson(requestObject.get(MarketAttribute.ADDRESS), Address.class);

            reasonCode = ReasonCode.NONE;
            if (address.getEmail() != null && address.getEmail().isEmpty() && !EmailValidator.validateEmail(address.getEmail())) {
                reasonCode = ReasonCode.INVALID_EMAIL;
            }

            if (address.getDialingCode() == null || address.getDialingCode().isEmpty() || !address.getDialingCode().matches("^\\+[0-9-]+")) {
                reasonCode = ReasonCode.DIALING_CODE_MENDATORY;
            }

            if (address.getMobile() == null || address.getMobile().isEmpty() || address.getMobile().length() < 8 || !address.getMobile().matches("^[0-9-]+")) {
                reasonCode = ReasonCode.INVALID_MOBILE;
            }

            if (address.getCity() == null || address.getCity().isEmpty()) {
                reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
            }

            if (address.getContactName() == null || address.getContactName().isEmpty()) {
                reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
            }
            if (address.getCountry() == null || address.getCountry().isEmpty()) {
                reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
            }

            if (reasonCode == ReasonCode.NONE) {
                boolean isDefault = false;
                if (requestObject.has(MarketAttribute.IS_DEFAULT)) {
                    isDefault = requestObject.get(MarketAttribute.IS_DEFAULT).getAsBoolean();
                }
                byte type = 0;
                if (requestObject.has(MarketAttribute.TYPE)) {
                    type = requestObject.get(MarketAttribute.TYPE).getAsByte();
                }
                ObjectReturnDTO<String> addShippingAddress = MarketRepository.INSTANCE.getShippingAddressRepository().addShippingAddress(buyerId, address, isDefault, type);
                reasonCode = addShippingAddress.getReasonCode();
                if (reasonCode == ReasonCode.NONE) {
                    success = true;
                    response.addProperty(MarketAttribute.ID, addShippingAddress.getValue());
                    message = "Shipping address added successfully.";
                } else {
                    message = "Adding shipping address failed. Please try again!";
                }
            }

        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        if (message != null) {
            response.addProperty(MarketAttribute.MESSAGE, message);
        }
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.GET_USER_SHIPPING_ADDRESSES)
    public static void getShippingAddress(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide buyer Id";
        } else {
            Long buyerId = requestObject.get(MarketAttribute.USER_ID).getAsLong();
            ListReturnDTO<EntityShippingAddress> shippingAddresses = MarketRepository.INSTANCE.getShippingAddressRepository().getShippingAddresses(buyerId);
            reasonCode = shippingAddresses.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                response.add(MarketAttribute.LIST, gson.toJsonTree(shippingAddresses.getList()));
            }
        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.GET_NEAREST_RING_STORE)
    public static void getNearestRingStores(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasPositiveLong(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide buyer Id";
        } //        else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.LAT)) || !JsonValidator.hasElement(requestObject.get(MarketAttribute.LON))) {
        //            message = "Please provide lat / lon";
        //        } 
        else {
//            Float lat = requestObject.get(MarketAttribute.LAT).getAsFloat();
//            Float lon = requestObject.get(MarketAttribute.LON).getAsFloat();
            Float lat = 0f;
            Float lon = 0f;

            ListReturnDTO<EntityShippingAddress> shippingAddresses = MarketRepository.INSTANCE.getShippingAddressRepository().getNearestRingStores(lat, lon, (byte) 1);
            reasonCode = shippingAddresses.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                response.add(MarketAttribute.LIST, gson.toJsonTree(shippingAddresses.getList()));
            }
        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.UPDATE_SHIPPING_ADDRESS)
    public static void updateShippingAddress(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ID))) {
            message = "Please provide address Id";
        } else if (!JsonValidator.hasElement(requestObject.get(MarketAttribute.ADDRESS))) {
            message = "Please provide address";
        } else {
            String addressId = requestObject.get(MarketAttribute.ID).getAsString();
            Address address = gson.fromJson(requestObject.get(MarketAttribute.ADDRESS), Address.class);

            reasonCode = ReasonCode.NONE;
            if (address.getEmail() != null && address.getEmail().isEmpty() && !EmailValidator.validateEmail(address.getEmail())) {
                reasonCode = ReasonCode.INVALID_EMAIL;
            }

            if (address.getDialingCode() == null || address.getDialingCode().isEmpty() || !address.getDialingCode().matches("^\\+[0-9-]+")) {
                reasonCode = ReasonCode.DIALING_CODE_MENDATORY;
            }

            if (address.getMobile() == null || address.getMobile().isEmpty() || address.getMobile().length() < 8 || !address.getMobile().matches("^[0-9-]+")) {
                reasonCode = ReasonCode.INVALID_MOBILE;
            }

            if (address.getCity() == null || address.getCity().isEmpty()) {
                reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
            }

            if (address.getContactName() == null || address.getContactName().isEmpty()) {
                reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
            }
            if (address.getCountry() == null || address.getCountry().isEmpty()) {
                reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
            }

            if (reasonCode == ReasonCode.NONE) {
                reasonCode = MarketRepository.INSTANCE.getShippingAddressRepository().updateShippingAddress(addressId, address);
                if (reasonCode == ReasonCode.NONE) {
                    success = true;
                }
            }
        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        if (message != null) {
            response.addProperty(MarketAttribute.MESSAGE, message);
        }
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.SET_DEFAULT_ADDRESS)
    public static void setDefaultAddress(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ID))) {
            message = "Please provide address Id";
        } else {
            String addressId = requestObject.get(MarketAttribute.ID).getAsString();
            reasonCode = MarketRepository.INSTANCE.getShippingAddressRepository().setDefault(addressId);
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                message = "Default address successfully seted!";
            }
        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.REMOVE_SHIPPING_ADDRESS)
    public static void removeShippingAddress(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ID))) {
            message = "Please provide address Id";
        } else {
            String addressId = requestObject.get(MarketAttribute.ID).getAsString();
            reasonCode = MarketRepository.INSTANCE.getShippingAddressRepository().removeShippingAddress(addressId);
            if (reasonCode == ReasonCode.NONE) {
                success = true;
            } else if (reasonCode == ReasonCode.PERMISSION_DENIED) {
                message = "Can't remove default address";
            }

        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.GET_USER_CART_ITEMS_COUNT)
    public static void getUserCartItemsCount(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide user Id";
        } else {
            Long userId = requestObject.get(MarketAttribute.USER_ID).getAsLong();
            ObjectReturnDTO<Long> cartItemsCount = MarketRepository.INSTANCE.getCartRepository().cartItemsCount(userId);
            reasonCode = cartItemsCount.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                response.addProperty(MarketAttribute.COUNT, cartItemsCount.getValue());
            }

        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        if (message != null) {
            response.addProperty(MarketAttribute.MESSAGE, message);
        }
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.SHIPMENT_RECEIVED)
    public static void shipmentReceived(JsonObject requestObject, ISender sender) {
        UserHandler.orderItemStatusUpdate(requestObject, sender, MarketOrderStatus.SHIPMENT_RECEIVED.getValue());
    }

    @Request(action = Action.CANCEL_ORDER)
    public static void orderItemsShipmentComplete(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;

        if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ORDER_ID))) {
            message = "Please provide order Id";
        } else {
            String orderId = requestObject.get(MarketAttribute.ORDER_ID).getAsString();
            Long userId = requestObject.get(MarketAttribute.USER_ID).getAsLong();

            reasonCode = MarketRepository.INSTANCE.getOrderRepository().updateOrderStatus(userId, orderId, MarketOrderStatus.CANCELLED.getValue());
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                message = "You have cancelled your order.";
            } else {
                message = Messages.OPERATION_FAILED;
            }
        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        if (message != null) {
            response.addProperty(MarketAttribute.MESSAGE, message);
        }
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(response);
    }

    @Request(action = Action.GET_NEW_INVOICE)
    public static void getNewInvoice(JsonObject requestObject, ISender sender) {
        boolean success = false;
        String message = null;
        JsonObject response = new JsonObject();

        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        response.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        response.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));

        if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.USER_ID))) {
            message = "Please provide user Id";
        } else if (!JsonValidator.hasValidString(requestObject.get(MarketAttribute.ORDER_ID))) {
            message = "Please provide order Id";
        } else {
            String orderId = requestObject.get(MarketAttribute.ORDER_ID).getAsString();
            Long userId = requestObject.get(MarketAttribute.USER_ID).getAsLong();

            ObjectReturnDTO<String> updateAndGetInvoiceId = MarketRepository.INSTANCE.getOrderRepository().updateAndGetInvoiceId(orderId, userId);
            reasonCode = updateAndGetInvoiceId.getReasonCode();

            if (reasonCode == ReasonCode.NONE) {
                success = true;
                response.addProperty(MarketAttribute.INVOICE_ID, updateAndGetInvoiceId.getValue());
            } else {
                message = Messages.OPERATION_FAILED;
            }
        }
        response.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        response.addProperty(MarketAttribute.SUCCESS, success);
        response.addProperty(MarketAttribute.MESSAGE, message);

        sender.sendToClient(response);
    }
}
