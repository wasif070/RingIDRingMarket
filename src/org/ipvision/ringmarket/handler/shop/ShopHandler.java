/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.handler.shop;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.Messages;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.controller.annotation.Request;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.repositories.shop.ShopRepository;
import org.ipvision.ringmarket.sender.ISender;
import org.ipvision.ringmarket.utils.JsonValidator;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class ShopHandler {

    private static final Gson gson = new Gson();
    private static final Logger LOGGER = LoggerFactory.getLogger(ShopHandler.class);

    @Request(action = Action.GET_STORE_DETAILS)
    public static void getStoreDetails(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();

        JsonElement element = jsonObject.get(MarketAttribute.SHOP_ID);
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        if (!JsonValidator.hasPositiveLong(element)) {
            responseObject.addProperty(MarketAttribute.SUCCESS, Boolean.FALSE);
            responseObject.addProperty(MarketAttribute.MESSAGE, "Please provide shop ID");
        } else {
            Long shopId = element.getAsLong();

            ShopRepository shopRepository = MarketRepository.INSTANCE.getShopRepository();
            EntityShop shopById = shopRepository.getShopById(shopId);

            if (shopById != null) {
//                if (shopById.getItemCount() > 0) {
//                    ListReturnDTO<EntityProduct> productsByShopId = MarketRepository.INSTANCE.getProductRepository().getProductsByShopId(null, shopId, Scroll.DOWN, 10, ProductStatus.AVAILABLE);
//                    shopById.setProducts(productsByShopId.getList());
//                }
                responseObject = (JsonObject) gson.toJsonTree(shopById);
                responseObject.addProperty(MarketAttribute.SUCCESS, Boolean.TRUE);
                responseObject.add(MarketAttribute.USER_ID, jsonObject.get(MarketAttribute.USER_ID));
            } else {
                responseObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_DATA_FOUND);
                responseObject.addProperty(MarketAttribute.REASON_CODE, ReasonCode.NO_DATA_FOUND);
                responseObject.addProperty(MarketAttribute.SUCCESS, Boolean.FALSE);
            }

//            ShopDetailsDTO shopDetailsDTO = new ShopDetailsDTO();
//            shopDetailsDTO.setName("Sample Store");
//            shopDetailsDTO.setImage("cloud/uploadedProfile-149/2110100310/6361881517654603524.jpg");
//            shopDetailsDTO.setRatings(3.2);
//            shopDetailsDTO.setItemCount(100);
//
//            List<ProductDTO> products = new LinkedList<>();
//
//            for (int i = 0; i < 10; i++) {
//                ProductDTO detailsDTO = new ProductDTO();
//                detailsDTO.setName("Store Product_" + i);
//                detailsDTO.setOldPrice(100d);
//                detailsDTO.setNewPrice(80d);
//                detailsDTO.setRatings(3.2);
//                detailsDTO.setCurrencyCode("$");
//                detailsDTO.setDiscount(20d);
//                detailsDTO.setDefaultImageUrl("cloud/uploadedProfile-149/2110100310/6361881517654603524.jpg");
//                products.add(detailsDTO);
//            }
//            shopDetailsDTO.setProducts(products);
//            responseObject = (JsonObject) gson.toJsonTree(shopDetailsDTO);
//            responseObject.addProperty(MarketAttribute.SUCCESS, Boolean.TRUE);
        }
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.GET_STORE_INFO)
    public static void storeInfo(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();

        JsonElement element = jsonObject.get(MarketAttribute.SHOP_ID);
        if (!JsonValidator.hasPositiveLong(element)) {
            responseObject.addProperty(MarketAttribute.SUCCESS, Boolean.FALSE);
            responseObject.addProperty(MarketAttribute.MESSAGE, "Please provide shop ID");
        } else {
            Long shopId = element.getAsLong();

            ShopRepository shopRepository = MarketRepository.INSTANCE.getShopRepository();
            EntityShop shopById = shopRepository.getShopById(shopId);

            if (shopById != null) {
                responseObject = (JsonObject) gson.toJsonTree(shopById);
                responseObject.addProperty(MarketAttribute.SUCCESS, Boolean.TRUE);
                responseObject.add(MarketAttribute.USER_ID, jsonObject.get(MarketAttribute.USER_ID));
            } else {
                responseObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_DATA_FOUND);
                responseObject.addProperty(MarketAttribute.REASON_CODE, ReasonCode.NO_DATA_FOUND);
                responseObject.addProperty(MarketAttribute.SUCCESS, Boolean.FALSE);
            }
        }
        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.UPDATE_SHOP_NAME)
    public static void updateShopName(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();

        JsonElement shopIdElement = jsonObject.get(MarketAttribute.ID);
        JsonElement shopNameElement = jsonObject.get(MarketAttribute.NAME);
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        String message = null;
        boolean success = false;
        if (!JsonValidator.hasPositiveLong(shopIdElement)) {
            message = "Please provide shop ID";
        } else if (!JsonValidator.hasValidString(shopNameElement)) {
            message = "Please provide shop name";
        } else {
            Long shopId = shopIdElement.getAsLong();
            String shopName = shopNameElement.getAsString();
            reasonCode = MarketRepository.INSTANCE.getShopRepository().updateShopName(shopId, shopName);

            if (reasonCode == ReasonCode.NONE) {
                success = true;
            } else {
                message = Messages.UPDATE_FAILED;
            }
        }
        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);

        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

    @Request(action = Action.UPDATE_SHOP_IMAGE)
    public static void updateShopImage(JsonObject jsonObject, ISender sender) {
        JsonObject responseObject = new JsonObject();

        JsonElement shopIdElement = jsonObject.get(MarketAttribute.ID);
        JsonElement imageUrlElement = jsonObject.get(MarketAttribute.IMAGE_URL);
        JsonElement heightElement = jsonObject.get(MarketAttribute.IMAGE_HEIGHT);
        JsonElement widthElement = jsonObject.get(MarketAttribute.IMAGE_WIDTH);
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        String message = null;
        boolean success = false;
        if (!JsonValidator.hasPositiveLong(shopIdElement)) {
            message = "Please provide shop ID";
        } else if (!JsonValidator.hasPositiveInteger(heightElement) || !JsonValidator.hasPositiveInteger(widthElement)) {
            message = "Please provide height/width";
            reasonCode = ReasonCode.INVALID_IMAGE_PROPERTY;
        } else if (!JsonValidator.hasValidString(imageUrlElement)) {
            message = "Please provide url";
            reasonCode = ReasonCode.INVALID_IMAGE_PROPERTY;
        } else {
            Long shopId = shopIdElement.getAsLong();
            String imageUrl = imageUrlElement.getAsString();
            int height = heightElement.getAsInt();
            int width = widthElement.getAsInt();
            reasonCode = MarketRepository.INSTANCE.getShopRepository().updateImage(shopId, imageUrl, height, width);

            if (reasonCode == ReasonCode.NONE) {
                success = true;
            } else {
                message = Messages.UPDATE_FAILED;
            }
        }
        responseObject.addProperty(MarketAttribute.SUCCESS, success);
        responseObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        responseObject.addProperty(MarketAttribute.MESSAGE, message);

        responseObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseObject);
    }

}
