/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.handler.category;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.ipvision.ringmarket.constants.FeaturePage;
import org.ipvision.ringmarket.constants.SortOrderType;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.Messages;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.controller.annotation.Request;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.menu.EntityFeaturePage;
import org.ipvision.ringmarket.entities.menu.EntityTopMenu;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.entities.user.basket.EntityUserBasket;
import org.ipvision.ringmarket.repositories.shop.ShopRepository;
import org.ipvision.ringmarket.sender.ISender;
import org.ipvision.ringmarket.utils.JsonValidator;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class MenuHandler {

    private static final Gson gson = new Gson();
    private static final Logger LOGGER = LoggerFactory.getLogger(MenuHandler.class);

    @Request(action = Action.GET_TOP_NAV_ITEMS)
    public static void getTopNavigationItems(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        boolean success = false;

        List<EntityTopMenu> items = MarketRepository.INSTANCE.getTopMenuRepository().getTopMenus();
        if (items != null && !items.isEmpty()) {
            success = true;
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items));
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_DATA_FOUND);
        }
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_FEATURE_PAGES)
    public static void getFeaturePages(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        boolean success = true;

        FeaturePage[] values = FeaturePage.values();
        List<EntityFeaturePage> featurePages = MarketRepository.INSTANCE.getFeaturePagesRepository().getFeaturePages();

        ShopRepository shopRepository = MarketRepository.INSTANCE.getShopRepository();
        int limit = 5;

        for (EntityFeaturePage item : featurePages) {

            switch (FeaturePage.getPage(item.getId())) {
                case BASKET: {
                    List<EntityUserBasket> items = MarketRepository.INSTANCE.getBasketRepository().getCelebrityBaskets(null, limit, Scroll.DOWN).getList();
                    item.setItems(items);
//                    item.setType(PageType.BASKET);
                    break;
                }
                case STORE_LIVE: {
                    item.setItems(shopRepository.getBrands(0, limit, Scroll.DOWN, true).getList());
//                    item.setType(PageType.SHOP);
                    break;

                }
                case STORES: {
                    item.setItems(shopRepository.getStores(0, limit, Scroll.DOWN, true).getList());
//                    item.setType(PageType.SHOP);
                    break;

                }
                case CELEBRITY_STORES: {
//                    item.setItems(MarketRepository.INSTANCE.getCelebrityStoreRepository().getCelebrityStore(0, limit, Scroll.DOWN).getList());
                    item.setItems(MarketRepository.INSTANCE.getShopRepository().getCelebrityStore(0, limit, Scroll.DOWN).getList());
//                    item.setType(PageType.SHOP);
                    break;

                }
                case HOT_DEALS: {

                    ListReturnDTO<EntityProduct> products = MarketRepository.INSTANCE.getProductRepository().getDiscountProducts(null, Scroll.DOWN, limit);

//                    for (EntityProduct exclusiveProduct : products.getList()) {
//                        exclusiveProduct.setOldPrice(exclusiveProduct.getPrice() + exclusiveProduct.getPrice() * .2);
//                        exclusiveProduct.setDiscount(20f);
//                    }
                    item.setItems(products.getList());
//                    item.setType(PageType.PRODUCT);
                    break;
                }

            }

//            featurePages.add(item);
        }

        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        responseJsonObject.add(MarketAttribute.FEATURE_PAGES, gson.toJsonTree(featurePages));
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_BRANDS)
    public static void getBrands(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();

        long shopId = 0;
        element = requestObject.get(MarketAttribute.SHOP_ID);
        if (JsonValidator.hasPositiveLong(element)) {
            shopId = element.getAsLong();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        ListReturnDTO<EntityShop> items = MarketRepository.INSTANCE.getShopRepository().getBrands(shopId, limit, scroll, true);

        if (items.getReasonCode() == ReasonCode.NONE) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items.getList()));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_BRAND);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, items.getReasonCode());
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_CELEBRITY_STORES)
    public static void getCelebrityStores(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();

        long shopId = 0;
        element = requestObject.get(MarketAttribute.SHOP_ID);
        if (JsonValidator.hasPositiveLong(element)) {
            shopId = element.getAsLong();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

//        ListReturnDTO<EntityShop> items = MarketRepository.INSTANCE.getCelebrityStoreRepository().getCelebrityStore(shopId, limit, scroll);
        ListReturnDTO<EntityShop> items = MarketRepository.INSTANCE.getShopRepository().getCelebrityStore(shopId, limit, scroll);

        if (items.getReasonCode() == ReasonCode.NONE) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items.getList()));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_STORE);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, items.getReasonCode());
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        sender.sendToClient(responseJsonObject);
    }
    
    @Request(action = Action.GET_SHOP_BY_NAME)
    public static void getStoreByName(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();

        long shopId = 0;
        element = requestObject.get(MarketAttribute.SHOP_ID);
        if (JsonValidator.hasPositiveLong(element)) {
            shopId = element.getAsLong();
        }
        String name = null;
        element = requestObject.get(MarketAttribute.NAME);
        if (JsonValidator.hasValidString(element)) {
            name = element.getAsString();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }
        
        ListReturnDTO<EntityShop> items = MarketRepository.INSTANCE.getShopRepository().getShopsByName( shopId, name, limit, scroll);

        if (items.getReasonCode() == ReasonCode.NONE) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items.getList()));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_STORE);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, items.getReasonCode());
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_NEW_BRANDS)
    public static void getNewBrands(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();

        long shopId = 0;
        element = requestObject.get(MarketAttribute.SHOP_ID);
        if (JsonValidator.hasPositiveLong(element)) {
            shopId = element.getAsLong();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        ListReturnDTO<EntityShop> items = MarketRepository.INSTANCE.getShopRepository().getBrands(shopId, limit, scroll, true);

        if (items.getReasonCode() == ReasonCode.NONE) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items.getList()));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_BRAND);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, items.getReasonCode());

        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_STORES)
    public static void getStores(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();

        long shopId = 0;
        element = requestObject.get(MarketAttribute.SHOP_ID);
        if (JsonValidator.hasPositiveLong(element)) {
            shopId = element.getAsLong();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        ListReturnDTO<EntityShop> items = MarketRepository.INSTANCE.getShopRepository().getStores(shopId, limit, scroll, true);

        if (items.getReasonCode() == ReasonCode.NONE) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items.getList()));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_STORE);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, items.getReasonCode());

        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_TRENDS)
    public static void getTrends(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();

        element = requestObject.get(MarketAttribute.PRODUCT_ID);
        String productPivotId = null;
        if (JsonValidator.hasValidString(element)) {
            productPivotId = element.getAsString();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        ListReturnDTO<EntityProduct> listReturnDTO = MarketRepository.INSTANCE.getProductRepository()
                .getTrendingProducts(productPivotId, scroll, limit);

        List<EntityProduct> items = listReturnDTO.getList();
        if (items != null & !items.isEmpty()) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_ITEMS);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, listReturnDTO.getReasonCode());
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_EXCLUSIVES)
    public static void getExclusives(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();

        element = requestObject.get(MarketAttribute.PRODUCT_ID);
        String productPivotId = null;
        if (JsonValidator.hasValidString(element)) {
            productPivotId = element.getAsString();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        ListReturnDTO<EntityProduct> listReturnDTO = MarketRepository.INSTANCE.getProductRepository()
                .getExclusiveProducts(productPivotId, scroll, limit);
        List<EntityProduct> items = listReturnDTO.getList();
        if (items != null & !items.isEmpty()) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(items));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_ITEMS);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, listReturnDTO.getReasonCode());
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_RECOMMENED_SALES)
    public static void getSales(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        boolean success = false;
        JsonObject responseJsonObject = new JsonObject();

        element = requestObject.get(MarketAttribute.PRODUCT_ID);
        String productPivotId = null;
        if (JsonValidator.hasValidString(element)) {
            productPivotId = element.getAsString();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        ListReturnDTO<EntityProduct> listReturnDTO = MarketRepository.INSTANCE.getProductRepository()
                .getDiscountProducts(productPivotId, scroll, limit);
        if (listReturnDTO.getReasonCode() == ReasonCode.NONE) {

//            for (EntityProduct exclusiveProduct : listReturnDTO.getList()) {
//                exclusiveProduct.setOldPrice(exclusiveProduct.getPrice() + exclusiveProduct.getPrice() * .2);
//                exclusiveProduct.setDiscount(20f);
//            }
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(listReturnDTO.getList()));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_ITEMS);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, listReturnDTO.getReasonCode());
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_CELEBRITY_BASKET_LIST)
    public static void getCelebrityBaskets(JsonObject requestObject, ISender sender) {
        int scrollValue = 2;
        int limit = 10;
        JsonElement element;
        String message = null;
        boolean success;
        JsonObject responseJsonObject = new JsonObject();
        String basketID = null;

        element = requestObject.get(MarketAttribute.BASKET_ID);
        if (JsonValidator.hasValidString(element)) {
            basketID = element.getAsString();
        }

        element = requestObject.get(MarketAttribute.USER_ID);
        Long userId = null;
        if (JsonValidator.hasPositiveLong(element)) {
            userId = element.getAsLong();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

//        List<Object> items = new LinkedList<>();;
//
//        for (int i = 0; i < limit; i++) {
//            BasketDTO basketDTO = new BasketDTO();
//            basketDTO.setUserId(100l + i);
//            basketDTO.setId(TimeUUID.timeBased());
//            basketDTO.setName("basket " + i);
//            basketDTO.setImage("cloud/uploadedProfile-149/2110100310/6361881517654603524.jpg");
//            items.add(basketDTO);
//        }
        ListReturnDTO<EntityUserBasket> basketList = MarketRepository.INSTANCE.getBasketRepository().getCelebrityBaskets(basketID, limit, scroll);
        if (basketList.getReasonCode() == ReasonCode.NONE) {
            responseJsonObject.add(MarketAttribute.ITEMS, gson.toJsonTree(basketList.getList()));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_DATA);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, basketList.getReasonCode());
            success = false;
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_SEARCH_ORDER_MAP)
    public static void getFilterList(JsonObject requestObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();

        SortOrderType[] filters = SortOrderType.values();
        Map<Integer, String> filterMap = new HashMap<>();
        for (SortOrderType filter : filters) {
            filterMap.put(filter.getId(), filter.getName());
        }

        responseJsonObject.addProperty(MarketAttribute.SUCCESS, true);
        responseJsonObject.add(MarketAttribute.ORDER_MAP, gson.toJsonTree(filterMap));
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

}
