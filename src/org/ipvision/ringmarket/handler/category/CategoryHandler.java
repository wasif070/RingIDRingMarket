/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.handler.category;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import org.ipvision.ringmarket.cache.ProductCategoryCache;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.Messages;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.controller.annotation.Request;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.repositories.category.CategoryRepository;
import org.ipvision.ringmarket.sender.ISender;
import org.ipvision.ringmarket.utils.JsonValidator;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class CategoryHandler {

    private static Gson gson = new Gson();
    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryHandler.class);

//     @Request(action = Action.GET_CATEGORIES)
//    public static void selectCategories(JsonObject jsonObject, ISender sender) {
//        int categoryPivotId = 0;
//        int scrollValue = 0;
//        int limit = 20;
//        JsonElement element;
//        element = jsonObject.get("catId");
//        if (JsonValidator.hasPositiveInteger(element)) {
//            categoryPivotId = element.getAsInt();
//        }
//        element = jsonObject.get("scrl");
//        if (JsonValidator.hasElement(element)) {
//            scrollValue = element.getAsInt();
//        }
//        Scroll scroll = (scrollValue > 0) ? Scroll.UP : Scroll.DOWN;
//        element = jsonObject.get("lmt");
//        if (JsonValidator.hasPositiveInteger(element)) {
//            limit = element.getAsInt();
//        }
//
//        CategoryRepository categoryHierarchy = MarketRepository.INSTANCE.getProductCategoryHierarchyRepository();
//        List<EntityProductCategoryHieararchy> rootCategories = categoryHierarchy.getCategoriesByScroll(0, categoryPivotId, limit, scroll);
//        GsonBuilder builder = new GsonBuilder();
////        builder.registerTypeAdapter(EntityCategory.class, new ProductCategoryHierarchyAdapter());
//        Gson gson = builder.create();
//        JsonObject responseJsonObject = new JsonObject();
//
//        if (rootCategories != null && !rootCategories.isEmpty()) {
//
//            JsonArray rootCategoryJsonArray = new JsonArray();
//            JsonArray subCategoryJsonArray;
//
//            for (EntityCategory rootCategory : rootCategories) {
//                List<EntityProductCategoryHieararchy> subCategories = categoryHierarchy.getCategoriesByScroll(rootCategory.getId(), 0, limit, Scroll.DOWN);
//                subCategoryJsonArray = new JsonArray();
//
//                for (EntityCategory subCategory : subCategories) {
//                    JsonElement subCategoryJson = gson.toJsonTree(subCategory, EntityCategory.class);
//                    subCategoryJsonArray.add(subCategoryJson);
//                }
//
//                JsonElement rootCategoryJson = gson.toJsonTree(rootCategory, EntityCategory.class);
//                if (subCategoryJsonArray.size() != 0) {
//                    rootCategoryJson.getAsJsonObject().add("subCatList", subCategoryJsonArray);
//                }
//                rootCategoryJsonArray.add(rootCategoryJson);
//            }
//
//            responseJsonObject.addProperty("sucs", Boolean.TRUE);
//            responseJsonObject.add("catList", rootCategoryJsonArray);
//
//        } else {
//            responseJsonObject.addProperty("sucs", Boolean.FALSE);
//            responseJsonObject.addProperty("mg", "No Category Found");
//        }
//
//        sender.sendToClient(responseJsonObject);
//    }
    @Request(action = Action.SELECT_ALL_CATEGORIES)
    public static void selectAllCategories(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();

        List<Integer> rootCategoryIds = ProductCategoryCache.getInstance().getRootCategoryIds();

        if (!rootCategoryIds.isEmpty()) {
            JsonArray rootCategoryJsonArray = new JsonArray();

            rootCategoryIds.stream().map((rootCategoryId) -> ProductCategoryCache.getInstance().getCategoryInfo(rootCategoryId)).filter((categoryInfo) -> (categoryInfo != null)).forEachOrdered((categoryInfo) -> {
                rootCategoryJsonArray.add(gson.toJsonTree(categoryInfo));
            });
            responseJsonObject.addProperty("sucs", Boolean.TRUE);
            responseJsonObject.add("catList", rootCategoryJsonArray);
        } else {
            responseJsonObject.addProperty("sucs", Boolean.FALSE);
            responseJsonObject.addProperty("mg", "No Category Found");
        }
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.SELECT_SUB_CATEGORIES)
    public static void selectSubCategories(JsonObject jsonObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        int categoryId;
        int subCategoryPivotId = 0;
        int scrollValue = 0;
        int limit = 20;
        JsonElement element;
        element = jsonObject.get("catId");
        if (JsonValidator.hasPositiveInteger(element)) {
            categoryId = element.getAsInt();
        } else {
            responseJsonObject.addProperty("sucs", Boolean.FALSE);
            responseJsonObject.addProperty("mg", "Please select a category.");
            sender.sendToClient(responseJsonObject);
            return;
        }

        element = jsonObject.get("subCatId");
        if (JsonValidator.hasPositiveInteger(element)) {
            subCategoryPivotId = element.getAsInt();
        }

        element = jsonObject.get("scrl");
        if (element != null) {
            scrollValue = element.getAsInt();
        }

        Scroll scroll = (scrollValue > 0) ? Scroll.UP : Scroll.DOWN;

        element = jsonObject.get("lmt");
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        CategoryRepository categoryHierarchy = MarketRepository.INSTANCE.getCategoryRepository();

        List<EntityCategory> subCategories;
        subCategories = categoryHierarchy.getCategoriesByScroll(categoryId, subCategoryPivotId, limit, scroll);
        if (subCategories == null || subCategories.isEmpty()) {
            responseJsonObject.addProperty("sucs", Boolean.FALSE);
            responseJsonObject.addProperty("mg", "No More Data");
        } else {
            responseJsonObject.add("subCatList", gson.toJsonTree(subCategories));
            responseJsonObject.addProperty("sucs", Boolean.TRUE);
        }
        responseJsonObject.add(MarketAttribute.PACKET_ID, jsonObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, jsonObject.get(MarketAttribute.ACTION));

        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.GET_CATEGORIES)
    public static void getCategories(JsonObject requestObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        boolean success = false;
        Integer categoryId = null;
        int scrollValue = 2;
        int limit = 20;
        int start = 0;
        String basketId = null;
        JsonElement element = requestObject.get(MarketAttribute.CATEGORY_ID);
        if (JsonValidator.hasPositiveInteger(element)) {
            categoryId = element.getAsInt();
        }
        element = requestObject.get(MarketAttribute.SCROLL);
        if (JsonValidator.hasPositiveInteger(element)) {
            scrollValue = element.getAsInt();
        }
        Scroll scroll = (scrollValue == 1) ? Scroll.UP : Scroll.DOWN;

        element = requestObject.get(MarketAttribute.LIMIT);
        if (JsonValidator.hasPositiveInteger(element)) {
            limit = element.getAsInt();
        }

        element = requestObject.get(MarketAttribute.START);
        if (JsonValidator.hasPositiveInteger(element)) {
            start = element.getAsInt();
        }
        element = requestObject.get(MarketAttribute.BASKET_ID);
        if (JsonValidator.hasValidString(element)) {
            basketId = element.getAsString();
        }
        Long shopId = null;
        element = requestObject.get(MarketAttribute.SHOP_ID);
        if (JsonValidator.hasValidString(element)) {
            shopId = element.getAsLong();
        }
        List<EntityCategory> categories;
        if (basketId != null) {
            categories = MarketRepository.INSTANCE.getBasketProductCategoryRepository().getBasketCategoriesById(basketId, start, limit, categoryId);
        } else if (shopId != null) {
            categories = MarketRepository.INSTANCE.getCategoryProductRepository().getStoresCategoriesById(shopId, start, limit, categoryId);
        } else {
            categories = ProductCategoryCache.getInstance().getCategories(categoryId, start, limit);
        }
        if (categories != null && !categories.isEmpty()) {
            responseJsonObject.add(MarketAttribute.CATEGORIES, gson.toJsonTree(categories));
            success = true;
        } else {
            responseJsonObject.addProperty(MarketAttribute.MESSAGE, Messages.NO_MORE_DATA);
            responseJsonObject.addProperty(MarketAttribute.REASON_CODE, ReasonCode.NO_MORE_DATA);
        }
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.ADD_CATEGORY)
    public static void addCategory(JsonObject requestObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        boolean success = false;
        EntityCategory category = gson.fromJson(requestObject, EntityCategory.class);
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        String message;
        if (category.getName() == null || category.getName().isEmpty()) {
            message = "Please provide category Name";
        } else if (category.getImageUrl() == null || category.getImageUrl().isEmpty()) {
            message = "Please provide Image";
        } else {
            ObjectReturnDTO<Integer> result;
            if (category.getParentId() <= 0) {
                result = MarketRepository.INSTANCE.getCategoryRepository().addRootCategory(category.getName(), category.getImageUrl());
            } else {
                result = MarketRepository.INSTANCE.getCategoryRepository().addSubCategory(category);
            }
            reasonCode = result.getReasonCode();
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                message = "Added successfully";
                responseJsonObject.addProperty(MarketAttribute.ID, result.getValue());
            } else {
                message = "Failed to add category";
            }
        }
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.CHANGE_CATEGORY_NAME)
    public static void changeCateogoryName(JsonObject requestObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        boolean success = false;
        JsonElement idElement = requestObject.get(MarketAttribute.ID);
        JsonElement nameElement = requestObject.get(MarketAttribute.NAME);
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        String message;
        if (!JsonValidator.hasPositiveInteger(idElement)) {
            message = "Please provide category Name";
        } else if (!JsonValidator.hasValidString(nameElement)) {
            message = "Please provide name";
        } else {
            int id = idElement.getAsInt();
            String name = nameElement.getAsString();
            reasonCode = MarketRepository.INSTANCE.getCategoryRepository().updateCategoryName(id, name);
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                message = "Updated Successfully";
            } else {
                message = Messages.OPERATION_FAILED;
            }
        }
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        sender.sendToClient(responseJsonObject);
    }

    @Request(action = Action.CHANGE_CATEGORY_IMAGE)
    public static void changeCateogoryImage(JsonObject requestObject, ISender sender) {
        JsonObject responseJsonObject = new JsonObject();
        boolean success = false;
        JsonElement idElement = requestObject.get(MarketAttribute.ID);
        JsonElement imageElement = requestObject.get(MarketAttribute.IMAGE_URL);
        int reasonCode = ReasonCode.INSUFFICIENT_INFORMATION;
        String message;
        if (!JsonValidator.hasPositiveInteger(idElement)) {
            message = "Please provide category Name";
        } else if (!JsonValidator.hasValidString(imageElement)) {
            message = "Please provide imageUrl";
        } else {
            int id = idElement.getAsInt();
            String imageUrl = imageElement.getAsString();
            reasonCode = MarketRepository.INSTANCE.getCategoryRepository().updateCategoryImage(id, imageUrl);
            if (reasonCode == ReasonCode.NONE) {
                success = true;
                message = "Updated Successfully";
            } else {
                message = Messages.OPERATION_FAILED;
            }
        }
        responseJsonObject.add(MarketAttribute.PACKET_ID, requestObject.get(MarketAttribute.PACKET_ID));
        responseJsonObject.add(MarketAttribute.ACTION, requestObject.get(MarketAttribute.ACTION));
        responseJsonObject.addProperty(MarketAttribute.SUCCESS, success);
        responseJsonObject.addProperty(MarketAttribute.MESSAGE, message);
        responseJsonObject.addProperty(MarketAttribute.REASON_CODE, reasonCode);
        sender.sendToClient(responseJsonObject);
    }
}
