/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.buyer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.EntitySavedProduct;
import org.ipvision.ringmarket.repositories.buyer.SavedProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * we will always have parris
 *
 * @author Imran
 */
public class SavedProduct implements SavedProductRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SavedProduct.class);

    /**
     *
     * @param buyerId
     * @param productId
     * @return
     */
    public boolean addToSavedList(long buyerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addToSavedList(entityManager, buyerId, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean addToSavedList(long buyerId, EntityProduct entityProduct) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addToSavedList(entityManager, buyerId, entityProduct);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean addToSavedList(EntitySavedProduct iSavedProducts) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addToSavedList(entityManager, iSavedProducts);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean deleteSavedProduct(long buyerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = deleteSavedProduct(entityManager, buyerId, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean isAlreadyOnSavedList(long buyerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            susc = isAlreadyOnSavedList(entityManager, buyerId, productId);
        } finally {
            entityManager.close();
        }
        return susc;
    }

    /**
     * Using idx_saved_product_buyer as index
     *
     * @param buyerId
     * @param pivotProductId(Default should be an empty string)
     * @param scroll
     * @param limit
     * @return
     */
    public List<EntitySavedProduct> getSavedProductList(long buyerId, String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntitySavedProduct> results;
        try {
            results = getSavedProductList(entityManager, buyerId, pivotProductId, scroll, limit);
        } finally {
            entityManager.close();
        }
        return results;
    }

    protected boolean deleteSavedProduct(EntityManager entityManager, long buyerId, String productId) {
        boolean susc = true;
        try {
//@NamedNativeQuery(name = "deleteFromSavedProduct", query = "delete FROM saved_product WHERE buyer_id = ?1 AND product_id = ?2")
            Query query = entityManager.createNamedQuery("deleteFromSavedProduct", EntitySavedProduct.class);
//            query.setParameter(1, buyerId);
//            query.setParameter(2, productId);
//            query.executeUpdate();
//            Query query = entityManager.createQuery("delete FROM EntitySavedProduct t0 WHERE t0.buyerId = :buyerId AND t0.productId = :productId");
            query.setParameter("buyerId", buyerId);
            query.setParameter("productId", productId);
            query.executeUpdate();
        } catch (Exception ex) {
            LOGGER.error("Error while delete product: ", ex);
            return false;
        }
        return susc;
    }

    protected boolean addToSavedList(EntityManager entityManager, EntitySavedProduct entitySavedProduct) {
        try {
            entityManager.persist(entitySavedProduct);
        } catch (Exception ex) {
            LOGGER.error("Error in addToSavedList: ", ex);
            return false;
        }
        return true;
    }

    protected boolean addToSavedList(EntityManager entityManager, long buyerId, String productId) {
        try {
            EntityProduct product = entityManager.find(EntityProduct.class, productId);
            if (product != null) {
                EntitySavedProduct entitySavedProduct = new EntitySavedProduct();
                entitySavedProduct.setBuyerId(buyerId);
                entitySavedProduct.setProductId(productId);
                entitySavedProduct.setProductName(product.getName());
                entitySavedProduct.setProductImageUrl(product.getImageUrl());
                entitySavedProduct.setImageHeight(product.getImageHeight());
                entitySavedProduct.setImageWidth(product.getImageWidth());
                return addToSavedList(entityManager, entitySavedProduct);
            }
        } catch (Exception ex) {
            LOGGER.error("Error in addToSavedList: ", ex);
        }
        return false;
    }

    protected boolean addToSavedList(EntityManager entityManager, long buyerId, EntityProduct entityProduct) {
        try {
            EntitySavedProduct entitySavedProduct = new EntitySavedProduct();
            entitySavedProduct.setBuyerId(buyerId);
            entitySavedProduct.setProductId(entityProduct.getId());
            entitySavedProduct.setProductName(entityProduct.getName());
            entitySavedProduct.setProductImageUrl(entityProduct.getImageUrl());
            entitySavedProduct.setImageHeight(entityProduct.getImageHeight());
            entitySavedProduct.setImageWidth(entityProduct.getImageWidth());
            return addToSavedList(entityManager, entitySavedProduct);
        } catch (Exception ex) {
            LOGGER.error("Error in addToSavedList: ", ex);
        }
        return false;
    }

    protected boolean isAlreadyOnSavedList(EntityManager entityManager, long buyerId, String productId) {
//        return entityManager.find(EntitySavedProduct.class, new SavedProductId(buyerId, productId));
        Query query = entityManager.createNamedQuery("getIsAlreadySaved");
        query.setParameter("buyerId", buyerId);
        query.setParameter("productId", productId);
        try {
            return Integer.valueOf(query.getSingleResult().toString()) == 1;
        } catch (Exception ex) {
            return false;
        }
//        List<EntitySavedProduct> myList = query.setMaxResults(1).getResultList();
//        if (myList == null || myList.isEmpty()) {
//            return null;
//        }
//        return myList.get(0);
    }

    protected ArrayList<EntitySavedProduct> getSavedProductList(EntityManager entityManager, long buyerId, String pivotProductId, Scroll scroll, int limit) {
        Query query;
        if (pivotProductId == null) {
            pivotProductId = "";
        }
        if (scroll.equals(Scroll.UP)) {
            query = entityManager.createNamedQuery("getUpwardSavedProductList");
        } else {
            query = entityManager.createNamedQuery("getDownwardSavedProductList");
        }
        query.setParameter("buyerId", buyerId);
        query.setParameter("pivotProductId", pivotProductId);
        try {
            ArrayList<EntitySavedProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (scroll.equals(Scroll.UP)) {
                Collections.reverse(results);
            }
            return results;
        } catch (Exception ex) {
            LOGGER.error("Error in getSavedProductList: ", ex);
            return null;
        }
    }
}
