/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.buyer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.OfferStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityOfferProduct;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.EntityRingMarketUser;
import org.ipvision.ringmarket.repositories.buyer.OfferProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class OfferProduct implements OfferProductRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(OfferProduct.class);

    public boolean addNewOffer(String id, long buyerId, String buyerName, String prodId, String productName, String imageUrl, double price, long offeredDate, OfferStatus offerStatus) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            entityManager.getTransaction().begin();
            addNewOffer(entityManager, id, buyerId, buyerName, prodId, productName, imageUrl, price, offeredDate, offerStatus);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean addNewOffer(String id, EntityRingMarketUser iru, EntityProduct iProduct, long offeredDate, OfferStatus offerStatus) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            entityManager.getTransaction().begin();
            susc = addNewOffer(entityManager, id, iru.getId(), iru.getName(), iProduct.getId(), iProduct.getName(), iProduct.getImageUrl(), iProduct.getPrice(), offeredDate, offerStatus);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean addNewOffer(EntityOfferProduct entityOfferProduct) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            entityManager.getTransaction().begin();
            susc = addNewOffer(entityManager, entityOfferProduct);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public EntityOfferProduct getOfferInfo(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityOfferProduct entityOfferProduct = null;
        try {
            entityOfferProduct = getOfferInfo(entityManager, id);
        } finally {
            entityManager.close();
        }
        return entityOfferProduct;
    }

    public boolean removeOffer(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            entityManager.getTransaction().begin();
            susc = removeOffer(entityManager, id);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean removeAllOfferOfProduct(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            entityManager.getTransaction().begin();
            susc = removeAllOfferOfProduct(entityManager, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean updateOfferStatus(String id, OfferStatus offerStatus) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            entityManager.getTransaction().begin();
            susc = updateOfferStatus(entityManager, id, offerStatus);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean updateOfferBid(EntityOfferProduct entityOfferProduct, double offerPrice, long offerDate) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            entityManager.getTransaction().begin();
            susc = updateOfferBid(entityManager, entityOfferProduct, offerPrice, offerDate);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public EntityOfferProduct getOfferInfoOfProduct(long buyerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityOfferProduct result;
        try {
            result = getOfferInfoOfProduct(entityManager, buyerId, productId);
        } finally {
            entityManager.close();
        }
        return result;
    }

    public boolean isAlreadyAccepted(String product_id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            susc = isAlreadyAccepted(entityManager, product_id);
        } finally {
            entityManager.close();
        }
        return susc;
    }

    /**
     * Default pivotOfferedDate is infinity when down scroll Default
     * pivotOfferedDate is 0 when up scroll
     *
     */
    public List<EntityOfferProduct> getAllOffersForProduct(String product_id, long pivotOfferedDate, int limit, Scroll scroll) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityOfferProduct> results;
        try {
            results = getAllOffersForProduct(entityManager, product_id, pivotOfferedDate, limit, scroll);
        } finally {
            entityManager.close();
        }
        return results;
    }

    public List<EntityOfferProduct> getOffersByBuyer(long buyerId, long pivotOfferedDate, int limit, Scroll scroll) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityOfferProduct> results = null;
        try {
            results = getOffersByBuyer(entityManager, buyerId, pivotOfferedDate, limit, scroll);
        } finally {
            entityManager.close();
        }
        return results;
    }

    protected boolean addNewOffer(EntityManager entityManager, String id, long buyerId, String buyerName, String prodId, String productName, String imageUrl, double price, long offeredDate, OfferStatus offerStatus) {
        EntityOfferProduct entityOfferProduct = new EntityOfferProduct();
        entityOfferProduct.setId(id);
        entityOfferProduct.setBuyerId(buyerId);
        entityOfferProduct.setBuyerName(buyerName);
        entityOfferProduct.setDefaultImageUrl(imageUrl);
        entityOfferProduct.setOfferedDate(offeredDate);
        entityOfferProduct.setOfferedPrice(price);
        entityOfferProduct.setProductId(prodId);
        entityOfferProduct.setProductName(productName);
        entityOfferProduct.setStatus(offerStatus);
//        entityOfferProduct.setStatus(offerStatus.ordinal());
        return addNewOffer(entityManager, entityOfferProduct);
    }

    protected boolean updateOfferBid(EntityManager entityManager, EntityOfferProduct entityOfferProduct, double offerPrice, long offerDate) {
        try {
            entityOfferProduct.setOfferedPrice(offerPrice);
            entityOfferProduct.setOfferedDate(offerDate);
            entityManager.merge(entityOfferProduct);
        } catch (Exception ex) {
            LOGGER.error("error while updating offer bid ", ex);
            return false;
        }
        return true;
    }

    public boolean updateOfferStatus(EntityManager entityManager, String id, OfferStatus offerStatus) {
        EntityOfferProduct entityOfferProduct = getOfferInfo(entityManager, id);
        entityOfferProduct.setStatus(offerStatus);
//        entityOfferProduct.setStatus(offerStatus.ordinal());
        try {
            entityManager.merge(entityOfferProduct);
            if (offerStatus.equals(OfferStatus.CANCELLED)) {
                boolean susc = removeOffer(entityManager, id);
                if (!susc) {
                    LOGGER.error("error while removeOffer ");
                    return false;
                }
            }
        } catch (Exception ex) {
            LOGGER.error("error while updating offer status ", ex);
            return false;
        }
        return true;
    }

    protected boolean removeOffer(EntityManager entityManager, String id) {
        try {
            EntityOfferProduct entityOfferProduct = getOfferInfo(entityManager, id);
            if (entityOfferProduct != null) {
                entityManager.remove(getOfferInfo(entityManager, id));
            } else {
                LOGGER.error("error while removing offer. No such offer found. Id: " + id);
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("error while removing offer ", ex);
            return false;
        }
        return true;
    }

    protected boolean addNewOffer(EntityManager entityManager, EntityOfferProduct entityOfferProduct) {
        try {
            entityManager.persist(entityOfferProduct);
        } catch (Exception ex) {
            LOGGER.error("Error inserting in addNewOffer: ", ex);
            return false;
        }
        return true;
    }

    protected EntityOfferProduct getOfferInfo(EntityManager entityManager, String id) {
        return entityManager.find(EntityOfferProduct.class, id);
    }

    protected EntityOfferProduct getOfferInfoOfProduct(EntityManager entityManager, long buyerId, String productId) {
        Query query = entityManager.createNamedQuery("getOfferInfoOfProduct");
        query.setParameter("buyerId", buyerId);
        query.setParameter("productId", productId);
        ArrayList<EntityOfferProduct> results = new ArrayList<>(query.getResultList());
//        if (results == null || results.isEmpty()) {
//            return null;
//        }
//        return results.get(0);
        return (results == null || results.isEmpty()) ? null : results.get(0);
    }

    protected boolean isAlreadyAccepted(EntityManager entityManager, String productId) {
        Query query = entityManager.createNamedQuery("getIsAlreadyAccepted");
        query.setParameter("status", OfferStatus.ACCEPTED.ordinal());
        query.setParameter("productId", productId);
        try {
            return (int) query.getSingleResult() == 1;
        } catch (Exception ex) {
            return false;
        }
//        ArrayList<EntityOfferProduct> results = new ArrayList<>(query.getResultList());
//        return !(results == null || results.isEmpty());
    }

    protected ArrayList<EntityOfferProduct> getAllOffersForProduct(EntityManager entityManager, String productId, long pivotOfferedDate, int limit, Scroll scroll) {
        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("getUpwardAllOffersForProduct");
        } else {
            query = entityManager.createNamedQuery("getDownwardAllOffersForProduct");
        }
        query.setParameter("productId", productId);
        query.setParameter("pivotOfferedDate", pivotOfferedDate);
        ArrayList<EntityOfferProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
        if (scroll.equals(Scroll.UP)) {
            Collections.reverse(results);
        }
        return results;
    }

    protected ArrayList<EntityOfferProduct> getOffersByBuyer(EntityManager entityManager, long buyerId, long pivotOfferedDate, int limit, Scroll scroll) {
        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("getUpwardOffersByBuyer");
        } else {
            query = entityManager.createNamedQuery("getDownwardOffersByBuyer");
        }
        query.setParameter("buyerId", buyerId);
        query.setParameter("pivotOfferedDate", pivotOfferedDate);

        try {
            ArrayList<EntityOfferProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (scroll.equals(Scroll.UP)) {
                Collections.reverse(results);
            }
            return results;
        } catch (Exception ex) {
            LOGGER.error("Error in getOffersByBuyer: ", ex);
            return null;
        }
    }

    protected ArrayList<EntityOfferProduct> getAllOffersForProduct(EntityManager entityManager, String productId) {
        Query query = entityManager.createNamedQuery("getAllOffersForProduct");
        query.setParameter("productId", productId);
        ArrayList<EntityOfferProduct> results = new ArrayList<>(query.getResultList());
        return results;
    }

    protected boolean removeAllOfferOfProduct(EntityManager entityManager, String productId) {
        try {
//delete from offered_product WHERE productId = ?1
            Query query = entityManager.createNamedQuery("deleteOffersForProduct");
            query.setParameter(1, productId).executeUpdate();
        } catch (Exception ex) {
            LOGGER.error("Error in removeAllOfferOfProduct: ", ex);
            return false;
        }
        return true;
    }

}
/**
 * vindicate, veracious venality
 */
