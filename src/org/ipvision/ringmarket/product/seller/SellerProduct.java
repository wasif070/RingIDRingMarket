/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.seller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntitySellerProduct;
import org.ipvision.ringmarket.repositories.seller.SellerProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class SellerProduct implements SellerProductRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SellerProduct.class);

    @Override
    public boolean addNewProduct(EntitySellerProduct entitySellerProduct) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;

        try {
            try {
                entityManager.getTransaction().begin();
                addNewProduct(entityManager, entitySellerProduct);
                entityManager.getTransaction().commit();
                susc = true;
            } catch (Exception e) {
                LOGGER.error("Exception in addNewProduct() : ", e);
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    @Override
    public boolean addNewProduct(Long sellerId, String productId, long addedDate, String productName, String imgUrl) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;

        try {
            entityManager.getTransaction().begin();
            addNewProduct(entityManager, sellerId, productId, productName, imgUrl, addedDate);
            entityManager.getTransaction().commit();
            susc = true;
        } catch (Exception e) {
            LOGGER.error("Exception in addNewProduct() : ", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean deleteProduct(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = deleteProduct(entityManager, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public EntitySellerProduct getProduct(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntitySellerProduct entitySellerProduct;
        try {
            entitySellerProduct = entityManager.find(EntitySellerProduct.class, productId);
        } finally {
            entityManager.close();
        }
        return entitySellerProduct;
    }

    public List<EntitySellerProduct> getProductList(Long sellerId, String pivotProdId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntitySellerProduct> results;
        try {
            results = getProductList(entityManager, sellerId, pivotProdId, scroll, limit);
        } finally {
            entityManager.close();
        }
        return results;
    }

    public void addNewProduct(EntityManager entityManager, Long sellerId, String productId, String productName, String imgUrl, Long addedDate) {
        EntitySellerProduct entitySellerProduct = new EntitySellerProduct();
        entitySellerProduct.setProductId(productId);
        entitySellerProduct.setAddedDate(addedDate);
        entitySellerProduct.setSellerId(sellerId);
        entitySellerProduct.setDefaultImageUrl(imgUrl);
        entitySellerProduct.setProductName(productName);
        addNewProduct(entityManager, entitySellerProduct);
    }

    private void addNewProduct(EntityManager entityManager, EntitySellerProduct entitySellerProduct) {
        entityManager.persist(entitySellerProduct);
    }

    @Override
    public boolean isProductOwner(Long sellerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        long userId = 0;

        try {
            EntitySellerProduct entitySellerProduct = entityManager.find(EntitySellerProduct.class, productId);
            if (entitySellerProduct != null) {
                userId = entitySellerProduct.getSellerId();
            }
        } finally {
            entityManager.close();
        }
        return userId == sellerId;
    }

    public boolean deleteProduct(EntityManager entityManager, String productId) {
        try {
//delete from seller_product where product_id = ?1
            Query query = entityManager.createNamedQuery("deleteFromSellerProduct");
            query.setParameter(1, productId).executeUpdate();
        } catch (Exception ex) {
            LOGGER.error("Error deleting in SellerProdcut: ", ex);
            return false;
        }
        return true;
    }

    private ArrayList<EntitySellerProduct> getProductList(EntityManager entityManager, Long sellerId, String pivotProdId, Scroll scroll, int limit) {
        if (pivotProdId == null) {
            pivotProdId = "";
        }
        Query query;
        if (scroll.equals(Scroll.UP)) {
            query = entityManager.createNamedQuery("getUpwardSellerProductList");
        } else {
            query = entityManager.createNamedQuery("getDownwardSellerProductList");
        }
        query.setParameter("sellerId", sellerId);
        query.setParameter("pivotProdId", pivotProdId);
        ArrayList<EntitySellerProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
        if (scroll.equals(Scroll.UP)) {
            Collections.reverse(results);
        }
        return results;
    }
}
