/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.seller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntitySellerRatingLog;
import org.ipvision.ringmarket.entities.id.SellerRatingLogId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ipvision.ringmarket.repositories.seller.SellerRatingLogRepository;

/**
 *
 * @author Imran
 */
public class SellerRatingLogRepositoryImpl implements SellerRatingLogRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SellerRatingLogRepositoryImpl.class);

    protected void addNewRating(EntityManager entityManager, long sellerId, long buyerId, byte rating) {
        EntitySellerRatingLog historySellerRating = new EntitySellerRatingLog();
        historySellerRating.setBuyerId(buyerId);
        historySellerRating.setSellerId(sellerId);
        historySellerRating.setRating(rating);
        entityManager.persist(historySellerRating);
    }

    protected void updateRating(EntityManager entityManager, EntitySellerRatingLog historySellerRating) {
        entityManager.merge(historySellerRating);
    }

    @Override
    public EntitySellerRatingLog findRatingByBuyer(long sellerId, long buyerId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntitySellerRatingLog historySellerRating;
        try {
            historySellerRating = entityManager.find(EntitySellerRatingLog.class, new SellerRatingLogId(buyerId, sellerId));
        } finally {
            entityManager.close();
        }
        return historySellerRating;
    }

    protected EntitySellerRatingLog findRatingByBuyer(EntityManager entityManager, long sellerId, long buyerId) {
        return entityManager.find(EntitySellerRatingLog.class, new SellerRatingLogId(buyerId, sellerId));
    }

    protected boolean removeRatingUser(EntityManager entityManager, EntitySellerRatingLog ihsr) {
        try {
            entityManager.remove(ihsr);
        } catch (Exception ex) {
            LOGGER.error("Exception in removeRatingUser. sellerId: " + ihsr.getSellerId(), ex);
            return false;
        }
        return true;
    }

    @Override
    public Map<Byte, Long> getRatingDetails(Long sellerId) {
        Map<Byte, Long> ratingMap = new LinkedHashMap<>();

        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            Query query = entityManager.createNamedQuery("EntitySellerRatingLog.getRatingDetails");
            try {
                query.setParameter("sellerId", sellerId);
                List<Object[]> resultList = query.getResultList();

                resultList.forEach((objects) -> {
                    ratingMap.put((Byte) objects[0], (Long) objects[1]);
                });
            } catch (Exception e) {
                LOGGER.error("Exception in getRatingDetails() : {sellerId:" + sellerId + "}", e);
            }
        } finally {
            entityManager.close();
        }
        return ratingMap;
    }

}
