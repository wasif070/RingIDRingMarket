/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.seller;

import javax.persistence.EntityManager;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntitySellerRatingLog;
import org.ipvision.ringmarket.entities.EntitySellerRating;
import org.ipvision.ringmarket.repositories.seller.SellerRatingRepository;
import org.ipvision.ringmarket.repositories.shop.ShopRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class SellerRating extends ShopRepositoryImpl implements SellerRatingRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SellerRating.class);

    @Override
    public boolean updateSellerRating(long sellerId, long buyerId, byte rating) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = updateSellerRating(entityManager, sellerId, buyerId, rating);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    @Override
    public boolean removeSellerRating(long sellerId, long buyerId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = removeSellerRating(entityManager, sellerId, buyerId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    @Override
    public EntitySellerRating getRating(long sellerId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntitySellerRating entitySellerRating;
        try {
            entitySellerRating = getRating(entityManager, sellerId);
        } finally {
            entityManager.close();
        }
        return entitySellerRating;
    }

    @Override
    public EntitySellerRating getRating(EntityManager entityManager, long sellerId) {
        return entityManager.find(EntitySellerRating.class, sellerId);
    }

    /**
     * returns -1 for exception
     *
     * @param sellerId
     * @param buyerId
     * @return
     */
    @Override
    public byte findRatingByBuyer(long sellerId, long buyerId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        byte rating;
        try {
            rating = findRatingByBuyer(entityManager, sellerId, buyerId);
        } finally {
            entityManager.close();
        }
        return rating;
    }

    protected boolean updateSellerRating(EntityManager entityManager, long sellerId, long buyerId, byte rating) {
        SellerRatingLogRepositoryImpl historySellerRating = new SellerRatingLogRepositoryImpl();
        EntitySellerRatingLog ehsr = historySellerRating.findRatingByBuyer(entityManager, sellerId, buyerId);
        boolean success;
        if (ehsr == null) {
            success = updateSellerRatingByNewUser(entityManager, sellerId, buyerId, rating);
        } else {
            success = updateSellerRatingByOldUser(entityManager, sellerId, rating, ehsr);
        }

        return success;
    }

    protected boolean updateSellerRatingByNewUser(EntityManager entityManager, long sellerId, long buyerId, byte rating) {
        SellerRatingLogRepositoryImpl historySellerRating = new SellerRatingLogRepositoryImpl();
        try {
            entityManager.find(EntitySellerRating.class, sellerId);
            EntitySellerRating entitySellerRating = entityManager.find(EntitySellerRating.class, sellerId);
            if (entitySellerRating == null) {
                entitySellerRating = new EntitySellerRating();
                entitySellerRating.setNumberOfRaters(1);
                entitySellerRating.setSellerId(sellerId);
                entitySellerRating.setTotalRating(rating);
                entityManager.persist(entitySellerRating);
            } else {
                entitySellerRating.setNumberOfRaters(entitySellerRating.getNumberOfRaters() + 1);
                entitySellerRating.setTotalRating(entitySellerRating.getTotalRating() + rating);
                entityManager.merge(entitySellerRating);
            }
            // adding to history
            historySellerRating.addNewRating(entityManager, sellerId, buyerId, rating);
            updateRating(entityManager, ((float) entitySellerRating.getTotalRating() / entitySellerRating.getNumberOfRaters()), sellerId);

        } catch (Exception ex) {
            LOGGER.error("Error in updateSellerRatingByNewUser. {sellerId: " + sellerId + "}", ex);
            return false;
        }
        return true;
    }

    protected boolean updateSellerRatingByOldUser(EntityManager entityManager, long sellerId, byte newRating, EntitySellerRatingLog ehsr) {
        SellerRatingLogRepositoryImpl historySellerRating = new SellerRatingLogRepositoryImpl();
        try {
            EntitySellerRating entitySellerRating = entityManager.find(EntitySellerRating.class, sellerId);
            entitySellerRating.setTotalRating(entitySellerRating.getTotalRating() - ehsr.getRating() + newRating);
            entityManager.merge(entitySellerRating);

            //adding to history
            ehsr.setRating(newRating);
            historySellerRating.updateRating(entityManager, ehsr);
            updateRating(entityManager, ((float) entitySellerRating.getTotalRating() / entitySellerRating.getNumberOfRaters()), sellerId);

        } catch (Exception ex) {
            LOGGER.error("Error in updateSellerRatingByOldUser. {sellerId: " + sellerId + "}", ex);
            return false;
        }
        return true;
    }

    protected boolean removeSellerRatingByUser(EntityManager entityManager, long sellerId, int rating) {
        try {
            EntitySellerRating entitySellerRating = getRating(entityManager, sellerId);
            entitySellerRating.setNumberOfRaters(entitySellerRating.getNumberOfRaters() - 1);
            entitySellerRating.setTotalRating(entitySellerRating.getTotalRating() - rating);
            entityManager.merge(entitySellerRating);
        } catch (Exception ex) {
            LOGGER.error("Error in removeSellerRatingByUser. {sellerId: " + sellerId + "}", ex);
            return false;
        }
        return true;
    }

    protected boolean removeSellerRating(EntityManager entityManager, long sellerId, long buyerId) {
        try {
            SellerRatingLogRepositoryImpl historySellerRating = new SellerRatingLogRepositoryImpl();
            EntitySellerRatingLog ehsr = historySellerRating.findRatingByBuyer(entityManager, sellerId, buyerId);
            historySellerRating.removeRatingUser(entityManager, ehsr);
        } catch (Exception ex) {
            LOGGER.error("Error in removeSellerRating. {sellerId: " + sellerId + "}", ex);
            return false;
        }
        return true;
    }

    protected byte findRatingByBuyer(EntityManager entityManager, long sellerId, long buyerId) {
        SellerRatingLogRepositoryImpl historySellerRating = new SellerRatingLogRepositoryImpl();
        try {
            return historySellerRating.findRatingByBuyer(entityManager, sellerId, buyerId).getRating();
        } catch (Exception ex) {
            LOGGER.error("Error in findRatingByBuyer: buyerId: " + buyerId, ex);
            return -1;
        }
    }
}
