/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.order;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.OfferStatus;
import org.ipvision.ringmarket.constants.OrderStatus;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityOrderProduct;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.product.buyer.OfferProduct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class OrderProduct {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderProduct.class);

    public boolean addNewOrder(String id, long buyerId, long sellerId, long agentId, String productId, Shipment shipment, double offeredPrice, double shipmentCost, long orderDate, long expDelDate, OrderStatus orderStatus) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addNewOrder(entityManager, id, buyerId, sellerId, agentId, productId, shipment, offeredPrice, shipmentCost, orderDate, expDelDate, orderStatus);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean addNewOrder(EntityOrderProduct entityOrderProduct) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addNewOrder(entityManager, entityOrderProduct);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public EntityOrderProduct getOrderInfo(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityOrderProduct entityOrderProduct;
        try {
            entityOrderProduct = getOrderInfo(entityManager, id);
        } finally {
            entityManager.close();
        }
        return entityOrderProduct;
    }

    public EntityOrderProduct getOrderInfo(EntityManager entityManager, String id) {
        return entityManager.find(EntityOrderProduct.class, id);
    }

    public EntityOrderProduct getOrderInfoByProductId(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityOrderProduct entityOrderProduct;
        try {
            entityOrderProduct = getOrderInfoByProductId(entityManager, productId);
        } finally {
            entityManager.close();
        }
        return entityOrderProduct;
    }

    public boolean isProductOrdered(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            susc = isProductOrdered(entityManager, productId);
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean removeOrder(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = removeOrder(entityManager, id);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean updateOrderStatus(String id, OrderStatus orderStatus, long completedDate) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = updateOrderStatus(entityManager, id, orderStatus, completedDate);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean updateOrderStatusCompleted(String id, long completedDate) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = updateOrderStatusCompleted(entityManager, id, completedDate);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public ArrayList<EntityOrderProduct> getOrdersBySeller(long sellerId, String pivotOrderId, int limit, Scroll scroll) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityOrderProduct> results;
        try {
            results = getOrdersBySeller(entityManager, sellerId, pivotOrderId, limit, scroll);
        } finally {
            entityManager.close();
        }
        return results;
    }

    public ArrayList<EntityOrderProduct> getOrdersByBuyer(long buyerId, String pivotOrderId, int limit, Scroll scroll) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityOrderProduct> results;
        try {
            results = getOrdersByBuyer(entityManager, buyerId, pivotOrderId, limit, scroll);
        } finally {
            entityManager.close();
        }
        return results;
    }

    public boolean isProductOrdered(EntityManager entityManager, String productId) {
        EntityOrderProduct entityOrderProduct = getOrderInfoByProductId(entityManager, productId);
        return entityOrderProduct != null;
    }

    public EntityOrderProduct getOrderInfoByProductId(EntityManager entityManager, String productId) {
        Query query = entityManager.createNamedQuery("getIsAlreadyOrdered");
        query.setParameter("productId", productId);
        List<EntityOrderProduct> myList = query.setMaxResults(1).getResultList();
        if (myList == null || myList.isEmpty()) {
            return null;
        }
        return myList.get(0);
    }

    protected boolean addNewOrder(EntityManager entityManager, EntityOrderProduct entityOrderProduct) {
        try {
            // adding new order
            entityManager.persist(entityOrderProduct);

            // updating offer status
            OfferProduct offerProduct = new OfferProduct();
            boolean susc = offerProduct.updateOfferStatus(entityManager, entityOrderProduct.getId(), OfferStatus.ORDERED);
            if (!susc) {
                LOGGER.error("Error while adding new order: ");
                return susc;
            }

            // add into agent order
            AgentOrder agentOrder = new AgentOrder();
            susc = agentOrder.addAgentOrder(entityManager, entityOrderProduct.getAgentId(), entityOrderProduct.getId(), entityOrderProduct.getExpDeliveryDate());
            if (!susc) {
                LOGGER.error("Error while adding new order: ");
                return susc;
            }

        } catch (Exception ex) {
            LOGGER.error("Error while adding new order: ", ex);
            return false;
        }
        return true;
    }

    protected boolean addNewOrder(EntityManager entityManager, String id, long buyerId, long sellerId, long agentId, String productId,
            Shipment shipment, double offeredPrice, double shipmentCost, long orderDate, long expDelDate, OrderStatus orderStatus) {
        EntityOrderProduct entityOrderProduct = new EntityOrderProduct();
        entityOrderProduct.setId(id);
        entityOrderProduct.setAgentId(agentId);
        entityOrderProduct.setBuyerId(buyerId);
        entityOrderProduct.setExpDeliveryDate(expDelDate);
        entityOrderProduct.setOfferedPrice(offeredPrice);
        entityOrderProduct.setOrderDate(orderDate);
        entityOrderProduct.setProductId(productId);
        entityOrderProduct.setSellerId(sellerId);
        entityOrderProduct.setShipmentMethod(shipment.ordinal());
        entityOrderProduct.setShipmentCost(shipmentCost);
        entityOrderProduct.setStatus(orderStatus);
        return addNewOrder(entityManager, entityOrderProduct);
    }

    public boolean removeOrder(EntityManager entityManager, String id) {
        try {
            entityManager.remove(getOrderInfo(entityManager, id));
            EntityOrderProduct entityOrderProduct = getOrderInfo(entityManager, id);
            if (entityOrderProduct != null) {
                entityManager.remove(entityOrderProduct);
            } else {
                LOGGER.error("Error in removeOrder. No such Order Found. Id: " + id);
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("Error in removeOrder: ", ex);
            return false;
        }
        return true;
    }

    public boolean updateOrderStatus(EntityManager entityManager, String id, OrderStatus orderStatus, long completedDate) {
        EntityOrderProduct entityOrderProduct = getOrderInfo(entityManager, id);
        if (entityOrderProduct == null) {
            return false;
        }
        entityOrderProduct.setStatus(orderStatus);
        try {
            entityManager.merge(entityOrderProduct);

            if (orderStatus.equals(OrderStatus.CANCELLED)) {
                boolean susc = updateForCancelledOrder(entityManager, entityOrderProduct, completedDate, orderStatus);
                if (!susc) {
                    LOGGER.error("Error in updateOrderStatus: ");
                    return false;
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error in updateOrderStatus: ", ex);
            return false;
        }
        return true;
    }

    public boolean updateOrderStatusCompleted(EntityManager entityManager, String id, long completedDate) {
        EntityOrderProduct entityOrderProduct = getOrderInfo(entityManager, id);
        if (entityOrderProduct == null) {
            return false;
        }
        entityOrderProduct.setStatus(OrderStatus.COMPLETED);
        try {
            entityManager.merge(entityOrderProduct);

            // update product status
            ProductRepositoryImpl product = new ProductRepositoryImpl();
            product.updateProductStatus(entityManager, entityOrderProduct.getProductId(), ProductStatus.SOLD_OUT);

            //update history order
            HistoryOrder historyOrder = new HistoryOrder();
            boolean susc = historyOrder.addNewHistory(entityManager, entityOrderProduct, completedDate);
            if (susc == false) {
                LOGGER.error("Error in updateOrderStatus. Not added to hisroty_order. productId: ", entityOrderProduct.getProductId());
                return false;
            }

            //removing the order from database
            removeOrder(entityManager, entityOrderProduct.getId());

            //remove from agentOrder and add to historyAgentOrder
            AgentOrder agentOrder = new AgentOrder();
            agentOrder.deleteAgentOrder(entityManager, entityOrderProduct.getId(), OrderStatus.COMPLETED);
        } catch (Exception ex) {
            LOGGER.error("Error in updateOrderStatus: ", ex);
            return false;
        }
        return true;
    }

    private boolean updateForCancelledOrder(EntityManager entityManager, EntityOrderProduct entityOrderProduct, long completedDate, OrderStatus orderStatus) {
        try {
            //update history order
            HistoryOrder historyOrder = new HistoryOrder();
            boolean susc = historyOrder.addNewHistory(entityManager, entityOrderProduct, completedDate);
            if (susc == false) {
                LOGGER.error("Error in updateOrderStatus. Not added to hisroty_order. productId: ", entityOrderProduct.getProductId());
                return false;
            }

            //removing the order from database
            removeOrder(entityManager, entityOrderProduct.getId());

            //remove from agentOrder and add to historyAgentOrder
            AgentOrder agentOrder = new AgentOrder();
            agentOrder.deleteAgentOrder(entityManager, entityOrderProduct.getId(), orderStatus);
        } catch (Exception e) {
            LOGGER.error("Error in updateOrderStatus: ", e);
            return false;
        }
        return true;
    }

    public ArrayList<EntityOrderProduct> getOrdersByBuyer(EntityManager entityManager, long buyerId, String pivotOrderId, int limit, Scroll scroll) {
        if (pivotOrderId == null) {
            pivotOrderId = "";
        }

        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("getUpwardOrdersByBuyer");
        } else {
            query = entityManager.createNamedQuery("getDownwardOrdersByBuyer");
        }

        query.setParameter("pivotOrderId", pivotOrderId);
        query.setParameter("buyerId", buyerId);

        try {
            ArrayList<EntityOrderProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (scroll.equals(Scroll.UP)) {
                Collections.reverse(results);
            }
            return results;
        } catch (Exception ex) {
            LOGGER.error("Error in getOrdersByBuyer: ", ex);
            return null;
        }
    }

    public ArrayList<EntityOrderProduct> getOrdersBySeller(EntityManager entityManager, long sellerId, String pivotOrderId, int limit, Scroll scroll) {
        if (pivotOrderId == null) {
            pivotOrderId = "";
        }

        javax.persistence.Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("getUpwardOrdersBySeller");
        } else {
            query = entityManager.createNamedQuery("getDownwardOrdersBySeller");
        }

        query.setParameter("pivotOrderId", pivotOrderId);
        query.setParameter("sellerId", sellerId);

        ArrayList<EntityOrderProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
        if (scroll.equals(Scroll.UP)) {
            Collections.reverse(results);
        }
        return results;
    }
}
