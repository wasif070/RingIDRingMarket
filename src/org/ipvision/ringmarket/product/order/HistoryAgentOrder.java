/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.order;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.OrderStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityHistoryAgentOrder;
import org.ipvision.ringmarket.repositories.agent.HistoryAgentOrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class HistoryAgentOrder implements HistoryAgentOrderRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(HistoryAgentOrder.class);

    public boolean addHistoryAgentOrder(long agentId, String OrderId, OrderStatus orderStatus) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addHistoryAgentOrder(entityManager, agentId, OrderId, orderStatus);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean addHistoryAgentOrder(EntityHistoryAgentOrder entityAgentOrder) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addHistoryAgentOrder(entityManager, entityAgentOrder);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public EntityHistoryAgentOrder getHistoryOrder(String orderId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityHistoryAgentOrder entityAgentOrder;
        try {
            entityAgentOrder = getHistoryOrder(entityManager, orderId);
        } finally {
            entityManager.close();
        }
        return entityAgentOrder;
    }

    protected EntityHistoryAgentOrder getHistoryOrder(EntityManager entityManager, String orderId) {
        return entityManager.find(EntityHistoryAgentOrder.class, orderId);
    }

    protected boolean deleteOrder(EntityManager entityManager, String orderId) {
        try {
            entityManager.remove(getHistoryOrder(entityManager, orderId));
        } catch (Exception ex) {
            LOGGER.error("Error in deleteOrder: ", ex);
            return false;
        }
        return true;
    }

    protected boolean addHistoryAgentOrder(EntityManager entityManager, long agentId, String OrderId, OrderStatus orderStatus) {
        EntityHistoryAgentOrder entityAgentOrder = new EntityHistoryAgentOrder();
        entityAgentOrder.setAgentId(agentId);
        entityAgentOrder.setOrderId(OrderId);
        entityAgentOrder.setOrderStatus(orderStatus);
        return addHistoryAgentOrder(entityManager, entityAgentOrder);
    }

    private boolean addHistoryAgentOrder(EntityManager session, EntityHistoryAgentOrder entityAgentOrder) {
        try {
            session.persist(entityAgentOrder);
        } catch (Exception ex) {
            LOGGER.error("Error in addHistoryAgentOrder: ", ex);
            return false;
        }
        return true;
    }

    public List<EntityHistoryAgentOrder> getAgentHistoryOrderList(long agentId, String pivotOrderId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityHistoryAgentOrder> results;
        try {
            results = getAgentHistoryOrderList(entityManager, agentId, pivotOrderId, scroll, limit);
        } finally {
            entityManager.close();
        }
        return results;
    }

    protected ArrayList<EntityHistoryAgentOrder> getAgentHistoryOrderList(EntityManager entityManager, long agentId, String pivotOrderId, Scroll scroll, int limit) {
        try {
            if (pivotOrderId == null) {
                pivotOrderId = "";
            }

            Query query;
            query = (scroll == Scroll.UP) ? entityManager.createNamedQuery("getUpwardHistoryAgentOrderList") : entityManager.createNamedQuery("getDownwardHistoryAgentOrderList");

            query.setParameter("pivotOrderId", pivotOrderId);
            query.setParameter("agentId", agentId);

            ArrayList<EntityHistoryAgentOrder> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (scroll.equals(Scroll.UP)) {
                Collections.reverse(results);
            }
            return results;
        } catch (Exception ex) {
            LOGGER.error("Error in getAgentOrderList: ", ex);
            return null;
        }
    }

}
