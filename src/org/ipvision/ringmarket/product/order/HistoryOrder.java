/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.order;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityHistoryOrder;
import org.ipvision.ringmarket.entities.EntityOrderProduct;
import org.ipvision.ringmarket.repositories.buyer.HistoryOrderRepository;

/**
 *
 * @author Imran
 */
public class HistoryOrder implements HistoryOrderRepository {

    public boolean addNewHistory(String id, long buyerId, long sellerId, long agentId, String productId,
            double offeredPrice, double shipmentCost, long cmpltDate, int status) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addNewHistory(entityManager, id, buyerId, sellerId, agentId, productId, offeredPrice, shipmentCost, cmpltDate, status);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    protected boolean addNewHistory(EntityManager entityManager, EntityOrderProduct iop, long cmpltDate) {
        return addNewHistory(entityManager, iop.getId(), iop.getBuyerId(), iop.getSellerId(), iop.getAgentId(), iop.getProductId(), iop.getOfferedPrice(), iop.getShipmentCost(), cmpltDate, iop.getStatus());
    }

    public boolean removeHistory(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = removeHistory(entityManager, id);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    protected boolean removeHistory(EntityManager entityManager, String id) {
        try {
            entityManager.remove(getHistory(entityManager, id));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public EntityHistoryOrder getHistory(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityHistoryOrder entityHistoryOrder;
        try {
            entityHistoryOrder = entityManager.find(EntityHistoryOrder.class, id);
        } finally {
            entityManager.close();
        }
        return entityHistoryOrder;
    }

    protected EntityOrderProduct getHistory(EntityManager entityManager, String id) {
        return entityManager.find(EntityOrderProduct.class, id);
    }

    protected boolean addNewHistory(EntityManager entityManager, String id, long buyerId, long sellerId, long agentId, String productId,
            double offeredPrice, double shipmentCost, long cmpltDate, int orderStatus) {
        EntityHistoryOrder entityHistoryOrder = new EntityHistoryOrder();
        entityHistoryOrder.setAgentId(agentId);
        entityHistoryOrder.setBuyerId(buyerId);
        entityHistoryOrder.setId(id);
        entityHistoryOrder.setOfferedPrice(offeredPrice);
        entityHistoryOrder.setOrderCompletedDate(cmpltDate);
        entityHistoryOrder.setProductId(productId);
        entityHistoryOrder.setSellerId(sellerId);
        entityHistoryOrder.setShipmentCost(shipmentCost);
        entityHistoryOrder.setOrderStatus(orderStatus);
        try {
            entityManager.persist(entityHistoryOrder);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public List<EntityHistoryOrder> getCompletedOrderByBuyer(long buyerId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityHistoryOrder> results;
        try {
            Query query = entityManager.createNamedQuery("getCompletedOrderByBuyer");
            query.setParameter("buyerId", buyerId);
            results = new ArrayList<>(query.getResultList());
        } finally {
            entityManager.close();
        }
        return results;
    }

    public List<EntityHistoryOrder> getCompletedOrderBySeller(long sellerId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityHistoryOrder> results;
        try {
            Query query = entityManager.createNamedQuery("getCompletedOrderBySeller");
            query.setParameter("sellerId", sellerId);
            results = new ArrayList<>(query.getResultList());
        } finally {
            entityManager.close();
        }
        return results;
    }
}
