/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.order;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.OrderStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityAgentOrder;
import org.ipvision.ringmarket.repositories.agent.AgentOrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class AgentOrder implements AgentOrderRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(AgentOrder.class);

    public boolean addAgentOrder(long agentId, String OrderId, long deliveryDate) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addAgentOrder(entityManager, agentId, OrderId, deliveryDate);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public List<EntityAgentOrder> getAgentOrderList(long agentId, String pivotOrderId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityAgentOrder> results;
        try {
            results = getAgentOrderList(entityManager, agentId, pivotOrderId, scroll, limit);
        } finally {
            entityManager.close();
        }
        return results;
    }

    public boolean addAgentOrder(EntityAgentOrder entityAgentOrder) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addAgentOrder(entityManager, entityAgentOrder);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean deleteAgentOrder(String orderId, OrderStatus orderStatus) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = deleteAgentOrder(entityManager, orderId, orderStatus);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public EntityAgentOrder getOrder(String orderId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityAgentOrder entityAgentOrder;
        try {
            entityAgentOrder = getOrder(entityManager, orderId);
        } finally {
            entityManager.close();
        }
        return entityAgentOrder;
    }

    protected EntityAgentOrder getOrder(EntityManager entityManager, String orderId) {
        return entityManager.find(EntityAgentOrder.class, orderId);
    }

    protected boolean deleteAgentOrder(EntityManager entityManager, String orderId, OrderStatus orderStatus) {
        try {
            EntityAgentOrder entityAgentOrder = getOrder(entityManager, orderId);
            if (entityAgentOrder != null) {
                entityManager.remove(entityAgentOrder);

                HistoryAgentOrder historyAgentOrder = new HistoryAgentOrder();
                historyAgentOrder.addHistoryAgentOrder(entityManager, entityAgentOrder.getAgentId(), entityAgentOrder.getOrderId(), orderStatus);
            } else {
                LOGGER.error("Error in deleteAgentOrder. No such orderId found." + orderId);
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("Error in deleteAgentOrder: ", ex);
            return false;
        }
        return true;
    }

    protected boolean addAgentOrder(EntityManager entityManager, long agentId, String OrderId, long deliveryDate) {
        EntityAgentOrder entityAgentOrder = new EntityAgentOrder();
        entityAgentOrder.setAgentId(agentId);
        entityAgentOrder.setExpDeliveryDate(deliveryDate);
        entityAgentOrder.setOrderId(OrderId);
        return addAgentOrder(entityManager, entityAgentOrder);
    }

    private boolean addAgentOrder(EntityManager session, EntityAgentOrder entityAgentOrder) {
        try {
            session.persist(entityAgentOrder);
        } catch (Exception ex) {
            LOGGER.error("Error in addAgentOrder: ", ex);
            return false;
        }
        return true;
    }

    protected ArrayList<EntityAgentOrder> getAgentOrderList(EntityManager entityManager, long agentId, String pivotOrderId, Scroll scroll, int limit) {
        try {
            if (pivotOrderId == null) {
                pivotOrderId = "";
            }

            Query query;
            query = (scroll == Scroll.UP) ? entityManager.createNamedQuery("getUpwardAgentOrderList") : entityManager.createNamedQuery("getDownwardAgentOrderList");

            query.setParameter("pivotOrderId", pivotOrderId);
            query.setParameter("agentId", agentId);

            ArrayList<EntityAgentOrder> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (scroll.equals(Scroll.UP)) {
                Collections.reverse(results);
            }
            return results;
        } catch (Exception ex) {
            LOGGER.error("Error in getAgentOrderList: ", ex);
            return null;
        }
    }
}
