/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.uniqueid;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alamgir
 */
public class UniqueNumberGenerator {

    private static long sequence = 1;
    private static final File file = new File("productSequencer.txt");
    private static final File APPLICATION_PROPERTIES_FILE = new File("./conf/application.properties");
    private static Scanner input;   
    private static int GENERATOR_ID = 1;
    private static int CLUSTER_ID  = 1;
    private static final Logger LOGGER = LoggerFactory.getLogger(UniqueNumberGenerator.class);

    static {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            input = new Scanner(file);
            sequence = readSequence();
            InputStream inputStream = new FileInputStream(APPLICATION_PROPERTIES_FILE);
            Properties properties = new Properties();
            properties.load(inputStream);
            GENERATOR_ID = Integer.parseInt(properties.getProperty("serverID"));
            CLUSTER_ID = Integer.parseInt(properties.getProperty("clusterID"));
            LOGGER.info("Properties loaded. {Generator ID : "+GENERATOR_ID +", Cluster ID : "+CLUSTER_ID+"}");
        } catch (Exception e) {
           LOGGER.error("Exception in configuration loading.." ,e);
        }
    }

    private static void writeSequence(long sequence) {
        try {
            if (file.exists()) {
                FileWriter fw = new FileWriter(file);
                
                fw.write(sequence + "");
                fw.close();
            }
        } catch (Exception e) {
            LOGGER.error("Exception writing sequence. " , e);
        }
    }
    private static long readSequence() {
        try {
            if (file.exists() && input.hasNextLong()) {
                return input.nextLong();
            }
        } catch (Exception e) {
           LOGGER.error("Exception reading sequence. " , e);
        }
        return 0;
    }

    public static String get() throws GeneratorException {
        IDGenerator generator = LocalUniqueIDGeneratorFactory.generatorFor(GENERATOR_ID, CLUSTER_ID);

        // Generate IDs
        byte[] id = generator.generate();

        BigInteger bi = new BigInteger(1, id);
        return bi.toString(10);
    }

    public static Long generateBarcodeId() {

        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(sequence);
        sequence++;
        writeSequence(sequence);
        
        buffer.array()[3] = (byte) GENERATOR_ID;
        final BigInteger bi = new BigInteger(buffer.array());
        return bi.longValue();

    }
}
