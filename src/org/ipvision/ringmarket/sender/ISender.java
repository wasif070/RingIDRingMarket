/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.sender;

import com.google.gson.JsonObject;

/**
 *
 * @author saikat
 */
public interface ISender {
    public void sendToClient(JsonObject response);
}
