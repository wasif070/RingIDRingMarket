/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author saikat
 */
public enum SortOrderType {
    PRICE_LOW_TO_HIGH(1, "Price Low to High"),
    PRICE_HIGH_TO_LOW(2, "Price High to Low"),
    DATE_ADDED(3, "Date Added(New to Old)"),;

    private int id;

    private String name;

    private SortOrderType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    private static final Map<Integer, SortOrderType> lookup = new HashMap<>();

    static {
        for (SortOrderType item : SortOrderType.values()) {
            lookup.put(item.getId(), item);
        }
    }

    public static SortOrderType getOrderType(int id) {
        return lookup.containsKey(id) ? lookup.get(id) : null;
    }

}
