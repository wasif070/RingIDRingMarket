/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

/**
 *
 * @author saikat
 */
public enum SearchType {
    PRODUCT(1, "Product"),
    CATEGORY(2, "Category"),
    
    
    ;
    
    private final int searchKey;
    private final String searchValue;
    private SearchType(int searchKey , String searchValue){
        this.searchKey = searchKey;
        this.searchValue = searchValue;
    }

    public int getSearchKey() {
        return searchKey;
    }

    public String getSearchValue() {
        return searchValue;
    }
    
    
}
