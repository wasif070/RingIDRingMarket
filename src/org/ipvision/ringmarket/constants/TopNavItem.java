/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author saikat
 */
public enum TopNavItem {
    ALL(0, "All"),
    BRANDS(1, "Brands"),
    TRENDS(2, "Trends"),
    EXCLUSIVE(3, "Exclusive"),
    SALE(4, "Sale"),;

    private final int id;
    private final String name;

    private TopNavItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    private static final Map<Integer, TopNavItem> lookup = new HashMap<>();

    static {
        for (TopNavItem navItem : TopNavItem.values()) {
            lookup.put(navItem.getId(), navItem);
        }
    }

    public static Map<Integer, TopNavItem> getTopNavItemMap() {
        return lookup;
    }

}
