/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Imran
 */
public enum OrderStatus {
//    @SerializedName("0")
    COMPLETED,
//    @SerializedName("1")
    PROCESSING,
//    @SerializedName("2")
    SHIPMENTED,
    CANCELLED
}
