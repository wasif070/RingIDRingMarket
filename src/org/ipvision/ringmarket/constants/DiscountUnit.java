/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author saikat
 */
public enum DiscountUnit {
    PERCENTAGE((byte) 1),
    CURRENCY((byte) 2),;

    private final byte value;

    private DiscountUnit(byte value) {
        this.value = value;
    }

    private static final Map<Byte, DiscountUnit> lookup = new HashMap<>();

    static {
        for (DiscountUnit page : DiscountUnit.values()) {
            lookup.put(page.getValue(), page);
        }
    }

    public byte getValue() {
        return value;
    }

    /**
     *
     * @param id
     * @return
     */
    public static DiscountUnit getDiscountUnit(byte id) {
        return lookup.get(id);
    }
}
