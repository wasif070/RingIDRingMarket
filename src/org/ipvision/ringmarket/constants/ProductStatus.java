/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Imran
 */
public enum ProductStatus implements Serializable {
    DELETED((byte) 0),
    AVAILABLE((byte) 1),
    SOLD_OUT((byte) 2),
    UNAVAILABLE((byte) 3),
    AWAITING_FOR_APPROVAL((byte) 4),
    DISABLED((byte) 5),;

    private final byte value;

    private static final Map<Byte, ProductStatus> lookup = new HashMap<>();

    static {
        for (ProductStatus status : ProductStatus.values()) {
            lookup.put(status.getValue(), status);
        }
    }

    private ProductStatus(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }
    
    public static ProductStatus getStatus(byte value){
        return lookup.get(value);
    }

}
