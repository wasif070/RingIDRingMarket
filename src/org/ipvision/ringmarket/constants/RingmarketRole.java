/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Imran
 */
public enum RingmarketRole {
//    @SerializedName("0")
    BUYER,
//    @SerializedName("1")
    SELLER,
//    @SerializedName("2")
    AGENT
}
