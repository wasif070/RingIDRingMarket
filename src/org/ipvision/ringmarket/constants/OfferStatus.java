/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Imran
 */
public enum OfferStatus {
//    @SerializedName("0")
    PROCESSING,
//    @SerializedName("1")
    ACCEPTED,
//    @SerializedName("2")
    REJECTED,
//    @SerializedName("3")
    CANCELLED,
//    @SerializedName("4")
    ORDERED;
}
