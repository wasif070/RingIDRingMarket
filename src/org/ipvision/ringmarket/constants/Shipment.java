/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Imran
 */
public enum Shipment {
    HOME_DELIVERY((byte) 1),
    PICKUP_FROM_STORE((byte) 2),
    FREE_DELIVERY((byte) 3);

    private final byte value;

    private Shipment(byte action) {
        this.value = action;
    }

    private static final Map<Byte, Shipment> lookup = new HashMap<>();

    public byte getValue() {
        return value;
    }

    static {
        for (Shipment shipment : Shipment.values()) {
            lookup.put(shipment.getValue(), shipment);
        }
    }

    public static Shipment getMethod(Byte value){
        return lookup.get(value);
    }

}
