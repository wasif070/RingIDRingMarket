/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

/**
 *
 * @author saikat
 */
public class ShopType {
    public static final int GENERAL_STORE = 0;
    public static final int BRANDED_STORE = 1;
    public static final int CELEBRITY_STORE = 2;
}
