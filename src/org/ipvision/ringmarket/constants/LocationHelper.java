/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

/**
 *
 * @author alamgir
 */
public class LocationHelper {

    /**
     * to obtain a coordinate in respect of a center point, distance from the point with an angle 
     * @param centerPoint (center point)
     * @param radius in meter
     * @param degree
     * @return 
     */
    public static LatLon getRadialCoordinate(LatLon centerPoint, double radius, double degree) {
        double EarthRadius = 6371000; // m

        double latA = Math.toRadians(centerPoint.x);
        double lonA = Math.toRadians(centerPoint.y);
        double angularDistance = radius / EarthRadius;
        double trueCourse = Math.toRadians(degree);

        double lat = Math.asin(
                Math.sin(latA) * Math.cos(angularDistance)
                + Math.cos(latA) * Math.sin(angularDistance)
                * Math.cos(trueCourse));

        double dlon = Math.atan2(
                Math.sin(trueCourse) * Math.sin(angularDistance)
                * Math.cos(latA),
                Math.cos(angularDistance) - Math.sin(latA) * Math.sin(lat));

        double lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

        lat = Math.toDegrees(lat);
        lon = Math.toDegrees(lon);

        LatLon newPoint = new LatLon((float) lat, (float) lon);

        return newPoint;

    }

    /**
     * is given point is as close as radius in respect of a center point?
     * @param center
     * @param pointForCheck
     * @param radius
     * @return 
     */
    public static boolean isCoordinateInsideCircle( LatLon center, LatLon pointForCheck, double radius) {
        return getDistanceBetweenTwoPoints(pointForCheck, center) <= radius;
    }

    /**
     * distance between two points p1 and p2
     * @param p1
     * @param p2
     * @return 
     */
    private static double getDistanceBetweenTwoPoints(LatLon p1, LatLon p2) {
        double R = 6371000; // m
        double dLat = Math.toRadians(p2.x - p1.x);
        double dLon = Math.toRadians(p2.y - p1.y);
        double lat1 = Math.toRadians(p1.x);
        double lat2 = Math.toRadians(p2.x);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2)
                * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;

        return d;
    }

    /**
     * lat, lon property class
     */
    public static class LatLon {
        public LatLon(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public float x;
        public float y;

    }
}
