/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

/**
 *
 * @author Saikat
 */
public class MenuType {

    public static final int BRANDS = 1;
    public static final int TRENDS = 2;
    public static final int HOT_DEALS = 3;
    public static final int EXCLUSIVE = 4;
    public static final int SHOP = 5;
}
