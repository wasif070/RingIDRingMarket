/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author saikat
 */
public enum MarketOrderStatus {

    PAYMENT_INCOMPLETE((byte) 0, "This %s is awaiting your payment"),
    PAYMENT_COMPLETE((byte) 1, "Payment Completed for this %s"),
    PROCESSING((byte) 2, "Seller is processing your %s"),
    ON_SHIPMENT((byte) 3, "Seller has shipped your %s"),
    SHIPMENT_COMPLETED((byte) 4, "This %s has been delivered"),
    SHIPMENT_RECEIVED((byte) 5, "This %s has been received"),
    DISPUTE((byte) 6, "This %s has been disputed"),
    CANCELLED((byte) 7, "This %s has been cancelled"),
    RETURNED((byte) 8, "This %s has been returned"),
    COMPLETED((byte) 9, "Finished"),
    
    
    ;

    private final byte value;
    private final String message;

    private MarketOrderStatus(byte value, String message) {
        this.value = value;
        this.message = message;
    }

    private static final Map<Byte, MarketOrderStatus> lookup = new HashMap<>();

    static {
        for (MarketOrderStatus status : MarketOrderStatus.values()) {
            lookup.put(status.getValue(), status);
        }
    }

//    public static final class ItemStatus {
//
//        public static byte ORDERED = 2;
//        public static byte PROCESSING = 3;
//        public static byte ON_SHIPMENT = 4;
//        public static byte SHIPMENT_COMPLETED = 5;
//        public static byte SHIPMENT_RECEIVED = 6;
//        public static byte DISPUTE = 7;
//        public static byte CANCELED = 8;
//        public static byte RETURNED = 9;
//    }
    public String getMessage() {
        return String.format(message, "order");
    }

    public String getMessage(String name) {
        return String.format(message, name);
    }

    public byte getValue() {
        return value;
    }

    public static MarketOrderStatus getStatus(byte value) {
        return lookup.get(value);
    }

}


