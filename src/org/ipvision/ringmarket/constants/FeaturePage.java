/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author saikat
 */
public enum FeaturePage {
    BASKET(1, "Celebrity's Collection"),
    STORE_LIVE(2, "Store"),
    HOT_DEALS(3, "Hot Deals"),
    STORES(4, "Stores"),
    CELEBRITY_STORES(5, " Celebrity Stores"),
    ;

    private final int id;

    private final String name;

    private FeaturePage(int id, String name) {
        this.id = id;
        this.name = name;
    }

     private static final Map<Integer, FeaturePage> lookup = new HashMap<>();

    static {
        for (FeaturePage page : FeaturePage.values()) {
            lookup.put(page.getId(), page);
        }
    }
    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public static FeaturePage getPage(int id){
        return lookup.get(id);
    }
}
