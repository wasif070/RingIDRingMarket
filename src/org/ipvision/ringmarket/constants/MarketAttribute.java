/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

/**
 *
 * @author Imran
 */
public class MarketAttribute {

    public static final String ACTION = "actn";
    public static final String ID = "id";
    public static final String NAME = "nm";
    public static final String STATUS = "sts";
    public static final String BASE_PRICE = "bsPrc";
    public static final String IS_FIXED_PRICE = "isFxdPrc";
    public static final String LON = "lon";
    public static final String LAT = "lat";
    public static final String CATEGORY_ID = "catId";
    public static final String CATEGORY_IDS = "catIds";
    public static final String CATEGORY_NAME = "catNm";
    public static final String VIEW_COUNT = "vwCnt";
    public static final String IS_EXCLUSIVE = "isExclsv";
    public static final String IS_TRENDING = "isTnd";
    public static final String IMAGE_URL = "imgUrl";
    public static final String OLD_IMAGE_URL = "oImgUrl";
    public static final String IMAGE_GALLERY = "imgGlry";
    public static final String IMAGE_HEIGHT = "imgH";
    public static final String IMAGE_WIDTH = "imgW";
    public static final String IMAGE_X = "imgX";
    public static final String IMAGE_Y = "imgY";
    public static final String AGENT_ID = "agntId";
    public static final String PRODUCT_ID = "prodId";
    public static final String EXPECTED_DELIVERY_DATE = "expDlvDt";
    public static final String EMAIL = "el";
    public static final String MOBILE_DIALING_CODE = "mblDc";
    public static final String MOBILE = "mbl";
    public static final String CONTACT_NAME = "cName";
    public static final String STREET = "street";
    public static final String APARTMENT = "apt";
    public static final String CITY = "cty";
    public static final String COUNTRY = "cnty";
    public static final String POST_CODE = "pCode";
    public static final String STATE = "state";
    public static final String OFFERED_PRICE = "ofrPrc";
    public static final String BUYER_ID = "byrId";
    public static final String SELLER_ID = "slrId";
    public static final String ADDED_DATE = "addDt";
    public static final String PARENT_ID = "prntId";
    public static final String POSITION = "pos";
    public static final String ORDINAL = "ord";
//    public static final String LIKE_COUNT = "lkCnt";
//    public static final String COMMENT_COUNT = "cmntCnt";
    public static final String DESCRIPTION = "desc";
    public static final String VIDEO_URL = "vdoUrl";
    public static final String THUMB_IMAGE_URL = "thmUrl";
    public static final String THUMB_IMAGE_WIDTH = "thmW";
    public static final String THUMB_IMAGE_HEIGHT = "thmH";
    public static final String ADDRESS = "addr";
    public static final String ADDRESS_ID = "addrId";
    public static final String DIALING_CODE = "mblDc";
    public static final String ROLE = "rl";
    public static final String PHONE_NO = "mbl";
    public static final String ABOUT_ME = "abtMe";
    public static final String PHOTO = "photo";
    public static final String RATING = "rat";
    public static final String TOTAL_RATING = "ttlRat";
    public static final String NUMBER_OF_RATERS = "nmbrOfRtrs";
    public static final String RECEIVABLE_DAY_LIMIT = "rcvDayLt";
    public static final String OFFERED_DATE = "ofrdDt";
    public static final String SHIPMENT_METHOD = "shpmntMtd";
    public static final String ORDER_DATE = "ordrDt";
    public static final String SHIPMENT_COST = "shpmntCst";
    public static final String ORDER_COMPLETED_DATE = "ordrCmpDt";
    public static final String SOLD_PRICE = "sldPrc";
    public static final String ORDER_ID = "ordrId";
    public static final String ORDER_STATUS = "ordrSts";
    public static final String BUYER_NAME = "byrNm";
    public static final String PRODUCT_NAME = "prodNm";
    public static final String REPORT_CAUSE = "rprtCs";
    public static final String ADDITIONAL_DESCRIPTION = "addDesc";
    public static final String REPORT_COUNT = "rprtCnt";
    public static final String MENU_TYPES = "mTps";

    public static final String LIMIT = "lmt";
    public static final String MENUS = "menus";
    public static final String MENU_ITEMS = "mItms";
    public static final String SUCCESS = "sucs";
    public static final String MESSAGE = "mg";
    public static final String FEATURES = "ftrs";
    public static final String SHOP_DETAILS = "shpDtls";
    public static final String OLD_PRICE = "oPrc";
    public static final String NEW_PRICE = "nPrc";
    public static final String PRICE = "Prc";
    public static final String QUANTITY = "qntity";
    public static final String STOCK_QUANTITY = "stkQ";
    public static final String SHOP_ID = "shpId";
    public static final String SHOP_NAME = "shpNm";
    public static final String STATUS_MESSAGE = "stsMg";
    public static final String TYPE = "type";
    public static final String NAV_ID = "nvId";
    public static final String ITEMS = "items";
    public static final String PACKET_ID = "pckId";
    public static final String DISCOUNT = "dscnt";
    public static final String DISCOUNT_AVALIABLE = "dscntA";
    public static final String DISCOUNT_UNIT = "dscntU";
    public static final String FEATURE_PAGES = "ftrPgs";
    public static final String SCROLL = "scl";
    public static final String PRODUCT_LIST = "prodLst";
    public static final String USER_ID = "utId";
    public static final String RING_ID = "rid";
    public static final String FRIEND_USER_ID = "futId";
    public static final String CURRENCY_CODE = "curCd";
    public static final String CREATION_TIME = "crt";
    public static final String VALIDITY_TIME = "vldt";
    public static final String ACTIVE_TIME = "acvt";
    public static final String ADDED_TIME = "addtm";
    public static final String PAYMENT_TIME = "paytm";
    public static final String SHIPMENT_TIME = "shiptm";
    public static final String PROTECTION_DURATION = "protctDn";
    public static final String CANCEL_DURATION = "cnclDn";
    public static final String FEEDBACK_DURATION = "fdbkDn";
    public static final String UPDATED_TIME = "ut";
    public static final String ITEM_COUNT = "itmCnt";
    public static final String ORDER_TYPE = "ordrT";
    public static final String ORDER_MAP = "ordrMp";
    public static final String BASKET_ID = "bsktId";
    public static final String START = "st";
    public static final String CATEGORIES = "cats";
    public static final String REASON_CODE = "rc";
    public static final String IDS = "ids";
    public static final String INVOICE_ID = "ivcId";
    public static final String LIST = "lst";
    public static final String PIVOT_STRING = "pivotStr";
    public static final String PIVOT_NUMBER = "pivotNum";
    public static final String USER_TYPE = "uTp";
    public static final String DETAILS = "dtls";
    public static final String IS_DEFAULT = "isDflt";
    public static final String COUNT = "cnt";
    public static final String PAGE_ID = "pgId";
    public static final String PAYMENT_METHOD = "payMtd";
    public static final String ITEM_STATUS = "itmSts";
    public static final String CARRIER_ID = "carr_id";
    public static final String DTO = "dto";
    public static final String CREDITED_AMOUNT = "crdAmnt";
    public static final String CURRENCY_ISO = "curnIso";
    public static final String CODE = "cd";
    public static final String IS_ENABLE = "enbl";
    public static final String SEARCH_PARAM = "schP";
    public static final String OWNER_ID = "ownrId";
    public static final String MINIMUM_ORDER = "mnOrdr";
    public static final String MAXIMUM_DISCOUNT_AMOUNT = "mxDscnt";
    public static final String IS_REDEEM_ALLOWED = "rdmAld";
    public static final String REFERRER_ID = "rfrId";

}
