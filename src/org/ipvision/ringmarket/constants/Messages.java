/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.constants;

/**
 *
 * @author saikat
 */
public class Messages {
    public static final String NO_MORE_DATA = "NO MORE DATA";
    public static final String NO_MORE_STORE = "NO MORE STORES";
    public static final String NO_MORE_BRAND = "NO MORE BRANDS";
    public static final String NO_MORE_ITEMS = "NO MORE ITEMS";
    public static final String NO_DATA_FOUND = "NO DATA FOUND";
    public static final String OPERATION_FAILED = "Operation Falied";
    public static final String UPDATE_FAILED = "Update Falied!";
}
