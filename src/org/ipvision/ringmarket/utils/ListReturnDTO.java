package org.ipvision.ringmarket.utils;

import java.util.Collections;
import java.util.List;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.entities.product.EntityProductStock;

/**
 *
 * @param <D> Type of Data Object
 */
public class ListReturnDTO<D> {

    private int rc = ReasonCode.NONE;
    private List<D> ls = Collections.emptyList();

    public int getReasonCode() {
        return rc;
    }

    public void setReasonCode(int reasonCode) {
        this.rc = reasonCode;
    }

    public List<D> getList() {
        return ls;
    }

    public void setList(List<D> list) {
        this.ls = list;
    }
}
