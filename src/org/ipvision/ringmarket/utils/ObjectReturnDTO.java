package org.ipvision.ringmarket.utils;

import org.ipvision.ringmarket.constants.ReasonCode;

/**
 *
 * @author saikat
 * @param <T>
 */
public class ObjectReturnDTO<T> {

    private int rc = ReasonCode.NONE;
    private T value;

    public int getReasonCode() {
        return rc;
    }

    public void setReasonCode(int reasonCode) {
        this.rc = reasonCode;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
