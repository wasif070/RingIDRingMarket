/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author bose
 */
public interface JpaConverterJson {

    public static final Type MAP_TYPE = new TypeToken<LinkedHashMap<String, Object>>(){}.getType();
    public static final Gson GSON = new Gson();
    public static final Logger LOGGER = LoggerFactory.getLogger(JpaConverterJson.class);
    
    static String convertToDatabaseColumn(Object meta) {
        try {
//            JSONObject object = new JSONObject((LinkedHashMap) meta);
//            return object.toString();
              return GSON.toJson((LinkedHashMap) meta);
        } catch (Exception ex) {
            LOGGER.error("Convert to database column error . "+ex.getMessage() ,ex);
            return null;
        }
    }

    static Object convertToEntityAttribute(String dbData) {
        try {
//            JSONObject object = new JSONObject(dbData);
//            return object.toMap();
            LinkedHashMap fromJson = GSON.fromJson(dbData, LinkedHashMap.class);
            return fromJson;
        } catch (JsonSyntaxException ex) {
            LOGGER.error("ConvertToEntityAttribute Error. column data="+dbData , ex);
            return new LinkedHashMap<>();
        }
    }

}
