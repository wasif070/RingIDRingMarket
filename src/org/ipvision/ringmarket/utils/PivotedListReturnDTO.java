package org.ipvision.ringmarket.utils;

/**
 *
 * @author user
 * @param <D> Type of Data Object
 * @param <P> Type of Pivot
 */
public class PivotedListReturnDTO<D, P> extends ListReturnDTO<D> {

    private P pvt;

    public P getPivot() {
        return pvt;
    }

    public void setPivot(P pivot) {
        this.pvt = pivot;
    }
}
