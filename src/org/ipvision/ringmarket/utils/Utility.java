/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.utils;

import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author user
 */
public final class Utility {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utility.class);
    public static final String EMPTY_DB_STRING = "''";
    
     private static final Comparator<EntityCategory> CATEGORY_ORDER_COMPARATOR = Comparator.comparingInt(EntityCategory::getOrdinal).reversed();

    public static Comparator<EntityCategory> getCategoryOrderCompartor() {
        return CATEGORY_ORDER_COMPARATOR;
    }
     
    public static long ipToLong(String ip_address) {
        long result = 0;
        String[] ip_parts = ip_address.split("\\.");
        for (int i = 3; i > 0; i--) {
            long ip = Long.parseLong(ip_parts[3 - i]);
            result |= ip << (i * 8);
        }
        long ip = Long.parseLong(ip_parts[3]);
        result |= ip;

        return result;
    }

    public static long dateToLong(String date, String separator) {
        String[] dmy = date.split(separator);
        int year = Integer.parseInt(dmy[0]);
        int month = Integer.parseInt(dmy[1]);
        int day = Integer.parseInt(dmy[2]);

        long dat = new GregorianCalendar(year, month - 1, day).getTimeInMillis();
        return dat;
    }

    public static String longToDate(long date, String separator) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy" + separator + "MM" + separator + "dd");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static String longToDateHHMMSS(long date, String separator) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy" + separator + "MM" + separator + "dd HH:mm:ss a");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static long dateTimeToLong(String dateTime) {
        long dateTimeLong = 1517421599000L;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        //String dateInString = "31-01-2018 23:59:59";
        try {
            Date date = formatter.parse(dateTime);
            dateTimeLong = date.getTime();
        } catch (ParseException ex) {
           LOGGER.error("dateTimeToLong-->" + ex.getMessage());
        }
        return dateTimeLong;
    }

    public static int getDay(long long_date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(long_date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static int getMinute() {
        Calendar calendar = new GregorianCalendar();
        return calendar.get(Calendar.MINUTE);
    }

    public static String removeBracket(String str) {
        if (str != null && str.length() > 1) {
            StringBuilder sb = new StringBuilder(str);
            sb.deleteCharAt(0);
            sb.deleteCharAt(sb.length() - 1);
            return sb.toString();
        }
        return str;
    }

    public static String listToString(List<Long> list) {
        StringBuilder sb = new StringBuilder();
        for (long s : list) {
            sb.append(",");
            sb.append(s);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(0);
            return new String(sb);
        }
        return "-1";
    }

    public static String IntegerListToString(List<Integer> list) {
        StringBuilder sb = new StringBuilder();
        for (Integer s : list) {
            sb.append(",");
            sb.append(s);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(0);
            return new String(sb);
        }
        return "-1";
    }
    
    public static String ByteListToString(Collection<Byte> list) {
        StringBuilder sb = new StringBuilder();
        for (Byte s : list) {
            sb.append(",");
            sb.append(s);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(0);
            return new String(sb);
        }
        return "-1";
    }

    public static String listToString(Set<Long> list) {
        StringBuilder sb = new StringBuilder();
        for (long s : list) {
            sb.append(",");
            sb.append(s);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(0);
            return new String(sb);
        }
        return "-1";
    }

    public static String stringListToString(Collection<String> list) {
        StringBuilder sb = new StringBuilder();
        for (String s : list) {
            sb.append(",'");
            sb.append(s);
            sb.append("'");
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(0);
            return new String(sb);
        }
        return "-1";
    }

    public static String uuidListToString(List<UUID> list) {
        StringBuilder sb = new StringBuilder();
        for (UUID s : list) {
            sb.append(",'");
            sb.append(s.toString());
            sb.append("'");
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(0);
            return new String(sb);
        }
        return "-1";
    }

    public static ArrayList<Long> setToList(Set<Long> list) {
        ArrayList<Long> returnList = new ArrayList<>();
        for (Long value : list) {
            returnList.add(value);
        }
        return returnList;
    }

    public static String getTime(long date) {
        String s[] = longToDateHHMMSS(date, "/").split(" ");
        return s[1];
    }

    public static long getTimeInLong(String datetime) {
        String s[] = datetime.split(":");
        return ((Long.parseLong(s[0]) * 60 * 60) + Long.parseLong(s[1]) * 60 + Long.parseLong(s[2])) * 1000;
    }

    public static String maskedValue(String originalValue, int startIndex, int length) {
        if (originalValue == null
                || originalValue.isEmpty()
                || startIndex < 0
                || length <= 0
                || startIndex >= originalValue.length()
                || length > originalValue.length()) {
            return originalValue;
        }

        StringBuilder builder = new StringBuilder(originalValue);
        for (int i = startIndex; i < length; i++) {
            if (builder.charAt(i) != '@') {
                builder.setCharAt(i, '*');
            }
        }

        return builder.toString();
    }
}
