/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.utils;

import com.google.gson.JsonElement;

/**
 *
 * @author saikat
 */
public final class JsonValidator {

    public static boolean hasElement(JsonElement jsonElement) {
        return jsonElement != null && !jsonElement.isJsonNull();
    }
    
    public static boolean hasValidString(JsonElement jsonElement){
        return hasElement(jsonElement) && !jsonElement.getAsString().trim().isEmpty();
    }
    
    public static boolean hasPositiveInteger(JsonElement jsonElement){
        return hasElement(jsonElement) && jsonElement.getAsInt() > 0;
    }
    
    public static boolean hasPositiveLong(JsonElement jsonElement){
        return hasElement(jsonElement) && jsonElement.getAsLong() > 0;
    }
    public static boolean hasPositiveDouble(JsonElement jsonElement){
        return hasElement(jsonElement) && jsonElement.getAsDouble()> 0;
    }
   
}
