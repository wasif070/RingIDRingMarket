/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.apache.openjpa.persistence.OpenJPAEntityManagerFactory;
import org.apache.openjpa.persistence.OpenJPAPersistence;
import org.apache.openjpa.persistence.QueryResultCache;
import org.apache.openjpa.persistence.StoreCache;

/**
 *
 * @author alamgir
 */
public class DBConnection {

    private static DBConnection instance;

    private static final Properties PROPS = new Properties();
    private static EntityManagerFactory entityManagerFactory;
    private static ExecutorService asynRequestExecutor;
    public static final boolean CACHEABLE = true;
    private static OpenJPAEntityManagerFactory jpaFactory;

    public static final int BATCH_SIZE = 50;

    private DBConnection() {
        try {
            InputStream is = new FileInputStream(new File("./conf/DBConfig.properties"));
            PROPS.load(is);
            String loadBalanceBlacklistTimeout = "";
            if (PROPS.getProperty("loadBalanceBlacklistTimeout") != null) {
                loadBalanceBlacklistTimeout = "&loadBalanceBlacklistTimeout=" + PROPS.getProperty("loadBalanceBlacklistTimeout");
            }
            String useUnicode = "&useUnicode=" + PROPS.getProperty("useUnicode");
            String characterEncoding = "&characterEncoding=" + PROPS.getProperty("characterEncoding");
            String connectTimeout = "&connectTimeout=" + PROPS.getProperty("connectTimeout");
            String socketTimeout = "&socketTimeout=" + PROPS.getProperty("socketTimeout");
            String timeZone = "&serverTimezone=UTC";

            String dbConnectURL = "jdbc:mysql://%s/%s?autoReconnect=true";
            Map<String, Object> dbConfig = new HashMap();
            String hostName = PROPS.get("host").toString().split(",")[0];
            dbConnectURL = String.format(dbConnectURL, hostName, PROPS.get("database"));

            dbConnectURL += loadBalanceBlacklistTimeout + useUnicode + characterEncoding + connectTimeout + socketTimeout + timeZone;

//            String connectionProperties = "DriverClassName=com.mysql.cj.jdbc.Driver,"
//                    + "Url=%s," 
//                    + "MaxActive=1,"
//                    + "MaxIdle=1,"
//                    + "MinIdle=1,"
//                    + "MaxWait=5000," 
//                    + "TestOnBorrow=true," 
//                    + "Username=%s," 
//                    + "Password=%s";
//            
//            connectionProperties = String.format(connectionProperties, dbConnectURL, props.get("user"), props.get("password"));
//            dbConfig.put("openjpa.ConnectionProperties", connectionProperties);
//            
            dbConfig.put("openjpa.ConnectionUserName", PROPS.get("user"));
            dbConfig.put("openjpa.ConnectionPassword", PROPS.get("password"));
            dbConnectURL = String.format(dbConnectURL, PROPS.get("host"), PROPS.get("database"));

            dbConnectURL += loadBalanceBlacklistTimeout + useUnicode + characterEncoding + connectTimeout + socketTimeout;

            dbConfig.put("openjpa.ConnectionURL", dbConnectURL);

            entityManagerFactory = Persistence.createEntityManagerFactory("ringmarket", dbConfig);
            jpaFactory = OpenJPAPersistence.cast(entityManagerFactory);

            asynRequestExecutor = Executors.newFixedThreadPool(50);
        } catch (IOException ex) {
        }
    }

    public static DBConnection getInstance() {
        if (instance == null) {
            instance = new DBConnection();
        }
        return instance;
    }

//    public Session getSession() {
//        Session session = factory.getSession();
//        return session;
//    }
    public EntityManager getEntityManager() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
       
        return entityManager;
    }

    public void closeFactory() {
        entityManagerFactory.close();
    }

    public StoreCache getCache() {
        return jpaFactory.getStoreCache();
    }

    public QueryResultCache getQueryCache() {
        return jpaFactory.getQueryResultCache();
    }

    public void submitAsyncJob(Runnable runnable) {
        asynRequestExecutor.execute(runnable);
    }
}
