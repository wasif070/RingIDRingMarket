/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.io;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.apache.mina.core.session.IoSession;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.controller.RequestController;
import org.ipvision.ringmarket.controller.action.Action;
import org.ipvision.ringmarket.sender.ISender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Wasif
 */
public class RequestProcessor extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(RequestProcessor.class);
    private volatile  boolean running;
    private final RequestController controller;
    private final ConnectionPool connectionPool;

    public RequestProcessor(RequestController controller, ConnectionPool connectionPool) {
        this.running = true;
        this.controller = controller;
        this.connectionPool = connectionPool;
    }

    @Override
    public void run() {
        LinkedBlockingQueue<Message> receiveMsgQueue = StorageQueue.getInstance().getReceiveMessageQueue();
        JsonParser parser = new JsonParser();
        while (running) {
            while (running && !receiveMsgQueue.isEmpty()) {
                Message msg = receiveMsgQueue.poll();
                if (msg != null) {
                    try {
                        JsonObject json = parser.parse(new String((byte[]) msg.getMessage())).getAsJsonObject();
                        if(json.has(MarketAttribute.USER_ID)){
                            msg.setRequesterId(json.get(MarketAttribute.USER_ID).getAsLong());
                        }
                        logger.debug("request : " + json.toString());
                        msg.setExecStarted(System.currentTimeMillis());

                        controller.getResponse(json, true, new TCPSender(msg, connectionPool));
                    } catch (Exception e) {
                        logger.error("Exception in RequestProcessor-->", e);
                    }
                }
            }

            try {
                TimeUnit.MILLISECONDS.sleep(50);
            } catch (InterruptedException iex) {
                logger.error("Thread Exception in RequestProcessor-->", iex);
            }
        }
    }

    public void stopService() {
        try {
            running = false;
            this.join();
        } catch (InterruptedException ex) {
            logger.error("Thread Exception in RequestProcessor-->", ex);
        }
    }

    private class TCPSender implements ISender {

        private final Message msg;
        private final ConnectionPool connectionPool;

        public TCPSender(Message msg, ConnectionPool connectionPool) {
            this.msg = msg;
            this.connectionPool = connectionPool;
        }

        /*@Override
        public void sendToClient(String src_pck_id, byte[] response) {
            IoSession session = connectionPool.getActiveSession(msg);
            if (session != null) {
                byte[] market_bytes = new byte[response.length + src_pck_id.length() + 1];
                market_bytes[0] = (byte) src_pck_id.length();
                System.arraycopy(src_pck_id.getBytes(), 0, market_bytes, 1, src_pck_id.length());
                System.arraycopy(response, 0, market_bytes, src_pck_id.length() + 1, response.length);

                logger.info("[TCPSender] response : " + new String(response) );
                session.write(market_bytes);
            }
        }*/
        @Override
        public void sendToClient(JsonObject response) {
            IoSession session = connectionPool.getActiveSession(msg);
            if (session != null) {
                String res = response.toString();
                session.write(res.getBytes());

                logger.debug("[TCPSender] response : " + res);
                int action = 0;
                String packetId = "";

                if (response.has(MarketAttribute.ACTION)) {
                    action = response.get(MarketAttribute.ACTION).getAsInt();
                }
                if (response.has(MarketAttribute.PACKET_ID)) {
                    packetId = response.get(MarketAttribute.PACKET_ID).getAsString();
                }

                String actionName = "INVALID_ACTION";
                if(Action.getAction(action) != null){
                    actionName = Action.getAction(action).toString();
                }
                logger.info(String.format("USER : %7d, Exec Time - %3d, ACTION : %s(%d), Pkt %s", msg.getRequesterId(), (System.currentTimeMillis() - msg.getExecStarted()), actionName , action, packetId));

            }
        }

    }
}
