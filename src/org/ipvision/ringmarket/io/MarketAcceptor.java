/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.filter.executor.ExecutorFilter;
import org.apache.mina.filter.executor.OrderedThreadPoolExecutor;
import org.apache.mina.transport.socket.SocketAcceptor;
import org.apache.mina.transport.socket.SocketSessionConfig;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.ipvision.ringmarket.controller.RequestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Wasif
 */
public class MarketAcceptor {

    private static MarketAcceptor instance;
    //private IoAcceptor acceptor;
    private SocketAcceptor acceptor;
    private ConnectionPool connectionPool;

    private int START_PORT;
    private int END_PORT;

    private final int BUFFER_SIZE = 8192;
    private RequestProcessor requestProcessor;
    private static final Logger LOG = LoggerFactory.getLogger(MarketAcceptor.class);
    private OrderedThreadPoolExecutor executor;
    private final ThreadFactory THREAD_FACTORY;
    private boolean isBind = false;
    private List<InetSocketAddress> socketList = new ArrayList<>();

    public static MarketAcceptor getInstance() {
        if (instance == null) {
            instance = new MarketAcceptor();
        }
        return instance;
    }

    public MarketAcceptor() {

        this.connectionPool = new ConnectionPool();
        this.THREAD_FACTORY = new ThreadFactory() {
            @Override
            public Thread newThread(final Runnable r) {
                return new Thread(null, r, "MinaThread", 64 * 1024);
            }
        };

        InputStream is = null;
        try {
            is = new FileInputStream(new File("./conf/application.properties"));
            Properties props = new Properties();
            props.load(is);
            String portRange = props.getProperty("port");

            String ports[] = portRange.split(":");
            START_PORT = Integer.parseInt(ports[0]);
            END_PORT = Integer.parseInt(ports[1]);

            for (int i = START_PORT; i <= END_PORT; i++) {
                socketList.add(new InetSocketAddress(i));
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void startProcess(RequestController requestController) {
        try {
            this.requestProcessor = new RequestProcessor(requestController, this.connectionPool);
            this.requestProcessor.start();

            this.executor = new OrderedThreadPoolExecutor(0, 1000, 10, TimeUnit.SECONDS, THREAD_FACTORY);

            this.acceptor = new NioSocketAcceptor(Runtime.getRuntime().availableProcessors() + 1);
            this.acceptor.setReuseAddress(true);
            this.acceptor.getSessionConfig().setReadBufferSize(BUFFER_SIZE);
            this.acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 10);
            ((SocketSessionConfig) this.acceptor.getSessionConfig()).setReuseAddress(true);
            ((SocketSessionConfig) this.acceptor.getSessionConfig()).setTcpNoDelay(true);
            ((SocketSessionConfig) this.acceptor.getSessionConfig()).setKeepAlive(false);

            this.acceptor.setHandler(new SessionHandler(this.connectionPool));

            this.acceptor.getFilterChain().addLast("threadPool", new ExecutorFilter(executor));
            this.acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new ObjectSerializationCodecFactory()));

            new Thread(() -> {
                bind();
            }).start();

        } catch (Exception ex) {
            LOG.error(MarketAcceptor.class.getName() + " Exception in startProcess-->", ex);
        }
    }

    private void bind() {
        while (!isBind && !this.acceptor.isActive()) {
            try {
                this.acceptor.bind(socketList);
                this.isBind = true;
            } catch (IOException ex) {
                try {
                    Thread.sleep(5000);
                    LOG.error(MarketAcceptor.class.getName() + " Exception in bind-->", ex);
                } catch (InterruptedException e) {
                    LOG.error(MarketAcceptor.class.getName() + " InterruptedException in bind-->", e);
                }
            }
        }
        LOG.debug(MarketAcceptor.class.getName() + " bind success");
    }

    public void stop() {
        if (acceptor != null) {
            try {
                for (IoSession session : acceptor.getManagedSessions().values()) {
                    session.closeNow();
                }
                this.isBind = true;
                if (!socketList.isEmpty()) {
                    this.acceptor.unbind(socketList);
                } else {
                    acceptor.unbind();
                }
                acceptor.dispose(true);
                while (!acceptor.isDisposed()) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        LOG.error(MarketAcceptor.class.getName() + "->stop InterruptedException ex-->" + ex.getMessage());
                    }
                }
                executor.shutdownNow();

                requestProcessor.stopService();

                System.out.println(MarketAcceptor.class.getName() + " shutdown successfully!");
                LOG.debug(MarketAcceptor.class.getName() + " shutdown successfully!");
            } catch (Exception ex) {
                LOG.error(MarketAcceptor.class.getName() + " Ex in stop-->", ex);
            }
        }
    }

}
