/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.io;

import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author Wasif
 */
public class StorageQueue {

    private static StorageQueue instance;
    private LinkedBlockingQueue<Message> rcvMsgQ;
    private LinkedBlockingQueue<Message> resMsgQ;

    public static StorageQueue getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new StorageQueue();
        }
    }

    private StorageQueue() {
        rcvMsgQ = new LinkedBlockingQueue<>();
        resMsgQ = new LinkedBlockingQueue<>();
    }

    public LinkedBlockingQueue<Message> getReceiveMessageQueue() {
        return rcvMsgQ;
    }

    public void setReceiveMessageQueue(LinkedBlockingQueue<Message> rcvMsgQ) {
        this.rcvMsgQ = rcvMsgQ;
    }

    public LinkedBlockingQueue<Message> getResponseMessageQueue() {
        return resMsgQ;
    }

    public void setResponseMessageQueue(LinkedBlockingQueue<Message> resMsgQ) {
        this.resMsgQ = resMsgQ;
    }

}
