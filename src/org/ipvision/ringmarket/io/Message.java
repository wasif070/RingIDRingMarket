/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.io;

import java.net.InetSocketAddress;

/**
 *
 * @author Wasif
 */
public class Message {

    private Object msg;
    private String host;
    private int port;
    private long requesterId;
    private long execStarted = 0;
    //private String pckId;
    //private String sId;

    public Message(Object message, InetSocketAddress remote) {
        this.msg = message;
        this.host = remote.getHostName();
        this.port = remote.getPort();
    }

    public Object getMessage() {
        return msg;
    }

    public void setMessage(Object msg) {
        this.msg = msg;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public long getExecStarted() {
        return execStarted;
    }

    public void setExecStarted(long execStarted) {
        this.execStarted = execStarted;
    }

    public long getRequesterId() {
        return requesterId;
    }

    public void setRequesterId(long requesterId) {
        this.requesterId = requesterId;
    }

}
