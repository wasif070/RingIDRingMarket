/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.io;

import java.net.InetSocketAddress;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Wasif
 */
public class SessionHandler extends IoHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(SessionHandler.class);
    private final ConnectionPool connectionPool;

    public SessionHandler(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    private void poolSession(IoSession session) {
        if (connectionPool != null) {
            connectionPool.poolConnection(session);
        }
    }

    @Override
    public void messageReceived(IoSession session, Object message) {
        try {
            Message msg = new Message(message, (InetSocketAddress) session.getRemoteAddress());
            StorageQueue.getInstance().getReceiveMessageQueue().put(msg);
            if (logger.isTraceEnabled()) {
                logger.trace("received message from " + session.getRemoteAddress().toString());
            }
            poolSession(session);
        } catch (InterruptedException ex) {
            logger.error("SessionHandler messageReceived ex-->", ex);
        }
    }

    @Override
    public void sessionClosed(IoSession session) {
        try {
//            logger.info("sessionClosed-->" + session.getRemoteAddress().toString());
        } catch (Exception ex) {
            logger.error("SessionHandler sessionClosed ex-->", ex);
        }
    }

    @Override
    public void sessionOpened(IoSession session) {
        try {
            if (logger.isTraceEnabled()) {
                logger.trace("sessionOpened-->" + session.getRemoteAddress().toString());
            }
        } catch (Exception ex) {
            logger.error("SessionHandler sessionOpened ex-->", ex);
        }

    }

    @Override
    public void sessionCreated(IoSession session) {
        try {
            if (logger.isTraceEnabled()) {
                logger.trace("sessionCreated-->" + session.getId() + "-->" + session.getRemoteAddress().toString());
            }
        } catch (Exception ex) {
            logger.error("SessionHandler sessionCreated ex-->", ex);
        }
    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus status) {
        try {
            if (logger.isTraceEnabled()) {
                logger.trace("sessionIdle " + session.getId() + "-->" + session.getRemoteAddress().toString() + " idle status : " + status.toString());
            }
        } catch (Exception ex) {
            logger.error("SessionHandler sessionIdle ex-->", ex);
        }
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) {
        logger.error("SessionHandler exceptionCaught for " + session.getRemoteAddress().toString() + " Cause : " + cause.getMessage(), cause);
    }

    @Override
    public void messageSent(IoSession session, Object message) {
        try {
            if (logger.isTraceEnabled()) {
                logger.trace("messageSent--> to " + session.getRemoteAddress().toString());
            }
        } catch (Exception ex) {
            logger.error("SessionHandler messageSent ex--> ", ex);
        }
    }

    @Override
    public void inputClosed(IoSession session) {
        try {
            session.closeNow();
        } catch (Exception ex) {
            logger.error("SessionHandler inputClosed ex--> ", ex);
        }
    }

}
