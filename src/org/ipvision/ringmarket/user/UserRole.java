/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.user;

import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.RingmarketRole;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityRingMarketUser;
import org.ipvision.ringmarket.entities.EntityUserRole;
import org.ipvision.ringmarket.entities.id.UserRoleId;
import org.ipvision.ringmarket.repositories.user.UserRoleRepository;

/**
 *
 * @author Imran
 */
public class UserRole implements UserRoleRepository {

    public boolean addUser(long id, RingmarketRole role) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            entityManager.getTransaction().begin();
            susc = addUser(entityManager, id, role);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    protected boolean addUser(EntityManager entityManager, long id, RingmarketRole role) {
        try {
            EntityUserRole entityUserRole = new EntityUserRole();
            entityUserRole.setRole(role.ordinal());
            entityUserRole.setUserId(id);
            entityManager.persist(entityUserRole);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public boolean removeUser(long id, RingmarketRole role) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean sucs = false;
        try {
            entityManager.getTransaction().begin();
            sucs = removeUser(entityManager, id, role);
            if (sucs) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return sucs;
    }

    protected boolean removeUser(EntityManager entityManager, long id, RingmarketRole role) {
        try {
            entityManager.remove(entityManager.find(EntityUserRole.class, new UserRoleId(id, role.ordinal())));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Using primary key as index returns a list of atmost 10 persons by the
     * role
     *
     * @param role
     * @return
     */
    public List<EntityUserRole> getUsersByRole(RingmarketRole role) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        List<EntityUserRole> roles;
        try {
            javax.persistence.Query query = entityManager.createNamedQuery("getRolesByRoleType");
            query.setParameter("role", role.ordinal());
            roles = query.getResultList();
        } finally {
            entityManager.close();
        }
        return roles;
    }

    public EntityRingMarketUser getAgent(double lat, double lon) {
        List<EntityUserRole> agents = getUsersByRole(RingmarketRole.AGENT);
        int x = ((new Random().nextInt()) % agents.size() + agents.size()) % agents.size();
        return new RingmarketUser().getUser(agents.get(x).getUserId());
    }
}
