/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.user;

import javax.persistence.EntityManager;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityRingMarketUser;
import org.ipvision.ringmarket.repositories.user.MarketUserRepository;

/**
 *
 * @author Imran
 */
public class RingmarketUser implements MarketUserRepository {

    public void addUser(long id, float lat, float lon) {
        addUser(id, lat, lon, "", "", "", "", "", "");
    }

    public void addUser(long id, float lat, float lon, String dialingCode, String phone, String address, String name, String abtMe, String photoUrl) {
        EntityRingMarketUser entityRingMarketUser = new EntityRingMarketUser();
        entityRingMarketUser.setAddress(address);
        entityRingMarketUser.setPhoto(photoUrl);
        entityRingMarketUser.setDialingCode(dialingCode);
        entityRingMarketUser.setPhoneNo(phone);
        entityRingMarketUser.setAboutMe(abtMe);
        entityRingMarketUser.setId(id);
        entityRingMarketUser.setLat(lat);
        entityRingMarketUser.setLon(lon);
        entityRingMarketUser.setName(name);
        addUser(entityRingMarketUser);
    }

    public void addUser(EntityRingMarketUser entityRingMarketUser) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(entityRingMarketUser);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    public void updateUserLocation(long id, float lat, float lon) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityRingMarketUser entityRingMarketUser = getUser(entityManager, id);
            entityRingMarketUser.setLat(lat);
            entityRingMarketUser.setLon(lon);
            entityManager.getTransaction().begin();
            entityManager.merge(entityRingMarketUser);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    public void updateUser(EntityRingMarketUser entityRingMarketUser) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(entityRingMarketUser);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    public void removeUser(long id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(getUser(id));
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    public EntityRingMarketUser getUser(long id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityRingMarketUser marketUser = getUser(entityManager, id);
            return marketUser;

        } finally {
            entityManager.close();
        }
    }

    private EntityRingMarketUser getUser(EntityManager entityManager, long id) {
        return entityManager.find(EntityRingMarketUser.class, id);
    }
}
