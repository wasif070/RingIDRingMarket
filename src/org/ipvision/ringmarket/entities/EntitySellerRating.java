package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seller_rating")
public class EntitySellerRating implements Serializable {

    @Column(name = "seller_id", columnDefinition = "bigint(20) NOT NULL", nullable = false)
    @SerializedName("slrId")
    @Id
    private long sellerId;

    @Column(name = "total_rating", columnDefinition = "int(11) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName("total_rating")
    private int totalRating;

    @Column(name = "number_of_raters", columnDefinition = "int(11) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName("nmbrOfRtrs")
    private int numberOfRaters;

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public int getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(int totalRating) {
        this.totalRating = totalRating;
    }

    public int getNumberOfRaters() {
        return numberOfRaters;
    }

    public void setNumberOfRaters(int numberOfRaters) {
        this.numberOfRaters = numberOfRaters;
    }
}
