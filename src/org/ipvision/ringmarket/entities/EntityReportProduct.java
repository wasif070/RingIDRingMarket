/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.ipvision.ringmarket.constants.ReportCause;

/**
 *
 * @author Imran
 */
@Entity
@Table(name = "report_product", uniqueConstraints = {
    @UniqueConstraint(name = "idx_report_buyer_product", columnNames = {"buyer_id", "product_id"})})
@NamedQueries({
//    @NamedQuery(name = "getIsAlreadyReported", query = "SELECT t0 FROM EntityReportProduct t0 WHERE t0.buyerId = :buyerId AND t0.productId = :productId")
    @NamedQuery(name = "getIsAlreadyReported", query = "SELECT COUNT(T0) FROM EntityReportProduct t0 WHERE t0.buyerId = :buyerId AND t0.productId = :productId")
})
public class EntityReportProduct implements Serializable {

    @Id
    @Column(name = "id", columnDefinition = "varchar(40) NOT NULL", length = 40, nullable = false)
    @SerializedName("id")
    private String id;

    @Column(name = "seller_id", columnDefinition = "bigint(20) NOT NULL", nullable = false)
    @SerializedName("slrId")
    private long sellerId;

    @Column(name = "buyer_id", columnDefinition = "bigint(20) NOT NULL", nullable = false)
    @SerializedName("byrId")
    private long buyerId;

    @Column(name = "product_id", columnDefinition = "varchar(40) NOT NULL", length = 40, nullable = false)
    @SerializedName("prodId")
    private String productId;

    @Column(name = "report_cause", columnDefinition = "int(2) NOT NULL", nullable = false)
    @SerializedName("rprtCs")
    private int reportCause;

    @Column(name = "additional_description", columnDefinition = "varchar(500) Default ''", length = 500)
    @SerializedName("addDesc")
    private String additionalDescription;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getReportCause() {
        return reportCause;
    }

    public void setReportCause(int reportCause) {
        this.reportCause = reportCause;
    }

    public void setReportCause(ReportCause reportCause) {
        this.reportCause = reportCause.ordinal();
    }

    public String getAdditionalDescription() {
        return additionalDescription;
    }

    public void setAdditionalDescription(String additionalDescription) {
        this.additionalDescription = additionalDescription;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

}
