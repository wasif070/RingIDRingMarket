/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.ipvision.ringmarket.constants.ProductStatus;

/**
 *
 * @author Imran
 */
@Table(name = "history_product")
@Entity
@NamedQueries(
        {
            @NamedQuery(name = "getCategoryWiseProductHistory", query = "select history_product from EntityHistoryProduct history_product where HISTORY_PRODUCT.categoryId = :categoryId")
        }
)

public class EntityHistoryProduct implements Serializable {

    @Id
    @Column(name = "id", nullable = false, length = 40)
    @SerializedName("id")
    private String id;

    @Column(name = "name", nullable = false, length = 500)
    @SerializedName("nm")
    private String name;

    @Column(name = "status", nullable = false, columnDefinition = "int(5) NOT NULL")
    @SerializedName("sts")
//    @Enumerated
//    private ProductStatus status;
    private int status;

    @Column(name = "sold_price", nullable = false, columnDefinition = "double(10,2) DEFAULT '0.00'")
    @SerializedName("soldPrice")
    private double soldPrice;

    @Column(name = "default_image_url", nullable = false, length = 200, columnDefinition = "varchar(200) DEFAULT ''")
    @SerializedName("imgUrl")
    private String imageUrl;

    @Column(name = "default_image_height", nullable = false, columnDefinition = "int(5) DEFAULT '0'")
    @SerializedName("imgH")
    private int imageHeight;

    @Column(name = "default_image_width", nullable = false, columnDefinition = "int(5) DEFAULT '0'")
    @SerializedName("imgW")
    private int imageWidth;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public ProductStatus getStatus() {
//        return status;
//    }
//
//    public void setStatus(ProductStatus status) {
//        this.status = status;
//    }

    public void setStatus(ProductStatus status) {
        this.status = status.ordinal();
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getSoldPrice() {
        return soldPrice;
    }

    public void setSoldPrice(double soldPrice) {
        this.soldPrice = soldPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

}
