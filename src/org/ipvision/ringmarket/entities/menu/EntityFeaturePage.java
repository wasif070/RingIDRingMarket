/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.menu;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author alamgir
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity
@Table(name = "feature_page")
@NamedQueries(
        {
            @NamedQuery(name = "EntityFeaturePage.getEnabledPages", query = "select page from EntityFeaturePage page where page.enable = :enable order by page.ordinal desc"),}
)
public class EntityFeaturePage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @SerializedName(MarketAttribute.ID)
    private int id;

    @Column(name = "type", nullable = false)
    @SerializedName(MarketAttribute.TYPE)
    private int type;

    @Column(name = "name", length = 30, nullable = false)
    @SerializedName(MarketAttribute.NAME)
    private String name;

    @Column(name = "enable", columnDefinition = "bit(1) DEFAULT b'0'", nullable = false)
    private Boolean enable;

    @Column(name = "ordinal", columnDefinition = "tinyint(1) DEFAULT '0'")
    private int ordinal;

    @Transient
    private List<?> items;

    public void setItems(List<?> items) {
        this.items = items;
    }
    
    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isEnable() {
        return enable;
    }

    public void setIsEnable(Boolean enable) {
        this.enable = enable;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
