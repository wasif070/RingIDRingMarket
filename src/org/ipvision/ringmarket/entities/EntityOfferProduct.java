/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.OfferStatus;

/**
 *
 * @author alamgir
 */
@Entity
@Table(name = "offer_product", uniqueConstraints = {
    @UniqueConstraint(name = "idx_unique_offer", columnNames = {"buyer_id", "product_id"}),})
@NamedQueries(
        {
            @NamedQuery(name = "getOfferInfoOfProduct", query = "select offer_product from EntityOfferProduct offer_product WHERE offer_product.buyerId = :buyerId AND offer_product.productId=:productId ORDER BY offer_product.offeredDate DESC"),
//            @NamedQuery(name = "getIsAlreadyAccepted", query = "select offer_product from EntityOfferProduct offer_product WHERE offer_product.status = :status AND offer_product.productId=:productId"),
            @NamedQuery(name = "getIsAlreadyAccepted", query = "select count(OFFER_PRODUCT) from EntityOfferProduct OFFER_PRODUCT WHERE offer_product.status = :status AND offer_product.productId=:productId"),
            @NamedQuery(name = "getUpwardAllOffersForProduct", query = "select offer_product from EntityOfferProduct offer_product WHERE offer_product.offeredDate > :pivotOfferedDate AND offer_product.productId=:productId ORDER BY offer_product.offeredDate ASC"),
            @NamedQuery(name = "getDownwardAllOffersForProduct", query = "select offer_product from EntityOfferProduct offer_product WHERE offer_product.offeredDate < :pivotOfferedDate AND offer_product.productId=:productId ORDER BY offer_product.offeredDate DESC"),
            @NamedQuery(name = "getAllOffersForProduct", query = "select offer_product from EntityOfferProduct offer_product WHERE offer_product.productId=:productId"),
//            @NamedQuery(name = "deleteAllOffersForProduct", query = "delete from EntityOfferProduct offer_product WHERE offer_product.productId=:productId"),
            @NamedQuery(name = "getUpwardOffersByBuyer", query = "select offer_product from EntityOfferProduct offer_product WHERE offer_product.offeredDate > :pivotOfferedDate AND offer_product.buyerId=:buyerId ORDER BY offer_product.offeredDate ASC"),
            @NamedQuery(name = "getDownwardOffersByBuyer", query = "select offer_product from EntityOfferProduct offer_product WHERE offer_product.offeredDate < :pivotOfferedDate AND offer_product.buyerId=:buyerId ORDER BY offer_product.offeredDate DESC")
        }
)
@NamedNativeQueries({
    @NamedNativeQuery(name = "deleteOffersForProduct", query = "delete from offer_product WHERE product_id = ?1")
})
public class EntityOfferProduct implements Serializable {

    @Id
    @Column(name = "id", length = 40, nullable = false)
    @SerializedName("id")
    private String id;

    @Column(name = "buyer_id", nullable = false)
    @SerializedName("byrId")
    private long buyerId;

    @Column(name = "buyer_name", length = 100, nullable = false)
    @SerializedName("byrNm")
    private String buyerName;

    @Index(name = "idx_offer_product_id", columnNames = {"product_id"})
    @Column(name = "product_id", length = 40, nullable = false)
    @SerializedName("prodId")
    private String productId;

    @Column(name = "product_name", length = 500, nullable = false)
    @SerializedName("prodNm")
    private String productName;

    @Column(name = "default_image_url", length = 200, nullable = false, columnDefinition = "varchar(200) DEFAULT ''")
    @SerializedName("imgUrl")
    private String defaultImageUrl;

    @Column(name = "offered_price", nullable = false, columnDefinition = "double(10,2) DEFAULT '0.00'")
    @SerializedName("ofrPrc")
    private double offeredPrice;

    @Index(name = "idx_offered_date", columnNames = {"offered_date"})
    @SerializedName("ofrdDate")
    @Column(name = "offered_date", nullable = false, columnDefinition = "bigint(20) DEFAULT '0'")
    private long offeredDate;

    @Column(name = "status", nullable = false, columnDefinition = "int(5) NOT NULL")
    @SerializedName("sts")
//    @Enumerated(EnumType.ORDINAL)
//    OfferStatus status;
    int status;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDefaultImageUrl() {
        return defaultImageUrl;
    }

    public void setDefaultImageUrl(String defaultImageUrl) {
        this.defaultImageUrl = defaultImageUrl;
    }

    public double getOfferedPrice() {
        return offeredPrice;
    }

    public void setOfferedPrice(double offeredPrice) {
        this.offeredPrice = offeredPrice;
    }

    public long getOfferedDate() {
        return offeredDate;
    }

    public void setOfferedDate(long offeredDate) {
        this.offeredDate = offeredDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatus(OfferStatus status) {
        this.status = status.ordinal();
    }

//    public OfferStatus getStatus() {
//        return status;
//    }
//
//    public void setStatus(OfferStatus status) {
//        this.status = status;
//    }
}
