/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.order;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author saikat
 */
@Table(name = "shipping_address")
@NamedQueries({
    @NamedQuery(name = "EntityShippingAddress.getAddressesOfUser", query = "SELECT addr FROM EntityShippingAddress AS addr WHERE addr.userId =:userId"),
    @NamedQuery(name = "EntityShippingAddress.getNearestRingStores", query = "SELECT addr FROM EntityShippingAddress AS addr WHERE addr.type =:type"),
    @NamedQuery(name = "EntityShippingAddress.setDefaultFalse", query = "UPDATE EntityShippingAddress AS ADDR SET ADDR.isDefault = FALSE WHERE ADDR.userId=:userId AND ADDR.id<> :id")
})
@Cacheable(DBConnection.CACHEABLE)
@Entity
public class EntityShippingAddress implements Serializable {

    @Id
    @Column(name = "id", nullable = false, columnDefinition = "varchar(36) not null")
    private String id;

    @Column(name = "is_default", columnDefinition = "bit(1) DEFAULT b'0'")
    @SerializedName(MarketAttribute.IS_DEFAULT)
    private boolean isDefault;

    @Column(name = "user_id", nullable = false, columnDefinition = "bigint(20) not null default '0'")
    @Index(name = "idx_shipping_addr_user_id", columnNames = {"user_id"})
    @SerializedName(MarketAttribute.USER_ID)
    private Long userId;

    @Column(name = "lat", columnDefinition = "float DEFAULT '0'", nullable = true)
    @SerializedName(MarketAttribute.LAT)
    private Float lat;

    @Column(name = "lon", columnDefinition = "float DEFAULT '0'", nullable = true)
    @SerializedName(MarketAttribute.LON)
    private Float lon;

    @Column(name = "type", columnDefinition = "tinyint(1) DEFAULT '0'", nullable = true)
    @SerializedName(MarketAttribute.TYPE)
    private Byte type;

    @Embedded
    @AttributeOverrides(value = {
        @AttributeOverride(name = "dialingCode", column = @Column(name = "dialing_code", nullable = false, length = 5)),
        @AttributeOverride(name = "contactName", column = @Column(name = "contact_name", nullable = false, length = 30)),
        @AttributeOverride(name = "postCode", column = @Column(name = "post_code", nullable = false)),
//        @AttributeOverride(name = "email", column = @Column(name = "email", nullable = false, length = 20))
//        ,
        @AttributeOverride(name = "mobile", column = @Column(name = "mobile", nullable = false, length = 20)),
        @AttributeOverride(name = "country", column = @Column(name = "country", nullable = false, length = 30)),
        @AttributeOverride(name = "city", column = @Column(name = "city", nullable = false, length = 30)),})
    @SerializedName(MarketAttribute.ADDRESS)
    private Address address;

//    @Persistent
//    @Column(name = "address", nullable = false)
//    @Externalizer("org.ipvision.ringmarket.utils.JpaConverterJson.convertToDatabaseColumn")
//    @Factory("org.ipvision.ringmarket.utils.JpaConverterJson.convertToEntityAttribute")
//    private JsonObject addressJson;
//    public JsonObject getAddressJson() {
//        return addressJson;
//    }
//
//    public void setAddressJson(JsonObject addressJson) {
//        this.addressJson = addressJson;
//    }
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

}
