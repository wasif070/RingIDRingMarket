/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.order;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import org.ipvision.ringmarket.constants.MarketAttribute;

/**
 *
 * @author bose
 */
@Embeddable
@Access(AccessType.FIELD)
public class Address implements Serializable {

//    @NotNull
    @SerializedName(MarketAttribute.EMAIL)
    private String email;
    @NotNull
    @SerializedName(MarketAttribute.MOBILE_DIALING_CODE)
    private String dialingCode;
    @NotNull
    @SerializedName(MarketAttribute.MOBILE)
    private String mobile;
    @NotNull
    @SerializedName(MarketAttribute.COUNTRY)
    private String country;
    @NotNull
    @SerializedName(MarketAttribute.CITY)
    private String city;
    @SerializedName(MarketAttribute.APARTMENT)
    private String apartment;
    @SerializedName(MarketAttribute.POST_CODE)
    private Short postCode;
    @NotNull
    @SerializedName(MarketAttribute.CONTACT_NAME)
    private String contactName;
    @SerializedName(MarketAttribute.STATE)
    private String state;
    @SerializedName(MarketAttribute.STREET)
    private String street;
    @SerializedName(MarketAttribute.USER_ID)
    private Long ringID;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDialingCode() {
        return dialingCode;
    }

    public void setDialingCode(String dialingCode) {
        this.dialingCode = dialingCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public Short getPostCode() {
        return postCode;
    }

    public void setPostCode(Short postCode) {
        this.postCode = postCode;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

}
