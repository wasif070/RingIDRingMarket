/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.order;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.entities.id.OrderProductId;

/**
 *
 * @author saikat
 */
@Entity
@Table(name = "order_details")
@IdClass(OrderProductId.class)
@NamedQueries(
        {
            @NamedQuery(name = "EntityOrderDetails.getMultipleOrderDetails", query = "select c from EntityOrderDetails c where  c.orderId in :idList"),
            @NamedQuery(name = "EntityOrderDetails.getOrderDetails", query = "select c from EntityOrderDetails c where  c.orderId =:id"),
            @NamedQuery(name = "EntityOrderDetails.deleteDetailsByOrderId", query = "DELETE FROM EntityOrderDetails e where  e.orderId =:id"),
            @NamedQuery(name = "EntityOrderDetails.updateProductImage", query = "UPDATE EntityOrderDetails c SET c.productImage =:url WHERE c.productId = :productId")
        }
)

public class EntityOrderDetails implements Serializable {

    @Id
    @Column(name = "order_id", nullable = false, columnDefinition = "varchar(36) NOT NULL")
    @SerializedName(MarketAttribute.ORDER_ID)
    private String orderId;

    @Id
    @SerializedName(MarketAttribute.PRODUCT_ID)
    @Column(name = "product_id", nullable = false, columnDefinition = "varchar(36) NOT NULL")
    private String productId;

    @SerializedName(MarketAttribute.PRODUCT_NAME)
    @Column(name = "product_name", nullable = false, length = 500)
    private String productName;

    @SerializedName(MarketAttribute.IMAGE_URL)
    @Column(name = "product_image", nullable = false, length = 200)
    private String productImage;

    @SerializedName(MarketAttribute.QUANTITY)
    @Column(name = "quantity", nullable = false, columnDefinition = "int(5) NOT NULL")
    private Integer quantity;

    @SerializedName(MarketAttribute.SHIPMENT_COST)
    @Column(name = "shipment_price", columnDefinition = "double(10,2) NOT NULL DEFAULT '0' ")
    private Double shipmentPrice;

    @SerializedName(MarketAttribute.CURRENCY_CODE)
    @Column(name = "currency_code", length = 10, nullable = false, columnDefinition = "varchar(10) NOT NULL DEFAULT '$'")
    private String currecyCode;

    @SerializedName(MarketAttribute.PRICE)
    @Column(name = "product_price", nullable = false, columnDefinition = "double(10,2) NOT NULL")
    private Double price;

    @SerializedName(MarketAttribute.ITEM_STATUS)
    @Column(name = "item_status", nullable = false, columnDefinition = "tinyint(1) not null DEFAULT '0'")
    private Byte itemStatus;

    @SerializedName(MarketAttribute.SHIPMENT_TIME)
    @Column(name = "shipment_time", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private long shipmentTime;

    @SerializedName(MarketAttribute.STATUS_MESSAGE)
    @Transient
    private String statusMessage;

    public long getShipmentTime() {
        return shipmentTime;
    }

    public void setShipmentTime(long shipmentTime) {
        this.shipmentTime = shipmentTime;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Byte getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(Byte itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getShipmentPrice() {
        return shipmentPrice;
    }

    public void setShipmentPrice(Double shipmentPrice) {
        this.shipmentPrice = shipmentPrice;
    }

    public String getCurrecyCode() {
        return currecyCode;
    }

    public void setCurrecyCode(String currecyCode) {
        this.currecyCode = currecyCode;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
