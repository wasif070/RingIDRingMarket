/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.order;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.entities.id.OrderDeliveryId;

/**
 *
 * @author saikat
 */
@Entity
@Table(name = "order_delivery")
@IdClass(OrderDeliveryId.class)
@NamedQueries(
        {})

public class EntityOrderDelivery implements Serializable {

    @Id
    @Column(name = "order_id", nullable = false, columnDefinition = "varchar(36) NOT NULL")
    @SerializedName(MarketAttribute.ORDER_ID)
    private String orderId;

    @Id
    @SerializedName(MarketAttribute.PRODUCT_ID)
    @Column(name = "product_id", nullable = false, columnDefinition = "varchar(36) NOT NULL")
    private String productId;

    @SerializedName(MarketAttribute.CARRIER_ID)
    @Column(name = "carrier_id", nullable = false, columnDefinition = "bigint(20) NOT NULL")
//    @Index(name = "idx_order_delivery_carrier_id", columnNames = {"carrier_id"})
    private Long carrierId;

    @SerializedName(MarketAttribute.CREATION_TIME)
    @Column(name = "add_time", nullable = false, columnDefinition = "bigint(20) DEFAULT '0'")
    private Long addedTime;

    @SerializedName(MarketAttribute.UPDATED_TIME)
    @Column(name = "update_time", nullable = false, columnDefinition = "bigint(20) DEFAULT '0'")
    private Long updateTime;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(Long addedTime) {
        this.addedTime = addedTime;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getTime() {
        return addedTime;
    }

    public void setTime(Long time) {
        this.addedTime = time;
    }

    public Long getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(Long carrierId) {
        this.carrierId = carrierId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

}
