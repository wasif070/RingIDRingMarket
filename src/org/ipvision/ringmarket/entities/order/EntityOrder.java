/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.order;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.apache.openjpa.persistence.Externalizer;
import org.apache.openjpa.persistence.Factory;
import org.apache.openjpa.persistence.Persistent;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;

/**
 *
 * @author saikat
 */
@Entity
@Table(name = "order_list")
@NamedQueries(
        {
            @NamedQuery(name = "EntityOrder.getOrdersByInvoice", query = "SELECT e FROM EntityOrder e where e.invoiceId =: invoiceId"),
            @NamedQuery(name = "EntityOrder.paymentCompleteByInvoice", query = "UPDATE  EntityOrder O SET O.status=:status , O.paymentTime=:paymentTime , O.paymentMethod=:paymentMethod  WHERE O.invoiceId =: invoiceId"),
            @NamedQuery(name = "EntityOrder.getOrdersByScrollUp", query = "select c from EntityOrder c where c.userId= :userId and  c.id >:pivotId order by c.id"),
//            @NamedQuery(name = "EntityOrder.getOrdersByScrollDown", query = "select c from EntityOrder c where c.userId= :userId and  c.id <:pivotId order by c.id desc")
//            ,
            @NamedQuery(name = "EntityOrder.getOrdersOfShop", query = "select c.id from EntityOrder c where c.shopId= :shopId"),
//            @NamedQuery(name = "EntityOrder.getOrdersByPivotEmptyAndScrollDown", query = "select c from EntityOrder c where c.userId= :userId and  c.id >:pivotId order by c.id desc")
//            ,
            @NamedQuery(name = "EntityOrder.updateShopName", query = "UPDATE EntityOrder c SET c.shopName =:name WHERE c.shopId=:shopId")
        }
)
@NamedNativeQueries({
    @NamedNativeQuery(name = "EntityOrder.getInvoiceList", query = "select invoice_id,user_id, count(id), sum(price),added_time from order_list group by invoice_id order by added_time desc"),
    @NamedNativeQuery(name = "EntityOrder.getOrdersByScrollDown", query = "SELECT o.id FROM order_list o WHERE (o.added_time,o.id) <(?1 , ?2) AND o.user_id = ?3 ORDER BY o.added_time DESC , o.id DESC"),
    @NamedNativeQuery(name = "EntityOrder.getOrdersByPivotEmptyAndScrollDown", query = "SELECT o.id FROM order_list o WHERE (o.added_time,o.id) >(?1 , ?2) AND o.user_id = ?3 ORDER BY o.added_time DESC , o.id DESC"),
    @NamedNativeQuery(name = "EntityOrder.getOrdersByScrollUp", query = "SELECT o.id FROM order_list o WHERE (o.added_time,o.id) >(?1 , ?2) AND o.user_id = ?3 ORDER BY o.added_time ASC , o.id ASC"),})
public class EntityOrder implements Serializable {

    @Id
    @Column(name = "id", nullable = false, length = 36, columnDefinition = "varchar(36) NOT NULL")
    @SerializedName(MarketAttribute.ID)
    private String id;

    @Index(name = "idx_invoice_id", columnNames = {"invoice_id"})
    @Column(name = "invoice_id", length = 40, nullable = false)
    @SerializedName(MarketAttribute.INVOICE_ID)
    private String invoiceId;

    @SerializedName(MarketAttribute.STATUS)
    @Column(name = "status", nullable = false, columnDefinition = "tinyint(1) NOT NULL DEFAULT '0'")
    private Byte status;

    @SerializedName(MarketAttribute.USER_ID)
    @Index(name = "idx_order_list_user", columnNames = {"user_id"})
    @Column(name = "user_id", nullable = false, columnDefinition = "bigint(20) NOT NULL")
    private Long userId;

    @SerializedName(MarketAttribute.SHOP_ID)
    @Index(name = "idx_order_list_shop", columnNames = {"shop_id"})
    @Column(name = "shop_id", nullable = false, columnDefinition = "bigint(20) NOT NULL")
    private Long shopId;

    @Column(name = "price", precision = 10, scale = 2, nullable = false)
    @SerializedName(MarketAttribute.PRICE)
    private BigDecimal price;

    @SerializedName(MarketAttribute.SHIPMENT_METHOD)
    @Column(name = "shipment_method", nullable = false, columnDefinition = "tinyint(1) NOT NULL DEFAULT '0'")
    private Byte shipmentMethod;

    @Column(name = "payment_method", nullable = false, columnDefinition = "tinyint(1) not null default 0")
    @SerializedName(MarketAttribute.PAYMENT_METHOD)
    private Byte paymentMethod;

    @Persistent
    @Column(name = "address", nullable = false)
    @Externalizer("org.ipvision.ringmarket.utils.JpaConverterJson.convertToDatabaseColumn")
    @Factory("org.ipvision.ringmarket.utils.JpaConverterJson.convertToEntityAttribute")
    @SerializedName(MarketAttribute.ADDRESS)
    private Object address;

    @SerializedName(MarketAttribute.ADDED_TIME)
    @Index(name = "idx_orders_add_tm_id", columnNames = {"added_time", "id"})
    @Column(name = "added_time", nullable = false, columnDefinition = "bigint(20) DEFAULT '0'")
    private Long addedTime;

    @SerializedName(MarketAttribute.PAYMENT_TIME)
    @Column(name = "payment_time", nullable = false, columnDefinition = "bigint(20) DEFAULT '0'")
    private long paymentTime;

    @SerializedName(MarketAttribute.PROTECTION_DURATION)
    @Column(name = "protection_duration", nullable = false, columnDefinition = "bigint(20) DEFAULT '0'")
    private Long protectionDuration;

    @SerializedName(MarketAttribute.CANCEL_DURATION)
    @Column(name = "cancel_duration", nullable = false, columnDefinition = "bigint(20) DEFAULT '0'")
    private Long cancelDuration;

    @SerializedName(MarketAttribute.FEEDBACK_DURATION)
    @Column(name = "feedback_duration", nullable = false, columnDefinition = "bigint(20) DEFAULT '0'")
    private Long feedbackDuration;

    @SerializedName(MarketAttribute.SHOP_NAME)
    @Column(name = "shop_name", nullable = false, length = 100, columnDefinition = "varchar(100) NOT NULL")
    private String shopName;

    @SerializedName(MarketAttribute.STATUS_MESSAGE)
    @Transient
    private String statusMessage;

    @SerializedName(MarketAttribute.DETAILS)
    @Transient
    private Collection<EntityOrderDetails> details;

    public Collection<EntityOrderDetails> getDetails() {
        return details;
    }

    public void setDetails(Collection<EntityOrderDetails> details) {
        this.details = details;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public LinkedHashMap getAddress() {
        return (LinkedHashMap) address;
    }

    public void setAddress(LinkedHashMap address) {
        this.address = address;
    }

    public Byte getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Byte paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Byte getShipmentMethod() {
        return shipmentMethod;
    }

    public void setShipmentMethod(Byte shipmentMethod) {
        this.shipmentMethod = shipmentMethod;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getStatus() {
        return status;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(Long addedTime) {
        this.addedTime = addedTime;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public long getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(long paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Long getProtectionDuration() {
        return protectionDuration;
    }

    public void setProtectionDuration(Long protectionDuration) {
        this.protectionDuration = protectionDuration;
    }

    public Long getCancelDuration() {
        return cancelDuration;
    }

    public void setCancelDuration(Long cancelDuration) {
        this.cancelDuration = cancelDuration;
    }

    public Long getFeedbackDuration() {
        return feedbackDuration;
    }

    public void setFeedbackDuration(Long feedbackDuration) {
        this.feedbackDuration = feedbackDuration;
    }

}
