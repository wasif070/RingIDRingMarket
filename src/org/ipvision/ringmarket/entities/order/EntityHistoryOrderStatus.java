/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.order;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import org.ipvision.ringmarket.constants.MarketAttribute;

/**
 *
 * @author saikat
 */
@Entity
@Table(name = "history_order_status")
@NamedQueries(
        {}
)

public class EntityHistoryOrderStatus implements Serializable {

    @Id
    @Column(name = "id", nullable = false, columnDefinition = "varchar(36) NOT NULL")
    @SerializedName(MarketAttribute.ID)
    private String id;
  
    @Column(name = "order_id", nullable = false, columnDefinition = "varchar(36) NOT NULL")
    @SerializedName(MarketAttribute.ORDER_ID)
    private String orderId;

    @SerializedName(MarketAttribute.PRODUCT_ID)
    @Column(name = "product_id", nullable = false, columnDefinition = "varchar(36) NOT NULL")
    private String productId;

    @SerializedName(MarketAttribute.USER_ID)
    @Column(name = "activist_id", nullable = false)
    private Long activistId;

    @SerializedName(MarketAttribute.ITEM_STATUS)
    @Column(name = "item_status", nullable = false, columnDefinition = "tinyint(1) not null DEFAULT '0'")
    private Byte itemStatus;

    @SerializedName(MarketAttribute.UPDATED_TIME)
    @Column(name = "time", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long time;

    public EntityHistoryOrderStatus() {
    }

    public EntityHistoryOrderStatus(String id, String orderId, String productId, Long activistId, Byte itemStatus, Long time) {
        this.id = id;
        this.orderId = orderId;
        this.productId = productId;
        this.activistId = activistId;
        this.itemStatus = itemStatus;
        this.time = time;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getActivistId() {
        return activistId;
    }

    public void setActivistId(Long activistId) {
        this.activistId = activistId;
    }

    public Byte getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(Byte itemStatus) {
        this.itemStatus = itemStatus;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
