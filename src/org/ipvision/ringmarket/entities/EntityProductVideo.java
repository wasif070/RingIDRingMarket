/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.entities.id.ProductVideoId;
import org.ipvision.ringmarket.utils.TimeUUID;

/**
 *
 * @author Imran
 */
@Entity
@Table(name = "product_video")
@IdClass(ProductVideoId.class)
@NamedQueries(
        {
            @NamedQuery(name = "getVideosOfProduct", query = "select product_video from EntityProductVideo product_video where product_video.productId = :productId")
        }
)
@NamedNativeQueries(
        {
            @NamedNativeQuery(name = "deleteVideosOfProduct", query = "delete from product_video where product_video = ?1")
        }
)
public class EntityProductVideo implements Serializable {

    @Id
    @Column(name = "video_url", columnDefinition = "varchar(200) NOT NULL", length = 200, nullable = false)
    @SerializedName("imgUrl")
    private String videoUrl;

    @Column(name = "thumb_image_url", columnDefinition = "varchar(200) NOT NULL", length = 200, nullable = false)
    @SerializedName("thmImgUrl")
    private String thumbImageUrl;

    @Column(name = "thumb_image_height", columnDefinition = "int(5) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName("thmImgHgt")
    private int thumbImageHeight;

    @Column(name = "thumb_image_width", columnDefinition = "int(5) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName("thmImgWdt")
    private int thumbImageWidth;

    @Id
    @Column(name = "product_id", columnDefinition = "varchar(40) NOT NULL", length = 40, nullable = false)
    @SerializedName("prodId")
    private String productId;

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getThumbImageUrl() {
        return thumbImageUrl;
    }

    public void setThumbImageUrl(String thumbImageUrl) {
        this.thumbImageUrl = thumbImageUrl;
    }

    public int getThumbImageHeight() {
        return thumbImageHeight;
    }

    public void setThumbImageHeight(int thumbImageHeight) {
        this.thumbImageHeight = thumbImageHeight;
    }

    public int getThumbImageWidth() {
        return thumbImageWidth;
    }

    public void setThumbImageWidth(int thumbImageWidth) {
        this.thumbImageWidth = thumbImageWidth;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
