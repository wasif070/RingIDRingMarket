package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.Index;

@Entity
@Table(name = "seller_product")
@NamedQueries({
    @NamedQuery(name = "getSellerProductByID", query = "select seller_product from EntitySellerProduct seller_product where SELLER_PRODUCT.productId = :product_id"),
    @NamedQuery(name = "getDownwardSellerProductList", query = "select seller_product FROM EntitySellerProduct seller_product WHERE seller_product.sellerId = :sellerId AND seller_product.productId>:pivotProdId ORDER BY seller_product.productId ASC"),
    @NamedQuery(name = "getUpwardSellerProductList", query = "select seller_product FROM EntitySellerProduct seller_product WHERE seller_product.sellerId = :sellerId AND seller_product.productId<:pivotProdId ORDER BY seller_product.productId DESC"),
})
@NamedNativeQueries({
    @NamedNativeQuery(name = "deleteFromSellerProduct", query = "delete from seller_product where product_id = ?1"),})
public class EntitySellerProduct implements Serializable {

    @Id
    @Column(name = "product_id", columnDefinition = "varchar(40) NOT NULL", length = 40, nullable = false)
    @SerializedName("prodId")
    public String productId;

    @Column(name = "product_name", columnDefinition = "varchar(500) NOT NULL", length = 500, nullable = false)
    @SerializedName("prodNm")
    private String productName;

    @Column(name = "default_image_url", columnDefinition = "varchar(200) NOT NULL DEFAULT ''", length = 200, nullable = false)
    @SerializedName("imgUrl")
    private String defaultImageUrl;

    @Index(name = "idx_seller", columnNames = {"seller_id"})
    @SerializedName("slrId")
    @Column(name = "seller_id", columnDefinition = "bigint(20) NOT NULL", nullable = false)
    private long sellerId;

    @Column(name = "added_date", columnDefinition = "bigint(20) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName("addDt")
    private long addedDate;
    
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDefaultImageUrl() {
        return defaultImageUrl;
    }

    public void setDefaultImageUrl(String defaultImageUrl) {
        this.defaultImageUrl = defaultImageUrl;
    }

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public long getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(long addedDate) {
        this.addedDate = addedDate;
    }

}
