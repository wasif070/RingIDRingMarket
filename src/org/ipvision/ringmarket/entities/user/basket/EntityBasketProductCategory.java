/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.user.basket;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.id.BasketCategoryProductId;

/**
 *
 * @author saikat
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity
@Table(name = "basket_category_product")
@IdClass(BasketCategoryProductId.class)
@NamedQueries(
        {
            @NamedQuery(name = "getCategoriesByBasketAndPosition", query = "select distinct(c.categoryId) from EntityBasketProductCategory c where c.basketId= :basketId and c.position = :position ORDER BY c.categoryId ASC")
            ,

            @NamedQuery(name = "EntityBasketProductCategory.getProductsOfBasketCatScrollDown", query = "select c.productId from EntityBasketProductCategory c where c.basketId= :basketId AND c.isProductEnabled = true  and c.categoryId = :categoryId and c.productId >:pivotProductId ORDER BY c.productId ASC")
            ,
            @NamedQuery(name = "EntityBasketProductCategory.getProductsOfBasketCatScrollUp", query = "select c.productId from EntityBasketProductCategory c where c.basketId= :basketId AND c.isProductEnabled = true and c.categoryId = :categoryId and c.productId <:pivotProductId ORDER BY c.productId DESC")
            ,
            @NamedQuery(name = "EntityBasketProductCategory.getProductsOfBasketScrollDown", query = "select c.productId from EntityBasketProductCategory c where c.basketId= :basketId  AND c.isProductEnabled = true  and c.productId >:pivotProductId ORDER BY c.productId ASC")
            ,
            @NamedQuery(name = "EntityBasketProductCategory.getProductsOfBasketScrollUp", query = "select c.productId from EntityBasketProductCategory c where c.basketId= :basketId  AND c.isProductEnabled = true and c.productId <:pivotProductId ORDER BY c.productId DESC")
            ,

            @NamedQuery(name = "EntityBasketProductCategory.getProductsOfBasketByUserIdScrollDown", query = "select c.productId from EntityBasketProductCategory c where c.userId= :userId  AND c.isProductEnabled = true  and c.productId >:pivotProductId ORDER BY c.productId ASC")
            ,
            @NamedQuery(name = "EntityBasketProductCategory.getProductsOfBasketByUserIdScrollUp", query = "select c.productId from EntityBasketProductCategory c where c.userId= :userId  AND c.isProductEnabled = true and c.productId <:pivotProductId ORDER BY c.productId DESC")
            ,

            @NamedQuery(name = "EntityBasketProductCategory.getBasketIdByUserIdAndProductId", query = "select c.basketId from EntityBasketProductCategory c where c.userId= :userId AND c.isProductEnabled = true  and c.productId = :productId")
            ,
            @NamedQuery(name = "EntityBasketProductCategory.getBasketByUserIdAndProductIdAndBasketId", query = "select c from EntityBasketProductCategory c where c.userId= :userId  AND c.isProductEnabled = true and c.productId = :productId and c.basketId = :basketId")
            ,
            @NamedQuery(name = "EntityBasketProductCategory.updateProductStatus", query = "update EntityCategoryProduct cp set cp.isProductEnabled = :status where cp.productId = :productId")
        }
)

//@NamedNativeQueries(
//        {
//            @NamedNativeQuery(name = "deleteProductFromBasket", query = "delete from basket_category_product where basket_id = ?1 and user_id=?2 and product_id = ?3")
//        }
//)
public class EntityBasketProductCategory implements Serializable {

    @Id
    @Column(name = "basket_id", length = 40, nullable = false)
    @SerializedName(MarketAttribute.BASKET_ID)
    private String basketId;

    @Id
    @Column(name = "category_id")
    @SerializedName(MarketAttribute.CATEGORY_ID)
    private int categoryId;

    @Id
    @Index(name = "idx_basket_category_product_id", columnNames = {"product_id"})
    @Column(name = "product_id", length = 40, nullable = false)
    @SerializedName(MarketAttribute.PRODUCT_ID)
    private String productId;

    @Column(name = "position", columnDefinition = "int(11) DEFAULT '1'", nullable = false)
    @SerializedName(MarketAttribute.POSITION)
    private int position = 1;

    @Index(name = "idx_basket_cat_user_id", columnNames = {"user_id"})
    @SerializedName(MarketAttribute.USER_ID)
    @Column(name = "user_id", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT  '0'")
    private Long userId;

    @Column(name = "prod_en", columnDefinition = "bit(1) DEFAULT b'1'")
    @SerializedName("prodEn")
    private Boolean isProductEnabled = true;

    public Boolean getIsProductEnabled() {
        return isProductEnabled;
    }

    public void setIsProductEnabled(Boolean isProductEnabled) {
        this.isProductEnabled = isProductEnabled;
    }

    public EntityBasketProductCategory() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getBasketId() {
        return basketId;
    }

    public void setBasketId(String basketId) {
        this.basketId = basketId;
    }

}
