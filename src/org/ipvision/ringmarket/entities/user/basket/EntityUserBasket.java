/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.user.basket;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author saikat
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity
@Table(
        name = "user_basket",
        uniqueConstraints = {
            @UniqueConstraint(name = "uk_user_basket_name", columnNames = {"user_id", "name"})
        }
)
@NamedQueries(
        {
            @NamedQuery(name = "getUpwardBasketsByUserType", query = "select b from EntityUserBasket b where b.ownerType = :ownerType and b.id < :pivotId order by b.id desc"),
            @NamedQuery(name = "getDownwardBasketsByUserType", query = "select b from EntityUserBasket b where b.ownerType = : ownerType and b.id > :pivotId order by b.id asc"),
            @NamedQuery(name = "EntityUserBasket.getUserBaskets", query = "select b from EntityUserBasket b where b.userId = : userId")
        }
)

public class EntityUserBasket implements Serializable {

    @SerializedName(MarketAttribute.ID)
    @Id
    @Column(name = "id", nullable = false, columnDefinition = "varchar(36) NOT NULL DEFAULT ''")
    private String id;

    @SerializedName(MarketAttribute.NAME)
    @NotNull
    @Column(name = "name", nullable = false, columnDefinition = "varchar(600) NOT NULL DEFAULT ''")
    private String name;

    @SerializedName(MarketAttribute.IMAGE_URL)
    @NotNull
    @Column(name = "image_url", nullable = false, columnDefinition = "varchar(200) NOT NULL DEFAULT ''")
    private String imageUrl;

    @SerializedName(MarketAttribute.IMAGE_HEIGHT)
    @Column(name = "image_height", columnDefinition = "int(4)")
    private Integer imageHeight;

    @SerializedName(MarketAttribute.IMAGE_WIDTH)
    @Column(name = "image_width", columnDefinition = "int(4)")
    private Integer imageWidth;

    @SerializedName(MarketAttribute.USER_TYPE)
    @NotNull
    @Column(name = "owner_type", nullable = false, columnDefinition = "tinyint(2) NOT NULL DEFAULT '0'")
    private Integer ownerType;

    @Index(name = "idx_basket_user_id", columnNames = "user_Id")
    @SerializedName(MarketAttribute.USER_ID)
    @NotNull
    @Column(name = "user_id", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long userId;

    @SerializedName(MarketAttribute.CREATION_TIME)
    @NotNull
    @Column(name = "creation_time", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long creationTime;

    @Transient
    @SerializedName(MarketAttribute.TYPE)
    private Short type = 0;

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(Integer imageHeight) {
        this.imageHeight = imageHeight;
    }

    public Integer getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(Integer imageWidth) {
        this.imageWidth = imageWidth;
    }

    public Integer getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(Integer ownerType) {
        this.ownerType = ownerType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

}
