/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.user.cart;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author saikat
 */
@Entity
@Table(name = "user_cart_item",
        uniqueConstraints = {
            @UniqueConstraint(name = "uqx_buyer_product", columnNames = {"product_id", "user_id"})
        }
)
@Cacheable(DBConnection.CACHEABLE)
@NamedQueries(
        {
            @NamedQuery(name = "EntityCartItem.getDownwardCartItems", query = "select c.id from EntityCartItem c where c.userId= :userId AND c.isProductEnabled = true and  c.id >:pivotId order by c.id")
            ,
            @NamedQuery(name = "EntityCartItem.getUpwardCartItems", query = "select c.id from EntityCartItem c where c.userId= :userId AND c.isProductEnabled = true and  c.id <:pivotId order by c.id desc")
            ,
            @NamedQuery(name = "getCartItemByUserIdAndProductId", query = "select c from EntityCartItem c where c.userId= :userId AND c.isProductEnabled = true and  c.productId =:productId")
            ,
            @NamedQuery(name = "EntityCartItem.getCartItemByIds", query = "select c from EntityCartItem c where  c.id in :idList")
            ,
            @NamedQuery(name = "EntityCartItem.cartItemsCount", query = "select COUNT(c.id) from EntityCartItem c where  c.userId =:userId AND c.isProductEnabled = true")
            ,
            @NamedQuery(name = "EntityCartItem.userCount", query = "select DISTINCT COUNT(c.userId) from EntityCartItem c")
            ,
            @NamedQuery(name = "EntityCartItem.updateShopName", query = "UPDATE EntityCartItem c SET c.shopName =:name WHERE c.shopId=:shopId")
            ,
            @NamedQuery(name = "EntityCartItem.updateProductStatus", query = "UPDATE EntityCartItem c SET c.isProductEnabled = :status WHERE c.productId = :productId")
            ,
            @NamedQuery(name = "EntityCartItem.updateProductImage", query = "UPDATE EntityCartItem c SET c.productImage =:url WHERE c.productId = :productId"),}
)
public class EntityCartItem implements Serializable {

    @Id
    @Column(name = "id", nullable = false, columnDefinition = "varchar(36) NOT NULL")
    private String id;

    @SerializedName(MarketAttribute.PRODUCT_ID)
    @Column(name = "product_id", nullable = false, columnDefinition = "varchar(36) NOT NULL")
    private String productId;

    @Index(name = "idx_user_cart_user_id", columnNames = {"user_id", "id"})
    @SerializedName(MarketAttribute.USER_ID)
    @Column(name = "user_id", nullable = false, columnDefinition = "bigint(20) NOT NULL")
    private Long userId;

    @SerializedName(MarketAttribute.CREATION_TIME)
    @Column(name = "creation_time", nullable = false, columnDefinition = "bigint(20) NOT NULL")
    private Long creationTIme;

    @SerializedName(MarketAttribute.SHOP_ID)
    @Column(name = "shop_id", nullable = false, columnDefinition = "bigint(20) NOT NULL")
    private Long shopId;

    @SerializedName(MarketAttribute.QUANTITY)
    @Column(name = "quantity", nullable = false, columnDefinition = "int(36) NOT NULL")
    private Integer quantity;

    @Transient
    @SerializedName(MarketAttribute.PRODUCT_NAME)
    //@Column(name = "product_name", nullable = false, length = 500)
    private String productName;

    @Transient
    @SerializedName(MarketAttribute.IMAGE_URL)
    //@Column(name = "product_image", nullable = false, columnDefinition = "varchar(100) DEFAULT '' ")
    private String productImage;

    @Transient
    @SerializedName(MarketAttribute.SHOP_NAME)
//    @Column(name = "shop_name", nullable = false, length = 100, columnDefinition = "varchar(100) NOT NULL")
    private String shopName;

    @Transient
    @SerializedName(MarketAttribute.SHIPMENT_COST)
//    @Column(name = "shipment_price", columnDefinition = "double(10,2) NOT NULL DEFAULT '0' ")
    private Double shipmentPrice = 0.0;

    @Transient
    @SerializedName(MarketAttribute.CURRENCY_CODE)
//    @Column(name = "currency_code", length = 10, nullable = false, columnDefinition = "varchar(10) NOT NULL DEFAULT '&#x24;'")
    private String currecyCode;

    @Transient
    @SerializedName(MarketAttribute.PRICE)
//    @Column(name = "product_price", nullable = false, columnDefinition = "double(10,2) NOT NULL")
    private Double price;

    @Transient
    @SerializedName(MarketAttribute.DISCOUNT)
    private Float discount;

    @Transient
    @SerializedName(MarketAttribute.DISCOUNT_UNIT)
    private Byte discountUnit;

    @Transient
    @SerializedName(MarketAttribute.DISCOUNT_AVALIABLE)
    private boolean isDiscounted;

    @Column(name = "prod_en", columnDefinition = "bit(1) DEFAULT b'1'")
    @SerializedName("prodEn")
    private Boolean isProductEnabled = true;

    public boolean isDiscounted() {
        return isDiscounted;
    }

    public void setIsDiscounted(boolean isDiscounted) {
        this.isDiscounted = isDiscounted;
    }

    public Boolean getIsProductEnabled() {
        return isProductEnabled;
    }

    public void setIsProductEnabled(Boolean isProductEnabled) {
        this.isProductEnabled = isProductEnabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Double getShipmentPrice() {
        return shipmentPrice;
    }

    public void setShipmentPrice(Double shipmentPrice) {
        this.shipmentPrice = shipmentPrice;
    }

    public String getCurrecyCode() {
        return currecyCode;
    }

    public void setCurrecyCode(String currecyCode) {
        this.currecyCode = currecyCode;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCreationTIme() {
        return creationTIme;
    }

    public void setCreationTIme(Long creationTIme) {
        this.creationTIme = creationTIme;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public Byte getDiscountUnit() {
        return discountUnit;
    }

    public void setDiscountUnit(Byte discountUnit) {
        this.discountUnit = discountUnit;
    }

}
