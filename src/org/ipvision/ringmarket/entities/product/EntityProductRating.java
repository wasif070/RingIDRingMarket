package org.ipvision.ringmarket.entities.product;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

@Entity
@Cacheable(DBConnection.CACHEABLE)
@Table(name = "product_rating")
public class EntityProductRating implements Serializable {

    @Column(name = "id", columnDefinition = "varchar(36) NOT NULL", nullable = false)
    @SerializedName(MarketAttribute.ID)
    @Id
    private String id;

    @Column(name = "total_rating", columnDefinition = "int(11) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName(MarketAttribute.TOTAL_RATING)
    private int totalRating;

    @Column(name = "no_of_raters", columnDefinition = "int(11) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName(MarketAttribute.NUMBER_OF_RATERS)
    private int numberOfRaters;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(int totalRating) {
        this.totalRating = totalRating;
    }

    public int getNumberOfRaters() {
        return numberOfRaters;
    }

    public void setNumberOfRaters(int numberOfRaters) {
        this.numberOfRaters = numberOfRaters;
    }

}
