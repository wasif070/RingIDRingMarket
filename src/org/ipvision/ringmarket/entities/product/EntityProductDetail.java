/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.product;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.apache.openjpa.persistence.Externalizer;
import org.apache.openjpa.persistence.Factory;
import org.apache.openjpa.persistence.Persistent;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author Imran
 */
@Entity
@Cacheable(DBConnection.CACHEABLE)
@Table(name = "product_detail")
public class EntityProductDetail implements Serializable {

    @Id
    @Column(name = "id", columnDefinition = "varchar(40) NOT NULL", length = 40, nullable = false)
    @SerializedName(MarketAttribute.ID)
    private String id;

    @Column(name = "description", columnDefinition = "varchar(1000) DEFAULT NULL", length = 1000)
    @SerializedName(MarketAttribute.DESCRIPTION)
    private String description;

    @Persistent
    @Column(name = "attributes", columnDefinition = "text DEFAULT NULL")
    @SerializedName(MarketAttribute.FEATURES)
    @Externalizer("org.ipvision.ringmarket.utils.JpaConverterJson.convertToDatabaseColumn")
    @Factory("org.ipvision.ringmarket.utils.JpaConverterJson.convertToEntityAttribute")
    private Object attributes;

    @Transient
    @SerializedName(MarketAttribute.IMAGE_GALLERY)
    private List<EntityProductImage> images;

    public void setImages(List<EntityProductImage> images) {
        this.images = images;
    }

    public List<EntityProductImage> getImageUrls() {
        return images;
    }

    public void setImageUrls(List<EntityProductImage> imageUrls) {
        this.images = imageUrls;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LinkedHashMap getAttributes() {
        return (LinkedHashMap) attributes;
    }

    public void setAttributes(LinkedHashMap attributes) {
        this.attributes = attributes;
    }

}
