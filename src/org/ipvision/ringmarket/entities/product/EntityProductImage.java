/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.product;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.DataCache;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.id.ProductImageId;

/**
 *
 * @author Imran
 */
@Entity
@Cacheable(DBConnection.CACHEABLE)
@Table(name = "product_image")
@IdClass(ProductImageId.class)
@NamedQueries(
        {
            @NamedQuery(name = "EntityProductImage.getImagesOfProduct", query = "select NEW EntityProductImage(PRODUCT_IMAGE.imageUrl,PRODUCT_IMAGE.imageHeight,PRODUCT_IMAGE.imageWidth) from EntityProductImage PRODUCT_IMAGE"
                    + " where PRODUCT_IMAGE.productId = :productId order by PRODUCT_IMAGE.addedTime")
        }
)
@NamedNativeQueries(
        {
            @NamedNativeQuery(name = "deleteImagesOfProduct", query = "delete from product_image where product_id = ?1")
        }
)

public class EntityProductImage implements Serializable {

    @Id
    @Column(name = "product_id", columnDefinition = "varchar(40) NOT NULL", length = 40, nullable = false)
    @SerializedName(MarketAttribute.ID)
    private String productId;

    @Id
    @Column(name = "image_url", columnDefinition = "varchar(200) NOT NULL DEFAULT''", length = 200, nullable = false)
    @SerializedName(MarketAttribute.IMAGE_URL)
    private String imageUrl;

    @Column(name = "image_height", columnDefinition = "int(5) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName(MarketAttribute.IMAGE_HEIGHT)
    private Integer imageHeight;

    @Column(name = "image_width", columnDefinition = "int(5) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName(MarketAttribute.IMAGE_WIDTH)
    private Integer imageWidth;

    @Column(name = "is_default", columnDefinition = "bit(1) DEFAULT b'0'")
    @SerializedName(MarketAttribute.IS_DEFAULT)
    private boolean isDefault;

    @SerializedName(MarketAttribute.ADDED_TIME)
    @Column(name = "added_time", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long addedTime;
    
    public EntityProductImage(String imageUrl, Integer imageHeight, Integer imageWidth, String productId) {
        this(imageUrl, imageHeight, imageWidth, productId, false, System.currentTimeMillis());
    }

    public EntityProductImage(String imageUrl, Integer imageHeight, Integer imageWidth, String productId, boolean isDefault, Long addedTime) {
        this.imageUrl = imageUrl;
        this.imageHeight = imageHeight;
        this.imageWidth = imageWidth;
        this.productId = productId;
        this.isDefault = isDefault;
        this.addedTime = addedTime;
    }

    public EntityProductImage(String imageUrl, Integer imageHeight, Integer imageWidth) {
        this.imageUrl = imageUrl;
        this.imageHeight = imageHeight;
        this.imageWidth = imageWidth;
    }

    public EntityProductImage() {
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(Integer imageHeight) {
        this.imageHeight = imageHeight;
    }

    public Integer getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(Integer imageWidth) {
        this.imageWidth = imageWidth;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Long getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(Long addedTime) {
        this.addedTime = addedTime;
    }

}
