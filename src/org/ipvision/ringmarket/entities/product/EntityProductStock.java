/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.product;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author saikat
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity
@Table(name = "product_stock")
@NamedQueries({
    @NamedQuery(name = "EntityProductStock.getProductsStock" , query = "SELECT STOCK FROM EntityProductStock AS STOCK WHERE STOCK.id IN : idList")
})

public class EntityProductStock implements Serializable {

    @Id
    @Column(name = "id", nullable = false, length = 36, columnDefinition = "varchar(36) NOT NULL")
    @SerializedName(MarketAttribute.ID)
    private String id;

    @SerializedName(MarketAttribute.UPDATED_TIME)
    @NotNull
    @Column(name = "update_time", columnDefinition = "bigint(20) not null DEFAULT '0'")
    private Long updateTime;

    @SerializedName(MarketAttribute.QUANTITY)
    @NotNull
    @Min(0)
    @Max(Integer.MAX_VALUE)
    @Column(name = "quantity", columnDefinition = "int(20) not null DEFAULT '0'")
    private Integer quantity;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
