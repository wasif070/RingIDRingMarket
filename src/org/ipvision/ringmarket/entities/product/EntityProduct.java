/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.product;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author alamgir
 */
/**
 * Here's the SQL statement that will find the closest 20 locations that are
 * within a radius of 25 miles to the (a, b) coordinate. It calculates the
 * distance based on the latitude/longitude of that row and the target
 * latitude/longitude, and then asks for only rows where the distance value is
 * less than 25, orders the whole query by distance, and limits it to 20
 * results. To search by kilometers instead of miles, replace 3959 with 6371.
 * SELECT id, ( 3959 * acos( cos( radians(a) ) * cos( radians( lat ) ) * cos(
 * radians( lon ) - radians(b) ) + sin( radians(a) ) * sin(radians(lat)) ) ) AS
 * distance FROM markers HAVING distance < 25 ORDER BY distance LIMIT 0 , 20;
 * ***
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity
//@Index(name = "idx_product_view_count", columnNames = {"view_count"})
@Table(name = "product", uniqueConstraints = {
    @UniqueConstraint(name = "uk_product_code", columnNames = "code")
})
@NamedQueries(
        {
            @NamedQuery(name = "getProductById", query = "select product from EntityProduct product where PRODUCT.id = :productId"),
            @NamedQuery(name = "getProductsByIds", query = "select product from EntityProduct product where PRODUCT.id in :productIds"),
//            @NamedQuery(name = "updateViewCount", query = "update EntityProduct product SET PRODUCT.viewCount=:viewCount where PRODUCT.id=:productId")
//            ,
            @NamedQuery(name = "EntityProduct.getUpwardFeaturedProducts", query = "select product.id from EntityProduct product where PRODUCT.isExclusive = TRUE and PRODUCT.id < :pivotProductId and PRODUCT.status =:status order by PRODUCT.id desc"),
            @NamedQuery(name = "EntityProduct.getDownwardFeaturedProducts", query = "select product.id from EntityProduct product where PRODUCT.isExclusive = TRUE and PRODUCT.id > :pivotProductId and PRODUCT.status =:status order by PRODUCT.id"),
            @NamedQuery(name = "EntityProduct.getUpwardTrendingProducts", query = "select product.id from EntityProduct product where PRODUCT.isTrending =: trending and PRODUCT.id < :pivotProductId and PRODUCT.status =:status order by PRODUCT.id desc"),
            @NamedQuery(name = "EntityProduct.getDownwardTrendingProducts", query = "select product.id from EntityProduct product where PRODUCT.isTrending =: trending and PRODUCT.id > :pivotProductId and PRODUCT.status =:status order by PRODUCT.id"),

            @NamedQuery(name = "EntityProduct.searchByNameScrollUp", query = "select product.id from EntityProduct product where PRODUCT.id < :pivotProductId and UPPER(PRODUCT.name) LIKE :param and PRODUCT.status =:status order by PRODUCT.id desc"),
            @NamedQuery(name = "EntityProduct.searchByNameScrollDown", query = "select product.id from EntityProduct product where PRODUCT.id > :pivotProductId and UPPER(PRODUCT.name) LIKE :param and PRODUCT.status =:status order by PRODUCT.id"),

            @NamedQuery(name = "EntityProduct.getUpwardNonExclusiveProducts", query = "select product.id from EntityProduct product where PRODUCT.id < :pivotProductId and PRODUCT.isExclusive = FALSE and PRODUCT.status =:status order by PRODUCT.id desc"),
            @NamedQuery(name = "EntityProduct.getDownwardNonExclusiveProducts", query = "select product.id from EntityProduct product where PRODUCT.id > :pivotProductId and PRODUCT.isExclusive = FALSE and PRODUCT.status =:status order by PRODUCT.id"),
            @NamedQuery(name = "EntityProduct.getUpwardDiscountProducts", query = "select product.id from EntityProduct product where PRODUCT.id < :pivotProductId and PRODUCT.isDiscounted = TRUE and PRODUCT.status =:status order by PRODUCT.id desc"),
            @NamedQuery(name = "EntityProduct.getDownwardDiscountProducts", query = "select product.id from EntityProduct product where PRODUCT.id > :pivotProductId and PRODUCT.isDiscounted = TRUE and PRODUCT.status =:status order by PRODUCT.id"),

            @NamedQuery(name = "EntityProduct.getOldestProductsOfShop", query = "select product.id from EntityProduct product where PRODUCT.shopId = :shopId and PRODUCT.id > :pivotProductId and PRODUCT.status =:status order by PRODUCT.id"),
            @NamedQuery(name = "EntityProduct.getLatestProductsOfShop", query = "select product.id from EntityProduct product where PRODUCT.shopId = :shopId and PRODUCT.id < :pivotProductId and PRODUCT.status =:status order by PRODUCT.id DESC"),
            @NamedQuery(name = "EntityProduct.getLatestProductsOfShopPivotEmpty", query = "select product.id from EntityProduct product where PRODUCT.shopId = :shopId and PRODUCT.status =:status order by PRODUCT.id DESC"),
            @NamedQuery(name = "EntityProduct.getProductsOfShop", query = "select product.id from EntityProduct product where PRODUCT.shopId = :shopId"),
//            @NamedQuery(name = "getUpwardMostViewedProducts", query = "select product from EntityProduct product where PRODUCT.id < :pivotProductId ORDER BY PRODUCT.viewCount ASC")
//            ,
//            @NamedQuery(name = "getDownwardMostViewedProducts", query = "select product from EntityProduct product where PRODUCT.id > :pivotProductId ORDER BY PRODUCT.viewCount DESC")
//            ,
            @NamedQuery(name = "getDownwardProductsInAPriceRange", query = "select product from EntityProduct product where PRODUCT.id < :pivotProductId and PRODUCT.price > :leastPrice and PRODUCT.price < :mostPrice and PRODUCT.status =:status ORDER BY PRODUCT.price DESC"),
            @NamedQuery(name = "getUpwardProductsInAPriceRange", query = "select product from EntityProduct product where PRODUCT.id < :pivotProductId and PRODUCT.price > :leastPrice and PRODUCT.price < :mostPrice and PRODUCT.status =:status ORDER BY PRODUCT.price DESC"),
            @NamedQuery(name = "getAllProducts", query = "select product from EntityProduct product"),
            @NamedQuery(name = "EntityProduct.getProductByCode", query = "select PRODUCT.id from EntityProduct PRODUCT WHERE PRODUCT.code=:code"),
            @NamedQuery(name = "EntityProduct.updateStatusByShop", query = "UPDATE EntityProduct p SET p.status=:updatedStatus WHERE p.shopId=:shopId AND p.status=:status"),
            @NamedQuery(name = "EntityProduct.updateStatusByShop", query = "UPDATE EntityProduct p SET p.status=:updatedStatus WHERE p.shopId=:shopId AND p.status=:status"),
            //            @NamedQuery(name = "getUpwardNearestBoundingBoxProducts", query = "select product from EntityProduct product where PRODUCT.id < :pivotProductId AND PRODUCT.lat > :x1 and PRODUCT.lat < :x2 and PRODUCT.lon < :y1 and PRODUCT.lon > :y2 ORDER BY PRODUCT.id ASC")
            //            ,
            //            @NamedQuery(name = "getDownwardNearestBoundingBoxProducts", query = "select product from EntityProduct product where PRODUCT.id > :pivotProductId AND PRODUCT.lat > :x1 and PRODUCT.lat < :x2 and PRODUCT.lon < :y1 and PRODUCT.lon > :y2 ORDER BY PRODUCT.id DESC"),
            @NamedQuery(name = "EntityProduct.productsOfShopScrollUp", query = "select product.id from EntityProduct product where product.shopId = :shopId and product.id <:pivotId and product.status =:status order by product.id desc"),
            @NamedQuery(name = "EntityProduct.productsOfShopScrollDown", query = "select product.id from EntityProduct product where product.shopId = :shopId and product.id >:pivotId and product.status =:status order by product.id asc"),
            @NamedQuery(name = "EntityProduct.productsOfShopScrollDownPivotEmpty", query = "select product.id from EntityProduct product where product.shopId = :shopId and product.status =:status order by product.id asc"),
            @NamedQuery(name = "EntityProduct.updateShopName", query = "UPDATE EntityProduct p SET p.shopName =:name WHERE p.shopId=:shopId")
        }
)
@NamedNativeQueries(
        {
            @NamedNativeQuery(name = "getDownwardNearestBoundingBoxProducts", query = "select p.* from product p where p.id > ?1 AND p.lat > ?2 and p.lat < ?3 AND p.lon < ?4 and p.lon > ?5 AND 6371 * 2 * ASIN(SQRT( POWER(SIN((?6 - p.lat) * pi()/180 / 2), 2) + COS(?6 * pi()/180) * COS(p.lat * pi()/180) * POWER(SIN((?7 -p.lon) * pi()/180 / 2), 2) )) < ?8"),
            @NamedNativeQuery(name = "getUpwardNearestBoundingBoxProducts", query = "select p.* from product p where p.id < ?1 AND p.lat > ?2 and p.lat < ?3 AND p.lon < ?4 and p.lon > ?5 AND 6371 * 2 * ASIN(SQRT( POWER(SIN((?6 - p.lat) * pi()/180 / 2), 2) + COS(?6 * pi()/180) * COS(p.lat * pi()/180) * POWER(SIN((?7 -p.lon) * pi()/180 / 2), 2) )) < ?8"),
            @NamedNativeQuery(name = "EntityProduct.searchFullTextScrollUp", query = "select product.id from product where MATCH (product.name) against(? in BOOLEAN MODE) and product.id<? and product.status =? order by product.id desc"),
            @NamedNativeQuery(name = "EntityProduct.searchFullTextScrollDown", query = "select product.id from product where MATCH (product.name) against(? in BOOLEAN MODE) and product.id>? and product.status =? order by product.id asc")
        }
)
public class EntityProduct implements Serializable {

    @Id
    @Column(name = "id", nullable = false, length = 36, columnDefinition = "varchar(36) NOT NULL")
    @SerializedName(MarketAttribute.ID)
    private String id;

    @Column(name = "name", length = 500, nullable = false)
    @SerializedName("nm")
    private String name;

    @SerializedName(value = MarketAttribute.IS_EXCLUSIVE)
    @Column(name = "is_exclusive", columnDefinition = "bit(1) DEFAULT b'0'")
    private Boolean isExclusive;

    @SerializedName(value = MarketAttribute.IS_TRENDING)
    @Column(name = "trending", columnDefinition = "bit(1) DEFAULT b'0'")
    private Boolean isTrending;

    @SerializedName(MarketAttribute.LAT)
    @Column(name = "lat", nullable = false, columnDefinition = "float NOT NULL DEFAULT '0'")
    private Float lat;

    @SerializedName(MarketAttribute.LON)
    @Column(name = "lon", nullable = false, columnDefinition = "float NOT NULL DEFAULT '0'")
    private Float lon;

    @SerializedName(MarketAttribute.STATUS)
    @Column(name = "status", nullable = false, columnDefinition = "tinyint(1) NOT NULL DEFAULT '3'")
    private Byte status;

    @SerializedName(MarketAttribute.PRICE)
    @Column(name = "base_price", nullable = false, columnDefinition = "double(10,2) NOT NULL DEFAULT '0.00'")
    private Double price;

//    @SerializedName(MarketAttribute.OLD_PRICE)
//    @Column(name = "old_price", nullable = false, columnDefinition = "double(10,2) NOT NULL DEFAULT '0.00'")
//    private Double oldPrice;
    @SerializedName(MarketAttribute.IMAGE_URL)
    @Column(name = "default_image_url", length = 200, nullable = false, columnDefinition = "varchar(200) NOT NULL DEFAULT ''")
    private String imageUrl;

    @SerializedName(MarketAttribute.IMAGE_HEIGHT)
    @Column(name = "default_image_height", nullable = false, columnDefinition = "int(5) NOT NULL DEFAULT '0'")
    private int imageHeight;

    @SerializedName(MarketAttribute.IMAGE_WIDTH)
    @Column(name = "default_image_width", nullable = false, columnDefinition = "int(5) NOT NULL DEFAULT '0'")
    private int imageWidth;

    @SerializedName(MarketAttribute.CURRENCY_CODE)
    @Column(name = "currency_code", length = 10, nullable = false, columnDefinition = "varchar(10) NOT NULL DEFAULT '$'")
    private String currencyCode;

    @SerializedName(MarketAttribute.CREATION_TIME)
    @Column(name = "creation_time", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long creationTime;

    @SerializedName(MarketAttribute.UPDATED_TIME)
    @Column(name = "update_time", nullable = true, columnDefinition = "bigint(20) DEFAULT '0'")
    private Long updateTime;

    @Index(name = "idx_product_shop_id", columnNames = {"shop_id", "id"})
    @SerializedName(MarketAttribute.SHOP_ID)
    @Column(name = "shop_id", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long shopId;

    @SerializedName(MarketAttribute.SHOP_NAME)
    @Column(name = "shop_name", length = 100, nullable = false, columnDefinition = "varchar(100) NOT NULL DEFAULT ''")
    private String shopName;

    @SerializedName(MarketAttribute.CODE)
    @Column(name = "code", length = 10, nullable = false, columnDefinition = "varchar(30) NOT NULL DEFAULT ''")
    private String code;

    @Column(name = "is_discounted", columnDefinition = "bit(1) DEFAULT b'0'")
    @SerializedName(MarketAttribute.DISCOUNT_AVALIABLE)
    private boolean isDiscounted;

    @Transient
    @SerializedName(MarketAttribute.RATING)
    private Double ratings;

    @Transient
    @SerializedName(MarketAttribute.OLD_PRICE)
    private Double oldPrice;

    @Transient
    @SerializedName(MarketAttribute.DISCOUNT)
    private Float discount;

    @Transient
    @SerializedName(MarketAttribute.DISCOUNT_UNIT)
    private Byte discountUnit;

    @Transient
    @SerializedName(MarketAttribute.VALIDITY_TIME)
    private Long validityTime;

    @Transient
    @SerializedName(MarketAttribute.ACTIVE_TIME)
    private Long activeTime;

//    @Transient
//    private BigDecimal discount;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isExclusive() {
        return isExclusive;
    }

    public void setIsExclusive(Boolean isExclusive) {
        this.isExclusive = isExclusive;
    }

    public Float getLat() {
        return lat;
    }

    public Long getValidityTime() {
        return validityTime;
    }

    public void setValidityTime(Long validityTime) {
        this.validityTime = validityTime;
    }

    public Long getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(Long activeTime) {
        this.activeTime = activeTime;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }

    public void setStatus(ProductStatus status) {
        this.status = status.getValue();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = Math.max(imageHeight, 300);
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

//    public Double getOldPrice() {
//        return oldPrice;
//    }
//
//    public void setOldPrice(Double oldPrice) {
//        this.oldPrice = oldPrice;
//    }
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Double getRatings() {
        return ratings;
    }

    public void setRatings(Double ratings) {
        this.ratings = ratings;
    }

    public Integer getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = Math.max(imageHeight, 300);
    }

    public Double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Double oldPrice) {
        this.oldPrice = oldPrice;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getIsTrending() {
        return isTrending;
    }

    public void setIsTrending(Boolean isTrending) {
        this.isTrending = isTrending;
    }

    public boolean isDiscounted() {
        return isDiscounted;
    }

    public void setIsDiscounted(boolean isDiscounted) {
        this.isDiscounted = isDiscounted;
    }

    public Byte getDiscountUnit() {
        return discountUnit;
    }

    public void setDiscountUnit(Byte discountUnit) {
        this.discountUnit = discountUnit;
    }

}
