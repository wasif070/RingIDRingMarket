/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.product;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.entities.id.ProductViewId;

/**
 *
 * @author alamgir
 */
@IdClass(ProductViewId.class)
@Entity
@Table(name = "product_view")
@NamedQueries({
    //    @NamedQuery(name = "getIsAlreadyViewed", query = "SELECT t0 FROM EntityProductView t0 WHERE t0.buyerId = :buyerId AND t0.productId = :productId")
    @NamedQuery(name = "getIsAlreadyViewed", query = "SELECT count(T0) FROM EntityProductView t0 WHERE t0.buyerId = :buyerId AND t0.productId = :productId"),
    @NamedQuery(name = "EntityProductView.incViewCount", query = "UPDATE EntityProductView t0 SET t0.viewCount = t0.viewCount + 1 WHERE t0.buyerId = :buyerId AND t0.productId = :productId")
})
public class EntityProductView implements Serializable {

    @Id
    @Column(name = "buyer_id", columnDefinition = "bigint(20) NOT NULL", nullable = false)
    @SerializedName(MarketAttribute.BUYER_ID)
    private long buyerId;

    @Id
    @Column(name = "product_id", columnDefinition = "varchar(36) NOT NULL", length = 36, nullable = false)
    @SerializedName(MarketAttribute.PRODUCT_ID)
    private String productId;

    @Column(name = "view_count", columnDefinition = "int(5) default 0")
    @SerializedName(MarketAttribute.VIEW_COUNT)
    private int viewCount;
    
    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

}
