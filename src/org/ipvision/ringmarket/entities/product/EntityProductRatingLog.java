package org.ipvision.ringmarket.entities.product;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.id.ProductRatingLogId;

@Cacheable(DBConnection.CACHEABLE)
@Entity
@Table(name = "product_rating_log")
@IdClass(ProductRatingLogId.class)
@NamedQueries(
        {
            @NamedQuery(name = "EntityProductRatingLog.getRatingDetails", query = "SELECT e.rating, COUNT(e.userId) FROM  EntityProductRatingLog e WHERE e.productId=:productId GROUP BY e.rating")
        }
)
public class EntityProductRatingLog implements Serializable {

    @Column(name = "user_id", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    @SerializedName(MarketAttribute.USER_ID)
    @Id
    private long userId;

    @Column(name = "product_id", nullable = false, columnDefinition = "varchar(36) NOT NULL DEFAULT ''")
    @SerializedName(MarketAttribute.PRODUCT_ID)
    @Id
    private String productId;

    @Column(name = "rating", nullable = false, columnDefinition = "tinyint(2) DEFAULT '0'")
    @SerializedName(MarketAttribute.RATING)
    private byte rating;
    
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public byte getRating() {
        return rating;
    }

    public void setRating(Byte rating) {
        this.rating = rating;
    }

}
