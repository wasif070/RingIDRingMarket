/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.ipvision.ringmarket.constants.OrderStatus;

/**
 *
 * @author Imran
 */
@Entity
@Table(name = "order_product", uniqueConstraints = {
    @UniqueConstraint(name = "idx_unique_order_product", columnNames = {"product_id"})})
@NamedQueries(
        {
            @NamedQuery(name = "getDownwardOrdersByBuyer", query = "SELECT order_product FROM EntityOrderProduct order_product where ORDER_PRODUCT.buyerId = :buyerId AND ORDER_PRODUCT.id > : pivotOrderId ORDER BY ORDER_PRODUCT.id ASC"),
            @NamedQuery(name = "getUpwardOrdersByBuyer", query = "SELECT order_product FROM EntityOrderProduct order_product where order_product.buyerId = :buyerId AND order_product.id < : pivotOrderId ORDER BY order_product.id DESC"),
            @NamedQuery(name = "getDownwardOrdersBySeller", query = "SELECT order_product FROM EntityOrderProduct order_product where order_product.sellerId = :sellerId AND order_product.id > : pivotOrderId ORDER BY order_product.id ASC"),
            @NamedQuery(name = "getUpwardOrdersBySeller", query = "SELECT order_product FROM EntityOrderProduct order_product where order_product.sellerId = :sellerId AND order_product.id < : pivotOrderId ORDER BY order_product.id DESC"),
            @NamedQuery(name = "getIsAlreadyOrdered", query = "select order_product from EntityOrderProduct order_product WHERE order_product.productId=:productId")
        }
)
public class EntityOrderProduct implements Serializable {

    @Column(name = "id", length = 40, nullable = false)
    @SerializedName("id")
    @Id
    private String id;

    @Column(name = "order_date", nullable = false)
    @SerializedName("ordrDate")
    private long orderDate;

    @Column(name = "agent_id", nullable = false)
    @SerializedName("agntId")
    private long agentId;

    @Column(name = "status", nullable = false)
    @SerializedName("sts")
//    @Enumerated
//    private OrderStatus status;
    private int status;

    @Column(name = "shipment_method", nullable = false, columnDefinition = "int(11) DEFAULT '0'")
    @SerializedName("shpmntMtd")
    private int shipmentMethod;

    @Column(name = "shipment_cost", nullable = false, columnDefinition = "double(10,2) DEFAULT '0.00'")
    @SerializedName("shpmntCst")
    private double shipmentCost;

    @Column(name = "exp_delivery_date", nullable = false)
    @SerializedName("expDlvyDate")
    private long expDeliveryDate;

    @Column(name = "offered_price", nullable = false, columnDefinition = "double(10,2) DEFAULT '0.00'")
    @SerializedName("ofrPrc")
    private double offeredPrice;

    @Column(name = "seller_id", nullable = false)
    @SerializedName("slrId")
    private long sellerId;

    @Column(name = "buyer_id", nullable = false)
    @SerializedName("byrId")
    private long buyerId;

    @Column(name = "product_id", length = 40, nullable = false)
    @SerializedName("prodId")
    private String productId;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(long orderDate) {
        this.orderDate = orderDate;
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

//    public OrderStatus getStatus() {
//        return status;
//    }
//
//    public void setStatus(OrderStatus status) {
//        this.status = status;
//    }
    public void setStatus(OrderStatus status) {
        this.status = status.ordinal();
    }

    public int getShipmentMethod() {
        return shipmentMethod;
    }

    public void setShipmentMethod(int shipmentMethod) {
        this.shipmentMethod = shipmentMethod;
    }

    public double getShipmentCost() {
        return shipmentCost;
    }

    public void setShipmentCost(double shipmentCost) {
        this.shipmentCost = shipmentCost;
    }

    public long getExpDeliveryDate() {
        return expDeliveryDate;
    }

    public void setExpDeliveryDate(long expDeliveryDate) {
        this.expDeliveryDate = expDeliveryDate;
    }

    public double getOfferedPrice() {
        return offeredPrice;
    }

    public void setOfferedPrice(double offeredPrice) {
        this.offeredPrice = offeredPrice;
    }

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

}
