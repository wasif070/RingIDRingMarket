/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.id;

import java.io.Serializable;

/**
 *
 * @author alamgir
 */
public class ProductLikeId implements Serializable {

    private long buyerId;
    private String productId;

    public ProductLikeId() {}

    public ProductLikeId(long buyerId, String productId) {
        this.buyerId = buyerId;
        this.productId = productId;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

}
