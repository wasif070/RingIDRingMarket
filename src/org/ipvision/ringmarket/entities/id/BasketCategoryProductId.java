/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.id;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alamgir
 */
public class BasketCategoryProductId implements Serializable {

    private int categoryId;
    private String productId;
    private String basketId;

    public BasketCategoryProductId() {
    }

    public BasketCategoryProductId(String basketId, int categoryId, String productId) {
        this.basketId = basketId;
        this.categoryId = categoryId;
        this.productId = productId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setBasketId(String basketId) {
        this.basketId = basketId;
    }

    public String getBasketId() {
        return basketId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BasketCategoryProductId)) {
            return false;
        }
        BasketCategoryProductId that = (BasketCategoryProductId) o;
        return Objects.equals(getCategoryId(), that.getCategoryId())
                && Objects.equals(getBasketId(), that.getBasketId())
                && Objects.equals(getProductId(), that.getProductId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCategoryId(), getBasketId(), getProductId());
    }

}
