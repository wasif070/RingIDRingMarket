/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.id;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alamgir
 */
public class CategoryProductId implements Serializable {

    int categoryId;
    String productId;
    Long shopId;

    public CategoryProductId() {
    }

    public CategoryProductId(int categoryId, Long shopId, String productId) {
        this.categoryId = categoryId;
        this.shopId = shopId;
        this.productId = productId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategoryProductId)) {
            return false;
        }
        CategoryProductId that = (CategoryProductId) o;
        return Objects.equals(getCategoryId(), that.getCategoryId())
                && Objects.equals(getShopId(), that.getShopId())
                && Objects.equals(getProductId(), that.getProductId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCategoryId(), getShopId(), getProductId());
    }

}
