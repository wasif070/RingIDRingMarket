/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.id;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alamgir
 */
public class SellerRatingLogId implements Serializable {

    long buyerId;
    long sellerId;

    public SellerRatingLogId(long buyerId, long sellerId) {
        this.buyerId = buyerId;
        this.sellerId = sellerId;
    }

    public SellerRatingLogId() {
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SellerRatingLogId)) {
            return false;
        }
        SellerRatingLogId that = (SellerRatingLogId) o;
        return Objects.equals(getBuyerId(), that.getBuyerId())
                && Objects.equals(getSellerId(), that.getSellerId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBuyerId(), getSellerId());
    }

}
