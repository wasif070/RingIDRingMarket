/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.id;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alamgir
 */
public class ProductRatingLogId implements Serializable {

    long userId;
    String productId;

    public ProductRatingLogId(String productId, long userId) {
        this.userId = userId;
        this.productId = productId;
    }

    public ProductRatingLogId() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

 @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductRatingLogId)) {
            return false;
        }
        ProductRatingLogId that = (ProductRatingLogId) o;
        return Objects.equals(getUserId(), that.getUserId())
                && Objects.equals(getProductId(), that.getProductId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getProductId());
    }

}
