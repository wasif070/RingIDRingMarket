/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.id;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alamgir
 */
public class OrderDeliveryId implements Serializable {

    private Long carrierId;
    private String orderId;
    private String productId;

    public OrderDeliveryId() {
    }

    public OrderDeliveryId(Long carrierId, String orderId, String productId) {
        this.carrierId = carrierId;
        this.orderId = orderId;
        this.productId = productId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(Long carrierId) {
        this.carrierId = carrierId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderDeliveryId)) {
            return false;
        }
        OrderDeliveryId that = (OrderDeliveryId) o;
        return Objects.equals(getCarrierId(), that.getCarrierId())
                && Objects.equals(getOrderId(), that.getOrderId())
                && Objects.equals(getProductId(), that.getProductId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCarrierId(), getOrderId(), getProductId());
    }

}
