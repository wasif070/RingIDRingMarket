/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.id;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alamgir
 */
public class ProductViewId implements Serializable {

    private long buyerId;
    private String productId;

    public ProductViewId() {}

    public ProductViewId(long buyerId, String productId) {
        this.buyerId = buyerId;
        this.productId = productId;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

 @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductViewId)) {
            return false;
        }
        ProductViewId that = (ProductViewId) o;
        return Objects.equals(getBuyerId(), that.getBuyerId())
                && Objects.equals(getProductId(), that.getProductId());
    }

    @Override
    public int hashCode() {
        return Objects.hash( getBuyerId(), getProductId());
    }
}
