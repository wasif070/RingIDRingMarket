/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.ipvision.ringmarket.cache.annotation.Cache;
import org.ipvision.ringmarket.cache.annotation.CacheType;
import org.ipvision.ringmarket.constants.RingmarketRole;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.id.UserRoleId;


/**
 *
 * @author alamgir
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity 
@IdClass(UserRoleId.class)
@Table(name = "user_role")
@NamedQueries(
    {
        @NamedQuery(name = "getRolesByRoleType", query = "select user_role from EntityUserRole user_role where USER_ROLE.role = :role")
    }
)
public class EntityUserRole implements Serializable {
    
    @Id
    @Column(name = "id")
    private long userId;

    @Id
    @Column(name = "role")
    private int role;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public void setRole(RingmarketRole role) {
        this.role = role.ordinal();
    }
}