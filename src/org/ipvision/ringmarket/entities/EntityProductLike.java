/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import org.ipvision.ringmarket.entities.id.ProductLikeId;

/**
 *
 * @author alamgir
 */
@IdClass(ProductLikeId.class)
@Entity
@Table(name = "product_like")
public class EntityProductLike implements Serializable {

    @Id
    @Column(name = "buyer_id", columnDefinition = "bigint(20) NOT NULL", nullable = false)
    @SerializedName("byrId")
    private long buyerId;

    @Id
    @Column(name = "product_id", columnDefinition = "varchar(40) NOT NULL", length = 40, nullable = false)
    @SerializedName("prodId")
    private String productId;

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

}
