/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.category;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author alamgir
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity
@Table(name = "category_product_count")

public class EntityCategoryProductCount implements Serializable {

    @Id
    @Column(name = "category_id", columnDefinition = "int(11) DEFAULT '0'", nullable = false)
    @SerializedName(MarketAttribute.CATEGORY_ID)
    private int categoryId;

    @Column(name = "count", columnDefinition = "int(11) DEFAULT '0'", nullable = false)
    private int count;

    public EntityCategoryProductCount(int categoryId, int count) {
        this.categoryId = categoryId;
        this.count = count;
    }

    protected EntityCategoryProductCount() {

    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
