/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.category;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.id.CategoryProductId;

/**
 *
 * @author alamgir
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity
@Table(name = "category_product")
@IdClass(CategoryProductId.class)
@NamedQueries(
        {
            @NamedQuery(name = "getCategoryProduct", query = "select category_product from EntityCategoryProduct category_product where category_product.categoryId = :categoryId and category_product.productId = :productId AND CATEGORY_PRODUCT.isProductEnabled = true")
            ,
            @NamedQuery(name = "getDownwardProductIdsByCategory", query = "select category_product.categoryId, category_product.shopId , category_product.productId from EntityCategoryProduct category_product where category_product.categoryId = :categoryId AND CATEGORY_PRODUCT.isProductEnabled = true and CATEGORY_PRODUCT.productId > :pivotProductId ORDER BY CATEGORY_PRODUCT.productId  ASC")
            ,
            @NamedQuery(name = "getUpwardProductIdsByCategory", query = "select category_product.categoryId, category_product.shopId , category_product.productId from EntityCategoryProduct category_product where category_product.categoryId = :categoryId  AND CATEGORY_PRODUCT.isProductEnabled = true and CATEGORY_PRODUCT.productId < :pivotProductId ORDER BY CATEGORY_PRODUCT.productId DESC")
            ,
            @NamedQuery(name = "EntityCategoryProduct.getProductIdsByCategoryAndShopScrollDown", query = "select category_product.categoryId, category_product.shopId , category_product.productId from EntityCategoryProduct category_product where category_product.shopId= :shopId  AND CATEGORY_PRODUCT.isProductEnabled = true and category_product.categoryId = :categoryId and CATEGORY_PRODUCT.productId > :pivotProductId ORDER BY CATEGORY_PRODUCT.productId ASC")
            ,
            @NamedQuery(name = "EntityCategoryProduct.getProductIdsByCategoryAndShopScrollUp", query = "select category_product.categoryId, category_product.shopId , category_product.productId from EntityCategoryProduct category_product where category_product.shopId= :shopId  AND CATEGORY_PRODUCT.isProductEnabled = true and category_product.categoryId = :categoryId and CATEGORY_PRODUCT.productId < :pivotProductId ORDER BY CATEGORY_PRODUCT.productId DESC")
            ,
            @NamedQuery(name = "getCategoriesByProduct", query = "select category_product from EntityCategoryProduct category_product where category_product.productId = :productId  AND CATEGORY_PRODUCT.isProductEnabled = true")
            ,
            @NamedQuery(name = "productCountByCategoryId", query = "select count(category_product.productId) from EntityCategoryProduct category_product where category_product.categoryId = :categoryId  AND CATEGORY_PRODUCT.isProductEnabled = true")
            ,
            @NamedQuery(name = "EntityCategoryProduct.getCategoriesByShopAndPosition", query = "select distinct(c.categoryId) from EntityCategoryProduct c where c.shopId= :shopId and c.position = :position ORDER BY c.categoryId ASC")
            ,
            @NamedQuery(name = "EntityCategoryProduct.updateProductStatus", query = "update EntityCategoryProduct cp set cp.isProductEnabled = :status where cp.productId = :productId")
            ,
            @NamedQuery(name = "EntityCategoryProduct.updateShopStatus", query = "update EntityCategoryProduct cp set cp.isProductEnabled = :status where cp.shopId = :shopId")
        }
)
@NamedNativeQueries(
        {
            @NamedNativeQuery(name = "deleteFromCategoryProduct", query = "delete from category_product where category_id = ?1 and product_id = ?2")
            ,
            @NamedNativeQuery(name = "deleteCategoriesOfProduct", query = "delete from category_product where product_id = ?1")
        }
)
public class EntityCategoryProduct implements Serializable {

//    @Index(name = "idx_category_product", columnNames = {"category_id"})
    @Id
    @Column(name = "category_id")
    @SerializedName(MarketAttribute.CATEGORY_ID)
    private int categoryId;

    @Id
    @Index(name = "idx_category_product_shop_id", columnNames = {"shop_id"})
    @SerializedName(MarketAttribute.SHOP_ID)
    @Column(name = "shop_id", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long shopId;

    @Id
    @Column(name = "product_id", length = 36, nullable = false)
    @SerializedName(MarketAttribute.PRODUCT_ID)
    private String productId;

    @Column(name = "position", columnDefinition = "int(11) DEFAULT '1'", nullable = false)
    @SerializedName(MarketAttribute.POSITION)
    private int position = 1;

    @Column(name = "prod_en", columnDefinition = "bit(1) DEFAULT b'1'")
    @SerializedName("prodEn")
    private Boolean isProductEnabled = true;

    public Boolean getIsProductEnabled() {
        return isProductEnabled;
    }

    public void setIsProductEnabled(Boolean isProductEnabled) {
        this.isProductEnabled = isProductEnabled;
    }

    public EntityCategoryProduct(int categoryId, String productId, Long shopId) {
        this.categoryId = categoryId;
        this.productId = productId;
        this.shopId = shopId;
    }

    public EntityCategoryProduct(int categoryId, String productId, Long shopId, int pos) {
        this.categoryId = categoryId;
        this.productId = productId;
        this.shopId = shopId;
        this.position = pos;
    }

    public EntityCategoryProduct() {
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
