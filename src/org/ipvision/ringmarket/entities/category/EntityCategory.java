/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.category;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.apache.openjpa.kernel.QueryHints;
import org.apache.openjpa.persistence.DataCache;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author alamgir
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity
@Table(name = "category", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"name", "parent"}, name = "idx_category")
})
@NamedQueries(
        {
            @NamedQuery(name = "getRootCategories", query = "select product_categories from EntityCategory product_categories where PRODUCT_CATEGORIES.parentId = 0 order by PRODUCT_CATEGORIES.ordinal desc"),
            @NamedQuery(name = "getSubCategoriesById", query = "select product_categories from EntityCategory product_categories where PRODUCT_CATEGORIES.parentId = :parentId"),
            @NamedQuery(name = "getDownwardCategoriesByScroll", query = "select product_categories from EntityCategory product_categories where PRODUCT_CATEGORIES.parentId = :parentId And product_categories.id > :pivotCategoryId Order By PRODUCT_CATEGORIES.id ASC"),
            @NamedQuery(name = "getUpwardCategoriesByScroll", query = "select product_categories from EntityCategory product_categories where PRODUCT_CATEGORIES.parentId = :parentId And product_categories.id < :pivotCategoryId Order By PRODUCT_CATEGORIES.id DESC"),}
)
public class EntityCategory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;

    @Index(name = "idx_category", columnNames = {"name", "parent"}, unique = true)
    @Column(name = "name", length = 100, nullable = false)
    @SerializedName(MarketAttribute.NAME)
    private String name;

    @Index(name = "idx_category_parent", columnNames = {"parent"}, unique = false)
    @Column(name = "parent", columnDefinition = "int(11) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName(MarketAttribute.PARENT_ID)
    private int parentId;

    @Column(name = "position", columnDefinition = "int(11) DEFAULT '1'", nullable = false)
    @SerializedName(MarketAttribute.POSITION)
    private int position = 1;

    @Column(name = "ordinal", columnDefinition = "int(11) DEFAULT '0'", nullable = false)
    @SerializedName(MarketAttribute.ORDINAL)
    private int ordinal;

    @Column(name = "imageUrl", length = 200)
    @SerializedName(MarketAttribute.IMAGE_URL)
    private String imageUrl;

    @Column(name = "hasNext", columnDefinition = "bit(1) DEFAULT b'0'")
    private Boolean hasNext = false;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean getHasNext() {
        return hasNext;
    }

    public void setHasNext(Boolean hasNext) {
        this.hasNext = hasNext;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    
}
