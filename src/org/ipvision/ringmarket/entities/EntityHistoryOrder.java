package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.ipvision.ringmarket.constants.OrderStatus;

@Entity
@Table(name = "history_order")
@NamedQueries(
        {
            @NamedQuery(name = "getCompletedOrderByBuyer", query = "SELECT history_order FROM EntityHistoryOrder history_order where history_order.buyerId := buyerId"),
            @NamedQuery(name = "getCompletedOrderBySeller", query = "SELECT history_order FROM EntityHistoryOrder history_order where history_order.sellerId := sellerId")
        }
)
public class EntityHistoryOrder implements Serializable {

    @Column(name = "id", nullable = false)
    @Id
    @SerializedName("id")
    private String id;

    @Column(name = "agent_id", nullable = false)
    @SerializedName("agntId")
    private long agentId;

    @Column(name = "shipment_cost", nullable = false, columnDefinition = "double(10,2) default '0.00'")
    @SerializedName("shpmntCst")
    private double shipmentCost;

    @Column(name = "order_completed_date", nullable = false, columnDefinition = "bigint(20) default '0'")
    @SerializedName("ordrCmptdDate")
    private long orderCompletedDate;

    @Column(name = "offered_price", nullable = false, columnDefinition = "double(10,2) default '0.00'")
    @SerializedName("ofrPrc")
    private double offeredPrice;

    @Column(name = "seller_id", nullable = false)
    @SerializedName("slrId")
    private long sellerId;

    @Column(name = "buyer_id", nullable = false)
    @SerializedName("byrId")
    private long buyerId;

    @Column(name = "product_id", nullable = false)
    @SerializedName("prodId")
    private String productId;

    @Column(name = "order_status", nullable = false, columnDefinition = "int(5) NOT NULL")
    @SerializedName("ordrSts")
    private int orderStatus;

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus.ordinal();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public double getShipmentCost() {
        return shipmentCost;
    }

    public void setShipmentCost(double shipmentCost) {
        this.shipmentCost = shipmentCost;
    }

    public long getOrderCompletedDate() {
        return orderCompletedDate;
    }

    public void setOrderCompletedDate(long orderCompletedDate) {
        this.orderCompletedDate = orderCompletedDate;
    }

    public double getOfferedPrice() {
        return offeredPrice;
    }

    public void setOfferedPrice(double offeredPrice) {
        this.offeredPrice = offeredPrice;
    }

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

}
