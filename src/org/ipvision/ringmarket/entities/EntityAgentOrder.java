package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.db.DBConnection;

@Cacheable(DBConnection.CACHEABLE)
@Entity
@Table(name = "agent_order")
@NamedQueries(
        {
            @NamedQuery(name = "getUpwardAgentOrderList", query = "select agent_order from EntityAgentOrder agent_order where AGENT_ORDER.agentId = :agentId AND AGENT_ORDER.orderId < :pivotOrderId ORDER BY AGENT_ORDER.orderId DESC"),
            @NamedQuery(name = "getDownwardAgentOrderList", query = "select agent_order from EntityAgentOrder agent_order where AGENT_ORDER.agentId = :agentId AND AGENT_ORDER.orderId > :pivotOrderId ORDER BY AGENT_ORDER.orderId ASC")
        }
)
public class EntityAgentOrder implements Serializable {

    @Column(name = "order_id", nullable = false, length = 40)
    @Id
    @SerializedName("ordrId")
    private String orderId;

    @Index(name = "idx_agent", columnNames = {"agent_id"})
    @Column(name = "agent_id", nullable = false)
    @SerializedName("agntId")
    private long agentId;

    @Column(name = "exp_delivery_date", nullable = false, columnDefinition = "bigint(20) NOT NULL")
    @SerializedName("expDlvyDate")
    private long expDeliveryDate;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public long getExpDeliveryDate() {
        return expDeliveryDate;
    }

    public void setExpDeliveryDate(long expDeliveryDate) {
        this.expDeliveryDate = expDeliveryDate;
    }
}
