package org.ipvision.ringmarket.entities;

//import com.mysql.clusterj.annotation.Column;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.OrderStatus;

@Entity
@Table(name = "history_agent_order")
@NamedQueries(
    {
        @NamedQuery(name = "getUpwardHistoryAgentOrderList", query = "select agent_order from EntityHistoryAgentOrder agent_order where AGENT_ORDER.agentId = :agentId AND AGENT_ORDER.orderId < :pivotOrderId ORDER BY AGENT_ORDER.orderId DESC"),
        @NamedQuery(name = "getDownwardHistoryAgentOrderList", query = "select agent_order from EntityHistoryAgentOrder agent_order where AGENT_ORDER.agentId = :agentId AND AGENT_ORDER.orderId > :pivotOrderId ORDER BY AGENT_ORDER.orderId ASC")
    }
)
public class EntityHistoryAgentOrder implements Serializable {

    @Column(name = "order_id", nullable = false, length = 40)
    @Id
    @SerializedName("ordrId")
    private String orderId;

    @Index(name = "idx_agent", columnNames = {"agent_id"})
    @Column(name = "agent_id", nullable = false)
    @SerializedName("agntId")
    private long agentId;

    @Column(name = "order_status", nullable = false, columnDefinition = "int(5) NOT NULL")
    @SerializedName("ordrSts")
    private int orderStatus;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus.ordinal();
    }
}
