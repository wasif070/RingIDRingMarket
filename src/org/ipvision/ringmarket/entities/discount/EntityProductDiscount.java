/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.discount;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;

/**
 *
 * @author saikat
 */
@Entity
@Table(name = "product_discount")
@Cacheable(DBConnection.CACHEABLE)

public class EntityProductDiscount implements Serializable {

    @Id
    @SerializedName(MarketAttribute.ID)
    @Column(name = "product_id", nullable = false, length = 36, columnDefinition = "VARCHAR(36) NOT NULL")
    private String productId;

    @SerializedName(MarketAttribute.DISCOUNT)
    @Column(name = "discount_value", nullable = false, columnDefinition = "FLOAT NOT NULL DEFAULT 0")
    private Float discountValue;

    @SerializedName(MarketAttribute.DISCOUNT_UNIT)
    @Column(name = "discount_unit", nullable = false, columnDefinition = "TINYINT(1) NOT NULL")
    private byte discountUnit;

    @SerializedName(MarketAttribute.CREATION_TIME)
    @Column(name = "creation_time", nullable = false, columnDefinition = "BIGINT NOT NULL DEFAULT 0")
    private Long creationTime;

    @SerializedName(MarketAttribute.VALIDITY_TIME)
    @Column(name = "validity_time", nullable = false, columnDefinition = "BIGINT NOT NULL DEFAULT 0")
    private Long validityTime;

    @SerializedName(MarketAttribute.ACTIVE_TIME)
    @Column(name = "active_time", nullable = false, columnDefinition = "BIGINT NOT NULL DEFAULT 0")
    private Long activeTime;

    @SerializedName(MarketAttribute.CODE)
    @Column(name = "coupon_code", nullable = false, length = 32, columnDefinition = "VARCHAR(32) NOT NULL DEFAULT ''")
    private String couponCode;

    @SerializedName(MarketAttribute.MINIMUM_ORDER)
    @Column(name = "minimum_order_value", columnDefinition = "INT DEFAULT 0")
    private Integer minimumOrderValue;

    @SerializedName(MarketAttribute.MAXIMUM_DISCOUNT_AMOUNT)
    @Column(name = "maximum_discount_amount", nullable = false, columnDefinition = "DOUBLE NOT NULL DEFAULT 0")
    private Double maximumDiscountAmount;

    @SerializedName(value = MarketAttribute.IS_REDEEM_ALLOWED)
    @Column(name = "is_redeem_allowed", columnDefinition = "bit(1) DEFAULT b'0'")
    private boolean isRedeemAllowed;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Float getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(Float discountValue) {
        this.discountValue = discountValue;
    }

    public byte getDiscountUnit() {
        return discountUnit;
    }

    public void setDiscountUnit(byte discountUnit) {
        this.discountUnit = discountUnit;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public Long getValidityTime() {
        return validityTime;
    }

    public void setValidityTime(Long validityTime) {
        this.validityTime = validityTime;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getMinimumOrderValue() {
        return minimumOrderValue;
    }

    public void setMinimumOrderValue(Integer minimumOrderValue) {
        this.minimumOrderValue = minimumOrderValue;
    }

    public Double getMaximumDiscountAmount() {
        return maximumDiscountAmount;
    }

    public void setMaximumDiscountAmount(Double maximumDiscountAmount) {
        this.maximumDiscountAmount = maximumDiscountAmount;
    }

    public boolean isIsRedeemAllowed() {
        return isRedeemAllowed;
    }

    public void setIsRedeemAllowed(boolean isRedeemAllowed) {
        this.isRedeemAllowed = isRedeemAllowed;
    }

    public Long getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(Long activeTime) {
        this.activeTime = activeTime;
    }

}
