package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.ipvision.ringmarket.entities.id.SellerRatingLogId;

/**
 *
 * @author saikat
 */
@Entity
@Table(name = "seller_rating_log")
@IdClass(SellerRatingLogId.class)
@NamedQueries(
        {
            @NamedQuery(name = "EntitySellerRatingLog.getRatingDetails", query = "SELECT e.rating, COUNT(e.sellerId) FROM  EntitySellerRatingLog e WHERE e.sellerId = :sellerId GROUP BY e.rating")
        }
)
public class EntitySellerRatingLog implements Serializable {

    @Column(name = "seller_id", nullable = false)
    @SerializedName("slrId")
    @Id
    private long sellerId;

    @Column(name = "buyer_id", nullable = false)
    @SerializedName("byrId")
    @Id
    private long buyerId;

    @Column(name = "rating", nullable = false, columnDefinition = "tinyint(11) DEFAULT '0'")
    @SerializedName("rating")
    private byte rating;

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public byte getRating() {
        return rating;
    }

    public void setRating(byte rating) {
        this.rating = rating;
    }

}
