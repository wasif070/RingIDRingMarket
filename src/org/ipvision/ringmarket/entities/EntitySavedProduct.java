/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Column;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.entities.id.SavedProductId;

/**
 *
 * @author Imran
 */
@Cacheable
@IdClass(SavedProductId.class)
@Entity
@Table(name = "saved_product")
@NamedQueries({
    @NamedQuery(name = "getDownwardSavedProductList", query = "select saved_product FROM EntitySavedProduct saved_product WHERE SAVED_PRODUCT.buyerId=:buyerId AND SAVED_PRODUCT.productId>:pivotProductId ORDER BY SAVED_PRODUCT.productId DESC"),
    @NamedQuery(name = "getUpwardSavedProductList", query = "select saved_product FROM EntitySavedProduct saved_product WHERE SAVED_PRODUCT.buyerId=:buyerId AND SAVED_PRODUCT.productId<:pivotProductId ORDER BY SAVED_PRODUCT.productId ASC"),
//    @NamedQuery(name = "getIsAlreadySaved", query = "SELECT t0 FROM EntitySavedProduct t0 WHERE t0.buyerId = :buyerId AND t0.productId = :productId"),
    @NamedQuery(name = "getIsAlreadySaved", query = "SELECT  COUNT(T0) FROM EntitySavedProduct t0 WHERE t0.buyerId = :buyerId AND t0.productId = :productId"),
    @NamedQuery(name = "deleteFromSavedProduct", query = "DELETE FROM EntitySavedProduct t0 WHERE t0.buyerId = :buyerId AND t0.productId = :productId"),
})
//@NamedNativeQueries({
//    @NamedNativeQuery(name = "deleteFromSavedProduct", query = "delete FROM saved_product where buyer_id = ?1 and product_id = ?2")
//})
public class EntitySavedProduct implements Serializable {

    @Id
    @Column(name = "product_id", columnDefinition = "varchar(40) NOT NULL", length = 40, nullable = false)
    @SerializedName("prodId")
    private String productId;

    @Id
    @Index(name = "idx_saved_product_buyer", columnNames = {"buyer_id"})
    @Column(name = "buyer_id", columnDefinition = "bigint(20) NOT NULL", nullable = false)
    @SerializedName("byrId")
    private long buyerId;

    @Column(name = "product_img_url", columnDefinition = "varchar(200) NOT NULL DEFAULT ''", length = 200, nullable = false)
    @SerializedName("imgUrl")
    private String productImageUrl;

    @Column(name = "product_name", columnDefinition = "varchar(500) NOT NULL", length = 500, nullable = false)
    @SerializedName("prodNm")
    private String productName;

    @Column(name = "product_img_width", columnDefinition = "int(5) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName("imgW")
    private int imageWidth;

    @Column(name = "product_img_height", columnDefinition = "int(5) NOT NULL DEFAULT '0'", nullable = false)
    @SerializedName("imgH")
    private int imageHeight;
    
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

}
