package org.ipvision.ringmarket.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ringmarket_user")
public class EntityRingMarketUser implements Serializable {

    @Id
    @Column(name = "id", columnDefinition = "bigint(20) NOT NULL", nullable = false)
    private long id;

    @Column(name = "lat", columnDefinition = "float NOT NULL", nullable = false)
    private float lat;

    @Column(name = "lon", columnDefinition = "float NOT NULL", nullable = false)
    private float lon;

    @Column(name = "about_me", columnDefinition = "varchar(500) DEFAULT NULL", length = 500)
    private String aboutMe;

    @Column(name = "photo", columnDefinition = "varchar(200) NOT NULL", length = 200, nullable = false)
    private String photo;

    @Column(name = "dialing_code", columnDefinition = "varchar(10) DEFAULT NULL", length = 10)
    private String dialingCode;

    @Column(name = "phone_no", columnDefinition = "varchar(15) DEFAULT NULL", length = 15)
    private String phoneNo;

    @Column(name = "address", columnDefinition = "varchar(200) NOT NULL", length = 200, nullable = false)
    private String address;

    @Column(name = "name", columnDefinition = "varchar(60) NOT NULL", length = 60, nullable = false)
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDialingCode() {
        return dialingCode;
    }

    public void setDialingCode(String dialingCode) {
        this.dialingCode = dialingCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
