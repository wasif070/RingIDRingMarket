///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.ipvision.ringmarket.entities.shop;
//
//import com.google.gson.annotations.SerializedName;
//import java.io.Serializable;
//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//import org.ipvision.ringmarket.constants.MarketAttribute;
//import org.ipvision.ringmarket.db.DBConnection;
////import org.ipvision.ringmarket.entities.id.ShopId;
//
///**
// *
// * @author saikat
// */
//@Cacheable(DBConnection.CACHEABLE)
//@Entity
////@IdClass(ShopId.class)
//@Table(name = "celebrity_store")
//
//@NamedQueries(
//        {
//            @NamedQuery(name = "EntityCelebrityStore.getStoresByScorllDown", query = "select s from EntityCelebrityStore s where s.shopId > :id and s.isEnable= TRUE order by s.shopId")
//            ,
//            @NamedQuery(name = "EntityCelebrityStore.getStoresByScorllUp", query = "select s from EntityCelebrityStore s where s.shopId < :id  and s.isEnable= TRUE order by s.shopId desc")
//             ,
//        })
//public class EntityCelebrityStore implements Serializable {
//
////    @Id
//    @SerializedName(MarketAttribute.CREATION_TIME)
//    @Column(name = "creation_time", nullable = false)
//    private Long creationTime;
//
//    @Id
//    @Column(name = "id", nullable = false)
//    @SerializedName(MarketAttribute.ID)
//    private long shopId;
//
//    
//    @Column(name = "is_enable", columnDefinition = "bit(1) DEFAULT b'0'")
//    private boolean isEnable;
//
//    public boolean isIsEnable() {
//        return isEnable;
//    }
//
//    public void setIsEnable(boolean isEnable) {
//        this.isEnable = isEnable;
//    }
//
//    
//    public long getId() {
//        return shopId;
//    }
//
//    public void setId(long id) {
//        this.shopId = id;
//    }
//
//    public Long getCreationTime() {
//        return creationTime;
//    }
//
//    public void setCreationTime(Long creationTime) {
//        this.creationTime = creationTime;
//    }
//
//}
