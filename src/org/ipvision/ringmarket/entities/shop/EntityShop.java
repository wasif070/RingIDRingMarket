/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities.shop;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.apache.openjpa.persistence.jdbc.Index;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProduct;

/**
 *
 * @author saikat
 */
@Cacheable(DBConnection.CACHEABLE)
@Entity
//@IdClass(ShopId.class)
@Table(name = "shop")

@NamedQueries(
        {
            @NamedQuery(name = "EntityShop.getDownwardShopsByScroll", query = "select s.shopId from EntityShop s where s.type = :type AND s.enabled=:enabled AND s.shopId > :id order by s.shopId"),
            @NamedQuery(name = "EntityShop.getUpwardShopsByScroll", query = "select s.shopId from EntityShop s where s.type = :type AND s.enabled=:enabled AND s.shopId < :id order by s.shopId desc"),
            @NamedQuery(name = "EntityShop.getDownwardShopByName", query = "select s.shopId from EntityShop s where s.shopId > :id AND lower(s.name) like :name"),
            @NamedQuery(name = "EntityShop.getUpwardShopByName", query = "select s.shopId from EntityShop s where s.shopId < :id AND lower(s.name) like :name order by s.shopId desc"),
            @NamedQuery(name = "EntityShop.getAllShopsByIsEnableScrollDown", query = "select s.shopId from EntityShop s where s.enabled=:enabled AND s.shopId > :id order by s.shopId"),
            @NamedQuery(name = "EntityShop.getAllShopsByIsEnableScrollUp", query = "select s.shopId from EntityShop s where s.enabled=:enabled AND s.shopId < :id order by s.shopId desc"),
            @NamedQuery(name = "EntityShop.getAllShopsByScrollDown", query = "select s.shopId from EntityShop s where  s.shopId > :id order by s.shopId"),
            @NamedQuery(name = "EntityShop.getAllShopsByScrollUp", query = "select s.shopId from EntityShop s where s.shopId < :id order by s.shopId desc"),
            @NamedQuery(name = "getShop", query = "select s from EntityShop s where s.shopId = :id"),
            @NamedQuery(name = "EntityShop.getShopsById", query = "select s from EntityShop s where s.shopId in :idList"),
            @NamedQuery(name = "EntityShop.incItemsCount", query = "UPDATE EntityShop s SET s.itemCount = s.itemCount+1 where s.shopId = :id"),
            @NamedQuery(name = "EntityShop.decItemsCount", query = "UPDATE EntityShop s SET s.itemCount = s.itemCount-1 where s.shopId = :id"),
            @NamedQuery(name = "EntityShop.getShopsOfOwnerScrollDown", query = "select s.shopId from EntityShop s where s.ownerId =:ownerId AND s.shopId > :id  order by s.shopId"),
            @NamedQuery(name = "EntityShop.getShopsOfOwnerScrollUp", query = "select s.shopId from EntityShop s where s.ownerId =:ownerId AND s.shopId < :id order by s.shopId desc")
        }
)
@NamedNativeQueries(
        {
            @NamedNativeQuery(name = "EntityShop.updateRating" , query = "update shop set ratings=? where id=?")
        }
)
public class EntityShop implements Serializable {

//    @Id
    @SerializedName(MarketAttribute.CREATION_TIME)
    @Column(name = "creation_time", nullable = false)
    private Long creationTime;

    @Id
    @Column(name = "id", nullable = false)
    @SerializedName(MarketAttribute.ID)
    private long shopId;

    @Column(name = "name", length = 100, nullable = false)
    @SerializedName(MarketAttribute.NAME)
    private String name;

    @Column(name = "type", nullable = false, columnDefinition = "tinyint(1) DEFAULT '0'")
    @SerializedName(MarketAttribute.TYPE)
    private Integer type;

    @Column(name = "imageUrl", length = 200, nullable = true)
    @SerializedName(MarketAttribute.IMAGE_URL)
    private String imageUrl;

    @SerializedName(MarketAttribute.LAT)
    @Column(name = "lat", nullable = false, columnDefinition = "float DEFAULT '0'")
    private Float lat;

    @SerializedName(MarketAttribute.LON)
    @Column(name = "lon", nullable = false, columnDefinition = "float DEFAULT '0'")
    private Float lon;

    @SerializedName(MarketAttribute.IMAGE_HEIGHT)
    @Column(name = "image_height", nullable = true, columnDefinition = "int(5) DEFAULT '0'")
    private Integer imageHeight;

    @SerializedName(MarketAttribute.IMAGE_WIDTH)
    @Column(name = "image_width", nullable = true, columnDefinition = "int(5) DEFAULT '0'")
    private Integer imageWidth;

    @SerializedName(MarketAttribute.RATING)
    @Column(name = "ratings", nullable = true, columnDefinition = "float DEFAULT '0'")
    private Float ratings = 5.0f;

    @Transient
    @SerializedName(MarketAttribute.PRODUCT_LIST)
    private List<EntityProduct> products;

    @SerializedName(MarketAttribute.ITEM_COUNT)
    @Column(name = "item_count", columnDefinition = "int(5) DEFAULT '0'")
    private int itemCount;

    @SerializedName(MarketAttribute.PROTECTION_DURATION)
    @Column(name = "protection_duration", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long protectionDuration;

    @SerializedName(MarketAttribute.CANCEL_DURATION)
    @Column(name = "cancel_duration", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long cancelDuration;

    @SerializedName(MarketAttribute.FEEDBACK_DURATION)
    @Column(name = "feedback_duration", nullable = false, columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    private Long feedbackDuration;

    @SerializedName(MarketAttribute.IS_ENABLE)
    @Column(name = "enabled", nullable = false, columnDefinition = "bit(1) NOT NULL DEFAULT 0")
    private Boolean enabled = Boolean.FALSE;

    @SerializedName(MarketAttribute.OWNER_ID)
    @NotNull
    @Column(name = "owner_id", columnDefinition = "bigint(20) NOT NULL DEFAULT '0'")
    @Index(name = "idx_shop_owner", columnNames = "owner_id")
    private Long ownerId;

    public long getId() {
        return shopId;
    }

    public void setId(long id) {
        this.shopId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }

    public Integer getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(Integer imageHeight) {
        this.imageHeight = imageHeight;
    }

    public Integer getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(Integer imageWidth) {
        this.imageWidth = imageWidth;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public Float getRatings() {
        return ratings;
    }

    public void setRatings(Float ratings) {
        this.ratings = ratings;
    }

    public List<EntityProduct> getProducts() {
        return products;
    }

    public void setProducts(List<EntityProduct> products) {
        this.products = products;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public Long getProtectionDuration() {
        return protectionDuration;
    }

    public void setProtectionDuration(Long protectionDuration) {
        this.protectionDuration = protectionDuration;
    }

    public Long getCancelDuration() {
        return cancelDuration;
    }

    public void setCancelDuration(Long cancelDuration) {
        this.cancelDuration = cancelDuration;
    }

    public Long getFeedbackDuration() {
        return feedbackDuration;
    }

    public void setFeedbackDuration(Long feedbackDuration) {
        this.feedbackDuration = feedbackDuration;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() {
        return ownerId;
    }
}
