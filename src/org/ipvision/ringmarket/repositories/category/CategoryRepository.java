/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.category;

import java.util.ArrayList;
import java.util.List;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.repositories.IRepository;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;

/**
 *
 * @author saikat
 */
public interface CategoryRepository extends IRepository<EntityCategory> {

    public ObjectReturnDTO<Integer> addRootCategory(String name, String image);

    public ObjectReturnDTO<Integer> addSubCategory(EntityCategory category);

    public boolean addCateogoryByList(ArrayList<EntityCategory> categories);

    public EntityCategory getCategoryById(int id);

    public EntityCategory getRootCategory(String name);

    public void updateCategoryPosition(int id, int newPos);

    public void updateCategoryToRoot(int id);

    public boolean deleteCategory(int id);

    public List<EntityCategory> getSubCategories(int parentId);

    public List<EntityCategory> getRootCategories();

    public List<EntityCategory> getCategoriesByScroll(int parentId, int pivotCategoryId, int limit, Scroll scroll);

    int updateCategoryName(int id, String name);

    int updateCategoryImage(int id, String image);
}
