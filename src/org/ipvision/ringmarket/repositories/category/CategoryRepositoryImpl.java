/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.category;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.ipvision.ringmarket.cache.ProductCategoryCache;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alamgir
 */
public class CategoryRepositoryImpl implements CategoryRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryRepositoryImpl.class);

    /**
     * add Root category with name and position is optional
     *
     * @param name
     * @param image
     * @return
     */
    @Override
    public ObjectReturnDTO<Integer> addRootCategory(String name, String image) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ObjectReturnDTO<Integer> addCategory;
        try {
            addCategory = addCategory(entityManager, 0, name, image, 1);
        } finally {
            entityManager.close();
        }
        return addCategory;
    }

    /**
     * Add sub category under a category
     *
     * @param category
     * @return
     */
    @Override
    public ObjectReturnDTO<Integer> addSubCategory(EntityCategory category) {
        ObjectReturnDTO<Integer> returnDTO = new ObjectReturnDTO<>();
        int reasonCode = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {

            try {
                EntityCategory parentCategory = ProductCategoryCache.getInstance().getCategoryInfo(category.getParentId());
                if (parentCategory == null) {
                    if (LOGGER.isTraceEnabled()) {
                        LOGGER.trace("Category is not found {parentId = " + category.getParentId() + "}");
                    }
                    reasonCode = ReasonCode.INVALID_INFORMATION;
                } else {
                    if (!parentCategory.getHasNext()) {
                        int categoryProductCount = MarketRepository.INSTANCE.getCategoryProductCountRepository().getCategoryProductCount(category.getParentId());
                        if (categoryProductCount > 0) {
                            reasonCode = ReasonCode.PERMISSION_DENIED;
                        }
                    }

                    if (reasonCode == ReasonCode.NONE) {
                        
                        EntityTransaction tx1 = entityManager.getTransaction();
                        tx1.begin();
                        category.setPosition(parentCategory.getPosition() + 1);
                        category.setHasNext(false);
                        entityManager.persist(category);
                        tx1.commit();
                        
                        returnDTO.setValue(category.getId());

                        if (!parentCategory.getHasNext()) {
                            EntityTransaction tx2 = entityManager.getTransaction();
                            try {
                                tx2.begin();
                                parentCategory.setHasNext(true);
                                entityManager.merge(parentCategory);
                                tx2.commit();
                                DBConnection.getInstance().getCache().evict(EntityCategory.class, parentCategory.getId());
                            } catch (Exception e) {
                                reasonCode = ReasonCode.EXCEPTION_OCCURED;
                                LOGGER.error("Exception updating parent category  in addSubCategory() : {parentCategory: " + new Gson().toJson(parentCategory) + "}", e);
                                if (tx1.isActive()) {
                                    tx1.rollback();
                                }
                                if (tx2.isActive()) {
                                    tx2.rollback();
                                }
                            }
                        }

                        if (reasonCode == ReasonCode.NONE) {
                            ProductCategoryCache.getInstance().addCategoryToCache(category);
                        }
                    }
                }
            } catch (Exception e) {
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
                LOGGER.error("Error in addSubCategory() : {category: " + new Gson().toJson(category) + "}", e);
            }

        } finally {
            entityManager.close();
        }
        returnDTO.setReasonCode(reasonCode);
        return returnDTO;
    }

//    private void addCategory(EntityManager entityManager, EntityCategory category) {
//        entityManager.persist(category);
//    }
    private ObjectReturnDTO<Integer> addCategory(EntityManager entityManager, int parentId, String name, String image, int pos) {
        ObjectReturnDTO<Integer> objectReturnDTO = new ObjectReturnDTO<>();
        try {
            EntityCategory productCategory = new EntityCategory();
            productCategory.setName(name);
            productCategory.setImageUrl(image);
            productCategory.setParentId(parentId);
            productCategory.setPosition(pos);
            entityManager.getTransaction().begin();
            entityManager.persist(productCategory);
            objectReturnDTO.setValue(productCategory.getId());
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            LOGGER.error("Error in addCategory(): ", ex);
            objectReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
        return objectReturnDTO;
    }

    /**
     *
     * @param categorys (required)
     * @return
     */
    @Override
    public boolean addCateogoryByList(ArrayList<EntityCategory> categorys) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            categorys.forEach((category) -> {
                entityManager.persist(category);
            });
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    /**
     * fetch category by id
     *
     * @param id > 0 (required)
     * @return
     */
    @Override
    public EntityCategory getCategoryById(int id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityCategory categoryHieararchy;

        try {
            categoryHieararchy = entityManager.find(EntityCategory.class, id);
        } finally {
            entityManager.close();
        }
        return categoryHieararchy;

    }

    private EntityCategory getCategoryById(EntityManager entityManager, int id) {
        return entityManager.find(EntityCategory.class, id);
    }

    /**
     * name and parentId are combined unique key and hash key
     *
     * @param name(required)
     * @return
     */
    @Override
    public EntityCategory getRootCategory(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private int updateCategory(EntityManager entityManager, EntityCategory category) {
        int reasonCode = ReasonCode.NONE;
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(category);
            entityManager.getTransaction().commit();
            ProductCategoryCache.getInstance().update(category);
        } catch (Exception e) {
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            LOGGER.error("Error in updateCategory(): {category: " + new Gson().toJson(category) + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
        return reasonCode;
    }

    /**
     *
     * @param id > 0 (required)
     * @param name
     * @return
     */
    @Override
    public int updateCategoryName(int id, String name) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode = ReasonCode.INVALID_INFORMATION;

        try {
            EntityCategory categoryInfo = getCategoryById(entityManager, id);
            if (categoryInfo != null) {
                categoryInfo.setName(name);
                reasonCode = updateCategory(entityManager, categoryInfo);
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    /**
     *
     * @param id >0 (required)
     * @param imageUrl
     * @return
     */
    @Override
    public int updateCategoryImage(int id, String imageUrl) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode = ReasonCode.INVALID_INFORMATION;

        try {
            EntityCategory categoryInfo = getCategoryById(entityManager, id);
            if (categoryInfo != null) {
                categoryInfo.setImageUrl(imageUrl);
                reasonCode = updateCategory(entityManager, categoryInfo);
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    /**
     *
     * @param id
     * @param newPos
     */
    @Override
    public void updateCategoryPosition(int id, int newPos) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityCategory productCategory = new EntityCategory();
            productCategory.setId(id);
            productCategory.setPosition(newPos);
            entityManager.merge(productCategory);
        } finally {
            entityManager.close();
        }
    }

    /**
     * Move a sub category to another root category
     *
     * @param id
     */
    @Override
    public void updateCategoryToRoot(int id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityCategory productCategory = new EntityCategory();
            productCategory.setId(id);
            productCategory.setParentId(0);
            entityManager.merge(productCategory);
        } finally {
            entityManager.close();
        }
    }

    /**
     * delete a category with all of it's sub categories and sub sub categories
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteCategory(int id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = deleteCategory(entityManager, id);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    private boolean deleteCategory(EntityManager entityManager, int id) {
        Stack<EntityCategory> categoryIds = new Stack<>();
        try {
            EntityCategory categoryById = getCategoryById(entityManager, id);
            if (categoryById != null) {
                categoryIds.push(categoryById);
            } else {
                return false;
            }
            while (!categoryIds.empty()) {
                EntityCategory epch = categoryIds.pop();
                List<EntityCategory> categorys = getSubCategories(entityManager, epch.getId());
                if (!categorys.isEmpty()) {
                    categoryIds.addAll(categorys);
                }
                entityManager.remove(epch);
            }
        } catch (Exception ex) {
            LOGGER.error("Error in deleteCategory() : {id: " + id + "}", ex);
            return false;
        }
        return true;
    }

    /**
     * get all sub categories under a category
     *
     * @param parentId
     * @return
     */
    @Override
    public List<EntityCategory> getSubCategories(int parentId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        List<EntityCategory> subCategories;
        try {
            subCategories = getSubCategories(entityManager, parentId);
        } finally {
            entityManager.close();
        }
        return subCategories;
    }

    public List<EntityCategory> getSubCategories(EntityManager entityManager, int parentId) {
        Query query = entityManager.createNamedQuery("getSubCategoriesById");
        query.setParameter("parentId", parentId);
        ArrayList<EntityCategory> subCategories = new ArrayList<>(query.getResultList());
        return subCategories;
    }

    /**
     * get parent categories not the sub categories
     *
     * @return
     */
    @Override
    public List<EntityCategory> getRootCategories() {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityCategory> categories;
        try {

            Query query = entityManager.createNamedQuery("getRootCategories");
            categories = new ArrayList<>(query.getResultList());
        } finally {
            entityManager.close();
        }
        return categories;
    }

    /**
     *
     * @param parentId
     * @param pivotCategoryId
     * @param limit
     * @param scroll
     * @return
     */
    public List<EntityCategory> getCategoriesByScroll(int parentId, int pivotCategoryId, int limit, Scroll scroll) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        List<EntityCategory> results;
        try {
            results = getCategoriesByScroll(entityManager, parentId, pivotCategoryId, limit, scroll);
        } finally {
            entityManager.close();
        }
        return results;
    }

    private List<EntityCategory> getCategoriesByScroll(EntityManager entityManager, int parentId, int pivotCategoryId, int limit, Scroll scroll) {
        Query query;
        if (scroll.equals(Scroll.UP)) {
            query = entityManager.createNamedQuery("getUpwardCategoriesByScroll");
        } else {
            query = entityManager.createNamedQuery("getDownwardCategoriesByScroll");
        }
        query.setParameter("parentId", parentId);
        query.setParameter("pivotCategoryId", pivotCategoryId);
        ArrayList<EntityCategory> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
        if (scroll.equals(Scroll.UP)) {
            Collections.reverse(results);
        }
        return results;
    }
}
