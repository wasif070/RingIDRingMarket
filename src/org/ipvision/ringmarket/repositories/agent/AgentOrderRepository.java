/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.agent;

import java.util.List;
import org.ipvision.ringmarket.constants.OrderStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.EntityAgentOrder;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface AgentOrderRepository extends IRepository<EntityAgentOrder> {

    public boolean addAgentOrder(long agentId, String OrderId, long deliveryDate);

    public List<EntityAgentOrder> getAgentOrderList(long agentId, String pivotOrderId, Scroll scroll, int limit);

    public boolean addAgentOrder(EntityAgentOrder entityAgentOrder);

    public boolean deleteAgentOrder(String orderId, OrderStatus orderStatus);

    public EntityAgentOrder getOrder(String orderId);

}
