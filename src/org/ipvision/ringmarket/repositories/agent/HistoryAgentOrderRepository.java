/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.agent;

import java.util.List;
import org.ipvision.ringmarket.constants.OrderStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.EntityHistoryAgentOrder;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface HistoryAgentOrderRepository extends IRepository<HistoryAgentOrderRepository>{
     public boolean addHistoryAgentOrder(long agentId, String OrderId, OrderStatus orderStatus);
     public boolean addHistoryAgentOrder(EntityHistoryAgentOrder entityAgentOrder);
     public EntityHistoryAgentOrder getHistoryOrder(String orderId);
     public List<EntityHistoryAgentOrder> getAgentHistoryOrderList(long agentId, String pivotOrderId, Scroll scroll, int limit);
     
}
