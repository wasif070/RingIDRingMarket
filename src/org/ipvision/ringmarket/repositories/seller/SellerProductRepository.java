/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.seller;

import java.util.List;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.EntitySellerProduct;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface SellerProductRepository extends IRepository<EntitySellerProduct> {

    public boolean addNewProduct(EntitySellerProduct entitySellerProduct);

    public boolean addNewProduct(Long sellerId, String productId, long addedDate, String productName, String imgUrl);

    public boolean deleteProduct(String productId);

    public EntitySellerProduct getProduct(String productId);

    public List<EntitySellerProduct> getProductList(Long sellerId, String pivotProdId, Scroll scroll, int limit);

    public boolean isProductOwner(Long sellerId, String productId);

}
