/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.seller;

import java.util.Map;
import org.ipvision.ringmarket.entities.EntitySellerRatingLog;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface SellerRatingLogRepository extends IRepository<EntitySellerRatingLog>{
    public EntitySellerRatingLog findRatingByBuyer(long sellerId, long buyerId);
    Map<Byte,Long> getRatingDetails(Long sellerId);
}
