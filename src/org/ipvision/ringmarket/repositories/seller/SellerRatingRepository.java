/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.seller;

import javax.persistence.EntityManager;
import org.ipvision.ringmarket.entities.EntitySellerRating;

/**
 *
 * @author saikat
 */
public interface SellerRatingRepository  {

    boolean updateSellerRating(long sellerId, long buyerId, byte rating);

    boolean removeSellerRating(long sellerId, long buyerId);

    EntitySellerRating getRating(long sellerId);

    EntitySellerRating getRating(EntityManager entityManager, long sellerId);

    byte findRatingByBuyer(long sellerId, long buyerId);

}
