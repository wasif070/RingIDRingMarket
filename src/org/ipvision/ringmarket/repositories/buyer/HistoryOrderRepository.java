/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.buyer;

import java.util.List;
import org.ipvision.ringmarket.entities.EntityHistoryOrder;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface HistoryOrderRepository extends IRepository<EntityHistoryOrder> {

    public boolean addNewHistory(String id, long buyerId, long sellerId, long agentId, String productId,
            double offeredPrice, double shipmentCost, long cmpltDate, int status);

    public boolean removeHistory(String id);

    public EntityHistoryOrder getHistory(String id);

    public List<EntityHistoryOrder> getCompletedOrderByBuyer(long buyerId);

    public List<EntityHistoryOrder> getCompletedOrderBySeller(long sellerId);

}
