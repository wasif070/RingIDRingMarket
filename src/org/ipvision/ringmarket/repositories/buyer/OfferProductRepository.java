/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.buyer;

import java.util.List;
import org.ipvision.ringmarket.constants.OfferStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.EntityOfferProduct;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.EntityRingMarketUser;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface OfferProductRepository extends IRepository<EntityOfferProduct> {

    public boolean addNewOffer(String id, long buyerId, String buyerName, String prodId, String productName, String imageUrl, double price, long offeredDate, OfferStatus offerStatus);
     public boolean addNewOffer(String id, EntityRingMarketUser iru, EntityProduct iProduct, long offeredDate, OfferStatus offerStatus);
     public boolean addNewOffer(EntityOfferProduct entityOfferProduct);
     public EntityOfferProduct getOfferInfo(String id);
     public boolean removeOffer(String id);
     public boolean removeAllOfferOfProduct(String productId);
     public boolean updateOfferStatus(String id, OfferStatus offerStatus);
     public boolean updateOfferBid(EntityOfferProduct entityOfferProduct, double offerPrice, long offerDate) ;
      public EntityOfferProduct getOfferInfoOfProduct(long buyerId, String productId) ;
      public boolean isAlreadyAccepted(String product_id) ;
      public List<EntityOfferProduct> getAllOffersForProduct(String product_id, long pivotOfferedDate, int limit, Scroll scroll);
      public List<EntityOfferProduct> getOffersByBuyer(long buyerId, long pivotOfferedDate, int limit, Scroll scroll);
      
}
