/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.buyer;

import org.ipvision.ringmarket.constants.ReportCause;
import org.ipvision.ringmarket.entities.EntityReportProduct;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface ReportProductRepository extends IRepository<EntityReportProduct> {

    public boolean addReportProduct(String id, long buyerId, long sellerId, String productId, ReportCause reportCause, String desc);

    public boolean addReportProduct(String id, long buyerId, long sellerId, String productId, int reportCause, String desc);

    public boolean addReportProduct(EntityReportProduct entityReportProduct);

    public boolean isAlreadyReported(long buyerId, String productId);

}
