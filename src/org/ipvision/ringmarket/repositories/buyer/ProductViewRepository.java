/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.buyer;

import org.ipvision.ringmarket.entities.product.EntityProductView;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface ProductViewRepository extends IRepository<EntityProductView> {

    public boolean addToViewList(long buyerId, String productId);

    public boolean findViewedByBuyer(long buyerId, String productId);

    int incrementViewCount(Long buyerId, String productId);

    void addToProductViewCache(Long userId, String productId);
    
    int syncProductView();
}
