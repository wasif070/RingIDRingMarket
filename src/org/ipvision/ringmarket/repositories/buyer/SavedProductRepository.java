/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.buyer;

import java.util.List;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.EntitySavedProduct;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface SavedProductRepository extends IRepository<EntitySavedProduct> {

    public boolean addToSavedList(long buyerId, String productId);

    public boolean addToSavedList(long buyerId, EntityProduct entityProduct);

    public boolean addToSavedList(EntitySavedProduct iSavedProducts);

    public boolean deleteSavedProduct(long buyerId, String productId);

    public boolean isAlreadyOnSavedList(long buyerId, String productId);

    public List<EntitySavedProduct> getSavedProductList(long buyerId, String pivotProductId, Scroll scroll, int limit);

}
