/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.shop;

import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.repositories.IRepository;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;

/**
 *
 * @author bose
 */
public interface ShopRepository extends IRepository<EntityShop> {

    int addShop(EntityShop shop, int userType);

    int deleteShop(long id);

    ListReturnDTO<EntityShop> getBrands(long pivotShopId, int limit, Scroll scroll, Boolean enabled);

    EntityShop getShopById(long id);

    ListReturnDTO<EntityShop> getStores(long pivotShopId, int limit, Scroll scroll, Boolean enabled);

    boolean updateShop(EntityShop shop);

    int toggleShopStatus(long shopId, boolean enable, Long activistId);

    PivotedListReturnDTO<EntityShop, Long> getAllStores(long pivotShopId, int limit, Scroll scroll, Boolean enabled);

    int toggleStoreType(long shopId, int type, Long activistId);

    PivotedListReturnDTO<EntityShop, Long> getShopsOfOwner(long pivotShopId, Long ownerId, int limit, Scroll scroll);

    ListReturnDTO<EntityShop> getCelebrityStore(long pivotShopId, int limit, Scroll scroll);
    
    ListReturnDTO<EntityShop> getShopsByName(long pivotShopId, String name, int limit, Scroll scroll);

    int toggleCelebrityStore(long shopId, Long activistId, Boolean enable);

    int updateShopName(long id, String name);

    int updateImage(long id, String imageUrl, int height, int width);

}
