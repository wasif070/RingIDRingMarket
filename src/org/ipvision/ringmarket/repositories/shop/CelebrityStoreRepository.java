///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.ipvision.ringmarket.repositories.shop;
//
//import javax.persistence.EntityManager;
//import org.ipvision.ringmarket.constants.Scroll;
//import org.ipvision.ringmarket.entities.shop.EntityCelebrityStore;
//import org.ipvision.ringmarket.entities.shop.EntityShop;
//import org.ipvision.ringmarket.utils.ListReturnDTO;
//
///**
// *
// * @author saikat
// */
//public interface CelebrityStoreRepository {
//
//    int addStore(EntityCelebrityStore shop);
//
//    int deleteCelebrityStore(Long shopId);
//
//    ListReturnDTO<EntityShop> getCelebrityStore(long pivotShopId, int limit, Scroll scroll);
//
//    int toggleCelebrityStore( long shopId, Long activistId, Boolean enable);
//
//}
