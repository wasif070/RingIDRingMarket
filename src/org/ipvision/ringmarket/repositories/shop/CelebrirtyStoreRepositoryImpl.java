//<<<<<<< Updated upstream
///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.ipvision.ringmarket.repositories.shop;
//
//import com.google.gson.Gson;
//import java.util.Collections;
//import java.util.LinkedList;
//import java.util.List;
//import javax.persistence.EntityManager;
//import javax.persistence.Query;
//import org.ipvision.ringmarket.constants.ReasonCode;
//import org.ipvision.ringmarket.constants.Scroll;
//import org.ipvision.ringmarket.constants.ShopType;
//import org.ipvision.ringmarket.db.DBConnection;
//import org.ipvision.ringmarket.entities.shop.EntityCelebrityStore;
//import org.ipvision.ringmarket.entities.shop.EntityShop;
//import org.ipvision.ringmarket.utils.ListReturnDTO;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// *
// * @author saikat
// */
//public class CelebrirtyStoreRepositoryImpl extends ShopRepositoryImpl implements CelebrityStoreRepository {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(CelebrirtyStoreRepositoryImpl.class);
//
//    @Override
//    public int addStore(EntityCelebrityStore shop) {
//        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
//        int reasonCode = addStore(entityManager, shop);
//        entityManager.close();
//        return reasonCode;
//    }
//
//    private int addStore(EntityManager entityManager, EntityCelebrityStore shop) {
//        int reasonCode = ReasonCode.NONE;
//        try {
//            entityManager.getTransaction().begin();
//            entityManager.persist(shop);
//            entityManager.getTransaction().commit();
//        } catch (Exception e) {
//            LOGGER.error("Exception in addStore() : {shop:" + new Gson().toJson(shop) + "}", e);
//            reasonCode = ReasonCode.EXCEPTION_OCCURED;
//        }
//        return reasonCode;
//    }
//
//    private EntityCelebrityStore find(EntityManager em, Long shopId) {
//        return em.find(EntityCelebrityStore.class,  shopId);
//    }
//
//    @Override
//    public int deleteCelebrityStore(Long shopId) {
//        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
//        int reasonCode = deletStore(entityManager, shopId);
//        entityManager.close();
//        return reasonCode;
//    }
//
//    int deletStore(EntityManager entityManager,  Long shopId) {
//        int reasonCode = ReasonCode.NONE;
//        try {
//
//            EntityCelebrityStore store = find(entityManager, shopId);
//            if (store != null) {
//                entityManager.getTransaction().begin();
//                entityManager.remove(store);
//                entityManager.getTransaction().commit();
//            } else {
//                reasonCode = ReasonCode.INVALID_INFORMATION;
//            }
//        } catch (Exception e) {
//            LOGGER.error("Exception in deletStore() : {shopId:" + shopId + "}", e);
//            reasonCode = ReasonCode.EXCEPTION_OCCURED;
//        }
//        return reasonCode;
//    }
//
//    @Override
//    public ListReturnDTO<EntityShop> getCelebrityStore(long pivotShopId, int limit, Scroll scroll) {
//        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
//        ListReturnDTO<EntityShop> stores = getShops(entityManager, pivotShopId, limit, scroll);
//        entityManager.close();
//        return stores;
//    }
//
//    private ListReturnDTO<EntityShop> getShops(EntityManager entityManager, long pivotShopId, int limit, Scroll scroll) {
//        ListReturnDTO<EntityShop> listReturnDTO = new ListReturnDTO<>();
//        try {
//            Query query;
//            if (scroll.equals(Scroll.UP)) {
//                query = entityManager.createNamedQuery("EntityCelebrityStore.getStoresByScorllUp", EntityCelebrityStore.class);
//            } else {
//                query = entityManager.createNamedQuery("EntityCelebrityStore.getStoresByScorllDown", EntityCelebrityStore.class);
//            }
//            query.setParameter("id", pivotShopId);
//            query.setMaxResults(limit);
//            List<EntityCelebrityStore> results = query.setMaxResults(limit).getResultList();
//            if (results.isEmpty()) {
//                if (pivotShopId > 0) {
//                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
//                } else {
//                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
//                }
//            } else {
//                if (scroll.equals(Scroll.UP)) {
//                    Collections.reverse(results);
//                }
//                List<EntityShop> shopList = new LinkedList<>();
//                results.forEach((result) -> {
//                    shopList.add(findShop(entityManager, result.getId()));
//                });
//                listReturnDTO.setList(shopList);
//            }
//        } catch (Exception e) {
//            LOGGER.error("Exception in getShops(): {pivotShopId:" + pivotShopId + "}", e);
//            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
//        }
//
//        return listReturnDTO;
//    }
//
//    @Override
//    public int toggleCelebrityStore( long shopId, Long activistId, Boolean enable) {
//        EntityManager em = DBConnection.getInstance().getEntityManager();
//        int toggleCelebrityStore = toggleCelebrityStore(em, shopId, activistId, enable);
//        em.close();
//        return toggleCelebrityStore;
//    }
//
//    int toggleCelebrityStore(EntityManager em,long shopId, Long activistId, Boolean enable) {
//        int reasonCode = ReasonCode.NONE;
//        try {
//            EntityCelebrityStore shop = find(em, shopId);
//            if (shop == null) {
//                if (!enable) {
//                    reasonCode = ReasonCode.NO_DATA_FOUND;
//                } else {
//                    shop = new EntityCelebrityStore();
//                    shop.setId(shopId);
//                    shop.setIsEnable(enable);
//                    reasonCode = addStore(entityManager, shop);
//                    if (reasonCode == ReasonCode.NONE) {
//                        reasonCode = toggleStoreType(entityManager,shopId, ShopType.CELEBRITY_STORE, activistId);
//
//                        if (reasonCode != ReasonCode.NONE) {
//                            if (entityManager.getTransaction().isActive()) {
//                                entityManager.getTransaction().rollback();
//                            }
//                        }
//                    }
//                }
//            } else {
//                entityManager.getTransaction().begin();
//                shop.setIsEnable(enable);
//                entityManager.getTransaction().commit();
//                
//                /**
//                 * Removing from cache
//                 */
//                DBConnection.getInstance().getCache().evict(EntityCelebrityStore.class, shopId);
//                
//                
//                int type;
//                if (enable) {
//                    type = ShopType.CELEBRITY_STORE;
//                } else {
//                    type = ShopType.GENERAL_STORE;
//                }
//                reasonCode = toggleStoreType(entityManager,shopId, type, activistId);
//
//                if (reasonCode != ReasonCode.NONE) {
//                    if (entityManager.getTransaction().isActive()) {
//                        entityManager.getTransaction().rollback();
//                    }
//                }
//            }
//        } catch (Exception e) {
//            LOGGER.error("Exception in toggleCelebrityStore() : ", e);
//            reasonCode = ReasonCode.EXCEPTION_OCCURED;
//            if (entityManager.getTransaction().isActive()) {
//                entityManager.getTransaction().rollback();
//            }
//        }
//        return reasonCode;
//    }
//}
