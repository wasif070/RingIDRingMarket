/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.shop;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.ShopType;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProduct;
//import org.ipvision.ringmarket.entities.shop.EntityCelebrityStore;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
//import org.ipvision.ringmarket.entities.id.ShopId;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.order.OrderRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.CategoryProductRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.repositories.user.basket.BasketProductCategoryRepository;
import org.ipvision.ringmarket.repositories.user.basket.BasketProductCategoryRepositoryImpl;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class ShopRepositoryImpl implements ShopRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShopRepositoryImpl.class);

    @Override
    public int addShop(EntityShop shop, int userType) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            int reasonCode = addShop(entityManager, shop, userType);
            return reasonCode;

        } finally {
            entityManager.close();
        }
    }

    private int addShop(EntityManager entityManager, EntityShop shop, int userType) {
        int reasonCode = ReasonCode.NONE;
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(shop);
            entityManager.getTransaction().commit();
//            if (userType == UserTypeConstants.CELEBRITY) {
//                EntityCelebrityStore celebrityStore = new EntityCelebrityStore();
//                celebrityStore.setCreationTime(shop.getCreationTime());
//                celebrityStore.setId(shop.getId());
//                celebrityStore.setIsEnable(true);
//
//                reasonCode = MarketRepository.INSTANCE.getCelebrityStoreRepository().addStore(celebrityStore);
//                if (reasonCode != ReasonCode.NONE) {
//                    entityManager.getTransaction().rollback();
//                }
//            }
        } catch (Exception e) {
            LOGGER.error("Exception in addShop() : {shop:" + new Gson().toJson(shop) + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    @Override
    public EntityShop getShopById(long id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityShop shop = findShop(entityManager, id);
            return shop;

        } finally {
            entityManager.close();
        }
    }

    public EntityShop findShop(EntityManager entityManager, long id) {
        return entityManager.find(EntityShop.class, id);
    }

    protected void updateRating(EntityManager entityManager, Float rating, Long shopId) {
        Query query = entityManager.createNamedQuery("EntityShop.updateRating");
        query.setParameter(1, rating);
        query.setParameter(2, shopId);
        int count = query.executeUpdate();
        if (count > 0) {
            DBConnection.getInstance().getCache().evict(EntityShop.class, shopId);
        }
    }

    @Override
    public boolean updateShop(EntityShop shop) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            boolean success = false;
            try {
                EntityShop find = entityManager.find(EntityShop.class, shop.getId());
                if (find != null) {
                    entityManager.getTransaction().begin();
                    entityManager.merge(shop);
                    entityManager.getTransaction().commit();
                    DBConnection.getInstance().getCache().evict(EntityShop.class, find.getId());
                }
                success = true;
            } catch (Exception ex) {
                LOGGER.error("Exception in update shop {shopId:" + shop.getId() + "}", ex);
                entityManager.getTransaction().rollback();
            }

            return success;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public int deleteShop(long id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            int susc = deleteShop(entityManager, id);
            return susc;

        } finally {
            entityManager.close();
        }
    }

    private int deleteShop(EntityManager entityManager, long id) {
        int reasonCode = ReasonCode.NONE;
        try {
            EntityShop shopById = findShop(entityManager, id);
            if (shopById != null) {
                entityManager.getTransaction().begin();
                entityManager.remove(shopById);

//                int deletStore = MarketRepository.INSTANCE.getCelebrityStoreRepository().deleteCelebrityStore(shopById.getId());
//                if (LOGGER.isDebugEnabled()) {
//                    LOGGER.debug("Delete Celebrity Store {reasonCode :" + deletStore + "}");
//                }
//                if (deletStore == ReasonCode.EXCEPTION_OCCURED) {
//                    if (entityManager.getTransaction().isActive()) {
//                        entityManager.getTransaction().rollback();
//                    }
//                    reasonCode = ReasonCode.OPERATION_FAILED;
//                }
                entityManager.getTransaction().commit();

//                CelebrirtyStoreRepositoryImpl celebrirtyStoreRepositoryImpl = new CelebrirtyStoreRepositoryImpl();
//                int deleteCelebrityStore = celebrirtyStoreRepositoryImpl.deletStore(entityManager, shopById.getCreationTime(), id);
//                if (deleteCelebrityStore == ReasonCode.EXCEPTION_OCCURED) {
//                    entityManager.getTransaction().rollback();
//                    reasonCode = deleteCelebrityStore;
//                }
            } else {
                reasonCode = ReasonCode.NO_DATA_FOUND;
            }
        } catch (Exception ex) {
            LOGGER.error("Error in removing shop {shopId:" + id + "}", ex);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    @Override
    public ListReturnDTO<EntityShop> getBrands(long pivotShopId, int limit, Scroll scroll, Boolean enabled) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityShop> brands = getShopsByType(entityManager, pivotShopId, limit, scroll, ShopType.BRANDED_STORE, enabled);
            return brands;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public ListReturnDTO<EntityShop> getStores(long pivotShopId, int limit, Scroll scroll, Boolean enabled) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
//            ListReturnDTO<EntityShop> stores = getShopsByType(entityManager, pivotShopId, limit, scroll, ShopType.GENERAL_STORE, enabled);
            ListReturnDTO<EntityShop> stores = getAllShops(entityManager, pivotShopId, limit, scroll, enabled);
            return stores;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public PivotedListReturnDTO<EntityShop, Long> getAllStores(long pivotShopId, int limit, Scroll scroll, Boolean enabled) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            PivotedListReturnDTO<EntityShop, Long> allShops = getAllShops(entityManager, pivotShopId, limit, scroll, enabled);
            return allShops;

        } finally {
            entityManager.close();
        }
    }

    private ListReturnDTO<EntityShop> getShopsByType(EntityManager entityManager, long pivotShopId, int limit, Scroll scroll, int type, Boolean enabled) {
        ListReturnDTO<EntityShop> listReturnDTO = new ListReturnDTO<>();
        try {
            Query query;
            if (scroll.equals(Scroll.UP)) {
                query = entityManager.createNamedQuery("EntityShop.getUpwardShopsByScroll");
            } else {
                query = entityManager.createNamedQuery("EntityShop.getDownwardShopsByScroll");
            }
            query.setParameter("type", type);
            query.setParameter("id", pivotShopId);
            query.setParameter("enabled", enabled);
            query.setMaxResults(limit);
//            ArrayList<EntityShop> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            ArrayList<Long> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (results.isEmpty()) {
                if (pivotShopId > 0) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                }
            } else {
                if (scroll.equals(Scroll.UP)) {
                    Collections.reverse(results);
                }
                List<EntityShop> shops = new LinkedList<>();
                for (Long result : results) {
                    shops.add(findShop(entityManager, result));
                }
//                listReturnDTO.setList(results);
                listReturnDTO.setList(shops);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getShops(): {pivotShopId:" + pivotShopId + " ,type:" + type + "}", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }

        return listReturnDTO;
    }

    @Override
    public ListReturnDTO<EntityShop> getShopsByName(long pivotShopId, String name, int limit, Scroll scroll) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityShop> allShops = getShopsByName(entityManager, name, pivotShopId, limit, scroll, true);
            return allShops;

        } finally {
            entityManager.close();
        }
    }

    private ListReturnDTO<EntityShop> getShopsByName(EntityManager entityManager, String name, long pivotShopId, int limit, Scroll scroll, Boolean enabled) {
        ListReturnDTO<EntityShop> listReturnDTO = new ListReturnDTO<>();
        try {
            Query query;
            if (scroll.equals(Scroll.UP)) {
                query = entityManager.createNamedQuery("EntityShop.getUpwardShopByName");
            } else {
                query = entityManager.createNamedQuery("EntityShop.getDownwardShopByName");
            }
            query.setParameter("id", pivotShopId);
//            query.setParameter("enabled", enabled);
            query.setParameter("name", "%" + name.trim().toLowerCase() + "%");
            query.setMaxResults(limit);
//            ArrayList<EntityShop> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            ArrayList<Long> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (results.isEmpty()) {
                if (pivotShopId > 0) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                }
            } else {
                if (scroll.equals(Scroll.UP)) {
                    Collections.reverse(results);
                }
                List<EntityShop> shops = new LinkedList<>();
                for (Long result : results) {
                    shops.add(findShop(entityManager, result));
                }
//                listReturnDTO.setList(results);
                listReturnDTO.setList(shops);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getShops(): {pivotShopId:" + pivotShopId + " }", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }

        return listReturnDTO;
    }

    private PivotedListReturnDTO<EntityShop, Long> getAllShops(EntityManager entityManager, long pivotShopId, int limit, Scroll scroll, Boolean enabled) {
        PivotedListReturnDTO<EntityShop, Long> listReturnDTO = new PivotedListReturnDTO<>();
        try {
            Query query;
            if (enabled == null) {
                if (scroll.equals(Scroll.UP)) {
                    query = entityManager.createNamedQuery("EntityShop.getAllShopsByScrollUp", EntityShop.class);
                } else {
                    query = entityManager.createNamedQuery("EntityShop.getAllShopsByScrollDown", EntityShop.class);
                }
            } else {
                if (scroll.equals(Scroll.UP)) {
                    query = entityManager.createNamedQuery("EntityShop.getAllShopsByIsEnableScrollDown", EntityShop.class);
                } else {
                    query = entityManager.createNamedQuery("EntityShop.getAllShopsByIsEnableScrollDown", EntityShop.class);
                }
                query.setParameter("enabled", enabled);
            }
            query.setParameter("id", pivotShopId);
            query.setMaxResults(limit);
            //            List<EntityShop> results = query.setMaxResults(limit).getResultList();
            List<Long> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (results.isEmpty()) {
                if (pivotShopId > 0) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                }
            } else {
//                pivotShopId = results.get(results.size() - 1).getId();
                pivotShopId = results.get(results.size() - 1);
                if (scroll == Scroll.UP) {
                    Collections.reverse(results);
                }
                List<EntityShop> shops = new ArrayList<>();
                for (Long result : results) {
                    shops.add(findShop(entityManager, result));
                }
                listReturnDTO.setList(shops);
            }
            listReturnDTO.setPivot(pivotShopId);
        } catch (Exception e) {
            LOGGER.error("Exception in getAllShops(): {pivotShopId:" + pivotShopId + " ,enable:" + enabled + "}", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }

        return listReturnDTO;
    }

    @Override
    public PivotedListReturnDTO<EntityShop, Long> getShopsOfOwner(long pivotShopId, Long ownerId, int limit, Scroll scroll) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            PivotedListReturnDTO<EntityShop, Long> shopsOfOwner = getShopsOfOwner(entityManager, pivotShopId, ownerId, limit, scroll);
            return shopsOfOwner;

        } finally {
            entityManager.close();
        }
    }

    private PivotedListReturnDTO<EntityShop, Long> getShopsOfOwner(EntityManager entityManager, long pivotShopId, Long ownerId, int limit, Scroll scroll) {
        PivotedListReturnDTO<EntityShop, Long> listReturnDTO = new PivotedListReturnDTO<>();
        try {
            Query query;
            if (scroll.equals(Scroll.UP)) {
                query = entityManager.createNamedQuery("EntityShop.getShopsOfOwnerScrollUp", EntityShop.class);
            } else {
                query = entityManager.createNamedQuery("EntityShop.getShopsOfOwnerScrollDown", EntityShop.class);
            }

            query.setParameter("id", pivotShopId);
            query.setParameter("ownerId", ownerId);

            query.setMaxResults(limit);
//            List<EntityShop> results = query.setMaxResults(limit).getResultList();
            List<Long> results = query.setMaxResults(limit).getResultList();
            if (results.isEmpty()) {
                if (pivotShopId > 0) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                }
            } else {
//                pivotShopId = results.get(results.size() - 1).getId();
                pivotShopId = results.get(results.size() - 1);
                List<EntityShop> shops = new LinkedList<>();
                for (Long result : results) {
                    shops.add(findShop(entityManager, result));
                }
                listReturnDTO.setList(shops);
            }
            listReturnDTO.setPivot(pivotShopId);
        } catch (Exception e) {
            LOGGER.error("Exception in getAllShops(): {pivotShopId:" + pivotShopId + ", ownerId:" + ownerId + "}", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }

        return listReturnDTO;
    }

    public void incrementItemsCount(EntityManager entityManager, Long shopId) {
        Query query = entityManager.createNamedQuery("EntityShop.incItemsCount");
        query.setParameter("id", shopId);
        query.executeUpdate();
    }

    public void decrementItemsCount(EntityManager entityManager, Long shopId) {
        Query query = entityManager.createNamedQuery("EntityShop.decItemsCount");
        query.setParameter("id", shopId);
        query.executeUpdate();
    }

    int toggleShopStatus(EntityManager entityManager, long shopId, boolean enable, Long activistId) {
        EntityShop findShop = findShop(entityManager, shopId);
        int reasonCode = ReasonCode.NONE;
        if (findShop == null) {
            reasonCode = ReasonCode.NO_DATA_FOUND;
        } else if (findShop.getEnabled() == enable) {
            reasonCode = ReasonCode.OPERATION_FAILED;
        } else {
            try {
                entityManager.getTransaction().begin();
                findShop.setEnabled(enable);
                entityManager.merge(findShop);
                entityManager.getTransaction().commit();
                /**
                 * Remove from cache
                 */

                ProductStatus updateStatus;
                ProductStatus previousStatus;
                if (enable) {
                    previousStatus = ProductStatus.UNAVAILABLE;
                    updateStatus = ProductStatus.AVAILABLE;
                } else {
                    previousStatus = ProductStatus.AVAILABLE;
                    updateStatus = ProductStatus.UNAVAILABLE;
                }
                ProductRepositoryImpl productRepositoryImpl = new ProductRepositoryImpl();
                ListReturnDTO<String> productIdsOfShop = productRepositoryImpl.getProductIdsOfShop(shopId, null, Scroll.DOWN, Integer.MAX_VALUE, previousStatus);

                if (productIdsOfShop.getReasonCode() == ReasonCode.NONE) {

                    reasonCode = productRepositoryImpl.updateProductStatusByShop(entityManager, updateStatus, previousStatus, shopId, activistId);
                    if (reasonCode == ReasonCode.EXCEPTION_OCCURED) {
                        entityManager.getTransaction().rollback();
                        return reasonCode;
                    } else {

                        CategoryProductRepositoryImpl categoryProductRepositoryImpl = new CategoryProductRepositoryImpl();
                        reasonCode = categoryProductRepositoryImpl.updateShopStatus(shopId, enable);
                        if (reasonCode == ReasonCode.EXCEPTION_OCCURED) {
                            entityManager.getTransaction().rollback();
                            return reasonCode;
                        } else {
                            BasketProductCategoryRepositoryImpl basketProductCategoryRepository = (BasketProductCategoryRepositoryImpl) MarketRepository.INSTANCE.getBasketProductCategoryRepository();
                            reasonCode = basketProductCategoryRepository.updateProductStatus(productIdsOfShop.getList(), enable);
                            if (reasonCode == ReasonCode.EXCEPTION_OCCURED) {
                                entityManager.getTransaction().rollback();
                                return reasonCode;
                            }
                        }
                    }
                    DBConnection.getInstance().getCache().evictAll(EntityProduct.class, productIdsOfShop.getList());

                }
                DBConnection.getInstance().getCache().evict(EntityShop.class, shopId);

//                if (findShop.getType() == ShopType.CELEBRITY_STORE) {
//                    CelebrirtyStoreRepositoryImpl celebrirtyStoreRepositoryImpl = new CelebrirtyStoreRepositoryImpl();
//                    int toggleCelebrityStore = celebrirtyStoreRepositoryImpl.toggleCelebrityStore(entityManager, shopId, activistId, enable);
//                    if (toggleCelebrityStore == ReasonCode.EXCEPTION_OCCURED) {
//                        entityManager.getTransaction().rollback();
//                        reasonCode = ReasonCode.EXCEPTION_OCCURED;
//                        return reasonCode;
//                    }
//                }
                LOGGER.info("toggleShopStatus(): {activistId:" + activistId + ", shopId:" + shopId + ", enable:" + enable + "}");
            } catch (Exception e) {
                LOGGER.error("Exception in toggleShopStatus(): {shopId:" + shopId + ", enable:" + enable + "}", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        }
        return reasonCode;
    }

    @Override
    public int toggleShopStatus(long shopId, boolean enable, Long activistId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            int toggleShopStatus = toggleShopStatus(entityManager, shopId, enable, activistId);
            return toggleShopStatus;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public int toggleStoreType(long shopId, int type, Long activistId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            int toggleShopStatus = toggleStoreType(entityManager, shopId, type, activistId);
            return toggleShopStatus;

        } finally {
            entityManager.close();
        }
    }

    protected int toggleStoreType(EntityManager entityManager, long shopId, int type, Long activistId) {
        EntityShop findShop = findShop(entityManager, shopId);
        int reasonCode = ReasonCode.NONE;
        if (findShop == null) {
            reasonCode = ReasonCode.NO_DATA_FOUND;
        } else if (findShop.getType() == type) {
            reasonCode = ReasonCode.OPERATION_FAILED;
        } else {
            try {
                entityManager.getTransaction().begin();
                findShop.setType(type);
                entityManager.merge(findShop);
                entityManager.getTransaction().commit();

                /**
                 * Removing from cache
                 */
                DBConnection.getInstance().getCache().evict(EntityShop.class, shopId);

                LOGGER.info("toggleStoreType(): {activistId:" + activistId + ", shopId:" + shopId + ", type:" + type + "}");
            } catch (Exception e) {
                LOGGER.error("Exception in toggleStoreType(): {shopId:" + shopId + ", type:" + type + "}", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        }
        return reasonCode;
    }

    @Override
    public ListReturnDTO<EntityShop> getCelebrityStore(long pivotShopId, int limit, Scroll scroll) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityShop> stores = getShopsByType(entityManager, pivotShopId, limit, scroll, ShopType.CELEBRITY_STORE, Boolean.TRUE);
            return stores;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public int toggleCelebrityStore(long shopId, Long activistId, Boolean enable) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            int toggleCelebrityStore = toggleCelebrityStore(entityManager, shopId, activistId, enable);
            return toggleCelebrityStore;

        } finally {
            entityManager.close();
        }
    }

    int toggleCelebrityStore(EntityManager entityManager, long shopId, Long activistId, Boolean enable) {
        int reasonCode = ReasonCode.NONE;
        int type;
        if (enable) {
            type = ShopType.CELEBRITY_STORE;
        } else {
            type = ShopType.GENERAL_STORE;
        }
        reasonCode = toggleStoreType(entityManager, shopId, type, activistId);
        return reasonCode;
    }

    private int updateShop(EntityManager entityManager, EntityShop shop) {
        int reasonCode = ReasonCode.NONE;
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(shop);
            entityManager.getTransaction().commit();
            DBConnection.getInstance().getCache().evict(EntityShop.class, shop.getId());
        } catch (Exception e) {
            LOGGER.error("Exception in updateShop() : {shop:" + new Gson().toJson(shop) + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
        return reasonCode;
    }

    @Override
    public int updateShopName(long id, String name) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            int reasonCode = ReasonCode.NONE;
            EntityShop findShop = findShop(entityManager, id);
            if (findShop == null) {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            } else {
                findShop.setName(name);
                entityManager.getTransaction().begin();
                try {
                    entityManager.merge(findShop);
                    ProductRepositoryImpl productRepositoryImpl = (ProductRepositoryImpl) MarketRepository.INSTANCE.getProductRepository();
                    int productUpdateResult = productRepositoryImpl.updateShopName(entityManager, id, name);
//                    int cartUpdateResult = 0;
                    int orderUpdateResult = 0;
                    OrderRepositoryImpl orderRepositoryImpl = null;
                    if (productUpdateResult > 0) {
//                        CartItemRepositoryImpl cartItemRepositoryImpl = (CartItemRepositoryImpl) MarketRepository.INSTANCE.getCartRepository();
//                        cartUpdateResult = cartItemRepositoryImpl.updateShopName(entityManager, id, name);

                        orderRepositoryImpl = (OrderRepositoryImpl) MarketRepository.INSTANCE.getOrderRepository();
                        orderUpdateResult = orderRepositoryImpl.updateShopName(entityManager, id, name);
                    }
                    entityManager.getTransaction().commit();
                    DBConnection.getInstance().getCache().evict(EntityShop.class, id);
                    if (productUpdateResult > 0) {
                        productRepositoryImpl.evictProductOfShop(entityManager, id);
                    }

//                    if (cartUpdateResult > 0) {
//                        DBConnection.getInstance().getCache().evict(EntityCartItem.class);
//                    }
                    if (orderUpdateResult > 0) {
                        orderRepositoryImpl.evictOrdersOfShop(entityManager, id);
                    }

                } catch (Exception e) {
                    LOGGER.error("Exception in updateShopName() : {shopId: " + id + ",name: " + name + "}", e);
                    reasonCode = ReasonCode.EXCEPTION_OCCURED;
                    if (entityManager.getTransaction().isActive()) {
                        entityManager.getTransaction().rollback();
                    }
                }
            }
            return reasonCode;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public int updateImage(long id, String imageUrl, int height, int width) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            int reasonCode;
            EntityShop findShop = findShop(entityManager, id);
            if (findShop == null) {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            } else {
                findShop.setImageUrl(imageUrl);
                findShop.setImageHeight(height);
                findShop.setImageWidth(width);
                reasonCode = updateShop(entityManager, findShop);
            }
            return reasonCode;

        } finally {
            entityManager.close();
        }
    }
}
