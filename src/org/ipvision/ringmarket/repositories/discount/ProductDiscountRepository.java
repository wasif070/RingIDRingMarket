/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.discount;

import org.ipvision.ringmarket.constants.DiscountUnit;
import org.ipvision.ringmarket.entities.discount.EntityProductDiscount;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;

/**
 *
 * @author saikat
 */
public interface ProductDiscountRepository {

    int add(EntityProductDiscount discount);

    int deleteFromDb(String productId);

    int updateDiscountValidityTime(String productId, Long activeTime, Long validityTime);

    int updateDiscountValue(String productId, Float value);

    int changeDiscountUnit(String productId, DiscountUnit discountUnit);

    ObjectReturnDTO<EntityProductDiscount> findDiscountById(String productId);

}
