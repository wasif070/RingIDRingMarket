/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.discount;

import com.google.gson.Gson;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.DiscountUnit;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.discount.EntityProductDiscount;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class ProductDiscountRepositoryImpl implements ProductDiscountRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDiscountRepositoryImpl.class);

    @Override
    public int add(EntityProductDiscount discount) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode = ReasonCode.EXCEPTION_OCCURED;
        try {
            reasonCode = addDiscount(entityManager, discount);
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public int updateDiscountValue(String productId, Float value) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode = ReasonCode.EXCEPTION_OCCURED;
        try {
            EntityProductDiscount productDiscount = entityManager.find(EntityProductDiscount.class, productId);

            if (productDiscount == null) {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            } else {
                ProductRepositoryImpl productRepositoryImpl = (ProductRepositoryImpl) MarketRepository.INSTANCE.getProductRepository();
                if (!validDiscountCheck(productRepositoryImpl.getProductById(entityManager, productId).getPrice(), value, productDiscount.getDiscountUnit())) {
                    reasonCode = ReasonCode.INVALID_PARAMETER;
                } else {
                    productDiscount.setDiscountValue(value);
                    reasonCode = updateDiscount(entityManager, productDiscount);
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public int changeDiscountUnit(String productId, DiscountUnit discountUnit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode = ReasonCode.EXCEPTION_OCCURED;
        try {
            EntityProductDiscount productDiscount = entityManager.find(EntityProductDiscount.class, productId);
            if (productDiscount == null) {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            } else {
                ProductRepositoryImpl productRepositoryImpl = (ProductRepositoryImpl) MarketRepository.INSTANCE.getProductRepository();
                if (productDiscount.getDiscountUnit() == discountUnit.getValue() || !validDiscountCheck(productRepositoryImpl.getProductById(entityManager, productId).getPrice(), productDiscount.getDiscountValue(), discountUnit.getValue())) {
                    reasonCode = ReasonCode.INVALID_PARAMETER;
                } else {
                    productDiscount.setDiscountUnit(discountUnit.getValue());
                    reasonCode = updateDiscount(entityManager, productDiscount);
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public int updateDiscountValidityTime(String productId, Long activeTime, Long validityTime) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode = ReasonCode.EXCEPTION_OCCURED;
        try {
            EntityProductDiscount productDiscount = entityManager.find(EntityProductDiscount.class, productId);
            if (productDiscount == null) {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            } else {
                productDiscount.setActiveTime(activeTime);
                productDiscount.setValidityTime(validityTime);
                reasonCode = updateDiscount(entityManager, productDiscount);
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    private int updateDiscount(EntityManager em, EntityProductDiscount discount) {
        int reasonCode = ReasonCode.NONE;
        try {
            em.getTransaction().begin();
            em.persist(em);
            em.getTransaction().commit();
            DBConnection.getInstance().getCache().evict(EntityProduct.class, discount.getProductId());
        } catch (Exception e) {
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            LOGGER.error("Error in updateDiscount() {discount: " + new Gson().toJson(discount) + "}", e);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        }
        return reasonCode;
    }

    private boolean validDiscountCheck(Double price, Float dicountValue, byte discountUnit) {
        Double newPrice;
        if (discountUnit == DiscountUnit.PERCENTAGE.getValue()) {
            newPrice = price - (price * dicountValue / 100);
        } else {
            newPrice = price - dicountValue;
        }
        return newPrice > 0;
    }

    private int addDiscount(EntityManager em, EntityProductDiscount discount) {
        int reasonCode = ReasonCode.NONE;
        ProductRepositoryImpl productRepositoryImpl = (ProductRepositoryImpl) MarketRepository.INSTANCE.getProductRepository();
        try {
            EntityProduct productById = productRepositoryImpl.getProductById(em, discount.getProductId());
            if (productById == null || !validDiscountCheck(productById.getPrice(), discount.getDiscountValue(), discount.getDiscountUnit())) {
                return ReasonCode.INVALID_PARAMETER;
            }
            em.getTransaction().begin();
            em.persist(discount);
            productById.setIsDiscounted(true);
            productRepositoryImpl.update(em, productById);
            em.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Error in addDiscount() : {discount: +" + new Gson().toJson(discount) + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        }
        return reasonCode;
    }

    public ObjectReturnDTO<EntityProductDiscount> getValidDiscount(String productId, Long currentTime) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ObjectReturnDTO<EntityProductDiscount> returnDTO;
        try {
            returnDTO = getValidDiscount(entityManager, productId, currentTime);
        } finally {
            entityManager.close();
        }
        return returnDTO;
    }

    public ObjectReturnDTO<EntityProductDiscount> getValidDiscount(EntityManager entityManager, String productId, Long currentTime) {
        ObjectReturnDTO<EntityProductDiscount> returnDTO = new ObjectReturnDTO<>();
        EntityProductDiscount productDiscount = entityManager.find(EntityProductDiscount.class, productId);
        if (productDiscount == null) {
            returnDTO.setReasonCode(ReasonCode.INVALID_INFORMATION);
        } else {
            if (productDiscount.getActiveTime() > currentTime) {
                returnDTO.setReasonCode(ReasonCode.RingMarket.VALIDITY_OVER);
            } else {
                if (productDiscount.getValidityTime() > 0 && currentTime > productDiscount.getValidityTime()) {
                    returnDTO.setReasonCode(ReasonCode.RingMarket.VALIDITY_OVER);
                } else {
                    returnDTO.setValue(productDiscount);
                }
            }
        }
        return returnDTO;
    }

    @Override
    public int deleteFromDb(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int deleteEntry = ReasonCode.EXCEPTION_OCCURED;
        try {
            deleteEntry = deleteEntry(entityManager, productId);
        } finally {
            entityManager.close();
        }
        return deleteEntry;
    }

    private int deleteEntry(EntityManager em, String productId) {
        int reasonCode = ReasonCode.NONE;
        EntityProductDiscount discount = em.find(EntityProductDiscount.class, productId);
        if (discount != null) {
            try {
                em.getTransaction().begin();
                em.remove(discount);
                ProductRepositoryImpl productRepositoryImpl = (ProductRepositoryImpl) MarketRepository.INSTANCE.getProductRepository();
                reasonCode = productRepositoryImpl.enableDiscount(em, productId, false);
                if (reasonCode == ReasonCode.NONE) {
                    em.getTransaction().commit();
                } else {
                    em.getTransaction().rollback();
                }
                DBConnection.getInstance().getCache().evict(EntityProductDiscount.class, productId);
            } catch (Exception e) {
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
                LOGGER.error("Error in deleteEntry() : {productId: " + productId + "}", e);
                if (em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
            }
        } else {
            reasonCode = ReasonCode.INVALID_INFORMATION;
        }
        return reasonCode;
    }

    @Override
    public ObjectReturnDTO<EntityProductDiscount> findDiscountById(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ObjectReturnDTO<EntityProductDiscount> returnDTO;
        try {
            returnDTO = findDiscountById(entityManager, productId);
        } finally {
            entityManager.close();
        }
        return returnDTO;
    }

    public EntityProductDiscount find(EntityManager em, String id) {
        return em.find(EntityProductDiscount.class, id);
    }

    private ObjectReturnDTO<EntityProductDiscount> findDiscountById(EntityManager em, String productId) {
        ObjectReturnDTO<EntityProductDiscount> returnDTO = new ObjectReturnDTO<>();
        try {
            EntityProductDiscount find = em.find(EntityProductDiscount.class, productId);
            if (find != null) {
                returnDTO.setValue(find);
            } else {
                returnDTO.setReasonCode(ReasonCode.INVALID_INFORMATION);
            }
        } catch (Exception e) {
            LOGGER.error("Error in findDiscountById() : {productId: " + productId + "}", e);
            returnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return returnDTO;
    }

}
