/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.menu;

import java.util.List;
import org.ipvision.ringmarket.entities.menu.EntityTopMenu;

/**
 *
 * @author saikat
 */
public interface TopMenuRepository {

    boolean addMenu(String menuName, int menuType, String icon, Boolean enable, int type, int ordinal);

    boolean addMenu(EntityTopMenu menu);

    List<EntityTopMenu> getTopMenus();
    
}
