/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.menu;

import java.util.List;
import org.ipvision.ringmarket.entities.menu.EntityFeaturePage;

/**
 *
 * @author saikat
 */
public interface FeaturePagesRepository {

    boolean addFeatureMenu(String menuName, Boolean enable, int type, int ordinal);

    boolean addFeatureMenu(EntityFeaturePage menu);

    List<EntityFeaturePage> getFeaturePages();
    
}
