/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.menu;

import java.util.List;
import org.ipvision.ringmarket.db.DBConnection;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.entities.menu.EntityFeaturePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class FeaturePagesRepositoryImpl implements FeaturePagesRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeaturePagesRepositoryImpl.class);

    @Override
    public boolean addFeatureMenu(String menuName, Boolean enable, int type, int ordinal) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean success = false;

        try {
            entityManager.getTransaction().begin();
            try {
                EntityFeaturePage menu = new EntityFeaturePage();
                menu.setName(menuName);
                menu.setType(type);
                menu.setIsEnable(enable);
                addMenu(entityManager, menu);
                entityManager.getTransaction().commit();

                success = true;
            } catch (Exception ex) {
                LOGGER.error("Error in addFeatureMenu(): {menuName: " + menuName + " , type: " + type + ",ordinal:" + ordinal + "} ", ex);
            }
        } finally {
            entityManager.close();
        }
        return success;
    }

    @Override
    public boolean addFeatureMenu(EntityFeaturePage menu) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean success = false;

        try {
            entityManager.getTransaction().begin();
            try {

                addMenu(entityManager, menu);
                entityManager.getTransaction().commit();

                success = true;
            } catch (Exception ex) {
                LOGGER.error("Error in addFeatureMenu(): {menuName: " + menu.getName() + " , type: " + menu.getType() + ",ordinal:" + menu.getOrdinal() + "} ", ex);
            }
        } finally {
            entityManager.close();
        }
        return success;
    }

    private void addMenu(EntityManager entityManager, EntityFeaturePage menu) {
        entityManager.persist(menu);
    }

    @Override
    public List<EntityFeaturePage> getFeaturePages() {
        List<EntityFeaturePage> featurePages = null;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            Query query = entityManager.createNamedQuery("EntityFeaturePage.getEnabledPages", EntityFeaturePage.class);
            query.setParameter("enable", true);
            try {
                featurePages = query.getResultList();
            } catch (Exception e) {
                LOGGER.error("Exception in getTopMenus(): ", e);
            }
        } finally {
            entityManager.close();
        }
        return featurePages;
    }
}
