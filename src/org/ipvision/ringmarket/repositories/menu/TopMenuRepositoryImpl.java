/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.menu;

import java.util.List;
import org.ipvision.ringmarket.db.DBConnection;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.entities.menu.EntityTopMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class TopMenuRepositoryImpl implements TopMenuRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(TopMenuRepositoryImpl.class);

    @Override
    public boolean addMenu(String menuName, int menuType, String icon, Boolean enable, int type, int ordinal) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean success = false;

        try {
            entityManager.getTransaction().begin();
            try {
                EntityTopMenu menu = new EntityTopMenu();
                menu.setIcon(icon);
                menu.setName(menuName);
                menu.setType(type);
                menu.setIsEnable(enable);
                addMenu(entityManager, menu);
                entityManager.getTransaction().commit();

                success = true;
            } catch (Exception ex) {
                LOGGER.error("Error in addMenu: {menuName: " + menuName + " , type: " + type + ",ordinal:" + ordinal + "} ", ex);
            }
        } finally {
            entityManager.close();
        }
        return success;
    }

    @Override
    public boolean addMenu(EntityTopMenu menu) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean success = false;

        try {
            entityManager.getTransaction().begin();
            try {

                addMenu(entityManager, menu);
                entityManager.getTransaction().commit();

                success = true;
            } catch (Exception ex) {
                LOGGER.error("Error in addMenu: {menuName: " + menu.getName() + " , type: " + menu.getType() + ",ordinal:" + menu.getOrdinal() + "} ", ex);
            }
        } finally {
            entityManager.close();
        }
        return success;
    }

    private void addMenu(EntityManager entityManager, EntityTopMenu menu) {
        entityManager.persist(menu);
    }

    @Override
    public List<EntityTopMenu> getTopMenus() {
        List<EntityTopMenu> entityTopMenus = null;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            Query query = entityManager.createNamedQuery("getEnabledTopMenu", EntityTopMenu.class);
            query.setParameter("enable", true);
            try {
                entityTopMenus = query.getResultList();
            } catch (Exception e) {
                LOGGER.error("Exception in getTopMenus(): ", e);
            }
        } finally {
            entityManager.close();
        }
        return entityTopMenus;
    }
}
