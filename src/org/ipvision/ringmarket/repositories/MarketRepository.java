/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories;

import org.ipvision.ringmarket.repositories.product.CategoryProductCountImpl;
import org.ipvision.ringmarket.repositories.product.CategoryProductRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.HistoryProduct;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductDetailRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductImageRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductLike;
import org.ipvision.ringmarket.repositories.product.ProductRatingLogRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductRatingRepository;
import org.ipvision.ringmarket.repositories.product.ProductRatingRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductVideo;
import org.ipvision.ringmarket.repositories.product.ProductViewRepoImpl;
import org.ipvision.ringmarket.repositories.product.ReportProduct;
import org.ipvision.ringmarket.product.buyer.OfferProduct;
import org.ipvision.ringmarket.product.buyer.SavedProduct;
import org.ipvision.ringmarket.product.order.AgentOrder;
import org.ipvision.ringmarket.repositories.category.CategoryRepositoryImpl;
import org.ipvision.ringmarket.product.order.HistoryAgentOrder;
import org.ipvision.ringmarket.product.order.HistoryOrder;
import org.ipvision.ringmarket.product.seller.SellerProduct;
import org.ipvision.ringmarket.product.seller.SellerRating;
import org.ipvision.ringmarket.product.seller.SellerRatingLogRepositoryImpl;
import org.ipvision.ringmarket.user.RingmarketUser;
import org.ipvision.ringmarket.user.UserRole;
import org.ipvision.ringmarket.repositories.agent.AgentOrderRepository;
import org.ipvision.ringmarket.repositories.agent.HistoryAgentOrderRepository;
import org.ipvision.ringmarket.repositories.buyer.HistoryOrderRepository;
import org.ipvision.ringmarket.repositories.product.HistoryProductRepository;
import org.ipvision.ringmarket.repositories.user.MarketUserRepository;
import org.ipvision.ringmarket.repositories.buyer.OfferProductRepository;
import org.ipvision.ringmarket.repositories.product.ProductDetailsRepository;
import org.ipvision.ringmarket.repositories.product.ProductImageRepository;
import org.ipvision.ringmarket.repositories.product.ProductLikeRepository;
import org.ipvision.ringmarket.repositories.product.ProductRepository;
import org.ipvision.ringmarket.repositories.product.ProductVideoRepository;
import org.ipvision.ringmarket.repositories.buyer.ProductViewRepository;
import org.ipvision.ringmarket.repositories.buyer.ReportProductRepository;
import org.ipvision.ringmarket.repositories.buyer.SavedProductRepository;
import org.ipvision.ringmarket.repositories.order.OrderDetailsRepository;
import org.ipvision.ringmarket.repositories.order.OrderDetailsRepositoryImpl;
import org.ipvision.ringmarket.repositories.order.OrderRepository;
import org.ipvision.ringmarket.repositories.order.OrderRepositoryImpl;
import org.ipvision.ringmarket.repositories.order.ShippingAddressRepoImpl;
import org.ipvision.ringmarket.repositories.order.ShippingAddressRepository;
import org.ipvision.ringmarket.repositories.product.CategoryProductRepository;
import org.ipvision.ringmarket.repositories.seller.SellerProductRepository;
import org.ipvision.ringmarket.repositories.seller.SellerRatingRepository;
import org.ipvision.ringmarket.repositories.user.UserRoleRepository;
import org.ipvision.ringmarket.repositories.category.CategoryRepository;
import org.ipvision.ringmarket.repositories.discount.ProductDiscountRepository;
import org.ipvision.ringmarket.repositories.discount.ProductDiscountRepositoryImpl;
import org.ipvision.ringmarket.repositories.menu.FeaturePagesRepository;
import org.ipvision.ringmarket.repositories.menu.FeaturePagesRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.CategoryProductCountRepository;
import org.ipvision.ringmarket.repositories.product.ProductRatingLogRepository;
import org.ipvision.ringmarket.repositories.seller.SellerRatingLogRepository;
//import org.ipvision.ringmarket.repositories.shop.CelebrirtyStoreRepositoryImpl;
//import org.ipvision.ringmarket.repositories.shop.CelebrityStoreRepository;
import org.ipvision.ringmarket.repositories.shop.ShopRepository;
import org.ipvision.ringmarket.repositories.user.basket.BasketProductCategoryRepository;
import org.ipvision.ringmarket.repositories.user.cart.CartRepository;
import org.ipvision.ringmarket.repositories.user.basket.UserBasketRepository;
import org.ipvision.ringmarket.repositories.menu.TopMenuRepository;
import org.ipvision.ringmarket.repositories.menu.TopMenuRepositoryImpl;
import org.ipvision.ringmarket.repositories.order.HistoryOrderStatusRepoImpl;
import org.ipvision.ringmarket.repositories.order.HistoryOrderStatusRepository;
import org.ipvision.ringmarket.repositories.product.ProductStockRepository;
import org.ipvision.ringmarket.repositories.product.ProductStockRepositoryImpl;
import org.ipvision.ringmarket.repositories.shop.ShopRepositoryImpl;
import org.ipvision.ringmarket.repositories.user.basket.BasketProductCategoryRepositoryImpl;
import org.ipvision.ringmarket.repositories.user.basket.UserBasketRepositoryImpl;
import org.ipvision.ringmarket.repositories.user.cart.CartItemRepositoryImpl;

/**
 *
 * @author saikat
 */
public enum MarketRepository {
    INSTANCE;

    private final CategoryRepository categoryRepository;
    private final HistoryProductRepository historyProductRepository;
    private final ProductDiscountRepository productDiscounRepository;
    private final ProductRepository productRepository;
    private final ProductDetailsRepository productDetailsRepository;
    private final ProductImageRepository productImageRepository;
    private final ProductLikeRepository productLikeRepository;
    private final ProductVideoRepository productVideoRepository;
    private final ProductViewRepository productViewRepository;
    private final ReportProductRepository reportProductRepository;
    private final AgentOrderRepository agentOrderRepository;
    private final HistoryAgentOrderRepository historyAgentOrderRepository;
    private final OfferProductRepository offerProductRepository;
    private final SavedProductRepository savedProductRepository;
    private final HistoryOrderRepository historyOrderRepository;
    private final SellerProductRepository sellerProductRepository;
    private final SellerRatingRepository sellerRatingRepository;
    private final MarketUserRepository marketUserRepository;
    private final UserRoleRepository userRoleRepository;
    private final CategoryProductRepository categoryProductRepository;
    private final CategoryProductCountRepository categoryProductCountRepository;
    private final ShopRepository shopRepository;
    private final ProductRatingRepository productRatingRepository;
    private final UserBasketRepository basketRepository;
    private final BasketProductCategoryRepository basketProductCategoryRepository;
    private final CartRepository cartRepository;
    private final TopMenuRepository topMenuRepository;
    private final OrderRepository orderRepository;
    private final OrderDetailsRepository orderDetailsRepository;
    private final ProductRatingLogRepository productRatingLogRepository;
    private final SellerRatingLogRepository sellerRatingLogRepository;
    private final ShippingAddressRepository shippingAddressRepository;
//    private final CelebrityStoreRepository celebrityStoreRepository;
    private final FeaturePagesRepository featurePagesRepository;
    private final HistoryOrderStatusRepository historyOrderStatusRepository;
    private final ProductStockRepository productStockRepository;

    private MarketRepository() {
        this.categoryRepository = new CategoryRepositoryImpl();
        this.historyProductRepository = new HistoryProduct();
        this.productRepository = new ProductRepositoryImpl();
        this.productStockRepository = new ProductStockRepositoryImpl();
        this.productDetailsRepository = new ProductDetailRepositoryImpl();
        this.productImageRepository = new ProductImageRepositoryImpl();
        this.productLikeRepository = new ProductLike();
        this.productVideoRepository = new ProductVideo();
        this.productViewRepository = new ProductViewRepoImpl();
        this.reportProductRepository = new ReportProduct();
        this.agentOrderRepository = new AgentOrder();
        this.historyAgentOrderRepository = new HistoryAgentOrder();
        this.offerProductRepository = new OfferProduct();
        this.savedProductRepository = new SavedProduct();
        this.historyOrderRepository = new HistoryOrder();
        this.sellerProductRepository = new SellerProduct();
        this.sellerRatingRepository = new SellerRating();
        this.marketUserRepository = new RingmarketUser();
        this.userRoleRepository = new UserRole();
        this.categoryProductRepository = new CategoryProductRepositoryImpl();
        this.categoryProductCountRepository = new CategoryProductCountImpl();
        this.shopRepository = new ShopRepositoryImpl();
        this.productRatingRepository = new ProductRatingRepositoryImpl();
        this.basketRepository = new UserBasketRepositoryImpl();
        this.basketProductCategoryRepository = new BasketProductCategoryRepositoryImpl();
        this.cartRepository = new CartItemRepositoryImpl();
        this.topMenuRepository = new TopMenuRepositoryImpl();
        this.orderRepository = new OrderRepositoryImpl();
        this.orderDetailsRepository = new OrderDetailsRepositoryImpl();
        this.productRatingLogRepository = new ProductRatingLogRepositoryImpl();
        this.sellerRatingLogRepository = new SellerRatingLogRepositoryImpl();
        this.shippingAddressRepository = new ShippingAddressRepoImpl();
//        this.celebrityStoreRepository = new CelebrirtyStoreRepositoryImpl();
        this.featurePagesRepository = new FeaturePagesRepositoryImpl();
        this.historyOrderStatusRepository = new HistoryOrderStatusRepoImpl();
        this.productDiscounRepository = new ProductDiscountRepositoryImpl();
    }

    public ProductDiscountRepository getProductDiscounRepository() {
        return productDiscounRepository;
    }

    public ProductStockRepository getProductStockRepository() {
        return productStockRepository;
    }

    public HistoryOrderStatusRepository getHistoryOrderStatusRepository() {
        return historyOrderStatusRepository;
    }

    public FeaturePagesRepository getFeaturePagesRepository() {
        return featurePagesRepository;
    }

//    public CelebrityStoreRepository getCelebrityStoreRepository() {
//        return celebrityStoreRepository;
//    }
    public ShippingAddressRepository getShippingAddressRepository() {
        return shippingAddressRepository;
    }

    public SellerRatingLogRepository getSellerRatingLogRepository() {
        return sellerRatingLogRepository;
    }

    public ProductRatingLogRepository getProductRatingLogRepository() {
        return productRatingLogRepository;
    }

    public OrderDetailsRepository getOrderDetailsRepository() {
        return orderDetailsRepository;
    }

    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    public TopMenuRepository getTopMenuRepository() {
        return topMenuRepository;
    }

    public CartRepository getCartRepository() {
        return cartRepository;
    }

    public BasketProductCategoryRepository getBasketProductCategoryRepository() {
        return basketProductCategoryRepository;
    }

    public UserBasketRepository getBasketRepository() {
        return basketRepository;
    }

    public ProductRatingRepository getProductRatingRepository() {
        return productRatingRepository;
    }

    public ShopRepository getShopRepository() {
        return shopRepository;
    }

    public CategoryProductCountRepository getCategoryProductCountRepository() {
        return categoryProductCountRepository;
    }

    public CategoryProductRepository getCategoryProductRepository() {
        return categoryProductRepository;
    }

    public CategoryRepository getCategoryRepository() {
        return categoryRepository;
    }

    public HistoryProductRepository getHistoryProductRepository() {
        return historyProductRepository;
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public ProductDetailsRepository getProductDetailsRepository() {
        return productDetailsRepository;
    }

    public ProductImageRepository getProductImageRepository() {
        return productImageRepository;
    }

    public ProductLikeRepository getProductLikeRepository() {
        return productLikeRepository;
    }

    public ProductVideoRepository getProductVideoRepository() {
        return productVideoRepository;
    }

    public ProductViewRepository getProductViewRepository() {
        return productViewRepository;
    }

    public ReportProductRepository getReportProductRepository() {
        return reportProductRepository;
    }

    public AgentOrderRepository getAgentOrderRepository() {
        return agentOrderRepository;
    }

    public HistoryAgentOrderRepository getHistoryAgentOrderRepository() {
        return historyAgentOrderRepository;
    }

    public OfferProductRepository getOfferProductRepository() {
        return offerProductRepository;
    }

    public SavedProductRepository getSavedProductRepository() {
        return savedProductRepository;
    }

    public HistoryOrderRepository getHistoryOrderRepository() {
        return historyOrderRepository;
    }

    public SellerProductRepository getSellerProductRepository() {
        return sellerProductRepository;
    }

    public SellerRatingRepository getSellerRatingRepository() {
        return sellerRatingRepository;
    }

    public MarketUserRepository getMarketUserRepository() {
        return marketUserRepository;
    }

    public UserRoleRepository getUserRoleRepository() {
        return userRoleRepository;
    }

}
