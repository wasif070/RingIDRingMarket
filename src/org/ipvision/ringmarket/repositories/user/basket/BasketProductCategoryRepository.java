/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user.basket;

import java.util.List;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.utils.ListReturnDTO;

/**
 *
 * @author saikat
 */
public interface BasketProductCategoryRepository {

    int addBasketProductCategory(String basketId, String productId, Long userId);

    List<EntityCategory> getBasketCategoriesById(String basketId, int start, int limit, Integer categoryId);

    ListReturnDTO<EntityProduct> getProductsOfBasket(String basketId, Integer categoryId, String pivotProductId, Scroll scroll, int limit);

    int deleteProductFromBasket(String basketId, String productId,Long userId);
    
    String getBasketIdByUserIdAndProductId(Long userId, String productId);
    
    ListReturnDTO<EntityProduct> getProductsOfBasket(Long userId, String pivotProductId, Scroll scroll, int limit) ;
}
