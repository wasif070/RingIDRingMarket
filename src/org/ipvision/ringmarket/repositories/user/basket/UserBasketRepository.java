/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user.basket;

import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.user.basket.EntityUserBasket;
import org.ipvision.ringmarket.utils.ListReturnDTO;

/**
 *
 * @author saikat
 */
public interface UserBasketRepository {

    int addBasket(EntityUserBasket basket);

    EntityUserBasket findBasket(String basketId);

    ListReturnDTO<EntityUserBasket> getCelebrityBaskets(String pivotId, int limit, Scroll scroll);

    boolean removeBasket(String basketId);

    boolean updateUserBasket(String basketId, String basketName, String image, Integer imageHeight, Integer imageWith, Integer ownerType);
    
    ListReturnDTO<EntityUserBasket> getUserBaskets(Long userId);
    
}
