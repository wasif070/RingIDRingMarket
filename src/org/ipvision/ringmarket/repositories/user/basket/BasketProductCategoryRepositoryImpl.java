/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user.basket;

import org.ipvision.ringmarket.db.DBConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import org.ipvision.ringmarket.cache.ProductCategoryCache;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.user.basket.EntityBasketProductCategory;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.entities.id.BasketCategoryProductId;
import org.ipvision.ringmarket.retryutil.Retry;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class BasketProductCategoryRepositoryImpl implements BasketProductCategoryRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasketProductCategoryRepositoryImpl.class);

    @Override
    public int addBasketProductCategory(String basketId, String productId, Long userId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode = ReasonCode.NONE;
        try {
            ListReturnDTO<EntityCategory> categoriesByProduct = MarketRepository.INSTANCE.getCategoryProductRepository().getCategoriesByProduct(productId);
            if (categoriesByProduct.getReasonCode() == ReasonCode.NONE) {

                reasonCode = addBasketProductCategories(entityManager, basketId, categoriesByProduct.getList(), productId, userId);
            } else {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public List<EntityCategory> getBasketCategoriesById(String basketId, int start, int limit, Integer categoryId) {
        List<EntityCategory> categories = null;
        if (start >= 0) {
            if (categoryId == null) {
                categories = getBasketCategoriesById(basketId, start, limit, 1, categoryId);
            } else {
                EntityCategory categoryInfo = ProductCategoryCache.getInstance().getCategoryInfo(categoryId);
                if (categoryInfo != null) {
                    categories = getBasketCategoriesById(basketId, start, limit, categoryInfo.getPosition() + 1, categoryId);
                }
            }
        }
        return categories;
    }

    private List<EntityCategory> getBasketCategoriesById(String basketId, int start, int limit, int position, Integer parentId) {
        List<EntityCategory> categories = null;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                Query query = entityManager.createNamedQuery("getCategoriesByBasketAndPosition", Integer.class);
                query.setParameter("basketId", basketId);
                query.setParameter("position", position);
                List<Integer> list = query.setFirstResult(start).setMaxResults(limit).getResultList();
//                List<Integer> list = query.setFirstResult(start).getResultList();
                if (!list.isEmpty()) {
                    if (parentId != null && parentId != 0) {
                        List<Integer> subCategoryIds = ProductCategoryCache.getInstance().getSubCategoryIds(parentId);
                        list = new ArrayList<>(list);
                        list.retainAll(subCategoryIds);
                    }
                    categories = new ArrayList<>();
                    for (Integer categoryId : list) {
                        categories.add(ProductCategoryCache.getInstance().getCategoryInfo(categoryId));
                    }
                    if (!categories.isEmpty()) {
                        categories.sort(Utility.getCategoryOrderCompartor());
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Exception in getBasketCategoriesById():", e);
            }

        } finally {
            entityManager.close();
        }
        return categories;
    }

    @Override
    public ListReturnDTO<EntityProduct> getProductsOfBasket(String basketId, Integer categoryId, String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ListReturnDTO<EntityProduct> productList = null;

        try {
            if (categoryId != null) {
                productList = getProductsOfBasketCategory(entityManager, basketId, categoryId, pivotProductId, scroll, limit);
            } else {
                productList = getProductsOfBasket(entityManager, basketId, pivotProductId, scroll, limit);
            }

        } finally {
            entityManager.close();
        }
        return productList;
    }

    @Override
    public ListReturnDTO<EntityProduct> getProductsOfBasket(Long userId, String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ListReturnDTO<EntityProduct> productList = null;

        try {
            productList = getProductsOfBasket(entityManager, userId, pivotProductId, scroll, limit);
        } finally {
            entityManager.close();
        }
        return productList;
    }

    private ListReturnDTO<EntityProduct> getProductsOfBasket(EntityManager entityManager, String basketId, String pivotProductId, Scroll scroll, int limit) {
        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();

        try {
            if (pivotProductId == null) {
                pivotProductId = "";
            }
            List<EntityProduct> products;
            Query query;
            if (scroll == Scroll.UP) {
                query = entityManager.createNamedQuery("EntityBasketProductCategory.getProductsOfBasketScrollUp");
            } else {
                query = entityManager.createNamedQuery("EntityBasketProductCategory.getProductsOfBasketScrollDown");
            }
            query.setParameter("basketId", basketId);
            query.setParameter("pivotProductId", pivotProductId);
            List<String> productIds = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (!productIds.isEmpty()) {
                if (scroll.equals(Scroll.UP)) {
                    Collections.reverse(productIds);
                }

                products = new ArrayList<>();
                for (String productId : productIds) {
                    EntityProduct productById = MarketRepository.INSTANCE.getProductRepository().getProductById(productId);
                    if (productById != null) {
                        products.add(productById);
                    }
                }
                listReturnDTO.setList(products);
            } else if (pivotProductId.isEmpty()) {
                listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
            } else {
                listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getProductsOfBasket()", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    private ListReturnDTO<EntityProduct> getProductsOfBasket(EntityManager entityManager, Long userId, String pivotProductId, Scroll scroll, int limit) {
        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();

        try {
            if (pivotProductId == null) {
                pivotProductId = "";
            }
            List<EntityProduct> products;
            Query query;
            if (scroll == Scroll.UP) {
                query = entityManager.createNamedQuery("EntityBasketProductCategory.getProductsOfBasketByUserIdScrollUp");
            } else {
                query = entityManager.createNamedQuery("EntityBasketProductCategory.getProductsOfBasketByUserIdScrollDown");
            }
            query.setParameter("userId", userId);
            query.setParameter("pivotProductId", pivotProductId);
            List<String> productIds = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (!productIds.isEmpty()) {
                if (scroll.equals(Scroll.UP)) {
                    Collections.reverse(productIds);
                }

                products = new ArrayList<>();
                for (String productId : productIds) {
                    EntityProduct productById = MarketRepository.INSTANCE.getProductRepository().getProductById(productId);
                    if (productById != null) {
                        products.add(productById);
                    }
                }
                listReturnDTO.setList(products);
            } else if (pivotProductId.isEmpty()) {
                listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
            } else {
                listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getProductsOfBasket()", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    private ListReturnDTO<EntityProduct> getProductsOfBasketCategory(EntityManager entityManager, String basketId, int categoryId, String pivotProductId, Scroll scroll, int limit) {
        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();
        try {
            if (pivotProductId == null) {
                pivotProductId = "";
            }
            List<EntityProduct> categoryProducts;
            Query query;
            if (scroll == Scroll.UP) {
                query = entityManager.createNamedQuery("EntityBasketProductCategory.getProductsOfBasketCatScrollUp");
            } else {
                query = entityManager.createNamedQuery("EntityBasketProductCategory.getProductsOfBasketCatScrollDown");
            }
            query.setParameter("categoryId", categoryId);
            query.setParameter("basketId", basketId);
            query.setParameter("pivotProductId", pivotProductId);
            List<String> productIds = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (!productIds.isEmpty()) {
                if (scroll.equals(Scroll.UP)) {
                    Collections.reverse(productIds);
                }

                categoryProducts = new ArrayList<>();
                for (String productId : productIds) {
                    EntityProduct productById = MarketRepository.INSTANCE.getProductRepository().getProductById(productId);
                    if (productById != null) {
                        categoryProducts.add(productById);
                    }
                }
                listReturnDTO.setList(categoryProducts);
            } else if (pivotProductId.isEmpty()) {
                listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
            } else {
                listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getProductsOfBasketCategory()", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }

        return listReturnDTO;
    }

    private int addBasketProductCategories(EntityManager entityManager, String basketId, List<EntityCategory> categoriesByProduct, String productId, Long userId) {
        int success = ReasonCode.NONE;
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            for (EntityCategory entityCategory : categoriesByProduct) {
                EntityBasketProductCategory basketProductCategory = new EntityBasketProductCategory();
                basketProductCategory.setBasketId(basketId);
                basketProductCategory.setCategoryId(entityCategory.getId());
                basketProductCategory.setPosition(entityCategory.getPosition());
                basketProductCategory.setProductId(productId);
                basketProductCategory.setUserId(userId);
                entityManager.persist(basketProductCategory);
            }
            transaction.commit();
        } catch (Exception e) {
            LOGGER.error("Exception in addBasketProductCategories(): {basketId:" + basketId + ", productId:" + productId + "}", e);
            success = ReasonCode.EXCEPTION_OCCURED;
        }
        return success;
    }

    /**
     *
     * @param basketId
     * @param productId
     * @return
     */
    @Override
    public int deleteProductFromBasket(String basketId, String productId, Long userId) {
        int success = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {

            Query query = entityManager.createNamedQuery("EntityBasketProductCategory.getBasketByUserIdAndProductIdAndBasketId");
            query.setParameter("basketId", basketId);
            query.setParameter("userId", userId);
            query.setParameter("productId", productId);
            List<EntityBasketProductCategory> basketCategoryProduct = query.getResultList();
            if (basketCategoryProduct.isEmpty()) {
                return ReasonCode.INVALID_INFORMATION;
            }

            entityManager.getTransaction().begin();
            for (EntityBasketProductCategory entity : basketCategoryProduct) {
                entityManager.remove(entity);
                DBConnection.getInstance().getCache().evict(EntityBasketProductCategory.class, new BasketCategoryProductId(basketId, entity.getCategoryId(), productId));
            }
            entityManager.getTransaction().commit();

        } catch (Exception e) {
            LOGGER.error("Exception in deleteProductFromBasket(): {basketId:" + basketId + " , productId:" + productId + " , userId:" + userId + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return success;
    }

    @Retry(retryAttempts = 5, sleepInterval = 1500)
    public int updateProductStatus(String productId, boolean status) {
        int success = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            Query query = entityManager.createNamedQuery("EntityBasketProductCategory.updateProductStatus");
            query.setParameter("productId", productId);
            query.setParameter("status", status);

            int count = query.executeUpdate();
            entityManager.getTransaction().commit();
            if (count > 0) {
                DBConnection.getInstance().getCache().evict(EntityBasketProductCategory.class);
            }
        } catch (OptimisticLockException e) {
            entityManager.getTransaction().rollback();
            throw e;
        } catch (Exception e) {
            LOGGER.error("Exception in updateProductStatus(): {cartitemId:" + productId + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            success = ReasonCode.EXCEPTION_OCCURED;
        } finally {
            entityManager.close();
        }
        return success;
    }

    @Retry(retryAttempts = 2, sleepInterval = 100)
    public int updateProductStatus(Collection<String> productIds, boolean status) {
        int success = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            int executionCount = 0;
            int count = 0;
            for (String productId : productIds) {
                Query query = entityManager.createNamedQuery("EntityBasketProductCategory.updateProductStatus");
                query.setParameter("status", status);
                query.setParameter("productId", productId);
                count += query.executeUpdate();
                executionCount++;
                if ((executionCount % DBConnection.BATCH_SIZE) == 0) {
                    entityManager.getTransaction().commit();
                    entityManager.clear();
                    entityManager.getTransaction().begin();
                }
            }
            entityManager.getTransaction().commit();
            if (count > 0) {
                DBConnection.getInstance().getCache().evict(EntityBasketProductCategory.class);
                DBConnection.getInstance().getQueryCache().evictAll(EntityBasketProductCategory.class);
            }
        } catch (OptimisticLockException e) {
            entityManager.getTransaction().rollback();
            throw e;
        } catch (Exception e) {
            LOGGER.error("Exception in updateProductStatus(): {productIds:" + productIds + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            success = ReasonCode.EXCEPTION_OCCURED;
        } finally {
            entityManager.close();
        }
        return success;
    }

    @Override
    public String getBasketIdByUserIdAndProductId(Long userId, String productId) {
        String basketId = null;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            Query query = entityManager.createNamedQuery("EntityBasketProductCategory.getBasketIdByUserIdAndProductId", String.class);
            query.setParameter("userId", userId);
            query.setParameter("productId", productId);
            try {
                List<String> resultList = query.setMaxResults(1).getResultList();
                if (!resultList.isEmpty()) {
                    basketId = resultList.get(0);
                }
            } catch (Exception e) {
                LOGGER.error("Exception in getBasketIdByUserIdAndProductId(): {userId:" + userId + ", productId:" + productId + "}", e);
            }
        } finally {
            entityManager.close();
        }
        return basketId;
    }
}
