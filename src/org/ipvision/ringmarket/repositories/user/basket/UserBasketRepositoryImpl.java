/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user.basket;

import org.ipvision.ringmarket.repositories.user.basket.UserBasketRepository;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.UserTypeConstants;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.user.basket.EntityUserBasket;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class UserBasketRepositoryImpl implements UserBasketRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserBasketRepositoryImpl.class);

    @Override
    public int addBasket(EntityUserBasket basket) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode = ReasonCode.NONE;
        try {
            try {
                entityManager.getTransaction().begin();
                entityManager.persist(basket);
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                LOGGER.error("Exception in creation basket for {userId:" + basket.getUserId() + "}", e);
                reasonCode = ReasonCode.OPERATION_FAILED;
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public ListReturnDTO<EntityUserBasket> getCelebrityBaskets(String pivotId, int limit, Scroll scroll) {
        return getBasketList(pivotId, limit, scroll, UserTypeConstants.CELEBRITY);
    }

    private ListReturnDTO<EntityUserBasket> getBasketList(String pivotId, int limit, Scroll scroll, int type) {
        ListReturnDTO<EntityUserBasket> listReturnDTO = new ListReturnDTO<>();

        List<EntityUserBasket> userBaskets = null;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            if (pivotId == null) {
                pivotId = "";
            }
            try {
                Query query;
                if (scroll == Scroll.UP) {
                    query = entityManager.createNamedQuery("getUpwardBasketsByUserType", EntityUserBasket.class);
                } else {
                    query = entityManager.createNamedQuery("getDownwardBasketsByUserType", EntityUserBasket.class);
                }
                query.setParameter("ownerType", type);
                query.setParameter("pivotId", pivotId);
                userBaskets = query.setMaxResults(limit).getResultList();
                if (userBaskets.isEmpty()) {
                    if (pivotId.isEmpty()) {
                        listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                    } else {
                        listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                    }
                } else {
                    if (scroll == Scroll.UP) {
                        Collections.reverse(userBaskets);
                    }
                    listReturnDTO.setList(userBaskets);
                }
            } catch (Exception e) {
                LOGGER.error("Exception in getUserBasket(): {pivotId:" + pivotId + ", type:" + type + "}", e);
                listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
            }

        } finally {
            entityManager.close();
        }
        return listReturnDTO;
    }

    /**
     *
     * @param userId
     * @return
     */
    @Override
    public ListReturnDTO<EntityUserBasket> getUserBaskets(Long userId) {
        ListReturnDTO<EntityUserBasket> listReturnDTO = new ListReturnDTO<>();

        List<EntityUserBasket> userBaskets;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            Query query;
            query = entityManager.createNamedQuery("EntityUserBasket.getUserBaskets", EntityUserBasket.class);
            query.setParameter("userId", userId);
            userBaskets = query.getResultList();
            if (userBaskets.isEmpty()) {
                listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
            } else {
                listReturnDTO.setList(userBaskets);
            }

        } catch (Exception e) {
            LOGGER.error("Exception in getUserBasket(): {userId:" + userId + "}", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        } finally {
            entityManager.close();
        }
        return listReturnDTO;
    }

    private boolean updateUserBasket(EntityManager entityManager, EntityUserBasket basket) {
        boolean success = false;
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(basket);
            entityManager.getTransaction().commit();
            success = true;
        } catch (Exception e) {
            LOGGER.error("Exception in updateUserBasket(): {basketId:" + basket.getId() + "}", e);
            entityManager.getTransaction().rollback();
        }
        return success;
    }

    private EntityUserBasket findBasket(EntityManager entityManager, String basketId) {
        return entityManager.find(EntityUserBasket.class, basketId);
    }

    @Override
    public EntityUserBasket findBasket(String basketId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityUserBasket findBasket;
        try {
            findBasket = findBasket(entityManager, basketId);
        } finally {
            entityManager.close();
        }
        return findBasket;
    }

    @Override
    public boolean updateUserBasket(String basketId, String basketName, String image, Integer imageHeight, Integer imageWith, Integer ownerType) {
        boolean success = false;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityUserBasket findBasket = findBasket(entityManager, basketId);
            if (findBasket != null) {
                if (basketName != null && !basketName.isEmpty()) {
                    findBasket.setName(basketName);
                }
                if (image != null && !image.isEmpty()) {
                    findBasket.setImageUrl(image);
                }
                if (imageHeight != null) {
                    findBasket.setImageHeight(imageHeight);
                }
                if (imageWith != null) {
                    findBasket.setImageWidth(imageWith);
                }

                if (ownerType != null) {
                    findBasket.setOwnerType(ownerType);
                }

                success = updateUserBasket(entityManager, findBasket);
            }
        } finally {
            entityManager.close();
        }
        return success;
    }

    @Override
    public boolean removeBasket(String basketId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean success = false;
        try {
            entityManager.getTransaction().begin();
            EntityUserBasket basket = entityManager.find(EntityUserBasket.class, basketId);
            entityManager.remove(basket);
            entityManager.getTransaction().commit();
            success = true;
        } catch (Exception e) {
            LOGGER.error("Exception in removeBasket(): {basketId:" + basketId + "}", e);
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return success;
    }
}
