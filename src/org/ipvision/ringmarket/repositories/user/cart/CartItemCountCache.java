/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user.cart;

import java.util.Objects;
import java.util.function.Supplier;
import org.ipvision.ringmarket.cache.redis.CacheTransactions;
import org.ipvision.ringmarket.cache.redis.RedisClusterHelper;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class CartItemCountCache {

    private static final String KEY_PREFIX = RedisClusterHelper.getInstance().getCachePrefix() + "." + "cartItemCount:";

    private final CacheTransactions cacheTransactions;

    private static final Logger LOGGER = LoggerFactory.getLogger(CartItemCountCache.class);

    public CartItemCountCache(CacheTransactions cacheTransactions) {
        this.cacheTransactions = Objects.requireNonNull(cacheTransactions, "Probided cache transaction is  null");
    }
    
    public Long getItemCount(Long userId) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - {method: getItemCount , userId: " + userId + "}");
        }
        Long itemCount = null;
        if (cacheTransactions.isCacheEnable()) {
            try {
                String value = this.cacheTransactions.getCacheString(KEY_PREFIX + userId);
                if (value != null) {
                    itemCount = Long.parseLong(value);
                }
            } catch (Exception e) {
                LOGGER.error("Error in getItemCount(): {userId:" + userId + "}", e);
            }
        }
        return itemCount;
    }

    public int putItemCount(Long userId, Long itemCount) {
        int reasonCode = ReasonCode.NONE;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - {method: putItemCount , userId: " + userId + ", itemCount:" + itemCount + "}");
        }
        if (cacheTransactions.isCacheEnable()) {
            try {
                this.cacheTransactions.setCacheString(KEY_PREFIX + userId, itemCount.toString());
            } catch (Exception e) {
                LOGGER.error("Error in putItemCount(): {userId:" + userId + "}", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
            }
        }
        return reasonCode;
    }

    public int increaseItemCount(Long userId, Long itemCount) {
        int reasonCode = ReasonCode.NONE;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - {method: increaseItemCount , userId: " + userId + ", itemCount:" + itemCount + "}");
        }
        if (cacheTransactions.isCacheEnable()) {
            try {
                String value = this.cacheTransactions.getCacheString(KEY_PREFIX + userId);
                if (value != null && !value.isEmpty()) {
                    itemCount += Long.parseLong(value);
                }
                this.cacheTransactions.setCacheString(KEY_PREFIX + userId, itemCount.toString());
            } catch (Exception e) {
                LOGGER.error("Error in increaseItemCount(): {userId:" + userId + "}", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
            }
        }
        return reasonCode;
    }

    public int decreaseItemCount(Long userId, Long itemCount) {
        int reasonCode = ReasonCode.NONE;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - {method: decreaseItemCount , userId: " + userId + ", itemCount:" + itemCount + "}");
        }
        if (cacheTransactions.isCacheEnable()) {
            try {
                String value = this.cacheTransactions.getCacheString(KEY_PREFIX + userId);
                if (value != null && !value.isEmpty()) {
                    long longValue = Long.parseLong(value);
                    if ((longValue - itemCount) > 0) {
                        itemCount = longValue - itemCount;
                    } else {
                        itemCount = 0l;
                    }
                }
                this.cacheTransactions.setCacheString(KEY_PREFIX + userId, itemCount.toString());
            } catch (Exception e) {
                LOGGER.error("Error in decreaseItemCount(): {userId:" + userId + "}", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
            }
        }
        return reasonCode;
    }

    public int removeItemCount(Long userId) {
        int reasonCode = ReasonCode.NONE;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - {method: removeItemCount , userId: " + userId + "}");
        }
        if (cacheTransactions.isCacheEnable()) {
            try {
                this.cacheTransactions.deleteCache(KEY_PREFIX + userId);
            } catch (Exception e) {
                LOGGER.error("Error in removeItemCount(): {userId:" + userId + "}", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
            }
        }
        return reasonCode;
    }

}
