/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user.cart;

import java.util.List;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;

/**
 *
 * @author saikat
 */
public interface CartRepository {

    int addCartItem(String productId, Long userId, int quantity);

    boolean deleteCartItems(Long userId, List<String> cartItemIds);

    PivotedListReturnDTO<EntityCartItem, String> getUserCartItems(Long userId, String pivotId, Scroll scroll, int limit);
    PivotedListReturnDTO<EntityCartItem,String> getCartItems(Long userId, Long shopId, String pivotId, Scroll scroll, int limit);

    int updateCartItemQuantity(String cartItemId, int quantity);
    
    ListReturnDTO<EntityCartItem> getCartItems(List<String> cartItemIds);
    
    ObjectReturnDTO<String> orderProductsFromCart(Long activistId , Long userId, List<String> cartItemIds , Shipment shipment, String addressId , Byte paymentType);
    
    ObjectReturnDTO<Long> cartItemsCount(long userId);
    ObjectReturnDTO<Long> userCount();
}
