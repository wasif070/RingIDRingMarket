/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user.cart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import org.apache.openjpa.kernel.DataCacheRetrieveMode;
import org.apache.openjpa.persistence.OpenJPAQuery;
import org.ipvision.ringmarket.cache.redis.CacheTransactionsImpl;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.order.OrderRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductStockRepositoryImpl;
import org.ipvision.ringmarket.retryutil.Retry;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.ipvision.ringmarket.utils.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class CartItemRepositoryImpl implements CartRepository {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CartItemRepositoryImpl.class);
    private ProductStockRepositoryImpl productStockRepositoryImpl = new ProductStockRepositoryImpl();
    private final CartItemCountCache itemCountCache;
    private ProductRepositoryImpl productRepository = new ProductRepositoryImpl();
    
    public CartItemRepositoryImpl() {
        this.itemCountCache = new CartItemCountCache(new CacheTransactionsImpl());
    }
    
    @Override
    public int addCartItem(String productId, Long userId, int quantity) {
        int reasonCode;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProduct productById = productStockRepositoryImpl.getProductById(entityManager, productId);
            if (productById != null) {
                
                Query query = entityManager.createNamedQuery("getCartItemByUserIdAndProductId", EntityCartItem.class);
                query.setParameter("userId", userId);
                query.setParameter("productId", productId);
                List<EntityCartItem> cartItems = query.getResultList();
                EntityCartItem cartItem;
                
                if (!cartItems.isEmpty()) {
                    cartItem = cartItems.get(0);
                    cartItem.setQuantity(cartItem.getQuantity() + quantity);
                    reasonCode = productStockRepositoryImpl.stockCheck(entityManager, productId, cartItem.getQuantity());
                    if (reasonCode == ReasonCode.NONE) {
                        reasonCode = updateCartItem(entityManager, cartItem);
                    }
                } else {
                    reasonCode = productStockRepositoryImpl.stockCheck(entityManager, productId, quantity);
                    if (reasonCode == ReasonCode.NONE) {
                        cartItem = new EntityCartItem();
                        cartItem.setId(TimeUUID.timeBased().toString());
                        cartItem.setProductId(productId);
                        cartItem.setPrice(productById.getPrice());
                        cartItem.setProductName(productById.getName());
                        cartItem.setProductImage(productById.getImageUrl());
                        cartItem.setQuantity(quantity);
                        cartItem.setShopId(productById.getShopId());
                        cartItem.setShopName(productById.getShopName());
                        cartItem.setCurrecyCode(productById.getCurrencyCode());
                        cartItem.setUserId(userId);
                        cartItem.setShipmentPrice(0d);
                        cartItem.setCreationTIme(System.currentTimeMillis());
                        
                        reasonCode = addToCart(entityManager, cartItem);
                    }
                }
            } else {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }
    
    private int addToCart(EntityManager entityManager, EntityCartItem cartItem) {
        int reasonCode = ReasonCode.NONE;
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(cartItem);
            entityManager.getTransaction().commit();
            reasonCode = itemCountCache.increaseItemCount(cartItem.getUserId(), 1l);
            if (reasonCode != ReasonCode.NONE) {
                entityManager.getTransaction().rollback();
            }
        } catch (Exception e) {
            LOGGER.error("Exception in addToCart(): {userId:" + cartItem.getUserId() + " , productId:" + cartItem.getProductId() + "}", e);
            entityManager.getTransaction().rollback();
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }
    
    @Override
    public PivotedListReturnDTO<EntityCartItem, String> getUserCartItems(Long userId, String pivotId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            PivotedListReturnDTO<EntityCartItem, String> items = getUserCartItems(entityManager, userId, pivotId, scroll, limit);
            return items;
            
        } finally {
            entityManager.close();
        }
    }
    
    @Override
    public PivotedListReturnDTO<EntityCartItem, String> getCartItems(Long userId, Long shopId, String pivotId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            PivotedListReturnDTO<EntityCartItem, String> items = getCartItems(entityManager, userId, shopId, pivotId, scroll, limit);
            return items;
            
        } finally {
            entityManager.close();
        }
    }
    
    private PivotedListReturnDTO<EntityCartItem, String> getUserCartItems(EntityManager entityManager, Long userId, String pivotId, Scroll scroll, int limit) {
        PivotedListReturnDTO<EntityCartItem, String> listReturnDTO = new PivotedListReturnDTO<>();
        boolean isEmptyPivot = false;
        if (pivotId == null) {
            pivotId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }
        List<EntityCartItem> cartItems = new ArrayList<>();
        try {
            Query query;
            if (scroll == Scroll.UP) {
                query = entityManager.createNamedQuery("EntityCartItem.getUpwardCartItems");
            } else {
                query = entityManager.createNamedQuery("EntityCartItem.getDownwardCartItems");
            }
            query.setParameter("userId", userId);
            query.setParameter("pivotId", pivotId);
            List<String> cartItemIds = query.setMaxResults(limit).getResultList();
            
            for (String cartItemId : cartItemIds) {
                EntityCartItem findCartById = findCartById(entityManager, cartItemId);
                if (findCartById != null) {
                    fillCartItemProperty(entityManager, findCartById);
                    cartItems.add(findCartById);
                } else {
                    LOGGER.error("No EntityCartItem found for {id: " + cartItemId + "}");
                }
            }
            if (cartItems.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }
            } else {
                pivotId = cartItems.get(cartItems.size() - 1).getId();
//                cartItems = new LinkedList<>();
//                for (String cartItemId : cartItemIds) {
//                    EntityCartItem findCartById = findCartById(entityManager, cartItemId);
//                    if (findCartById != null) {
//                        fillCartItemProperty(entityManager, findCartById);
//                        cartItems.add(findCartById);
//                    } else {
//                        LOGGER.error("No EntityCartItem found for {id: " + cartItemId + "}");
//                    }
//                }
                listReturnDTO.setList(cartItems);
            }
            
        } catch (Exception e) {
            LOGGER.error("Exception in getUserCartItems(): {userId:" + userId + " , pivotId:" + pivotId + ", scroll:" + scroll + " , limit:" + limit + "}", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        listReturnDTO.setPivot(pivotId);
        return listReturnDTO;
    }
    
    private PivotedListReturnDTO<EntityCartItem, String> getCartItems(EntityManager entityManager, Long userId, Long shopId, String pivotId, Scroll scroll, int limit) {
        PivotedListReturnDTO<EntityCartItem, String> listReturnDTO = new PivotedListReturnDTO<>();
        boolean isEmptyPivot = false;
        if (pivotId == null) {
            pivotId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }
        List<EntityCartItem> cartItems;
        try {
            String sql = "select c from EntityCartItem c where ";
            
            if (userId != null) {
                sql += "c.userId= " + userId + " and  ";
            }
            if (shopId != null) {
                sql += "c.shopId= " + shopId + " and  ";
            }
            
            if (scroll == Scroll.DOWN) {
                sql += "c.id >:pivotId order by c.id";
            } else {
                sql += "c.id <:pivotId order by c.id desc";
            }
            
            Query query = entityManager.createQuery(sql, EntityCartItem.class);
            query.setParameter("pivotId", pivotId);
            
            cartItems = query.setMaxResults(limit).getResultList();
            fillCartItemsProperty(entityManager, cartItems);
            if (cartItems.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }
            } else {
                pivotId = cartItems.get(cartItems.size() - 1).getId();
                listReturnDTO.setList(cartItems);
            }
            listReturnDTO.setPivot(pivotId);
        } catch (Exception e) {
            LOGGER.error("Exception in getCartItems(): {shopId:" + shopId + "userId:" + userId + " , pivotId:" + pivotId + ", scroll:" + scroll + " , limit:" + limit + "}", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        
        return listReturnDTO;
    }
    
    @Override
    public ListReturnDTO<EntityCartItem> getCartItems(List<String> cartItemIds) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityCartItem> listReturnDTO = getCartItems(entityManager, cartItemIds);
            return listReturnDTO;
            
        } finally {
            entityManager.close();
        }
    }
    
    protected ListReturnDTO<EntityCartItem> getCartItems(EntityManager entityManager, List<String> cartItemIds) {
        ListReturnDTO<EntityCartItem> listReturnDTO = new ListReturnDTO<>();
        List<EntityCartItem> cartItems;
        try {
            Query query = entityManager.createNamedQuery("EntityCartItem.getCartItemByIds", EntityCartItem.class);
            query.setParameter("idList", cartItemIds);
            cartItems = query.getResultList();
            fillCartItemsProperty(entityManager, cartItems);
            if (cartItems.isEmpty()) {
                listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
            } else if (cartItemIds.size() != cartItems.size()) {
                listReturnDTO.setReasonCode(ReasonCode.INVALID_INFORMATION);
            } else {
                listReturnDTO.setList(cartItems);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getCartItems(): {cartItemIds:" + cartItemIds + "}", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        
        return listReturnDTO;
    }
    
    protected EntityCartItem findCartById(EntityManager entityManager, String id) {
        return entityManager.find(EntityCartItem.class, id);
    }
    
    @Retry(retryAttempts = 5, sleepInterval = 1500)
    private int updateCartItem(EntityManager entityManager, EntityCartItem item) {
        int success = ReasonCode.NONE;
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(item);
            entityManager.getTransaction().commit();
            DBConnection.getInstance().getCache().evict(EntityCartItem.class, item.getId());
        } catch (OptimisticLockException e) {
            entityManager.getTransaction().rollback();
            throw e;
        } catch (Exception e) {
            LOGGER.error("Exception in updateCartItemQuantity(): {cartitemId:" + item.getId() + " ,quantity:" + item.getQuantity() + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            success = ReasonCode.EXCEPTION_OCCURED;
        }
        return success;
    }
    
    @Retry(retryAttempts = 5, sleepInterval = 1500)
    public int updateProductStatus(String productId, boolean status) {
        int success = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            Query query = entityManager.createNamedQuery("EntityCartItem.updateProductStatus");
            query.setParameter("productId", productId);
            query.setParameter("status", status);
            
            int count = query.executeUpdate();
            entityManager.getTransaction().commit();
            
            if (count > 0) {
                DBConnection.getInstance().getCache().evict(EntityCartItem.class);
            }
        } catch (OptimisticLockException e) {
            entityManager.getTransaction().rollback();
            throw e;
        } catch (Exception e) {
            LOGGER.error("Exception in updateProductStatus(): {cartitemId:" + productId + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            success = ReasonCode.EXCEPTION_OCCURED;
        } finally {
            entityManager.close();
        }
        return success;
    }
    
    @Override
    public int updateCartItemQuantity(String cartItemId, int quantity) {
        int success = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityCartItem item = findCartById(entityManager, cartItemId);
            if (item != null) {
                productStockRepositoryImpl = (ProductStockRepositoryImpl) MarketRepository.INSTANCE.getProductStockRepository();
                success = productStockRepositoryImpl.stockCheck(entityManager, item.getProductId(), quantity);
                if (success == ReasonCode.NONE) {
                    item.setQuantity(quantity);
                    success = updateCartItem(entityManager, item);
                }
            } else {
                success = ReasonCode.INVALID_INFORMATION;
            }
        } finally {
            entityManager.close();
        }
        return success;
    }
    
    private int deleteCartItems(EntityManager entityManager, Long userId, List<String> idList) {
        
        String deleteIds = Utility.stringListToString(idList);
        String queryString = "delete from user_cart_item where user_id=" + userId + " and id in (" + deleteIds + ")";
        
        Query query = entityManager.createNativeQuery(queryString);
        int result = query.executeUpdate();
        if (result > 0) {
            LOGGER.info(queryString, deleteIds);
            int reasonCode = this.itemCountCache.decreaseItemCount(userId, new Long(result));
            if (reasonCode != ReasonCode.NONE) {
                result = 0;
            } else {
                DBConnection.getInstance().getCache().evictAll(EntityCartItem.class, idList);
                DBConnection.getInstance().getQueryCache().evictAll(EntityCartItem.class);
            }
        }
        return result;
    }

    /**
     *
     * @param userId
     * @param cartItemIds
     * @return
     */
    @Override
    public boolean deleteCartItems(Long userId, List<String> cartItemIds) {
        boolean success = false;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            try {
                if (deleteCartItems(entityManager, userId, cartItemIds) == cartItemIds.size()) {
                    entityManager.getTransaction().commit();
                    success = true;
                } else {
                    entityManager.getTransaction().rollback();
                }
                
            } catch (Exception e) {
                LOGGER.error("Exception in deleteCartItems(): {userId:" + userId + " , cartItems:" + cartItemIds.toString() + "}", e);
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return success;
    }

    /**
     *
     * @param activistId
     * @param userId
     * @param cartItemIds
     * @param shipment
     * @param addressId
     * @param paymentType
     * @return
     */
    @Override
    public ObjectReturnDTO<String> orderProductsFromCart(Long activistId, Long userId, List<String> cartItemIds, Shipment shipment, String addressId, Byte paymentType) {
        ObjectReturnDTO<String> objectReturnDTO = new ObjectReturnDTO<>();
        ListReturnDTO<EntityCartItem> cartItems = getCartItems(cartItemIds);
        
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            if (cartItems.getReasonCode() == ReasonCode.NONE) {
                
                OrderRepositoryImpl orderRepositoryImpl = new OrderRepositoryImpl();
                
                int itemStockCheck = orderRepositoryImpl.itemStockCheck(entityManager, cartItems.getList());
                
                if (itemStockCheck == ReasonCode.NONE) {
                    
                    try {
                        entityManager.getTransaction().begin();
                        
                        String invoiceId = orderRepositoryImpl.orderCartItemsAndGetInvoiceId(entityManager, activistId, cartItems.getList(), shipment, addressId, paymentType);
                        
                        if (invoiceId != null) {
                            int deleteCartItems = deleteCartItems(entityManager, userId, cartItemIds);
                            if (deleteCartItems == cartItemIds.size()) {
                                objectReturnDTO.setValue(invoiceId);
                                entityManager.getTransaction().commit();
                            } else {
                                entityManager.getTransaction().rollback();
                                objectReturnDTO.setReasonCode(ReasonCode.OPERATION_FAILED);
                            }
                        } else {
                            entityManager.getTransaction().rollback();
                            objectReturnDTO.setReasonCode(ReasonCode.OPERATION_FAILED);
                        }
                    } catch (Exception e) {
                        LOGGER.error("Exception in orderProductsFromCart() : {userId:" + userId + ", cartItemIds:" + cartItemIds.toString() + "}", e);
                        if (entityManager.getTransaction().isActive()) {
                            entityManager.getTransaction().rollback();
                        }
                        objectReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
                    }
                } else {
                    objectReturnDTO.setReasonCode(itemStockCheck);
                }
                
            } else {
                objectReturnDTO.setReasonCode(cartItems.getReasonCode());
            }
        } finally {
            entityManager.close();
        }
        return objectReturnDTO;
    }
    
    @Override
    public ObjectReturnDTO<Long> cartItemsCount(long userId) {
        ObjectReturnDTO<Long> returnDTO = new ObjectReturnDTO<>();
        Long itemCount = this.itemCountCache.getItemCount(userId);
        if (itemCount == null) {
            EntityManager entityManager = DBConnection.getInstance().getEntityManager();
            try {
                try {
                    OpenJPAQuery query = (OpenJPAQuery) entityManager.createNamedQuery("EntityCartItem.cartItemsCount", Long.class);
                    query.setParameter("userId", userId);
//            query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
                    query.getFetchPlan().setCacheRetrieveMode(DataCacheRetrieveMode.BYPASS);
                    
                    List<Long> withTypedQuery = query.setMaxResults(1).getResultList();
                    Long count = withTypedQuery.get(0);
//            Long count = query.getSingleResult();
                    returnDTO.setValue(count);
                    this.itemCountCache.putItemCount(userId, count);
                } catch (Exception e) {
                    LOGGER.error("Exception in cartItemsCount() : {userId:" + userId + "}", e);
                    returnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
                }
            } finally {
                entityManager.close();
            }
        } else {
            returnDTO.setValue(itemCount);
        }
        
        return returnDTO;
    }
    
    @Override
    public ObjectReturnDTO<Long> userCount() {
        ObjectReturnDTO<Long> returnDTO = new ObjectReturnDTO<>();
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                Query query = entityManager.createNamedQuery("EntityCartItem.userCount", Long.class);
                Long count = (Long) query.getSingleResult();
                returnDTO.setValue(count);
            } catch (Exception e) {
                LOGGER.error("Exception in userCount() : ", e);
                returnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
            }
        } finally {
            entityManager.close();
        }
        return returnDTO;
    }

//    public int updateShopName(EntityManager entityManager, long shopId, String shopName) {
//        Query query = entityManager.createNamedQuery("EntityCartItem.updateShopName");
//        query.setParameter("name", shopName);
//        query.setParameter("shopId", shopId);
//        return query.executeUpdate();
//    }
//    
//    public int updateProductImageUrl(EntityManager entityManager, String productId, String imageUrl) {
//        Query query = entityManager.createNamedQuery("EntityCartItem.updateProductImage");
//        query.setParameter("url", imageUrl);
//        query.setParameter("productId", productId);
//        return query.executeUpdate();
//    }
    public void fillCartItemProperty(EntityManager entityManager, EntityCartItem cartItem) {
        EntityProduct product = productRepository.getProductById(entityManager, cartItem.getProductId());
        
        cartItem.setProductName(product.getName());
        cartItem.setProductImage(product.getImageUrl());
        
        cartItem.setShopName(product.getShopName());
        
        cartItem.setIsDiscounted(product.isDiscounted());
        cartItem.setCurrecyCode(product.getCurrencyCode());
        cartItem.setPrice(product.getPrice());
        cartItem.setDiscount(product.getDiscount());
        cartItem.setDiscountUnit(product.getDiscountUnit());
    }
    
    public void fillCartItemsProperty(EntityManager entityManager, List<EntityCartItem> cartItems) {
        cartItems.stream().forEach((cartItem) -> {
            fillCartItemProperty(entityManager, cartItem);
        });
    }
}
