/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user;

import java.util.List;
import org.ipvision.ringmarket.constants.RingmarketRole;
import org.ipvision.ringmarket.entities.EntityRingMarketUser;
import org.ipvision.ringmarket.entities.EntityUserRole;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface UserRoleRepository extends IRepository<EntityUserRole>{
    public boolean addUser(long id, RingmarketRole role);
    public boolean removeUser(long id, RingmarketRole role);
    public List<EntityUserRole> getUsersByRole(RingmarketRole role) ;
    public EntityRingMarketUser getAgent(double lat, double lon);
    
}
