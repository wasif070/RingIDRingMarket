/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user;

import org.ipvision.ringmarket.entities.EntityRingMarketUser;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface MarketUserRepository extends IRepository<EntityRingMarketUser>{
    public void addUser(long id, float lat, float lon) ;
    public void addUser(long id, float lat, float lon, String dialingCode, String phone, 
            String address, String name, String abtMe, String photoUrl);
    public void addUser(EntityRingMarketUser entityRingMarketUser);
    public void updateUserLocation(long id, float lat, float lon);
    public void updateUser(EntityRingMarketUser entityRingMarketUser);
    public void removeUser(long id);
    public EntityRingMarketUser getUser(long id);
    
}
