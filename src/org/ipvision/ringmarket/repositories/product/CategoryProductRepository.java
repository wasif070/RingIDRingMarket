/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.entities.category.EntityCategoryProduct;
import org.ipvision.ringmarket.repositories.IRepository;
import org.ipvision.ringmarket.utils.ListReturnDTO;

/**
 *
 * @author saikat
 */
public interface CategoryProductRepository extends IRepository<EntityCategoryProduct> {

    boolean addCategoryProduct(int catId, String productId, Long shopId);

    boolean addMultipleCategoriesProduct(Set<Integer> catList, String productId, Long shopId);

    ListReturnDTO<EntityCategory> getCategoriesByProduct(String productId);

    ListReturnDTO<EntityProduct> getCategoryWiseProduct(int categoryId, String pivotProductId, Long shopId, Scroll scroll, int limit, ProductStatus status);

    ListReturnDTO<EntityProduct> getCategoryWiseProductBasedLocation(int categoryId, String pivotProductId, float lat, float lon, int radius, Scroll scroll, int limit);

    ListReturnDTO<EntityProduct> getCategoryWiseProductInPriceRange(int categoryId, String pivotProductId, double minPrice, double maxPrice, Scroll scroll, int limit);

    boolean removeCategoryProduct(int catId, String productId, Long shopId);

    boolean removeProduct(String productId);

    EntityCategoryProduct findById(int catId, String productId, Long shopId);
    
    List<EntityCategory> getStoresCategoriesById(Long shopId, int start, int limit, Integer categoryId);

}
