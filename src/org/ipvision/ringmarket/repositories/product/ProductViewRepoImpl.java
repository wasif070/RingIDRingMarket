package org.ipvision.ringmarket.repositories.product;

import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import org.apache.openjpa.kernel.DataCacheStoreMode;
import org.apache.openjpa.persistence.OpenJPAQuery;
import org.ipvision.ringmarket.cache.redis.CacheTransactionsImpl;
import org.ipvision.ringmarket.cache.redis.RedisClusterHelper;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.id.ProductViewId;
import org.ipvision.ringmarket.entities.product.EntityProductView;
import org.ipvision.ringmarket.repositories.buyer.ProductViewRepository;
import org.ipvision.ringmarket.retryutil.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class ProductViewRepoImpl implements ProductViewRepository {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductViewRepoImpl.class);
    private final ProductViewCache productViewCache;
    
    public ProductViewRepoImpl() {
        this.productViewCache = new ProductViewCache(new CacheTransactionsImpl());
    }
    
    @Override
    public boolean addToViewList(long buyerId, String productId) {
        boolean success = true;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                entityManager.getTransaction().begin();
                addToViewList(entityManager, buyerId, productId);
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                success = false;
                LOGGER.error("Exception in addToViewList() : {buyerId:" + buyerId + ", productId:" + productId + "}", e);
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        } finally {
            entityManager.close();
        }
        return success;
    }
    
    protected boolean addToViewList(EntityProductView entityProductView) {
        boolean success = true;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                entityManager.getTransaction().begin();
                addToViewList(entityManager, entityProductView);
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                success = false;
                LOGGER.error("Exception in addToViewList() : {buyerId:" + entityProductView.getBuyerId() + ", productId:" + entityProductView.getProductId() + "}", e);
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        } finally {
            entityManager.close();
        }
        return success;
    }
    
    @Override
    public boolean findViewedByBuyer(long buyerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            boolean susc = findViewedByBuyer(entityManager, buyerId, productId);
            return susc;
            
        } finally {
            entityManager.close();
        }
    }
    
    protected void addToViewList(EntityManager entityManager, long buyerId, String productId) {
        EntityProductView entityProductView = new EntityProductView();
        entityProductView.setBuyerId(buyerId);
        entityProductView.setProductId(productId);
        entityProductView.setViewCount(1);
        addToViewList(entityManager, entityProductView);
    }
    
    private void addToViewList(EntityManager entityManager, EntityProductView entityProductView) {
        entityManager.persist(entityProductView);
    }
    
    private EntityProductView find(EntityManager em, Long buyerId, String productId) {
        return em.find(EntityProductView.class, new ProductViewId(buyerId, productId));
    }
    
    protected boolean findViewedByBuyer(EntityManager entityManager, long buyerId, String productId) {
        Query query = entityManager.createNamedQuery("getIsAlreadyViewed");
        query.setParameter("productId", productId);
        query.setParameter("buyerId", buyerId);
        try {
            return Integer.valueOf(query.getSingleResult().toString()) == 1;
        } catch (Exception ex) {
            return false;
        }
//        List<EntityProductView> myList = query.getResultList();
//        if (myList == null || myList.isEmpty()) {
//            return null;
//        }
//        return myList.get(0);
    }
    
    @Override
    public int incrementViewCount(Long buyerId, String productId) {
        int reasonCode = ReasonCode.NO_DATA_FOUND;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            
            OpenJPAQuery query = (OpenJPAQuery) entityManager.createNamedQuery("EntityProductView.incViewCount");

//        query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
//        query.setHint("javax.persistence.cache.storeMode", CacheStoreMode.BYPASS);
            query.getFetchPlan().setQueryResultCacheEnabled(false);
            query.getFetchPlan().setCacheStoreMode(DataCacheStoreMode.BYPASS);
            
            query.setParameter("buyerId", buyerId);
            query.setParameter("productId", productId);
            try {
                entityManager.getTransaction().begin();
                if (query.executeUpdate() > 0) {
                    reasonCode = ReasonCode.NONE;
                }
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                LOGGER.error("Exception in incrementViewCount() : {buyerId:" + buyerId + ", productId:" + productId + "}", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
            
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }
    
    @Override
    public void addToProductViewCache(Long userId, String productId) {
        int addProductView = this.productViewCache.addProductView(userId, productId);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("product view cache added with reason code = " + addProductView + " for {userId:" + userId + ", productId: " + productId + "}");
        }
    }
    
    @Override
    public int syncProductView() {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            LOGGER.info("Syncing product view");
            int updateProductView = updateProductView(entityManager);
            return updateProductView;
        } finally {
            entityManager.close();
        }
    }
    
    @Retry(retryAttempts = 3)
    private int updateProductView(EntityManager em) {
        int reasonCode = ReasonCode.NONE;
        Set<String> allKeys = RedisClusterHelper.getInstance().getAllKeys(this.productViewCache.getPrefix());
        int cursor = 0;
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        try {
            for (String key : allKeys) {
                String[] split = key.trim().split(":");
                if (split.length > 2) {
                    Long userId = Long.parseLong(split[1]);
                    String productId = split[2];
                    Integer productViewCount = this.productViewCache.productViewCount(userId, productId);
                    if (productViewCount != null) {
                        EntityProductView find = find(em, userId, productId);
                        if (find == null) {
                            find = new EntityProductView();
                            find.setBuyerId(userId);
                            find.setProductId(productId);
                        }
                        find.setViewCount(productViewCount);
                        em.persist(find);
                        cursor++;
                    }
                }
                if (cursor > 0 && cursor % DBConnection.getInstance().BATCH_SIZE == 0) {
                    em.getTransaction().commit();
                    em.clear();
                    em.getTransaction().begin();
                }
            }
            
            em.getTransaction().commit();
        } catch (OptimisticLockException e) {
            LOGGER.error("OptimasticLock Exception. M - " + e.getMessage(), e);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            throw e;
        } catch (Exception e) {
            LOGGER.error("Exception in updateProductView()", e);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        
        return reasonCode;
    }
    
}
