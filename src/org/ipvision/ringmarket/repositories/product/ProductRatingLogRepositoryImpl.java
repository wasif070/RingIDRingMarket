/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProductRatingLog;
import org.ipvision.ringmarket.entities.id.ProductRatingLogId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class ProductRatingLogRepositoryImpl implements ProductRatingLogRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductRatingLogRepositoryImpl.class);

    protected boolean addNewRating(EntityManager entityManager, String productId, long userId, byte rating) throws Exception {
        EntityProductRatingLog historyProductRatingLog = new EntityProductRatingLog();
        historyProductRatingLog.setUserId(userId);
        historyProductRatingLog.setProductId(productId);
        historyProductRatingLog.setRating(rating);
        entityManager.persist(historyProductRatingLog);
        return true;
    }

    protected boolean updateRating(EntityManager entityManager, EntityProductRatingLog historySellerRating) throws Exception {
        entityManager.merge(historySellerRating);
        return true;
    }

    @Override
    public EntityProductRatingLog findRatingByUser(String productId, long userId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProductRatingLog historySellerRating = entityManager.find(EntityProductRatingLog.class, new ProductRatingLogId(productId, userId));
            return historySellerRating;

        } finally {
            entityManager.close();
        }
    }

    protected EntityProductRatingLog findRatingByUser(EntityManager entityManager, String productId, long userId) {
        return entityManager.find(EntityProductRatingLog.class, new ProductRatingLogId(productId, userId));
    }

    protected boolean removeRatingUser(EntityManager entityManager, EntityProductRatingLog ihsr) throws Exception {
        entityManager.remove(ihsr);
        return true;
    }

    @Override
    public Map<Byte, Long> getRatingDetails(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            Query query = entityManager.createNamedQuery("EntityProductRatingLog.getRatingDetails");
            Map<Byte, Long> ratingMap = new LinkedHashMap<>();
            try {
                query.setParameter("productId", productId);
                List<Object[]> resultList = query.getResultList();

                resultList.forEach((objects) -> {
                    ratingMap.put((Byte) objects[0], (Long) objects[1]);
                });
                return ratingMap;

            } catch (Exception e) {
                LOGGER.error("Exception in getRatingDetails() : {productId:" + productId + "}", e);
                return null;
            }
        } finally {
            entityManager.close();
        }

    }

}
