/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.ReportCause;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityReportProduct;
import org.ipvision.ringmarket.repositories.buyer.ReportProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class ReportProduct implements ReportProductRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportProduct.class);

    public boolean addReportProduct(String id, long buyerId, long sellerId, String productId, ReportCause reportCause, String desc) {
        return addReportProduct(id, buyerId, sellerId, productId, reportCause.ordinal(), desc);
    }

    public boolean addReportProduct(String id, long buyerId, long sellerId, String productId, int reportCause, String desc) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = addReportProduct(entityManager, id, buyerId, sellerId, productId, reportCause, desc);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    public boolean addReportProduct(EntityReportProduct entityReportProduct) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = addReportProduct(entityManager, entityReportProduct);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    protected boolean addReportProduct(EntityManager entityManager, String id, long buyerId, long sellerId, String productId, int reportCause, String desc) {
        EntityReportProduct entityReportProduct = new EntityReportProduct();
        entityReportProduct.setAdditionalDescription(desc);
        entityReportProduct.setBuyerId(buyerId);
        entityReportProduct.setId(id);
        entityReportProduct.setProductId(productId);
        entityReportProduct.setReportCause(reportCause);
        entityReportProduct.setSellerId(sellerId);
        return addReportProduct(entityManager, entityReportProduct);
    }

    protected boolean addReportProduct(EntityManager entityManager, EntityReportProduct entityReportProduct) {
        boolean susc = false;
        try {
            entityManager.persist(entityReportProduct);
            susc = true;
            //update report count to product detail table
//            ProductDetail productDetail = new ProductDetail();
//            susc = productDetail.updateReportCount(entityManager, entityReportProduct.getProductId());
            if (!susc) {
                LOGGER.error("Error in addReportProduct. productId: " + entityReportProduct.getProductId());
                return susc;
            }
        } catch (Exception ex) {
            LOGGER.error("Error in addReportProduct. productId: " + entityReportProduct.getProductId(), ex);
        }
        return susc;
    }

    public boolean isAlreadyReported(long buyerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            boolean susc = getReportedByBuyer(entityManager, buyerId, productId);
            return susc;

        } finally {
            entityManager.close();
        }
    }

    protected boolean getReportedByBuyer(EntityManager entityManager, long buyerId, String productId) {
        Query query = entityManager.createNamedQuery("getIsAlreadyReported");
        query.setParameter("productId", productId);
        query.setParameter("buyerId", buyerId);
        try {
            return Integer.valueOf(query.getSingleResult().toString()) == 1;
        } catch (Exception ex) {
            return false;
        }
//        List<EntityReportProduct> myList = query.getResultList();
//        if (myList == null || myList.isEmpty()) {
//            return null;
//        }
//        return myList.get(0);
    }

}
