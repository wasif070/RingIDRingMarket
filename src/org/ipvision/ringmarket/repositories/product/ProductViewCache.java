/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import org.ipvision.ringmarket.repositories.user.cart.*;
import org.ipvision.ringmarket.cache.redis.CacheTransactions;
import org.ipvision.ringmarket.cache.redis.RedisClusterHelper;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class ProductViewCache {

    private static final String KEY_PREFIX = RedisClusterHelper.getInstance().getCachePrefix() + "." + "productView:";

    private final CacheTransactions cacheTransactions;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductViewCache.class);

    public ProductViewCache(CacheTransactions cacheTransactions) {
        this.cacheTransactions = cacheTransactions;
    }

    public String getPrefix() {
        return KEY_PREFIX;
    }

    public Integer productViewCount(Long userId, String productId) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - {method: getItemCount , userId: " + userId + "}");
        }
        Integer itemCount = null;
        if (cacheTransactions.isCacheEnable()) {
            String key = userId + ":" + productId;
            try {
                String value = this.cacheTransactions.getCacheString(KEY_PREFIX + key);
                if (value != null && !value.isEmpty()) {
                    itemCount = Integer.parseInt(value);
                }
            } catch (Exception e) {
                LOGGER.error("Error in productViewCount(): {userId:" + userId + ", productId:" + "}", e);
            }
        }
        return itemCount;
    }

    public int addProductView(Long userId, String productId) {
        int reasonCode = ReasonCode.NONE;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - {method: addProductView , userId: " + userId + ", productId:" + productId + "}");
        }
        if (cacheTransactions.isCacheEnable()) {
            String key = userId + ":" + productId;
            try {
                String value = this.cacheTransactions.getCacheString(KEY_PREFIX + key);
                int count = 0;
                if (value != null && !value.trim().isEmpty()) {
                    count = Integer.parseInt(value);
                }
                count++;
                this.cacheTransactions.setCacheString(KEY_PREFIX + key, String.valueOf(count));
            } catch (Exception e) {
                LOGGER.error("Error in addProductView(): {userId:" + userId + ", productId:" + productId + "}", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
            }
        }
        return reasonCode;
    }

}
