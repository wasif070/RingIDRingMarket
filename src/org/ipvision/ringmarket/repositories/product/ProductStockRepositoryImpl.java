/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import com.google.gson.Gson;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProductStock;
import org.ipvision.ringmarket.retryutil.Retry;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class ProductStockRepositoryImpl extends ProductRepositoryImpl implements ProductStockRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductStockRepositoryImpl.class);

    @Override
    public int addStock(String productId, Integer quantity) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            int addStock = addStock(entityManager, productId, quantity);
            return addStock;

        } finally {
            entityManager.close();
        }
    }

    void addStock(EntityManager entityManager, EntityProductStock productStock) {
        entityManager.persist(productStock);
    }

    int addStock(EntityManager entityManager, String productId, Integer quantity) {
        int reasonCode = ReasonCode.NONE;
        EntityProductStock entityProductStock = new EntityProductStock();
        entityProductStock.setId(productId);
        entityProductStock.setQuantity(quantity);
        entityProductStock.setUpdateTime(System.currentTimeMillis());
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(entityManager);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Exception in addStock() : {productId: " + productId + ", quantity:" + quantity + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    EntityProductStock findById(EntityManager entityManager, String id) {
        return entityManager.find(EntityProductStock.class, id);
    }

    @Override
    public EntityProductStock getStock(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProductStock findById = findById(entityManager, id);
            return findById;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public int increaseStock(String productId, Integer quantity) {
        int reasonCode = ReasonCode.NO_DATA_FOUND;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProductStock stock = findById(entityManager, productId);

            if (stock != null) {
                stock.setQuantity(stock.getQuantity() + quantity);
                stock.setUpdateTime(System.currentTimeMillis());
                reasonCode = updateStock(entityManager, stock);
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Retry(sleepInterval = 50L)
    int updateStock(EntityManager entityManager, EntityProductStock productStock) {
        int reasonCode = ReasonCode.NONE;
        entityManager.getTransaction().begin();
        try {
            entityManager.merge(productStock);
            entityManager.getTransaction().commit();
            DBConnection.getInstance().getCache().evict(EntityProductStock.class, productStock.getId());
        } catch (OptimisticLockException e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw e;
        } catch (Exception e) {
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            LOGGER.error("Exception in updateStock() : {productStock: " + new Gson().toJson(productStock) + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
        return reasonCode;
    }

//    @Retry(retryAttempts = 2, sleepInterval = 100)
    public int decreaseStock(EntityManager entityManager, Map<String, Integer> productQuantityMap) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - productQuantityMap:" + productQuantityMap);
        }
//        ListReturnDTO<EntityProductStock> productStockByIds = getProductStockByIds(entityManager, productQuantityMap.keySet());
        ListReturnDTO<EntityProductStock> productStockByIds = getProductStockByIds(productQuantityMap.keySet());
        int reasonCode = ReasonCode.NONE;

        List<EntityProductStock> list = productStockByIds.getList();

        int cursor = 0;
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        try {
            for (EntityProductStock stock : list) {
                Integer quantity = productQuantityMap.get(stock.getId());
                if (quantity != null) {
                    if (stock.getQuantity() - quantity >= 0) {
                        stock.setQuantity(stock.getQuantity() - quantity);
                        stock.setUpdateTime(System.currentTimeMillis());
                        entityManager.merge(stock);
                        DBConnection.getInstance().getCache().evict(EntityProductStock.class, stock.getId());
                        cursor++;
                        if (stock.getQuantity() == 0) {
                            boolean updateProductStatus = updateProductStatus(entityManager, stock.getId(), ProductStatus.SOLD_OUT);
                            if (updateProductStatus) {
                                cursor++;
                            } else {
                                reasonCode = ReasonCode.OPERATION_FAILED;
                                break;
                            }
                        }
                    } else {
                        LOGGER.error("Stock underflow. {id: " + stock.getId() + ", quantity: " + stock.getQuantity() + ", decQuantity: " + quantity + "}");
                        reasonCode = ReasonCode.RingMarket.STOCK_UNDERFLOW;
                        break;
                    }
                } else {
                    LOGGER.error("Stock not found. {id: " + stock.getId() + "}");
                    reasonCode = ReasonCode.NO_DATA_FOUND;
                    break;
                }

                if (cursor > 0 && cursor % DBConnection.getInstance().BATCH_SIZE == 0) {
                    entityManager.getTransaction().commit();
                    entityManager.clear();
                    entityManager.getTransaction().begin();
                }

            }
            if (reasonCode != ReasonCode.NONE) {
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            } else {
                entityManager.getTransaction().commit();
            }
        } catch (OptimisticLockException e) {
            LOGGER.error("OptimasticLock Exception. M - " + e.getMessage(), e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw e;
        } catch (Exception e) {
            LOGGER.error("Exception in decreaseStock() {productQuantityMap: " + productQuantityMap.toString() + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }

        return reasonCode;
    }

    @Override
    public int decreaseStock(String productId, Integer quantity) {
        int reasonCode = ReasonCode.NO_DATA_FOUND;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProductStock stock = findById(entityManager, productId);

            if (stock != null) {

                if (stock.getQuantity() - quantity >= 0) {
                    stock.setQuantity(stock.getQuantity() - quantity);
                    stock.setUpdateTime(System.currentTimeMillis());
                    reasonCode = updateStock(entityManager, stock);
                } else {
                    reasonCode = ReasonCode.OPERATION_FAILED;
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Retry()
    public int deleteStockFromDB(String productId) {
        int reasonCode = ReasonCode.NO_DATA_FOUND;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProductStock findById = findById(entityManager, productId);
            if (findById != null) {
                entityManager.getTransaction().begin();
                try {
                    entityManager.remove(findById);
                    entityManager.getTransaction().commit();
                    DBConnection.getInstance().getCache().evict(EntityProductStock.class, productId);
                    reasonCode = ReasonCode.NONE;
                } catch (OptimisticLockException e) {
                    entityManager.getTransaction().rollback();
                    throw e;
                } catch (Exception e) {
                    LOGGER.error("Exception in deleteStockFromDB {productId: " + productId + "}", e);
                    if (entityManager.getTransaction().isActive()) {
                        entityManager.getTransaction().rollback();
                    }
                    reasonCode = ReasonCode.EXCEPTION_OCCURED;
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public ListReturnDTO<EntityProductStock> getProductStockByIds(Collection<String> idList) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityProductStock> productStockByIds = getProductStockByIds(entityManager, idList);
            return productStockByIds;

        } finally {
            entityManager.close();
        }
    }

    ListReturnDTO<EntityProductStock> getProductStockByIds(EntityManager entityManager, Collection<String> idList) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - idList:" + idList);
        }
        ListReturnDTO<EntityProductStock> listReturnDTO = new ListReturnDTO<>();
        try {
//            Query query = entityManager.createNamedQuery("EntityProductStock.getProductsStock", EntityProductStock.class);
//            query.setParameter("idList", idList);
//            List<EntityProductStock> resultList = query.getResultList();
//            listReturnDTO.setList(resultList);
            List<EntityProductStock> entityProductStocks = new LinkedList<>();
            for (String id : idList) {
                EntityProductStock findById = findById(entityManager, id);
                if (findById != null) {
                    entityProductStocks.add(findById);
                }
            }
            listReturnDTO.setList(entityProductStocks);

            if (listReturnDTO.getList().isEmpty()) {
                listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getProductStockByIds() {idList:" + idList.toString() + "}", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    public int stockCheck(EntityManager entityManager, String productId, Integer quantity) {
        int reasonCode = ReasonCode.NONE;
        EntityProductStock stock = findById(entityManager, productId);
        if (stock == null) {
            reasonCode = ReasonCode.NO_DATA_FOUND;
        } else if (stock.getQuantity() == 0) {
            reasonCode = ReasonCode.RingMarket.STOCK_OUT;
        } else if (stock.getQuantity() < quantity) {
            reasonCode = ReasonCode.RingMarket.STOCK_LIMIT_OVER;
        }
        return reasonCode;
    }

}
