/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import org.ipvision.ringmarket.entities.category.EntityCategoryProductCount;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface CategoryProductCountRepository extends IRepository<EntityCategoryProductCount>{

    boolean addCategoryProductCount(int catId, int count);

    boolean decreaseCategoryProductCount(int catId);

    int getCategoryProductCount(int categoryId);

    boolean increaseCategoryProductCount(int catId);

    boolean removeCategoryProduct(int catId, String productId);
    
}
