package org.ipvision.ringmarket.repositories.product;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.id.ProductImageId;
import org.ipvision.ringmarket.entities.order.EntityOrder;
import org.ipvision.ringmarket.entities.order.EntityOrderDetails;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.product.EntityProductImage;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.order.OrderDetailsRepositoryImpl;
import org.ipvision.ringmarket.repositories.order.OrderRepositoryImpl;
import org.ipvision.ringmarket.repositories.user.cart.CartItemRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class ProductImageRepositoryImpl implements ProductImageRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductImageRepositoryImpl.class);

    @Override
    public boolean addImage(String url, int hgt, int wdt, String productId) {
        EntityProductImage entityProductImage = new EntityProductImage(url, hgt, wdt, productId);
        boolean susc = addImage(entityProductImage);
        return susc;
    }

    @Override
    public boolean addImage(EntityProductImage entityProductImage) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();

        boolean success = false;
        try {
            entityManager.getTransaction().begin();
            addImage(entityManager, entityProductImage);
            entityManager.getTransaction().commit();
            success = true;
        } catch (Exception e) {
            LOGGER.error("Error in addImage() : {entityProductImage: " + new Gson().toJson(entityProductImage) + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }

        return success;
    }

    @Override
    public int addImageList(String productId, List<EntityProductImage> entityProductImages) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode;
        try {
            reasonCode = ReasonCode.NONE;
            try {
                entityManager.getTransaction().begin();
                entityProductImages.stream().map((entityProductImage) -> {
                    entityProductImage.setProductId(productId);
                    entityProductImage.setAddedTime(System.currentTimeMillis());
                    return entityProductImage;
                }).forEachOrdered((entityProductImage) -> {
                    addImage(entityManager, entityProductImage);
                });
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                reasonCode = ReasonCode.OPERATION_FAILED;
                LOGGER.error("Exception in addImageList() :", e);
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public int deleteImage(String productId, String imageUrl) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode;
        try {
            EntityProductImage image = find(entityManager, productId, imageUrl);
            reasonCode = ReasonCode.INVALID_INFORMATION;
            if (image != null) {
                if (image.isDefault()) {
                    reasonCode = ReasonCode.PERMISSION_DENIED;
                } else {
                    reasonCode = deleteImage(entityManager, image);
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    public int deleteImageFromDatabase(String productId, String imageUrl) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode;
        try {
            EntityProductImage image = find(entityManager, productId, imageUrl);
            reasonCode = ReasonCode.INVALID_INFORMATION;
            if (image != null) {
                reasonCode = deleteImage(entityManager, image);
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public boolean deleteAllImageProduct(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = deleteAllImageProduct(entityManager, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    /**
     *
     * @param productId
     * @return
     */
    @Override
    public List<EntityProductImage> getImagesOfProduct(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ArrayList<EntityProductImage> results;
        try {
            results = getImagesOfProduct(entityManager, productId);
        } finally {
            entityManager.close();
        }
        return results;
    }

    private boolean deleteAllImageProduct(EntityManager entityManager, String productId) {
        try {
            Query query = entityManager.createNamedQuery("deleteImagesOfProduct");
            query.setParameter(1, productId).executeUpdate();
        } catch (Exception ex) {
            LOGGER.error("Error in deleteAllImageProduct: ", ex);
            return false;
        }
        return true;
    }

    private int deleteImage(EntityManager entityManager, EntityProductImage image) {
        int reasonCode = ReasonCode.NONE;
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(image);
            entityManager.getTransaction().commit();
            DBConnection.getInstance().getCache().evict(EntityProductImage.class, new ProductImageId(image.getProductId(), image.getImageUrl()));
        } catch (Exception ex) {
            LOGGER.error("Error in deleteImage(): {image: " + new Gson().toJson(image) + "}", ex);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
        return reasonCode;
    }

    void addImage(EntityManager entityManager, EntityProductImage entityProductImage) {
        entityManager.persist(entityProductImage);
    }

    private ArrayList<EntityProductImage> getImagesOfProduct(EntityManager entityManager, String productId) {
        Query query = entityManager.createNamedQuery("EntityProductImage.getImagesOfProduct");
        query.setParameter("productId", productId);
        ArrayList<EntityProductImage> results = new ArrayList<>(query.getResultList());
        return results;
    }

    private void updateEntity(EntityManager entityManager, EntityProductImage image) {
        entityManager.merge(image);
    }

    private EntityProductImage find(EntityManager entityManager, String productId, String imageUrl) {
        return entityManager.find(EntityProductImage.class, new ProductImageId(productId, imageUrl));
    }

    private int updateImage(EntityManager entityManager, EntityProductImage image) {
        int reasonCode = ReasonCode.NONE;
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(image);
//            int cartUpdateResult = 0;
            int orderDetailsUpdateResult = 0;

            if (image.isDefault()) {
                ProductRepositoryImpl impl = (ProductRepositoryImpl) MarketRepository.INSTANCE.getProductRepository();
                EntityProduct productById = impl.getProductById(image.getProductId());
                if (productById != null) {
                    productById.setImageUrl(image.getImageUrl());
                    productById.setImageHeight(image.getImageHeight());
                    productById.setImageWidth(image.getImageWidth());
                    impl.updateEntity(entityManager, productById);
                }
//                CartItemRepositoryImpl cartItemRepositoryImpl = (CartItemRepositoryImpl) MarketRepository.INSTANCE.getCartRepository();
                OrderDetailsRepositoryImpl orderDetailsRepositoryImpl = (OrderDetailsRepositoryImpl) MarketRepository.INSTANCE.getOrderDetailsRepository();
//                cartUpdateResult = cartItemRepositoryImpl.updateProductImageUrl(entityManager, image.getProductId(), image.getImageUrl());
                orderDetailsUpdateResult = orderDetailsRepositoryImpl.updateProductImageUrl(entityManager, image.getProductId(), image.getImageUrl());
            }
            entityManager.getTransaction().commit();
//            DBConnection.getInstance().getCache().evict(EntityProductImage.class, new ProductImageId(image.getProductId(), image.getImageUrl()));
            if (image.isDefault()) {
                DBConnection.getInstance().getCache().evict(EntityProduct.class, image.getProductId());
            }
//            if (cartUpdateResult > 0) {
//                DBConnection.getInstance().getCache().evict(EntityCartItem.class);
//            }
            if (orderDetailsUpdateResult > 0) {
                DBConnection.getInstance().getCache().evict(EntityOrderDetails.class);
            }

            DBConnection.getInstance().getCache().evict(EntityProductImage.class, new ProductImageId(image.getProductId(), image.getImageUrl()));
        } catch (Exception e) {
            LOGGER.error("Exception in updateImage() : {image: " + new Gson().toJson(image) + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
        return reasonCode;
    }

    @Override
    public int updateImage(String imageUrl, EntityProductImage updatedImageProperty) {
        int reasonCode = ReasonCode.INVALID_INFORMATION;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProductImage image = find(entityManager, updatedImageProperty.getProductId(), imageUrl);
            if (image != null) {
                updatedImageProperty.setAddedTime(image.getAddedTime());
                updatedImageProperty.setIsDefault(image.isDefault());
                reasonCode = updateImage(entityManager, updatedImageProperty);
                if (reasonCode == ReasonCode.NONE) {
                    reasonCode = deleteImage(entityManager, image);
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public int changeDefaultImage(String previousImageUrl, String newImageUrl, String productId) {
        int reasonCode = ReasonCode.INVALID_INFORMATION;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProductImage previousImage = find(entityManager, productId, previousImageUrl);
            EntityProductImage newImage = find(entityManager, productId, newImageUrl);
            if (previousImage != null && newImage != null) {
                newImage.setIsDefault(true);
                reasonCode = updateImage(entityManager, newImage);
                if (reasonCode == ReasonCode.NONE) {
                    previousImage.setIsDefault(false);
                    updateImage(entityManager, previousImage);
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }
}
