/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import org.ipvision.ringmarket.entities.product.EntityProductRating;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author bose
 */
public interface ProductRatingRepository extends IRepository<EntityProductRating>{

    /**
     * returns -1 for exception
     * @param productId
     * @param userId
     * @return 
     */
    byte findRatingByUser(String productId, long userId);

    EntityProductRating getRating(String productId);

    boolean removeProductRating(String productId, long userId);

    boolean updateProductRating(String productId, long userId, byte rating);
    
}
