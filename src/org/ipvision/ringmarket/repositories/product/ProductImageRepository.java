/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.List;
import org.ipvision.ringmarket.entities.product.EntityProductImage;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface ProductImageRepository extends IRepository<EntityProductImage> {

    public boolean addImage(String url, int hgt, int wdt, String productId);

    public boolean addImage(EntityProductImage entityProductImage);

    public int deleteImage(String productId , String imageUrl);

    public boolean deleteAllImageProduct(String productId);

    public List<EntityProductImage> getImagesOfProduct(String productId);

    int addImageList(String productId, List<EntityProductImage> entityProductImages);
    
    int updateImage(String imageUrl , EntityProductImage updatedImageProperty);
    
    int changeDefaultImage(String previousImageUrl, String newImageUrl, String productId);

}
