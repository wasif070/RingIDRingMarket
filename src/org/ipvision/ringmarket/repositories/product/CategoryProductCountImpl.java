/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import org.ipvision.ringmarket.db.DBConnection;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.entities.category.EntityCategoryProductCount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class CategoryProductCountImpl implements CategoryProductCountRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryProductCountImpl.class);

    @Override
    public boolean addCategoryProductCount(int catId, int count) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;

        try {
            entityManager.getTransaction().begin();
            addCategoryProductCount(entityManager, catId, count);
            entityManager.getTransaction().commit();
            susc = true;
        } finally {
            entityManager.close();
        }
        return susc;
    }

    @Override
    public boolean removeCategoryProduct(int catId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = removeCategoryProductCount(entityManager, catId);
        } finally {
            entityManager.close();
        }
        return susc;
    }

    @Override
    public int getCategoryProductCount(int categoryId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        Integer count;
        try {
            EntityCategoryProductCount findCountByCategory = findCountByCategory(entityManager, categoryId);

            if (findCountByCategory == null) {
                return 0;
            }
            count = findCountByCategory.getCount();
        } finally {
            entityManager.close();
        }
        return count;
    }

    private EntityCategoryProductCount findCountByCategory(EntityManager entityManager, int categoryId) {
        EntityCategoryProductCount find = null;
        try {
            find = entityManager.find(EntityCategoryProductCount.class, categoryId);
        } catch (Exception e) {
            LOGGER.error("Error in findCountByCategory ", e);
        }

        return find;
    }

    private void updateCategoryProductCount(EntityManager entityManager, EntityCategoryProductCount categoryProductCount) {
        entityManager.merge(categoryProductCount);
    }

    @Override
    public boolean increaseCategoryProductCount(int catId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean success = false;

        try {
            EntityCategoryProductCount findCountByCategory = findCountByCategory(entityManager, catId);
            try {
                entityManager.getTransaction().begin();
                if (findCountByCategory != null) {
                    findCountByCategory.setCount(findCountByCategory.getCount() + 1);
                    updateCategoryProductCount(entityManager, findCountByCategory);
                } else {
                    addCategoryProductCount(entityManager, catId, 1);
                }
                entityManager.getTransaction().commit();
                success = true;
            } catch (Exception e) {
                LOGGER.error("Error in increaseCategoryProductCount() ", e);

            }

        } finally {
            entityManager.close();
        }
        return success;
    }

    @Override
    public boolean decreaseCategoryProductCount(int catId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean success = false;

        try {
            EntityCategoryProductCount findCountByCategory = findCountByCategory(entityManager, catId);
            try {
                if (findCountByCategory != null && findCountByCategory.getCount() > 0) {
                    entityManager.getTransaction().begin();
                    findCountByCategory.setCount(findCountByCategory.getCount() - 1);
                    updateCategoryProductCount(entityManager, findCountByCategory);
                    entityManager.getTransaction().commit();
                    success = true;
                }
            } catch (Exception e) {
                LOGGER.error("Error in decreaseCategoryProductCount() ", e);

            }
        } finally {
            entityManager.close();
        }
        return success;
    }

    protected void addCategoryProductCount(EntityManager entityManager, int catId, int count) {
        EntityCategoryProductCount categoryProductCount = new EntityCategoryProductCount(catId, count);
        entityManager.persist(categoryProductCount);
    }

    protected boolean removeCategoryProductCount(EntityManager entityManager, int categoryId) {
        try {
            EntityCategoryProductCount categoryProductCount = entityManager.find(EntityCategoryProductCount.class, categoryId);
            entityManager.getTransaction().begin();
            entityManager.remove(categoryProductCount);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            LOGGER.error("Error in removeCategoryProductCount: { categoryId: " + categoryId + " } ", ex);
            entityManager.getTransaction().rollback();
            return false;
        }
        return true;
    }

}
