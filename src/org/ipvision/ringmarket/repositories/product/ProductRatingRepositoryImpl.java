/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import javax.persistence.EntityManager;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProductRating;
import org.ipvision.ringmarket.entities.product.EntityProductRatingLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class ProductRatingRepositoryImpl implements ProductRatingRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductRatingRepositoryImpl.class);

    @Override
    public boolean updateProductRating(String productId, long userId, byte rating) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean sucs = false;
            try {
                updateProductRating(entityManager, productId, userId, rating);
                entityManager.getTransaction().commit();
                sucs = true;
                return sucs;
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                LOGGER.error("Error in updateProductRating. {productId: " + productId + ", userId: " + userId + "}", e);
                return false;
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean removeProductRating(String productId, long userId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = removeProductRating(entityManager, productId, userId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public EntityProductRating getRating(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProductRating entityProductRating = getRating(entityManager, productId);
            return entityProductRating;

        } finally {
            entityManager.close();
        }
    }

    protected EntityProductRating getRating(EntityManager entityManager, String productId) {
        return entityManager.find(EntityProductRating.class, productId);
    }

    /**
     * returns -1 for exception
     */
    @Override
    public byte findRatingByUser(String productId, long userId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            byte rating = findRatingByUser(entityManager, productId, userId);
            return rating;

        } finally {
            entityManager.close();
        }
    }

    protected void updateProductRating(EntityManager entityManager, String productId, long userId, byte rating) throws Exception {
        ProductRatingLogRepositoryImpl productRatingLogRepositoryImpl = new ProductRatingLogRepositoryImpl();
        EntityProductRatingLog ehsr = productRatingLogRepositoryImpl.findRatingByUser(entityManager, productId, userId);

        if (ehsr == null) {
            updateProductRatingByNewUser(entityManager, productId, userId, rating);
        } else {
            updateProductRatingByOldUser(entityManager, productId, rating, ehsr);
        }
    }

    protected void updateProductRatingByNewUser(EntityManager entityManager, String productId, long userId, byte rating) throws Exception {
        ProductRatingLogRepositoryImpl productRatingLogRepositoryImpl = new ProductRatingLogRepositoryImpl();
        EntityProductRating entityProductRating = entityManager.find(EntityProductRating.class, productId);
        if (entityProductRating == null) {
            entityProductRating = new EntityProductRating();
            entityProductRating.setNumberOfRaters(1);
            entityProductRating.setId(productId);
            entityProductRating.setTotalRating(rating);
            entityManager.persist(entityProductRating);
        } else {
            entityProductRating.setNumberOfRaters(entityProductRating.getNumberOfRaters() + 1);
            entityProductRating.setTotalRating(entityProductRating.getTotalRating() + rating);
            entityManager.merge(entityProductRating);
        }
        // adding to history
        productRatingLogRepositoryImpl.addNewRating(entityManager, productId, userId, rating);
    }

    protected void updateProductRatingByOldUser(EntityManager entityManager, String productId, byte newRating, EntityProductRatingLog ehsr) throws Exception {
        ProductRatingLogRepositoryImpl productRatingLogRepositoryImpl = new ProductRatingLogRepositoryImpl();
        EntityProductRating entityProductRating = entityManager.find(EntityProductRating.class, productId);
        entityProductRating.setTotalRating(entityProductRating.getTotalRating() - ehsr.getRating() + newRating);
        entityManager.merge(entityProductRating);
        DBConnection.getInstance().getCache().evict(EntityProductRating.class, productId);

        //adding to history
        ehsr.setRating(newRating);
        productRatingLogRepositoryImpl.updateRating(entityManager, ehsr);
    }

    protected boolean removeProductRatingByUser(EntityManager entityManager, String productId, int rating) {
        try {
            EntityProductRating entityProductRating = getRating(entityManager, productId);
            entityProductRating.setNumberOfRaters(entityProductRating.getNumberOfRaters() - 1);
            entityProductRating.setTotalRating(entityProductRating.getTotalRating() - rating);
            entityManager.merge(entityProductRating);
        } catch (Exception ex) {
            LOGGER.error("Error in removeProductRatingByUser. {productId: " + productId + "}", ex);
            return false;
        }
        return true;
    }

    protected boolean removeProductRating(EntityManager entityManager, String productId, long userId) {
        try {
            ProductRatingLogRepositoryImpl productRatingLogRepositoryImpl = new ProductRatingLogRepositoryImpl();
            EntityProductRatingLog ehsr = productRatingLogRepositoryImpl.findRatingByUser(entityManager, productId, userId);
            productRatingLogRepositoryImpl.removeRatingUser(entityManager, ehsr);
        } catch (Exception ex) {
            LOGGER.error("Error in removeProductRating. {productId: " + productId + "}", ex);
            return false;
        }
        return true;
    }

    protected byte findRatingByUser(EntityManager entityManager, String productId, long userId) {
        ProductRatingLogRepositoryImpl productRatingLogRepositoryImpl = new ProductRatingLogRepositoryImpl();
        try {
            return productRatingLogRepositoryImpl.findRatingByUser(entityManager, productId, userId).getRating();
        } catch (Exception ex) {
            LOGGER.error("Error in findRatingByBuyer: userId: " + userId, ex);
            return -1;
        }
    }
}
