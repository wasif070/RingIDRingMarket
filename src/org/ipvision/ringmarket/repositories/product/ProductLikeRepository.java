/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import org.ipvision.ringmarket.entities.EntityProductLike;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface ProductLikeRepository extends IRepository<EntityProductLike> {

    public boolean addToLikeList(long buyerId, String productId);

    public boolean addToLikeList(EntityProductLike entityProductLike);

    public boolean removeFromLikeList(long buyerId, String productId);

    public EntityProductLike findLikedByBuyer(long buyerId, String productId);

}
