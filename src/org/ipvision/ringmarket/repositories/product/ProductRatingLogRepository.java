/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.Map;
import org.ipvision.ringmarket.entities.product.EntityProductRatingLog;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface ProductRatingLogRepository extends IRepository<EntityProductRatingLog>{

    EntityProductRatingLog findRatingByUser(String productId, long userId);
    Map<Byte,Long> getRatingDetails(String productId);
    
}
