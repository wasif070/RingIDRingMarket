/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.List;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.entities.EntityHistoryProduct;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface HistoryProductRepository extends IRepository<EntityHistoryProduct>{
    public boolean addNewHistory(EntityProduct entityProduct, double soldPrice, ProductStatus productStatus);
    public boolean addNewHistory(EntityHistoryProduct entityHistoryProduct);
    public boolean removeProductFromHistory(String id);
    public EntityHistoryProduct getProductFromHistory(String id);
    public List<EntityHistoryProduct> getCategoryWiseProductHistory(int catId);
}
