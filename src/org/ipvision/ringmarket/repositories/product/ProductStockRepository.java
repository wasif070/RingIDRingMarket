/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.Collection;
import java.util.List;
import org.ipvision.ringmarket.entities.product.EntityProductStock;
import org.ipvision.ringmarket.utils.ListReturnDTO;

/**
 *
 * @author saikat
 */
public interface ProductStockRepository {

    int addStock(String productId, Integer quantity);

    EntityProductStock getStock(String id);

    int increaseStock(String productId, Integer quantity);

    int decreaseStock(String productId, Integer quantity);

    ListReturnDTO<EntityProductStock> getProductStockByIds(Collection<String> idList);
    
}
