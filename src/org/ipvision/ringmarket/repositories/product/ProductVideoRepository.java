/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.List;
import org.ipvision.ringmarket.entities.EntityProductVideo;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface ProductVideoRepository extends IRepository<EntityProductVideo>{
     public boolean addVideo(String productId, String videoUrl, String imgUrl, int imgHgt, int imgWdt);
     public boolean addVideo(EntityProductVideo entityProductVideo) ;
     public boolean deleteVideo(EntityProductVideo entityProductVideo) ;
     public boolean deleteAllVideosOfProduct(String productId);
     public List<EntityProductVideo> getVideosOfProduct(String productId);
     
}
