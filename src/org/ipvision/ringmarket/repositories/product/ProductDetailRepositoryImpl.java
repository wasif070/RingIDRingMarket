package org.ipvision.ringmarket.repositories.product;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProductDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Imran
 */
public class ProductDetailRepositoryImpl implements ProductDetailsRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDetailRepositoryImpl.class);

    /**
     *
     * @param id
     * @param desc
     * @return
     */
    public boolean addProductDetail(String id, String desc) {
        return addProductDetail(id, desc, null);
    }

    /**
     *
     * @param id
     * @param desc
     * @param lc
     * @param cc
     * @return
     */
    public boolean addProductDetail(String id, String desc, LinkedHashMap attributes) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;

        try {
            try {
                entityManager.getTransaction().begin();
                addProductDetail(entityManager, id, desc, attributes);
                entityManager.getTransaction().commit();
                susc = true;
            } catch (Exception e) {
                LOGGER.error("Exception in addProductDetail(): ", e);
            }

        } finally {
            entityManager.close();
        }
        return susc;
    }

    /**
     *
     * @param entityProductDetail
     * @return
     */
    @Override
    public boolean addProductDetail(EntityProductDetail entityProductDetail) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean success = false;

        try {
            try {
                entityManager.getTransaction().begin();
                addProductDetail(entityManager, entityProductDetail);
                entityManager.getTransaction().commit();
                success = true;
            } catch (Exception e) {
                LOGGER.error("Exception in addProductDetail() : ", e);
            }
        } finally {
            entityManager.close();
        }
        return success;

    }

    /**
     *
     * @param iProductDetail
     * @return
     */
    @Override
    public int updateProductDetail(EntityProductDetail iProductDetail) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int susc;

        try {
            susc = updateProductDetail(entityManager, iProductDetail);
        } finally {
            entityManager.close();
        }
        return susc;
    }

    /**
     *
     * @param id
     * @return
     */
    public boolean deleteProductDetail(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = deleteProductDetail(entityManager, id);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    /**
     *
     * @param id
     * @param desc
     * @return
     */
    public boolean updateProductDescription(String id, String desc) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = updateProductDescription(entityManager, id, desc);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return true;
    }

    /**
     *
     * @param id
     * @return
     */
    public EntityProductDetail getProductDetail(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityProductDetail entityProductDetail;
        try {
            entityProductDetail = getProductDetail(entityManager, id);
        } finally {
            entityManager.close();
        }
        return entityProductDetail;
    }

    protected void addProductDetail(EntityManager entityManager, String id, String desc) {
        addProductDetail(entityManager, id, desc, null);
    }

    protected void addProductDetail(EntityManager entityManager, String id, String desc, LinkedHashMap attributes) {
        EntityProductDetail entityProductDetail = new EntityProductDetail();
        entityProductDetail.setDescription(desc);
        entityProductDetail.setId(id);
        entityProductDetail.setAttributes(attributes);
        addProductDetail(entityManager, entityProductDetail);
    }

    protected void addProductDetail(EntityManager entityManager, EntityProductDetail entityProductDetail) {
        entityManager.persist(entityProductDetail);
    }

    protected boolean deleteProductDetail(EntityManager entityManager, String id) {
        try {
            EntityProductDetail entityProductDetail = entityManager.find(EntityProductDetail.class, id);
            if (entityProductDetail != null) {
                entityManager.remove(entityProductDetail);
            } else {
                LOGGER.error("Error in deleteProductDetail. No such id found: {ProductId: " + id + "} ");
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("Error in deleteProductDetail: {ProductId: " + id + "} ", ex);
            return false;
        }
        return true;
    }

    protected EntityProductDetail getProductDetail(EntityManager entityManager, String id) {
        return entityManager.find(EntityProductDetail.class, id);
    }

    protected boolean updateProductDescription(EntityManager entityManager, String id, String desc) {
        EntityProductDetail entityProductDetail = getProductDetail(entityManager, id);
        entityProductDetail.setDescription(desc);
        return updateProductDetail(entityManager, entityProductDetail) == ReasonCode.NONE;
    }

    protected int updateProductDetail(EntityManager entityManager, EntityProductDetail updatedDetals) {
        int reasonCode = ReasonCode.NONE;
        try {
            EntityProductDetail productDetail = getProductDetail(entityManager, updatedDetals.getId());
            if (productDetail != null) {
                if (updatedDetals.getDescription() != null && !updatedDetals.getDescription().isEmpty()) {
                    productDetail.setDescription(updatedDetals.getDescription());
                }
                if (updatedDetals.getAttributes() != null && !((Map) updatedDetals.getAttributes()).isEmpty()) {
                    productDetail.setAttributes(updatedDetals.getAttributes());
                }
                entityManager.getTransaction().begin();
                entityManager.merge(productDetail);
                entityManager.getTransaction().commit();
                DBConnection.getInstance().getCache().evict(EntityProductDetail.class, updatedDetals.getId());
            } else {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            }
        } catch (Exception ex) {
            LOGGER.error("Error in updateProductDetail: {ProductId: " + updatedDetals.getId() + "} ", ex);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }
}
