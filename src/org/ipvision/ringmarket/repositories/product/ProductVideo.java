/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityProductVideo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class ProductVideo implements ProductVideoRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductVideo.class);

    public boolean addVideo(String productId, String videoUrl, String imgUrl, int imgHgt, int imgWdt) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = addVideo(entityManager, productId, videoUrl, imgUrl, imgHgt, imgWdt);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    public boolean addVideo(EntityProductVideo entityProductVideo) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = addVideo(entityManager, entityProductVideo);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    public boolean deleteVideo(EntityProductVideo entityProductVideo) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = deleteVideo(entityManager, entityProductVideo);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    public boolean deleteAllVideosOfProduct(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = deleteAllVideosOfProduct(entityManager, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    public List<EntityProductVideo> getVideosOfProduct(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ArrayList<EntityProductVideo> results = getVideosOfProduct(entityManager, productId);
            return results;
        } finally {
            entityManager.close();
        }

    }

    protected boolean deleteAllVideosOfProduct(EntityManager entityManager, String productId) {
        try {
//delete from product_video where product_video = ?1
            entityManager.createNamedQuery("deleteVideosOfProduct").setParameter(1, productId).executeUpdate();
        } catch (Exception ex) {
            LOGGER.error("Error in deleteAllVideosOfProduct: ", ex);
            return false;
        }
        return true;
    }

    protected boolean addVideo(EntityManager entityManager, String productId, String videoUrl, String imgUrl, int imgHgt, int imgWdt) {
        EntityProductVideo entityProductVideo = new EntityProductVideo();
        entityProductVideo.setProductId(productId);
        entityProductVideo.setThumbImageHeight(imgHgt);
        entityProductVideo.setThumbImageUrl(imgUrl);
        entityProductVideo.setThumbImageWidth(imgWdt);
        entityProductVideo.setVideoUrl(videoUrl);
        return addVideo(entityManager, entityProductVideo);
    }

    protected boolean addVideo(EntityManager entityManager, EntityProductVideo entityProductVideo) {
        try {
            entityManager.persist(entityProductVideo);
        } catch (Exception ex) {
            LOGGER.error("Error in addVideo: ", ex);
            return false;
        }
        return true;
    }

    protected boolean deleteVideo(EntityManager entityManager, EntityProductVideo entityProductVideo) {
        try {
            entityManager.remove(entityManager.find(EntityProductVideo.class, entityProductVideo));
        } catch (Exception ex) {
            LOGGER.error("Exception in deleteVideo: ", ex);
            return false;
        }
        return true;
    }

    protected ArrayList<EntityProductVideo> getVideosOfProduct(EntityManager entityManager, String productId) {
//        Query query = entityManager.createNamedQuery("getVideosOfProduct").setParameter("productId", productId);
        return new ArrayList<>(entityManager.createNamedQuery("getVideosOfProduct").setParameter("productId", productId).getResultList());
    }
}
