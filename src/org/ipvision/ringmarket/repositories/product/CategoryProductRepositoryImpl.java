/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import org.ipvision.ringmarket.db.DBConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import org.ipvision.ringmarket.cache.ProductCategoryCache;
import org.ipvision.ringmarket.constants.LocationHelper;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.category.EntityCategoryProduct;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.entities.discount.EntityProductDiscount;
import org.ipvision.ringmarket.entities.id.CategoryProductId;
import org.ipvision.ringmarket.entities.user.basket.EntityBasketProductCategory;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ipvision.ringmarket.repositories.category.CategoryRepository;
import org.ipvision.ringmarket.repositories.discount.ProductDiscountRepositoryImpl;
import org.ipvision.ringmarket.retryutil.Retry;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.Utility;

/**
 *
 * @author Imran
 */
public class CategoryProductRepositoryImpl implements CategoryProductRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryProductRepositoryImpl.class);

    @Override
    public boolean addCategoryProduct(int catId, String productId, Long shopId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addCategoryProduct(entityManager, catId, productId, shopId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    @Override
    public boolean addMultipleCategoriesProduct(Set<Integer> catList, String productId, Long shopId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc = false;
        try {
            entityManager.getTransaction().begin();
            addMultipleCategoriesProduct(entityManager, catList, productId, shopId);
            entityManager.getTransaction().commit();
            susc = true;
        } catch (Exception e) {
            LOGGER.error("Exception in addMultipleCategoriesProduct(): ", e);
        } finally {
            entityManager.close();
        }
        return susc;
    }

    @Override
    public boolean removeCategoryProduct(int catId, String productId, Long shopId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = removeCategoryProduct(entityManager, catId, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    @Override
    public EntityCategoryProduct findById(int catId, String productId, Long shopId) {
        EntityCategoryProduct find;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            find = findById(entityManager, new CategoryProductId(catId, shopId, productId));
        } finally {
            entityManager.close();
        }
        return find;
    }

    private EntityCategoryProduct findById(EntityManager entityManager, CategoryProductId categoryProductId) {
        return entityManager.find(EntityCategoryProduct.class, categoryProductId);
    }

    @Override
    public ListReturnDTO<EntityProduct> getCategoryWiseProduct(int categoryId, String pivotProductId, Long shopId, Scroll scroll, int limit, ProductStatus status) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ListReturnDTO<EntityProduct> productList;
        try {
            productList = getCategoryWiseProduct(entityManager, categoryId, pivotProductId, shopId, scroll, limit, status);
        } finally {
            entityManager.close();
        }
        return productList;
    }

    @Override
    public ListReturnDTO<EntityProduct> getCategoryWiseProductInPriceRange(int categoryId, String pivotProductId, double minPrice, double maxPrice, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ListReturnDTO<EntityProduct> productList;
        try {
            productList = getCategoryWiseProductInPriceRange(entityManager, categoryId, pivotProductId, minPrice, maxPrice, scroll, limit);
        } finally {
            entityManager.close();
        }
        return productList;
    }

    @Override
    public ListReturnDTO<EntityProduct> getCategoryWiseProductBasedLocation(int categoryId, String pivotProductId, float lat, float lon, int radius, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ListReturnDTO<EntityProduct> productList;
        try {
            productList = getCategoryWiseProductBasedLocation(entityManager, categoryId, pivotProductId, lat, lon, radius, scroll, limit);
        } finally {
            entityManager.close();
        }
        return productList;
    }

    @Override
    public ListReturnDTO<EntityCategory> getCategoriesByProduct(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ListReturnDTO<EntityCategory> epchs;
        try {
            epchs = getCategoriesByProduct(entityManager, productId);
        } finally {
            entityManager.close();
        }
        return epchs;
    }

    @Override
    public boolean removeProduct(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = removeProduct(entityManager, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    protected boolean addCategoryProduct(EntityManager entityManager, int catId, String productId, Long shopId) {
        try {
            EntityCategoryProduct ecp = new EntityCategoryProduct(catId, productId, shopId);
            entityManager.persist(ecp);
        } catch (Exception ex) {
            LOGGER.error("Error in addCategoryProduct: {ProductId: " + productId + " , catId: " + catId + ",shopId:" + shopId + "} ", ex);
            return false;
        }
        return true;
    }

    protected void addMultipleCategoriesProduct(EntityManager entityManager, Set<Integer> catList, String productId, Long shopId) {
        for (Integer catId : catList) {
            if (catId != null) {
                EntityCategory categoryInfo = ProductCategoryCache.getInstance().getCategoryInfo(catId);
                entityManager.persist(new EntityCategoryProduct(catId, productId, shopId, categoryInfo.getPosition()));
            }
        }
    }

    protected boolean isCategoryProductExists(EntityManager entityManager, int categoryId, String productId) {
        Query query = entityManager.createNamedQuery("getCategoryProduct");
        query.setParameter("categoryId", categoryId);
        query.setParameter("productId", productId);

        List<EntityCategoryProduct> categoryProducts = query.getResultList();

        return !(categoryProducts.isEmpty());
    }

    private ListReturnDTO<EntityCategoryProduct> getCategoryProductList(EntityManager entityManager, Integer categoryId, String pivotProductId, Long shopId, Scroll scroll, int limit) {
        boolean isEmptyPivot = false;
        if (pivotProductId == null || pivotProductId.isEmpty()) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }
        ListReturnDTO<EntityCategoryProduct> listReturnDTO = new ListReturnDTO<>();
        try {
            Query query;
            if (shopId == null) {
                if (scroll == Scroll.UP) {
                    query = entityManager.createNamedQuery("getUpwardProductIdsByCategory");
                } else {
                    query = entityManager.createNamedQuery("getDownwardProductIdsByCategory");
                }
                query.setParameter("categoryId", categoryId);
                query.setParameter("pivotProductId", pivotProductId);
            } else {
                if (scroll == Scroll.UP) {
                    query = entityManager.createNamedQuery("EntityCategoryProduct.getProductIdsByCategoryAndShopScrollUp");
                } else {
                    query = entityManager.createNamedQuery("EntityCategoryProduct.getProductIdsByCategoryAndShopScrollDown");
                }
                query.setParameter("categoryId", categoryId);
                query.setParameter("pivotProductId", pivotProductId);
                query.setParameter("shopId", shopId);
            }
//            ArrayList<EntityCategoryProduct> categoryProducts = new ArrayList<>(query.setMaxResults(limit).getResultList());
            List<EntityCategoryProduct> categoryProducts = new LinkedList<>();
//            List<Object[]> results = query.setMaxResults(limit).getResultList();
            List<Object[]> results = query.getResultList();
            if (results.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }
            } else {
                for (Object[] result : results) {
                    EntityCategoryProduct findById = findById(entityManager, new CategoryProductId((Integer) result[0], (Long) result[1], (String) result[2]));
                    if (findById != null) {
                        categoryProducts.add(findById);
                    } else {
                        LOGGER.error("No EntityCategoryProduct found for {categoryId: " + result[0] + ", shopId: " + result[1] + ", productId: " + result[2] + "}");
                    }
                }
                if (scroll.equals(Scroll.UP)) {
                    Collections.reverse(categoryProducts);
                }
            }

            listReturnDTO.setList(categoryProducts);
        } catch (Exception e) {
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
            LOGGER.error("Exception in getCategoryProductList(): {categoryId:" + categoryId + ", pivotProductId:" + pivotProductId + ", scroll:" + scroll + ", limit:" + limit + "}", e);
        }

        return listReturnDTO;
    }

    ListReturnDTO<EntityProduct> getCategoryWiseProduct(EntityManager entityManager, Integer categoryId, String pivotProductId, Long shopId, Scroll scroll, int limit, ProductStatus status) {
        int sz = 0;
        long currentTime = System.currentTimeMillis();

        if(categoryId == null){
            categoryId = 0;
        }
        
        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();
        ListReturnDTO<EntityCategoryProduct> categoryProductList = getCategoryProductList(entityManager, categoryId, pivotProductId, shopId, scroll, limit);
        if (categoryProductList.getReasonCode() == ReasonCode.NONE) {

            ProductRepositoryImpl product = new ProductRepositoryImpl();
            ProductDiscountRepositoryImpl productDiscountRepository = new ProductDiscountRepositoryImpl();

            List<EntityProduct> productList = new ArrayList<>();
            ObjectReturnDTO<EntityProductDiscount> validDiscount;
            List<EntityProduct> disableDiscountProductIds = new ArrayList<>();

            for (EntityCategoryProduct categoryProduct : categoryProductList.getList()) {
                EntityProduct ep = product.getProductById(entityManager, categoryProduct.getProductId());
                if (ep != null) {
                    if (ep.getStatus() == status.getValue()) {
                        if (ep.isDiscounted()) {
                            validDiscount = productDiscountRepository.getValidDiscount(entityManager, ep.getId(), currentTime);
                            switch (validDiscount.getReasonCode()) {
                                case ReasonCode.NONE: {
                                    ep.setDiscount(validDiscount.getValue().getDiscountValue());
                                    ep.setDiscountUnit(validDiscount.getValue().getDiscountUnit());
                                    break;
                                }
                                case ReasonCode.RingMarket.VALIDITY_OVER:
                                    disableDiscountProductIds.add(ep);
                                    break;
                            }
                        }
                        productList.add(ep);

                        ++sz;
                    }
                }
                if (sz == limit) {
                    break;
                }
            }
            if (!disableDiscountProductIds.isEmpty()) {
                Thread thread = new Thread(() -> {
                    product.disableDiscountProducts(disableDiscountProductIds);
                });
                DBConnection.getInstance().submitAsyncJob(thread);
            }
            listReturnDTO.setList(productList);
        }
        listReturnDTO.setReasonCode(categoryProductList.getReasonCode());
        return listReturnDTO;
    }

    protected ListReturnDTO<EntityCategory> getCategoriesByProduct(EntityManager entityManager, String productId) {
        ListReturnDTO<EntityCategory> listReturnDTO = new ListReturnDTO<>();

        Query query = entityManager.createNamedQuery("getCategoriesByProduct");
        query.setParameter("productId", productId);

        ArrayList<EntityCategoryProduct> categoryProducts = new ArrayList<>(query.getResultList());
        ArrayList<EntityCategory> categoryHieararchys = new ArrayList<>();
        CategoryRepository categoryRepository = MarketRepository.INSTANCE.getCategoryRepository();
        for (EntityCategoryProduct categoryProduct : categoryProducts) {
//                EntityCategory e = ProductCategoryCache.getInstance().getCategoryInfo(categoryProduct.getCategoryId());
            EntityCategory e = categoryRepository.getCategoryById(categoryProduct.getCategoryId());
            if (e != null) {
                categoryHieararchys.add(e);
            }
        }
        if (!categoryHieararchys.isEmpty()) {
            listReturnDTO.setList(categoryHieararchys);
        } else {
            listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
        }
        return listReturnDTO;
    }

    protected boolean removeProduct(EntityManager entityManager, String productId) {
        try {
//delete from category_product where product_id = ?1
            Query query = entityManager.createNamedQuery("deleteCategoriesOfProduct");
            query.setParameter(1, productId).executeUpdate();
        } catch (Exception ex) {
            LOGGER.error("Error in removeProduct from category_product: {ProductId: " + productId + "} ", ex);
            return false;
        }
        return true;
    }

    protected boolean removeCategoryProduct(EntityManager entityManager, int categoryId, String productId) {
        try {
//delete from category_product where category_id = ?1 and product_id = ?2            
            Query query = entityManager.createNamedQuery("deleteFromCategoryProduct");
            query.setParameter("1", categoryId)
                    .setParameter("2", productId)
                    .executeUpdate();
        } catch (Exception ex) {
            LOGGER.error("Error in removeCategoryProduct: {ProductId: " + productId + ", categoryId: " + categoryId + " } ", ex);
            return false;
        }
        return true;
    }

    protected ListReturnDTO<EntityProduct> getCategoryWiseProductInPriceRange(EntityManager entityManager, int categoryId, String pivotProductId, double minPrice, double maxPrice, Scroll scroll, int limit) {

        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();

        List<EntityProduct> productList = new ArrayList<>();
        int sz = 0;

        while (sz != limit) {
            ListReturnDTO<EntityCategoryProduct> categoryProductList = getCategoryProductList(entityManager, categoryId, pivotProductId, null, scroll, limit);
            if (categoryProductList.getReasonCode() != ReasonCode.NONE) {
                if (productList.isEmpty()) {
                    listReturnDTO.setReasonCode(categoryProductList.getReasonCode());
                } else {
                    listReturnDTO.setList(productList);
                }
                return listReturnDTO;
            }

            ProductRepositoryImpl product = new ProductRepositoryImpl();
            for (EntityCategoryProduct categoryProduct : categoryProductList.getList()) {
                EntityProduct ep = product.getProductById(entityManager, categoryProduct.getProductId());
                if (ep != null && ep.getPrice() >= minPrice && ep.getPrice() <= maxPrice) {
                    productList.add(ep);
                    ++sz;
                    if (sz == limit) {
                        break;
                    }
                }
            }
            if (scroll.equals(Scroll.UP)) {
                pivotProductId = categoryProductList.getList().get(0).getProductId();
            } else {
                pivotProductId = categoryProductList.getList().get(categoryProductList.getList().size() - 1).getProductId();
            }
        }
        return listReturnDTO;
    }

    protected ListReturnDTO<EntityProduct> getCategoryWiseProductBasedLocation(EntityManager entityManager, int categoryId, String pivotProductId, float lat, float lon, int radius, Scroll scroll, int limit) {

        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();

        ArrayList<EntityProduct> productList = new ArrayList<>();
        int sz = 0;
        LocationHelper.LatLon center = new LocationHelper.LatLon(lat, lon);
        radius = radius * 1000;

        while (sz != limit) {
            ListReturnDTO<EntityCategoryProduct> categoryProductList = getCategoryProductList(entityManager, categoryId, pivotProductId, null, scroll, limit);
            if (categoryProductList.getReasonCode() != ReasonCode.NONE) {
                if (productList.isEmpty()) {
                    listReturnDTO.setReasonCode(categoryProductList.getReasonCode());
                } else {
                    listReturnDTO.setList(productList);
                }
                return listReturnDTO;
            }

            ProductRepositoryImpl product = new ProductRepositoryImpl();
            for (EntityCategoryProduct categoryProduct : categoryProductList.getList()) {
                EntityProduct ep = product.getProductById(entityManager, categoryProduct.getProductId());

                if (ep != null) {
                    LocationHelper.LatLon checkPoint = new LocationHelper.LatLon(ep.getLat(), ep.getLon());
                    if (LocationHelper.isCoordinateInsideCircle(center, checkPoint, radius)) {
                        productList.add(ep);
                        ++sz;
                        if (sz == limit) {
                            break;
                        }
                    }
                }
            }
            if (scroll.equals(Scroll.UP)) {
                pivotProductId = categoryProductList.getList().get(0).getProductId();
            } else {
                pivotProductId = categoryProductList.getList().get(categoryProductList.getList().size() - 1).getProductId();
            }
        }
        return listReturnDTO;
    }

    /**
     *
     * @param shopId
     * @param start
     * @param limit
     * @param categoryId
     * @return
     */
    @Override
    public List<EntityCategory> getStoresCategoriesById(Long shopId, int start, int limit, Integer categoryId) {
        List<EntityCategory> categories = null;
        if (start >= 0) {
            if (categoryId == null) {
                categories = getStoresCategoriesById(shopId, start, limit, 1, categoryId);
            } else {
                EntityCategory categoryInfo = ProductCategoryCache.getInstance().getCategoryInfo(categoryId);
                if (categoryInfo != null) {
                    categories = getStoresCategoriesById(shopId, start, limit, categoryInfo.getPosition() + 1, categoryId);
                }
            }
        }
        return categories;
    }

    @Retry(retryAttempts = 5, sleepInterval = 1500)
    public int updateProductStatus(String productId, boolean status) {
        int success = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            Query query = entityManager.createNamedQuery("EntityCategoryProduct.updateProductStatus");
            query.setParameter("productId", productId);
            query.setParameter("status", status);

            int count = query.executeUpdate();
            entityManager.getTransaction().commit();
            if (count > 0) {
                DBConnection.getInstance().getCache().evict(EntityCategoryProduct.class);
            }
        } catch (OptimisticLockException e) {
            entityManager.getTransaction().rollback();
            throw e;
        } catch (Exception e) {
            LOGGER.error("Exception in updateProductStatus(): {productId:" + productId + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            success = ReasonCode.EXCEPTION_OCCURED;
        } finally {
            entityManager.close();
        }
        return success;
    }

    @Retry(retryAttempts = 2, sleepInterval = 100)
    public int updateShopStatus(long shopId, boolean status) {
        int success = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            Query query = entityManager.createNamedQuery("EntityCategoryProduct.updateShopStatus");
            query.setParameter("shopId", shopId);
            query.setParameter("status", status);

            int count = query.executeUpdate();
            entityManager.getTransaction().commit();
            if (count > 0) {
                DBConnection.getInstance().getCache().evict(EntityCategoryProduct.class);
                DBConnection.getInstance().getQueryCache().evictAll(EntityCategoryProduct.class);
            }
        } catch (OptimisticLockException e) {
            entityManager.getTransaction().rollback();
            throw e;
        } catch (Exception e) {
            LOGGER.error("Exception in updateProductStatus(): {shopId:" + shopId + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            success = ReasonCode.EXCEPTION_OCCURED;
        } finally {
            entityManager.close();
        }
        return success;
    }

    private List<EntityCategory> getStoresCategoriesById(Long shopId, int start, int limit, int position, Integer parentId) {
        List<EntityCategory> categories = null;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                Query query = entityManager.createNamedQuery("EntityCategoryProduct.getCategoriesByShopAndPosition", Integer.class);
                query.setParameter("shopId", shopId);
                query.setParameter("position", position);
//            List<Integer> list = query.setFirstResult(start).setMaxResults(limit).getResultList();
                List<Integer> list = query.setFirstResult(start).getResultList();
                if (!list.isEmpty()) {
                    if (parentId != null && parentId != 0) {
                        List<Integer> subCategoryIds = ProductCategoryCache.getInstance().getSubCategoryIds(parentId);
                        if (subCategoryIds != null && !subCategoryIds.isEmpty()) {
                            list = new ArrayList<>(list);
                            list.retainAll(subCategoryIds);
                        } else {
                            list = new ArrayList<>();
                        }
                    }
                    categories = new ArrayList<>();
                    for (Integer categoryId : list) {
                        EntityCategory categoryInfo = ProductCategoryCache.getInstance().getCategoryInfo(categoryId);
                        if (categoryInfo != null) {
                            categories.add(categoryInfo);
                        } else {
                            LOGGER.error("Not EntityCategory found for {id : " + categoryId + "}");
                        }
                    }
                    if (!categories.isEmpty()) {
                        categories.sort(Utility.getCategoryOrderCompartor());
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Exception in getStoresCategoriesById():", e);
            }

        } finally {
            entityManager.close();
        }
        return categories;
    }
}
