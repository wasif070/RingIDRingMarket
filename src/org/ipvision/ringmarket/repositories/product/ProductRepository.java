/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.repositories.IRepository;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;

/**
 *
 * @author saikat
 */
public interface ProductRepository extends IRepository<EntityProduct> {

    public EntityProduct addProduct(EntityProduct entityProduct, int categoryId);

    public EntityProduct getProductById(String productId);

    public EntityProduct getProductByCode(String code);

    public boolean deleteProduct(String id);

    public ObjectReturnDTO<String> addProduct(EntityProduct entityProduct, Set<Integer> categoryList, Integer quantity);

    public Integer updateProductStatus(String id, ProductStatus status);

    public boolean updateViewCount(String id);

    public ListReturnDTO<EntityProduct> getExclusiveProducts(String pivotProductId, Scroll scroll, int limit);

    public List<EntityProduct> getNearestLocaitonProducts(float lat, float lon, int radius, String pivotProductId, Scroll scroll, int limit);

    public List<EntityProduct> getMostViewedProducts(String pivotProductId, Scroll scroll, int limit);

    public List<EntityProduct> getProductsInAPriceRange(double leastPrice, double mostPrice, String pivotProductId, Scroll scroll, int limit);

    public boolean deleteProductFromDatabase(String id);

    public int updateProduct(EntityProduct product);

    public ListReturnDTO<EntityProduct> getNonExclusiveProducts(String pivotProductId, Scroll scroll, int limit);

    public ListReturnDTO<EntityProduct> getProductsByShopId(String pivotProductId, long shopId, Scroll scroll, int limit, ProductStatus status);

    public ListReturnDTO<EntityProduct> getFiltererProducts(Integer categoryId, Long shopId, String pivotProductId, Scroll scroll, int limit, ProductStatus status);

    PivotedListReturnDTO<EntityProduct, String> getRecommendedProducts(String productId, String pivotId, Scroll scroll, int limit);

    PivotedListReturnDTO<EntityProduct, String> searchProductByName(String pivotProductId, Scroll scroll, int limit, String param);

    ListReturnDTO<EntityProduct> getTrendingProducts(String pivotProductId, Scroll scroll, int limit);
    
    ListReturnDTO<EntityProduct> getDiscountProducts(String pivotProductId, Scroll scroll, int limit);
}
