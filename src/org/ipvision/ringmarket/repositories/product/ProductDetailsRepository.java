/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import com.google.gson.JsonObject;
import java.util.LinkedHashMap;
import java.util.Map;
import org.ipvision.ringmarket.entities.product.EntityProductDetail;
import org.ipvision.ringmarket.repositories.IRepository;

/**
 *
 * @author saikat
 */
public interface ProductDetailsRepository extends IRepository<EntityProductDetail> {

    public boolean addProductDetail(String id, String desc);

    public boolean addProductDetail(String id, String desc, LinkedHashMap attributes);

    public boolean addProductDetail(EntityProductDetail entityProductDetail);

    public int updateProductDetail(EntityProductDetail iProductDetail);

    public boolean deleteProductDetail(String id);

    public boolean updateProductDescription(String id, String desc);

    public EntityProductDetail getProductDetail(String id);

}
