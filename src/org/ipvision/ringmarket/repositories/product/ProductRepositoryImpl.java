/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import org.ipvision.ringmarket.constants.LocationHelper;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.ProductStatus;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import org.ipvision.ringmarket.cache.ProductCategoryCache;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.constants.LocationHelper.LatLon;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.entities.discount.EntityProductDiscount;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.product.EntityProductImage;
import org.ipvision.ringmarket.entities.product.EntityProductStock;
import org.ipvision.ringmarket.repositories.discount.ProductDiscountRepositoryImpl;
import org.ipvision.ringmarket.repositories.shop.ShopRepositoryImpl;
import org.ipvision.ringmarket.repositories.user.basket.BasketProductCategoryRepositoryImpl;
import org.ipvision.ringmarket.repositories.user.cart.CartItemRepositoryImpl;
import org.ipvision.ringmarket.retryutil.Retry;
import org.ipvision.ringmarket.uniqueid.UniqueNumberGenerator;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.ipvision.ringmarket.utils.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class ProductRepositoryImpl implements ProductRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductRepositoryImpl.class);
    private ProductDiscountRepositoryImpl productDiscountRepository = new ProductDiscountRepositoryImpl();
    private CategoryProductRepositoryImpl categoryProductImpl = new CategoryProductRepositoryImpl();

    /**
     *
     * @param entityProduct
     * @param categoryId
     * @return
     */
    @Override
    public EntityProduct addProduct(EntityProduct entityProduct, int categoryId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityProduct = addProduct(entityManager, entityProduct, categoryId);
            if (entityProduct != null) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            MarketRepository.INSTANCE.getCategoryProductCountRepository().increaseCategoryProductCount(categoryId);
        } finally {
            entityManager.close();
        }
        return entityProduct;
    }

    /**
     *
     * @param entityProduct
     * @return
     */
    public boolean addProduct(EntityProduct entityProduct) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = addProduct(entityManager, entityProduct);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public EntityProduct getProductById(String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProduct entityProduct = getProductById(entityManager, productId);
            return entityProduct;

        } finally {
            entityManager.close();
        }
    }

    /**
     * delete product and add to product and adding to product history
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteProduct(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = deleteProduct(entityManager, id);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    /**
     * IProduct type param ip set up before invoking addProduct
     *
     * @param entityProduct
     * @param categoryList
     * @param quantity
     * @return
     */
    @Override
    public ObjectReturnDTO<String> addProduct(EntityProduct entityProduct, Set<Integer> categoryList, Integer quantity) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ObjectReturnDTO<String> addedProduct = addProduct(entityManager, entityProduct, categoryList, quantity);

            categoryList.forEach((catId) -> {
                MarketRepository.INSTANCE.getCategoryProductCountRepository().increaseCategoryProductCount(catId);
            });
            return addedProduct;

        } finally {
            entityManager.close();
        }

    }

    /**
     * note to do: if status is deleted, product will move to history product
     * table updates product status
     *
     * @param id
     * @param status
     * @return
     */
    @Override
    public Integer updateProductStatus(String id, ProductStatus status) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = updateProductStatus(entityManager, id, status);
            if (susc) {
                DBConnection.getInstance().getCache().evict(EntityProduct.class, id);
                entityManager.getTransaction().commit();
                return ReasonCode.NONE;
            } else {
                entityManager.getTransaction().rollback();
                return ReasonCode.EXCEPTION_OCCURED;
            }

        } finally {
            entityManager.close();
        }

    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public boolean updateViewCount(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = updateViewCount(entityManager, id);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public ListReturnDTO<EntityProduct> getExclusiveProducts(String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityProduct> results = getExclusiveProducts(entityManager, pivotProductId, scroll, limit);
            return results;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public ListReturnDTO<EntityProduct> getTrendingProducts(String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityProduct> results = getTrendingProducts(entityManager, pivotProductId, scroll, limit, Boolean.TRUE);

            return results;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public PivotedListReturnDTO<EntityProduct, String> getRecommendedProducts(String productId, String pivotId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityProduct> results = getExclusiveProducts(entityManager, pivotId, scroll, limit);
            PivotedListReturnDTO<EntityProduct, String> pivotedListReturnDTO = new PivotedListReturnDTO<>();
            pivotedListReturnDTO.setReasonCode(results.getReasonCode());
            if (results.getReasonCode() == ReasonCode.NONE) {
                if (scroll == Scroll.DOWN) {
                    pivotId = results.getList().get(results.getList().size() - 1).getId();
                } else {
                    pivotId = results.getList().get(0).getId();
                }
                pivotedListReturnDTO.setList(results.getList());
            }
            pivotedListReturnDTO.setPivot(pivotId);
            return pivotedListReturnDTO;
        } finally {
            entityManager.close();
        }

    }

    @Override
    public ListReturnDTO<EntityProduct> getNonExclusiveProducts(String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityProduct> results = getNonExclusiveProducts(entityManager, pivotProductId, scroll, limit);
            return results;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public ListReturnDTO<EntityProduct> getDiscountProducts(String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityProduct> results = getDiscountProducts(entityManager, pivotProductId, scroll, limit);
            return results;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public ListReturnDTO<EntityProduct> getProductsByShopId(String pivotProductId, long shopId, Scroll scroll, int limit, ProductStatus status) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityProduct> results = getProductsOfShop(entityManager, shopId, pivotProductId, scroll, limit, status);
            return results;

        } finally {
            entityManager.close();
        }
    }

    /**
     *
     * @param lat
     * @param lon
     * @param radius
     * @param pivotProductId
     * @param scroll
     * @param limit
     * @return
     */
    @Override
    public List<EntityProduct> getNearestLocaitonProducts(float lat, float lon, int radius, String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            List<EntityProduct> results = getNearestLocaitonProducts(entityManager, lat, lon, radius, pivotProductId, scroll, limit);
            return results;

        } finally {
            entityManager.close();
        }
    }

    /**
     *
     * @param pivotProductId
     * @param scroll
     * @param limit
     * @return
     */
    @Override
    public List<EntityProduct> getMostViewedProducts(String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ArrayList<EntityProduct> results = getMostViewedProducts(entityManager, pivotProductId, scroll, limit);
            return results;

        } finally {
            entityManager.close();
        }
    }

    /**
     *
     * @param leastPrice
     * @param mostPrice
     * @param pivotProductId
     * @param scroll
     * @param limit
     * @return
     */
    @Override
    public List<EntityProduct> getProductsInAPriceRange(double leastPrice, double mostPrice, String pivotProductId, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            List<EntityProduct> results = getProductsInAPriceRange(entityManager, leastPrice, mostPrice, pivotProductId, scroll, limit);
            return results;

        } finally {
            entityManager.close();
        }
    }
//
//    public List<EntityProduct> getMostViewedProductsInAPriceRange(double leastPrice, double mostPrice, String pivotProductId, Scroll scroll, int limit) {
//        EntityManager entityManager = createEntityManager();
//        List<EntityProduct> results = getMostViewedProductsInAPriceRange(entityManager, leastPrice, mostPrice, pivotProductId, scroll, limit);
//        closeEntityManager(true, entityManager);
//        return results;
//    }
//
//    public List<EntityProduct> getSimilarNamedProducts(String name, String pivotProductId, Scroll scroll, int limit) {
//        EntityManager entityManager = createEntityManager();
//        List<EntityProduct> results = getSimilarNamedProducts(entityManager, name, pivotProductId, scroll, limit);
//        closeEntityManager(true, entityManager);
//        return results;
//    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteProductFromDatabase(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean susc = deleteProductFromDatabase(entityManager, id);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
            return susc;

        } finally {
            entityManager.close();
        }
    }

    /**
     * IProduct type param ip set up before invoking addProduct
     *
     * @param ip(IProduct)
     * @return
     */
    @Override
    public int updateProduct(EntityProduct ip) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            int updateProduct = updateProduct(entityManager, ip);
            return updateProduct;

        } finally {
            entityManager.close();
        }
    }

    protected EntityProduct addProduct(EntityManager entityManager, EntityProduct entityProduct, int categoryId) {
        try {
            entityProduct.setStatus(ProductStatus.AWAITING_FOR_APPROVAL);
            entityManager.persist(entityProduct);
            CategoryProductRepositoryImpl categoryProduct = new CategoryProductRepositoryImpl();
            do {
                boolean susc = categoryProduct.addCategoryProduct(entityManager, categoryId, entityProduct.getId(), entityProduct.getShopId());
                if (susc == false) {
                    LOGGER.error("Error in addCategoryProduct");
                    return null;
                }
                categoryId = ProductCategoryCache.getInstance().getParentId(categoryId);
            } while (categoryId > 0);

//            SellerProduct sellerProduct = new SellerProduct();
//            sellerProduct.addNewProduct(entityManager, entityProduct.getShopId(), entityProduct.getId(), entityProduct.getName(), entityProduct.getImageUrl(), entityProduct.getCreationTime());
        } catch (Exception ex) {
            LOGGER.error("Exception in addingProduct. {productName:" + entityProduct.getName() + ",shopId:" + entityProduct.getShopId() + "}", ex);
            return null;
        }
        return entityProduct;
    }

    /**
     *
     * @param entityManager
     * @param productId
     * @return
     */
    public EntityProduct getProductById(EntityManager entityManager, String productId) {
        EntityProduct product = entityManager.find(EntityProduct.class, productId);
        if (product != null && product.isDiscounted()) {
            long currentTime = System.currentTimeMillis();
            ObjectReturnDTO<EntityProductDiscount> validDiscount = productDiscountRepository.getValidDiscount(entityManager, productId, currentTime);
            switch (validDiscount.getReasonCode()) {
                case ReasonCode.NONE: {
                    product.setDiscount(validDiscount.getValue().getDiscountValue());
                    product.setDiscountUnit(validDiscount.getValue().getDiscountUnit());
                    product.setActiveTime(validDiscount.getValue().getActiveTime());
                    product.setValidityTime(validDiscount.getValue().getValidityTime());
                    break;
                }
                case ReasonCode.RingMarket.VALIDITY_OVER:
                    List<EntityProduct> entityProducts = new LinkedList<>();
                    entityProducts.add(product);
                    Thread thread = new Thread(() -> {
                        disableDiscountProducts(entityProducts);
                    });
                    DBConnection.getInstance().submitAsyncJob(thread);
                    break;
            }
        }
        return product;
    }

    public boolean isAvailable(EntityManager entityManager, String id) {
        EntityProduct productById = getProductById(entityManager, id);
        return (productById != null && productById.getStatus() == ProductStatus.AVAILABLE.getValue());
    }

    protected boolean addProduct(EntityManager entityManager, EntityProduct entityProduct) {
        try {
            entityProduct.setStatus(ProductStatus.AWAITING_FOR_APPROVAL);
            entityManager.persist(entityProduct);
        } catch (Exception ex) {
            LOGGER.error("Exception in addingProduct. {productId:" + entityProduct.getId() + "}", ex);
            return false;
        }
        return true;
    }

    protected ObjectReturnDTO<String> addProduct(
            EntityManager entityManager, EntityProduct entityProduct,
            Set<Integer> categoryList, Integer quantity) {
        ObjectReturnDTO<String> objectReturnDTO = new ObjectReturnDTO<>();

        try {
            entityManager.getTransaction().begin();
            //adding to product
            entityProduct.setId(TimeUUID.timeBased().toString());
            entityProduct.setCode(String.valueOf(UniqueNumberGenerator.generateBarcodeId()));
//            entityProduct.setStatus(ProductStatus.AWAITING_FOR_APPROVAL);
            entityProduct.setStatus(ProductStatus.AVAILABLE);
            entityManager.persist(entityProduct);

            //adding categories
            CategoryProductRepositoryImpl categoryProduct = new CategoryProductRepositoryImpl();
            categoryProduct.addMultipleCategoriesProduct(entityManager, categoryList, entityProduct.getId(), entityProduct.getShopId());

            //adding to sellerproduct
//            SellerProduct sellerProduct = new SellerProduct();
//            sellerProduct.addNewProduct(entityManager, entityProduct.getShopId(), entityProduct.getId(), entityProduct.getName(), entityProduct.getImageUrl(), entityProduct.getCreationTime());
            /**
             * increment items count
             */
            ShopRepositoryImpl impl = new ShopRepositoryImpl();
            impl.incrementItemsCount(entityManager, entityProduct.getShopId());

            //adding to image
            ProductImageRepositoryImpl productImage = new ProductImageRepositoryImpl();
            EntityProductImage entityProductImage = new EntityProductImage(entityProduct.getImageUrl(), entityProduct.getImageHeight(), entityProduct.getImageWidth(), entityProduct.getId(), true, System.currentTimeMillis());
            productImage.addImage(entityManager, entityProductImage);

            /**
             * add product stock
             */
            EntityProductStock productStock = new EntityProductStock();
            productStock.setId(entityProduct.getId());
            productStock.setQuantity(quantity);
            productStock.setUpdateTime(System.currentTimeMillis());

            new ProductStockRepositoryImpl().addStock(entityManager, productStock);

            entityManager.getTransaction().commit();
            objectReturnDTO.setReasonCode(ReasonCode.NONE);
            objectReturnDTO.setValue(entityProduct.getId());
        } catch (Exception ex) {
            LOGGER.error("Exception in addingProduct. {productName:" + entityProduct.getName() + ",shopId:" + entityProduct.getShopId() + "}", ex);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            objectReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return objectReturnDTO;
    }

    protected boolean deleteProduct(EntityManager entityManager, String productId) {
        try {
            EntityProduct entityProduct = getProductById(entityManager, productId);
            if (entityProduct != null) {
                //adding to history product
                HistoryProduct historyProduct = new HistoryProduct();
                boolean susc = historyProduct.addNewHistory(entityManager, entityProduct, entityProduct.getPrice(), ProductStatus.DELETED);
                if (susc == false) {
                    LOGGER.error("Exception in deleting product");
                    return false;
                }

                //removing from product
                susc = deleteProductFromDatabase(entityManager, productId);
                if (susc == false) {
                    LOGGER.error("Exception in deleting product");
                    return false;
                }

                //removing from product_detail
                ProductDetailRepositoryImpl productDetail = new ProductDetailRepositoryImpl();
                productDetail.deleteProductDetail(entityManager, productId);
//                if (susc == false) {
//                    LOGGER.error("Exception in deleting product");
//                    return false;
//                }

                //removing from seller_product
//                SellerProduct sellerProduct = new SellerProduct();
//                susc = sellerProduct.deleteProduct(entityManager, productId);
//                if (susc == false) {
//                    LOGGER.error("Exception in deleting product");
//                    return false;
//                }
                /**
                 * decrement items count
                 */
                ShopRepositoryImpl impl = new ShopRepositoryImpl();
                impl.decrementItemsCount(entityManager, entityProduct.getShopId());

                //removing from category_product
                CategoryProductRepositoryImpl categoryProduct = new CategoryProductRepositoryImpl();
                susc = categoryProduct.removeProduct(entityManager, productId);
                if (susc == false) {
                    LOGGER.error("Exception in deleting product");
                    return false;
                }

                //remove from offer_product
//                OfferProduct offerProduct = new OfferProduct();
//                susc = offerProduct.removeAllOfferOfProduct(productId);
//                if (susc == false) {
//                    LOGGER.error("Exception in deleting product");
//                    return false;
//                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception in deleting product", e);
            return false;
        }
        return true;

    }

    /**
     *
     * @param entityManager
     * @param id
     * @param status
     * @return
     */
    public boolean updateProductStatus(EntityManager entityManager, String id, ProductStatus status) {
        try {
            EntityProduct entityProduct = getProductById(entityManager, id);
            if (entityProduct != null) {
                //            entityProduct.setStatus(status.ordinal());
                entityProduct.setStatus(status);
                entityManager.merge(entityProduct);
                DBConnection.getInstance().getCache().evict(EntityProduct.class, id);
                if (!status.equals(ProductStatus.AVAILABLE)) {
                    CartItemRepositoryImpl cartItemRepo = new CartItemRepositoryImpl();
                    cartItemRepo.updateProductStatus(id, false);

                    BasketProductCategoryRepositoryImpl bcr = new BasketProductCategoryRepositoryImpl();
                    bcr.updateProductStatus(id, false);

                    CategoryProductRepositoryImpl cpi = new CategoryProductRepositoryImpl();
                    cpi.updateProductStatus(id, false);

                }
                //if deleted updating to history product
                if (status.equals(ProductStatus.DELETED)) {
                    HistoryProduct historyProduct = new HistoryProduct();
                    boolean susc = historyProduct.addNewHistory(entityManager, entityProduct, 0, status);
                    if (susc == false) {
                        LOGGER.error("Exception in updating product status");
                        return false;
                    }
                }
            } else {
                LOGGER.error("Exception in updating product status. No such Product found. Id: " + id);
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("Exception in updating product status", ex);
            return false;
        }
        return true;
    }

    /**
     * updates view count by 1
     *
     * @param entityManager
     * @param id
     * @return
     */
    protected boolean updateViewCount(EntityManager entityManager, String id) {
//        try {
//            EntityProduct entityProduct = entityManager.find(EntityProduct.class, id);
//            if (entityProduct != null) {
//                entityProduct.setViewCount(entityProduct.getViewCount() + 1);
//                entityManager.merge(entityProduct);
//                return true;
//            }
//            else {
//                LOGGER.error("Error while updating view count. No Such product found. productId: " + id);
//            }
//        }
//        catch (Exception ex) {
//            LOGGER.error("Error while updating view count. productId: " + id, ex);
//        }
        return false;
    }

    /**
     *
     * @param entityManager
     * @param id
     * @return
     */
    protected boolean deleteProductFromDatabase(EntityManager entityManager, String id) {
        try {
            entityManager.remove(entityManager.find(EntityProduct.class, id));
        } catch (Exception ex) {
            LOGGER.error("Exception in deleteProductFromDatabase. {productId:" + id + "}", ex);
            return false;
        }
        return true;
    }

    void updateEntity(EntityManager entityManager, EntityProduct entityProduct) {
        entityManager.merge(entityProduct);
    }

    private int updateProduct(EntityManager entityManager, EntityProduct entityProduct) {
        int reasonCode = ReasonCode.NONE;
        try {
            entityManager.getTransaction().begin();
            updateEntity(entityManager, entityProduct);
            DBConnection.getInstance().getCache().evict(EntityProduct.class, entityProduct.getId());
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            LOGGER.error("Error in updateproduct ", ex);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    /**
     * returns featured product list Using primary key as index
     *
     * @param pivotProductId(Default Should be empty String)
     * @param scroll(ScrollEnum)
     * @param limit(int)
     * @return
     */
    private ListReturnDTO<EntityProduct> getExclusiveProducts(EntityManager entityManager, String pivotProductId, Scroll scroll, int limit) {
        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();
        boolean isEmptyPivot = false;
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }

        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("EntityProduct.getUpwardFeaturedProducts");
        } else {
            query = entityManager.createNamedQuery("EntityProduct.getDownwardFeaturedProducts");
        }

        query.setParameter("pivotProductId", pivotProductId);
        query.setParameter("status", ProductStatus.AVAILABLE.getValue());

        try {
//            ArrayList<EntityProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            List<String> results = query.setMaxResults(limit).getResultList();
            if (results.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }

                return listReturnDTO;
            }
            List<EntityProduct> entityProducts = pushProduct(entityManager, results);
            if (scroll.equals(Scroll.UP)) {
//                Collections.reverse(results);
                Collections.reverse(entityProducts);
            }
//            listReturnDTO.setList(results);
            listReturnDTO.setList(entityProducts);
        } catch (Exception ex) {
            LOGGER.error("Exception while executing query to get featured products: ", ex);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    private List<EntityProduct> pushProduct(EntityManager entityManager, List<String> results) {
        List<EntityProduct> entityProducts = new ArrayList<>();

        for (String result : results) {
            EntityProduct productById = getProductById(entityManager, result);
            if (productById != null) {
                entityProducts.add(productById);
            }
        }
        return entityProducts;
    }

    private ListReturnDTO<EntityProduct> getTrendingProducts(EntityManager entityManager, String pivotProductId, Scroll scroll, int limit, boolean trending) {
        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();
        boolean isEmptyPivot = false;
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }

        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("EntityProduct.getUpwardTrendingProducts");
        } else {
            query = entityManager.createNamedQuery("EntityProduct.getDownwardTrendingProducts");
        }

        query.setParameter("pivotProductId", pivotProductId);
        query.setParameter("status", ProductStatus.AVAILABLE.getValue());
        query.setParameter("trending", trending);

        try {
//            ArrayList<EntityProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            List<String> results = query.setMaxResults(limit).getResultList();
            if (results.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }

                return listReturnDTO;
            }
            List<EntityProduct> entityProducts = pushProduct(entityManager, results);
            if (scroll.equals(Scroll.UP)) {
//                Collections.reverse(results);
                Collections.reverse(entityProducts);
            }
//            listReturnDTO.setList(results);
            listReturnDTO.setList(entityProducts);
        } catch (Exception ex) {
            LOGGER.error("Exception while executing query to get featured products: ", ex);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    private ListReturnDTO<EntityProduct> getNonExclusiveProducts(EntityManager entityManager, String pivotProductId, Scroll scroll, int limit) {
        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();
        boolean isEmptyPivot = false;
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }

        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("EntityProduct.getUpwardNonExclusiveProducts");
        } else {
            query = entityManager.createNamedQuery("EntityProduct.getDownwardNonExclusiveProducts");
        }

        query.setParameter("pivotProductId", pivotProductId);
        query.setParameter("status", ProductStatus.AVAILABLE.getValue());

        try {
//            ArrayList<EntityProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            List<String> results = query.setMaxResults(limit).getResultList();
            if (results.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }

                return listReturnDTO;
            }
            List<EntityProduct> entityProducts = pushProduct(entityManager, results);
            if (scroll.equals(Scroll.UP)) {
//                Collections.reverse(results);
                Collections.reverse(entityProducts);
            }
//            listReturnDTO.setList(results);
            listReturnDTO.setList(entityProducts);
        } catch (Exception ex) {
            LOGGER.error("Exception while executing query to get non exclusive products: ", ex);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    private ListReturnDTO<EntityProduct> getDiscountProducts(EntityManager entityManager, String pivotProductId, Scroll scroll, int limit) {
        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();
        boolean isEmptyPivot = false;
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }

        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("EntityProduct.getUpwardDiscountProducts");
        } else {
            query = entityManager.createNamedQuery("EntityProduct.getDownwardDiscountProducts");
        }

        query.setParameter("pivotProductId", pivotProductId);
        query.setParameter("status", ProductStatus.AVAILABLE.getValue());

        try {
//            ArrayList<EntityProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            List<String> results = query.setMaxResults(limit).getResultList();
            if (results.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }

                return listReturnDTO;
            }
            List<EntityProduct> entityProducts = pushProduct(entityManager, results);
            if (scroll.equals(Scroll.UP)) {
//                Collections.reverse(results);
                Collections.reverse(entityProducts);
            }
//            listReturnDTO.setList(results);
            listReturnDTO.setList(entityProducts);
        } catch (Exception ex) {
            LOGGER.error("Exception while executing query to get non exclusive products: ", ex);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    public ListReturnDTO<String> getProductIdsOfShop(long shopId, String pivotProductId, Scroll scroll, int limit, ProductStatus status) {
        ListReturnDTO<String> productIds = new ListReturnDTO<>();
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            boolean isEmptyPivot = false;
            if (pivotProductId == null) {
                pivotProductId = Utility.EMPTY_DB_STRING;
                isEmptyPivot = true;
            }
            List<String> productIdsOfShop = getProductIdsOfShop(entityManager, shopId, pivotProductId, isEmptyPivot, scroll, limit, status);
            if (productIdsOfShop.isEmpty()) {
                if (isEmptyPivot) {
                    productIds.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    productIds.setReasonCode(ReasonCode.NO_MORE_DATA);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error in getProductIdsOfShop() : ", e);
            productIds.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        } finally {
            entityManager.close();
        }
        return productIds;
    }

    private List<String> getProductIdsOfShop(EntityManager entityManager, long shopId, String pivotProductId, boolean isEmptyPivot, Scroll scroll, int limit, ProductStatus status) {
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }
        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("EntityProduct.getOldestProductsOfShop");
            query.setParameter("pivotProductId", pivotProductId);
        } else if (isEmptyPivot) {
            query = entityManager.createNamedQuery("EntityProduct.getLatestProductsOfShopPivotEmpty");
        } else {
            query = entityManager.createNamedQuery("EntityProduct.getLatestProductsOfShop");
            query.setParameter("pivotProductId", pivotProductId);
        }

        query.setParameter("shopId", shopId);
        query.setParameter("status", status.getValue());

        return query.setMaxResults(limit).getResultList();
    }

    private ListReturnDTO<EntityProduct> getProductsOfShop(EntityManager entityManager, long shopId, String pivotProductId, Scroll scroll, int limit, ProductStatus status) {
        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();
        boolean isEmptyPivot = false;
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }

//        Query query;
//        if (scroll == Scroll.UP) {
//            query = entityManager.createNamedQuery("EntityProduct.getOldestProductsOfShop");
//            query.setParameter("pivotProductId", pivotProductId);
//        } else if (isEmptyPivot) {
//            query = entityManager.createNamedQuery("EntityProduct.getLatestProductsOfShopPivotEmpty");
//        } else {
//            query = entityManager.createNamedQuery("EntityProduct.getLatestProductsOfShop");
//            query.setParameter("pivotProductId", pivotProductId);
//        }
//
//        query.setParameter("shopId", shopId);
//        query.setParameter("status", status.getValue());
        try {
//            List<String> results = query.setMaxResults(limit).getResultList();
            List<String> results = getProductIdsOfShop(entityManager, shopId, pivotProductId, isEmptyPivot, scroll, limit, status);
            if (results.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }

                return listReturnDTO;
            }
            List<EntityProduct> entityProducts = pushProduct(entityManager, results);
            if (scroll.equals(Scroll.UP)) {
                Collections.reverse(results);
            }

//            listReturnDTO.setList(results);
            listReturnDTO.setList(entityProducts);
        } catch (Exception ex) {
            LOGGER.error("Exception in getProductsOfShop(): ", ex);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    public List<EntityProduct> getAllProducts() {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ArrayList<EntityProduct> results = null;
            Query query;
            query = entityManager.createNamedQuery("getAllProducts", EntityProduct.class);

            try {
                results = new ArrayList<>(query.getResultList());
            } catch (Exception ex) {
                LOGGER.error("Exception while executing getAllProducts: ", ex);
            }
            return results;

        } finally {
            entityManager.close();
        }
    }

    /**
     * returns nearest products of a given location Using primary key as index
     *
     * @param entityManager
     * @param lat(float)
     * @param lon(float)
     * @param radius(int, kilometer)
     * @param pivotProductId(Default Should be empty String)
     * @param scroll(ScrollEnum)
     * @param limit(int)
     * @return
     */
    private List<EntityProduct> getNearestLocaitonProducts(EntityManager entityManager, float lat, float lon, int radius, String pivotProductId, Scroll scroll, int limit) {
        boolean isEmptyPivot = false;
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }

        LatLon center = new LatLon(lat, lon);
        radius = radius * 1000;

        LatLon p1 = LocationHelper.getRadialCoordinate(center, radius, 0);
        LatLon p2 = LocationHelper.getRadialCoordinate(center, radius, 90);
        LatLon p3 = LocationHelper.getRadialCoordinate(center, radius, 180);
        LatLon p4 = LocationHelper.getRadialCoordinate(center, radius, 270);

        /**
         * select p.* from product p where p.id > ?1 AND p.lat > ?2 and p.lat <
         * ?3 AND p.lon < ?4 and p.lon > ?5 AND 6371 * 2 * ASIN(SQRT(
         * POWER(SIN((?6 - p.lat) * pi()/180 / 2), 2) + COS(?6 * pi()/180) *
         * COS(p.lat * pi()/180) * POWER(SIN((?7 -p.lon) * pi()/180 / 2), 2) ))
         * < ?8 ORDER BY p.id DESC
         */
        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("getUpwardNearestBoundingBoxProducts", EntityProduct.class);
        } else {
            query = entityManager.createNamedQuery("getDownwardNearestBoundingBoxProducts", EntityProduct.class);
        }

//        query.setParameter("x1", p3.x);
//        query.setParameter("x2", p1.x);
//        query.setParameter("y1", p2.y);
//        query.setParameter("y2", p4.y);
//        query.setParameter("pivotProductId", pivotProductId);
        query.setParameter(2, p3.x);
        query.setParameter(3, p1.x);
        query.setParameter(4, p2.y);
        query.setParameter(5, p4.y);
        query.setParameter(1, pivotProductId);
        query.setParameter(8, radius / 1000);
        query.setParameter(6, lat);
        query.setParameter(7, lon);

        try {
            ArrayList<EntityProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (results.size() <= 0) {
                return results;
            }
            if (scroll.equals(Scroll.UP)) {
                Collections.reverse(results);
            }
//            List<EntityProduct> temp = new ArrayList<>();
//            for (int i = 0; i < results.size(); i++) {
//                EntityProduct product = results.get(i);
//                LatLon checkPoint = new LatLon(product.getLat(), product.getLon());
//                if (LocationHelper.isCoordinateInsideCircle(center, checkPoint, radius)) {
//                    temp.add(product);
//                }
//            }
//            return temp;
            return results;
        } catch (Exception ex) {
            LOGGER.error("Exception while executing query to get nearest products: ", ex);
            return null;
        }
    }

//    /**
//     * returns numbers of products with similar 'name' Using primary key as
//     * index // these days grace
//     *
//     * @param name(String)
//     * @param pivotProductId(Default Should be empty String)
//     * @param scroll(ScrollEnum)
//     * @param limit(int)
//     * @return
//     */
//    protected List<EntityProduct> getSimilarNamedProducts(EntityManager entityManager, String name, String pivotProductId, Scroll scroll, int limit) {
//        if (pivotProductId == null) {
//            pivotProductId = "''";
//        }
//
//        Session session = DBConnection.getInstance().getSession();
//        QueryBuilder builder = session.getQueryBuilder();
//        QueryDomainType<IProduct> domain = builder.createQueryDefinition(IProduct.class);
//        Predicate conditionName = domain.get(MarketAttribute.NAME.getFullName()).like(domain.param("name"));
//        Query<IProduct> query = session.createQuery(domain);
//        query.setParameter("name", "%" + name + "%");
//
//        PredicateOperand columnProductId = domain.get(MarketAttribute.ID.getFullName());
//        PredicateOperand paramProductId = domain.param("productId");
//        Predicate conditionPivot = (scroll == Scroll.UP) ? columnProductId.lessThan(paramProductId) : columnProductId.greaterThan(paramProductId);
//        query.setParameter("productId", pivotProductId);
//
//        query.setLimits(0, limit);
//        query.setOrdering((scroll == Scroll.UP) ? Ordering.DESCENDING : Ordering.ASCENDING, MarketAttribute.ID.getFullName());
//        domain.where(conditionName.and(conditionPivot));
//
//        try {
//            List<IProduct> results = query.getResultList();
//            if (scroll.equals(Scroll.UP)) {
//                Collections.reverse(results);
//            }
//            session.close();
//            return results;
//        }
//        catch (Exception ex) {
//            LOGGER.error("Exception while executing query to get similar named products: ", ex);
//            session.close();
//            return null;
//        }
//    }
    /**
     * returns most viewed products list Using idx_view_count as index
     *
     * @param entityManager
     * @param pivotProductId(Default Should be empty String)
     * @param scroll(ScrollEnum)
     * @param limit(int)
     * @return
     */
    /*==========================In future, this functionality will go in solr====================*/
    protected ArrayList<EntityProduct> getMostViewedProducts(EntityManager entityManager, String pivotProductId, Scroll scroll, int limit) {
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
        }

        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("getUpwardMostViewedProducts", EntityProduct.class);
        } else {
            query = entityManager.createNamedQuery("getDownwardMostViewedProducts", EntityProduct.class);
        }

        query.setParameter("pivotProductId", pivotProductId);

        try {
            ArrayList<EntityProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (scroll.equals(Scroll.UP)) {
                Collections.reverse(results);
            }
            return results;
        } catch (Exception ex) {
            LOGGER.error("Exception while executing query to get featured products: ", ex);
            return null;
        }
    }

//    /**
//     * returns products ordered by view count Using primary key as index
//     *
//     * @param leastPrice(double)
//     * @param mostPrice(double)
//     * @param pivotProductId(Default Should be empty String)
//     * @param scroll(ScrollEnum)
//     * @param limit(int)
//     * @return
//     */
    private List<EntityProduct> getProductsInAPriceRange(EntityManager entityManager, double leastPrice, double mostPrice, String pivotProductId, Scroll scroll, int limit) {
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
        }

        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("getUpwardProductsInAPriceRange");
        } else {
            query = entityManager.createNamedQuery("getDownwardProductsInAPriceRange");
        }

        query.setParameter("pivotProductId", pivotProductId);
        query.setParameter("leastPrice", leastPrice);
        query.setParameter("mostPrice", mostPrice);

        try {
            ArrayList<EntityProduct> results = new ArrayList<>(query.setMaxResults(limit).getResultList());
            if (scroll.equals(Scroll.UP)) {
                Collections.reverse(results);
            }
            return results;
        } catch (Exception ex) {
            LOGGER.error("Exception while executing query to get featured products: ", ex);
            return null;
        }
    }

    @Override
    public ListReturnDTO<EntityProduct> getFiltererProducts(Integer categoryId, Long shopId, String pivotProductId, Scroll scroll, int limit, ProductStatus status) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            ListReturnDTO<EntityProduct> filtererProducts = getFiltererProducts(entityManager, categoryId, shopId, pivotProductId, scroll, limit, status);
            return filtererProducts;

        } finally {
            entityManager.close();
        }
    }

    private ListReturnDTO<EntityProduct> getFiltererProducts(EntityManager entityManager, Integer categoryId, Long shopId, String pivotProductId, Scroll scroll, int limit, ProductStatus status) {

        ListReturnDTO<EntityProduct> listReturnDTO = new ListReturnDTO<>();
        boolean isEmptyPivot = false;
        List<EntityProduct> filtererProducts;
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }
        Query createQuery;
        if (shopId != null && (categoryId == null || categoryId <= 0)) {

            switch (scroll) {
                case UP:
                    createQuery = entityManager.createNamedQuery("EntityProduct.productsOfShopScrollUp", EntityProduct.class);
                    break;
                default: {
                    if (isEmptyPivot) {
                        createQuery = entityManager.createNamedQuery("EntityProduct.productsOfShopScrollDownPivotEmpty", EntityProduct.class);
                    } else {
                        createQuery = entityManager.createNamedQuery("EntityProduct.productsOfShopScrollDown", EntityProduct.class);
                    }
                    break;
                }
            }
            createQuery.setParameter("shopId", shopId);
            if ((scroll == Scroll.DOWN && !isEmptyPivot) || scroll == Scroll.UP) {
                createQuery.setParameter("pivotId", pivotProductId);
            }
            createQuery.setParameter("status", status.getValue());
//            filtererProducts = createQuery.setMaxResults(limit).getResultList();

            List<String> results = createQuery.setMaxResults(limit).getResultList();
            filtererProducts = pushProduct(entityManager, results);
        } else {
//            ListReturnDTO<EntityProduct> categoryWiseProduct = MarketRepository.INSTANCE.getCategoryProductRepository().getCategoryWiseProduct(categoryId, pivotProductId, shopId, scroll, limit, status);

            ListReturnDTO<EntityProduct> categoryWiseProduct = categoryProductImpl.getCategoryWiseProduct(entityManager, categoryId, pivotProductId, shopId, scroll, limit, status);
            filtererProducts = categoryWiseProduct.getList();
        }
        if (filtererProducts.isEmpty()) {
            if (isEmptyPivot) {
                listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
            } else {
                listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
            }
        } else {
            listReturnDTO.setList(filtererProducts);
        }

        return listReturnDTO;

    }

//
//    /**
//     * returns products ordered by view count in a price range Using
//     * idx_view_count as index
//     *
//     * @param leastPrice(double)
//     * @param mostPrice(double)
//     * @param pivotProductId(Default Should be empty String)
//     * @param scroll(ScrollEnum)
//     * @param limit(int)
//     * @return
//     */
//    /*==========================In future, this functionality will go in solr====================*/
//    protected List<EntityProduct> getMostViewedProductsInAPriceRange(EntityManager entityManager, double leastPrice, double mostPrice, String pivotProductId, Scroll scroll, int limit) {
//        if (pivotProductId == null) {
//            pivotProductId = "''";
//        }
//
//        QueryBuilder builder = session.getQueryBuilder();
//        QueryDomainType<IProduct> domain = builder.createQueryDefinition(IProduct.class);
//        Query<IProduct> query = session.createQuery(domain);
//
//        PredicateOperand columnViewCount = domain.get(MarketAttribute.VIEW_COUNT.getFullName());
//        PredicateOperand paramViewCount = domain.param("count");
//        Predicate conditionCount = columnViewCount.greaterEqual(paramViewCount);
//        query.setParameter("count", 0);
//
//        PredicateOperand columnBasePrice = domain.get(MarketAttribute.BASE_PRICE.getFullName());
//        PredicateOperand paramLeastPrice = domain.param("leastPrice");
//        PredicateOperand paramMostPrice = domain.param("mostPrice");
//        Predicate conditionPrice = columnBasePrice.between(paramLeastPrice, paramMostPrice);
//        query.setParameter("leastPrice", leastPrice);
//        query.setParameter("mostPrice", mostPrice);
//
//        PredicateOperand columnProductId = domain.get(MarketAttribute.ID.getFullName());
//        PredicateOperand paramProductId = domain.param("productId");
//        Predicate conditionPivot = (scroll == Scroll.UP) ? columnProductId.lessThan(paramProductId) : columnProductId.greaterThan(paramProductId);
//        query.setParameter("productId", pivotProductId);
//
//        query.setLimits(0, limit);
//        query.setOrdering((scroll == Scroll.UP) ? Ordering.DESCENDING : Ordering.ASCENDING, MarketAttribute.ID.getFullName());
//
//        domain.where(conditionCount.and(conditionPrice.and(conditionPivot)));
//
//        query.setOrdering(Query.Ordering.DESCENDING, "viewCount");
//
//        try {
//            List<IProduct> results = query.getResultList();
//            if (scroll.equals(Scroll.UP)) {
//                Collections.reverse(results);
//            }
//            return results;
//        }
//        catch (Exception ex) {
//            LOGGER.error("Exception while executing query: ", ex);
//            return null;
//        }
//    }
    public List<EntityProduct> getProductByIds(Collection<String> productIds) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            Query query = entityManager.createNamedQuery("productIds", EntityProduct.class);
            query.setParameter("productIds", productIds);

            List<EntityProduct> entityProducts = query.getResultList();
            return entityProducts;

        } finally {
            entityManager.close();
        }
    }

    public int updateProductStatusByShop(EntityManager entityManager, ProductStatus updatedStatus, ProductStatus previousStatus, Long shopId, Long activistId) {

        int reasonCode = ReasonCode.NONE;
        try {
            Query query = entityManager.createNamedQuery("EntityProduct.updateStatusByShop");
            query.setParameter("updatedStatus", updatedStatus.getValue());
            query.setParameter("status", previousStatus.getValue());
            query.setParameter("shopId", shopId);
            entityManager.getTransaction().begin();
            query.executeUpdate();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Exception in updateProductStatusByShop(): {updatedStatus:" + updatedStatus.getValue() + ", previousStatus:" + previousStatus + ", shopId:" + shopId + " , activistId:" + activistId + "}", e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    private EntityProduct getProductByCode(EntityManager entityManager, String code) {
        EntityProduct entityProduct = null;
        try {
            Query query = entityManager.createNamedQuery("EntityProduct.getProductByCode", EntityProduct.class);
            query.setParameter("code", code);
            List<String> resultList = query.getResultList();
            if (!resultList.isEmpty()) {
                entityProduct = getProductById(entityManager, resultList.get(0));
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getProductByCode(): {code:" + code + "}", e);
        }
        return entityProduct;
    }

    @Override
    public EntityProduct getProductByCode(String code) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProduct productByCode = getProductByCode(entityManager, code);
            return productByCode;

        } finally {
            entityManager.close();
        }
    }

    @Override
    public PivotedListReturnDTO<EntityProduct, String> searchProductByName(String pivotProductId, Scroll scroll, int limit, String param) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
//            PivotedListReturnDTO<EntityProduct, String> searchProductByName = searchProductByName(entityManager, pivotProductId, scroll, limit, param);
            PivotedListReturnDTO<EntityProduct, String> searchProductByName = searchText(entityManager, pivotProductId, scroll, limit, param);
            return searchProductByName;

        } finally {
            entityManager.close();
        }
    }

    private PivotedListReturnDTO<EntityProduct, String> searchProductByName(EntityManager entityManager, String pivotProductId, Scroll scroll, int limit, String param) {
        PivotedListReturnDTO<EntityProduct, String> listReturnDTO = new PivotedListReturnDTO<>();
        boolean isEmptyPivot = false;
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }

        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("EntityProduct.searchByNameScrollUp");
        } else {
            query = entityManager.createNamedQuery("EntityProduct.searchByNameScrollDown");
        }

        query.setParameter("pivotProductId", pivotProductId);
        query.setParameter("status", ProductStatus.AVAILABLE.getValue());
        query.setParameter("param", "%" + param.trim().toUpperCase() + "%");

        try {
//            List<EntityProduct> results = query.setMaxResults(limit).getResultList();
            List<String> results = query.setMaxResults(limit).getResultList();
            if (results.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }
            } else {
                List<EntityProduct> entityProducts = pushProduct(entityManager, results);
//                pivotProductId = results.get(results.size() - 1).getId();
                pivotProductId = entityProducts.get(results.size() - 1).getId();
//                listReturnDTO.setList(results);
                listReturnDTO.setList(entityProducts);
            }
        } catch (Exception ex) {
            LOGGER.error("Exception while executing query to get featured products: ", ex);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        listReturnDTO.setPivot(pivotProductId);
        return listReturnDTO;
    }

    private PivotedListReturnDTO<EntityProduct, String> searchText(EntityManager entityManager, String pivotProductId, Scroll scroll, int limit, String param) {
        PivotedListReturnDTO<EntityProduct, String> listReturnDTO = new PivotedListReturnDTO<>();
        boolean isEmptyPivot = false;
        if (pivotProductId == null) {
            pivotProductId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }
        String text;
        String[] splits = param.trim().split(" ");
        if (splits.length > 1) {
            StringBuilder stringBuilder = new StringBuilder();
            for (String split : splits) {
                stringBuilder.append("*" + split + "* ");
            }
            text = stringBuilder.toString().trim();
        } else {
            text = "*" + param + "*";
        }
        LOGGER.info("Search text = " + text);
        Query query;
        if (scroll == Scroll.UP) {
            query = entityManager.createNamedQuery("EntityProduct.searchFullTextScrollUp");
        } else {
            query = entityManager.createNamedQuery("EntityProduct.searchFullTextScrollDown");
        }

        query.setParameter(1, text);
        query.setParameter(2, pivotProductId);
        query.setParameter(3, ProductStatus.AVAILABLE.getValue());

        try {
//            List<EntityProduct> results = query.setMaxResults(limit).getResultList();
            List<String> results = query.setMaxResults(limit).getResultList();
            List<EntityProduct> entityProducts = pushProduct(entityManager, results);
            if (results.size() != entityProducts.size()) {
                DBConnection.getInstance().getQueryCache().evict(query);
            }
            if (entityProducts.isEmpty()) {
                if (isEmptyPivot) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }
            } else {
//                pivotProductId = results.get(results.size() - 1).getId();
                pivotProductId = entityProducts.get(results.size() - 1).getId();
//                listReturnDTO.setList(results);
                listReturnDTO.setList(entityProducts);
            }
        } catch (Exception ex) {
            LOGGER.error("Exception while executing query to get featured products: ", ex);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        listReturnDTO.setPivot(pivotProductId);
        return listReturnDTO;
    }

    public void evictProductOfShop(EntityManager entityManager, long shopId) {
        Query query = entityManager.createNamedQuery("EntityProduct.getProductsOfShop");
        query.setParameter("shopId", shopId);
        List<String> productIds = query.getResultList();
        DBConnection.getInstance().getCache().evictAll(EntityProduct.class, productIds);
    }

    public int updateShopName(EntityManager entityManager, long shopId, String shopName) {
        Query query = entityManager.createNamedQuery("EntityProduct.updateShopName");
        query.setParameter("name", shopName);
        query.setParameter("shopId", shopId);
        int executeUpdate = query.executeUpdate();
        return executeUpdate;
    }

    public void update(EntityManager em, EntityProduct entityProduct) {
        em.merge(entityProduct);
        DBConnection.getInstance().getCache().evict(EntityProduct.class, entityProduct.getId());
    }

    public int enableDiscount(EntityManager em, String productId, boolean discounted) {
        int reasonCode = ReasonCode.NONE;
        try {
            EntityProduct product = em.find(EntityProduct.class, productId);
            if (product != null) {
                product.setIsDiscounted(discounted);
                em.merge(product);
                DBConnection.getInstance().getCache().evict(EntityProduct.class, productId);
            } else {
                reasonCode = ReasonCode.INVALID_INFORMATION;
            }
        } catch (Exception e) {
            LOGGER.error("Error in enableDiscount () :{productId : " + productId + ", discounted: " + discounted + "}", e);
            reasonCode = ReasonCode.OPERATION_FAILED;
        }
        return reasonCode;
    }

    @Retry(retryAttempts = 3, sleepInterval = 30)
    public void disableDiscountProducts(List<EntityProduct> products) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode = ReasonCode.NONE;
        try {
            disableDiscount(entityManager, products);

        } catch (OptimisticLockException e) {
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            LOGGER.error("OptimasticLock Exception. M - " + e.getMessage(), e);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            throw e;
        } catch (Exception ex) {
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            LOGGER.error("Discount disable failed. ", ex);
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }

        } finally {
            entityManager.close();
        }
    }

    private void disableDiscount(EntityManager em, List<EntityProduct> products) {

        int cursor = 0;
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        for (EntityProduct product : products) {
            if (product != null) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("R - productIds:" + product.getId());
                }
                product = getProductById(em, product.getId());
                product.setIsDiscounted(false);
                em.persist(product);
                EntityProductDiscount discount = productDiscountRepository.find(em, product.getId());
                em.remove(discount);
                cursor = +2;
                DBConnection.getInstance().getCache().evict(EntityProductDiscount.class, product.getId());
                DBConnection.getInstance().getCache().evict(EntityProduct.class, product.getId());
            }
            if (cursor > 0 && cursor % DBConnection.getInstance().BATCH_SIZE == 0) {
                em.getTransaction().commit();
                em.clear();
                em.getTransaction().begin();
            }
        }

        em.getTransaction().commit();
    }

}
