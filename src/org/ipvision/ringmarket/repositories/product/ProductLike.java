package org.ipvision.ringmarket.repositories.product;

import javax.persistence.EntityManager;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityProductLike;
import org.ipvision.ringmarket.entities.id.ProductLikeId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class ProductLike implements ProductLikeRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductLike.class);

    public boolean addToLikeList(long buyerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addToLikeList(entityManager, buyerId, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean addToLikeList(EntityProductLike entityProductLike) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addToLikeList(entityManager, entityProductLike);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean removeFromLikeList(long buyerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = removeFromLikeList(entityManager, buyerId, productId);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public EntityProductLike findLikedByBuyer(long buyerId, String productId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityProductLike entityProductView = findLikedByBuyer(entityManager, buyerId, productId);
            return entityProductView;
        } finally {
            entityManager.close();
        }
    }

    protected boolean addToLikeList(EntityManager entityManager, long buyerId, String productId) {
        EntityProductLike entityProductLike = new EntityProductLike();
        entityProductLike.setBuyerId(buyerId);
        entityProductLike.setProductId(productId);
        return addToLikeList(entityManager, entityProductLike);
    }

    protected boolean addToLikeList(EntityManager entityManager, EntityProductLike entityProductLike) {
        boolean susc = false;
        try {
            entityManager.persist(entityProductLike);
            susc = true;
            //update like count on product_detail table
//            ProductDetail productDetail = new ProductDetail();
//            susc = productDetail.updateProductLikeCount(entityManager, entityProductLike.getProductId());
            if (!susc) {
                LOGGER.error("Error in addToLikeList. productId: " + entityProductLike.getProductId());
                return susc;
            }
        } catch (Exception ex) {
            LOGGER.error("Error in addToLikeList. productId: " + entityProductLike.getProductId(), ex);
        }
        return susc;
    }

    protected boolean removeFromLikeList(EntityManager entityManager, long buyerId, String productId) {
        EntityProductLike entityProductLike = findLikedByBuyer(entityManager, buyerId, productId);
        boolean susc = false;
        if (entityProductLike != null) {
            try {
                entityManager.remove(entityProductLike);
                susc = true;
                //decrease like count on product_detail table
//                ProductDetail productDetail = new ProductDetail();
//                susc = productDetail.decreaseProductLikeCount(entityManager, entityProductLike.getProductId());
                if (!susc) {
                    LOGGER.error("Error in removeFromLikeList. productId: " + entityProductLike.getProductId());
                    return susc;
                }
            } catch (Exception ex) {
                LOGGER.error("Error in removeFromLikeList. productId: " + entityProductLike.getProductId(), ex);
            }
        }
        return susc;
    }

    protected EntityProductLike findLikedByBuyer(EntityManager entityManager, long buyerId, String productId) {
        return entityManager.find(EntityProductLike.class, new ProductLikeId(buyerId, productId));
    }
}
