/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.EntityHistoryProduct;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Imran
 */
public class HistoryProduct implements HistoryProductRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(HistoryProduct.class);

    @Override
    public boolean addNewHistory(EntityProduct entityProduct, double soldPrice, ProductStatus productStatus) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addNewHistory(entityManager, entityProduct, soldPrice, productStatus);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean addNewHistory(EntityHistoryProduct entityHistoryProduct) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = addNewHistory(entityManager, entityHistoryProduct);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public boolean removeProductFromHistory(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        boolean susc;
        try {
            entityManager.getTransaction().begin();
            susc = removeProductFromHistory(entityManager, id);
            if (susc) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return susc;
    }

    public EntityHistoryProduct getProductFromHistory(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityHistoryProduct iHistoryProduct;
        try {
            iHistoryProduct = getProductFromHistory(entityManager, id);
        } finally {
            entityManager.close();
        }
        return iHistoryProduct;
    }

    public List<EntityHistoryProduct> getCategoryWiseProductHistory(int catId) {
        List<EntityHistoryProduct> results;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            results = getCategoryWiseProductHistory(entityManager, catId);
        } finally {
            entityManager.close();
        }
        return results;
    }

    protected boolean addNewHistory(EntityManager entityManager, EntityProduct entityProduct, double soldPrice, ProductStatus productStatus) {
        EntityHistoryProduct entityHistoryProduct = new EntityHistoryProduct();
        entityHistoryProduct.setId(entityProduct.getId());
        entityHistoryProduct.setImageHeight(entityProduct.getImageHeight());
        entityHistoryProduct.setImageWidth(entityProduct.getImageWidth());
        entityHistoryProduct.setName(entityProduct.getName());
        entityHistoryProduct.setSoldPrice(soldPrice);
        entityHistoryProduct.setStatus(productStatus);
        return addNewHistory(entityManager, entityHistoryProduct);
    }

    protected boolean addNewHistory(EntityManager entityManager, EntityHistoryProduct entityHistoryProduct) {
        try {
            entityManager.persist(entityHistoryProduct);
        } catch (Exception ex) {
            LOGGER.error("Error in addNewHistory in table history_product: {ProductId: " + entityHistoryProduct.getId() + "} ", ex);
            return false;
        }
        return true;
    }

    protected boolean removeProductFromHistory(EntityManager entityManager, String id) {
        try {
            entityManager.remove(entityManager.find(EntityHistoryProduct.class, id));
        } catch (Exception ex) {
            LOGGER.error("Error in removeProductFromHistory in table history_product: {ProductId: " + id + "} ", ex);
            return true;
        }
        return true;
    }

    protected EntityHistoryProduct getProductFromHistory(EntityManager entityManager, String id) {
        try {
            return entityManager.find(EntityHistoryProduct.class, id);
        } catch (Exception ex) {
            LOGGER.error("Error in getProductFromHistory: ", ex);
            return null;
        }
    }

    protected List<EntityHistoryProduct> getCategoryWiseProductHistory(EntityManager entityManager, int catId) {
        Query query = entityManager.createNamedQuery("getCategoryWiseProductHistory");
        query.setParameter("categoryId", catId);

        List<EntityHistoryProduct> categoryProducts = query.getResultList();
        return categoryProducts;
    }

}
