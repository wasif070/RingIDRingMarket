/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import javax.persistence.EntityManager;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.order.EntityInvoiceIdGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class InvoiceIdGenerartor {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceIdGenerartor.class);

    static Integer getInvoiceId() {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityInvoiceIdGenerator idGenerator = new EntityInvoiceIdGenerator();

        try {
            try {
                entityManager.getTransaction().begin();
                entityManager.persist(idGenerator);
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                LOGGER.error("Exception getInvoiceId()", e);
            }

        } finally {
            entityManager.close();
        }
        return idGenerator.getId();
    }

}
