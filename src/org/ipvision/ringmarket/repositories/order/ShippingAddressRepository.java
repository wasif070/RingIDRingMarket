/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import com.google.gson.JsonObject;
import org.ipvision.ringmarket.entities.order.Address;
import org.ipvision.ringmarket.entities.order.EntityShippingAddress;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;

/**
 *
 * @author saikat
 */
public interface ShippingAddressRepository {

    ObjectReturnDTO<String> addShippingAddress(Long userId, Address address, boolean isDefault , byte type);

    ListReturnDTO<EntityShippingAddress> getShippingAddresses(long userId);

    int removeShippingAddress(String id);

    int updateShippingAddress(String id, Address address);

    int setDefault(String id);
    
    ListReturnDTO<EntityShippingAddress> getNearestRingStores(float lat, float lon, byte type);
    
    EntityShippingAddress findAddressById(String id);

}
