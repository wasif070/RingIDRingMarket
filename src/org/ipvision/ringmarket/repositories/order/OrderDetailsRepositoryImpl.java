/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.MarketOrderStatus;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.order.EntityOrderDetails;
import org.ipvision.ringmarket.entities.id.OrderProductId;
import org.ipvision.ringmarket.entities.order.EntityHistoryOrderStatus;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.product.ProductStockRepositoryImpl;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.ipvision.ringmarket.utils.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class OrderDetailsRepositoryImpl implements OrderDetailsRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDetailsRepositoryImpl.class);

    void addOrderDetails(EntityManager entityManager, List<EntityOrderDetails> orderDetailses) {
        orderDetailses.forEach((orderDetails) -> {
            entityManager.persist(orderDetails);
        });
    }

    void addOrderDetails(EntityManager entityManager, EntityOrderDetails orderDetails) {
        entityManager.persist(orderDetails);
    }

    List<EntityOrderDetails> getOrderDetails(EntityManager entityManager, Collection<String> orderIds) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - orderIds:" + orderIds);
        }
        Query query = entityManager.createNamedQuery("EntityOrderDetails.getMultipleOrderDetails", EntityOrderDetails.class);
        query.setParameter("idList", orderIds);
        return query.getResultList();
    }

    @Override
    public ListReturnDTO<EntityOrderDetails> getOrderDetails(String orderId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ListReturnDTO<EntityOrderDetails> orderDetails;
        try {
            orderDetails = getOrderDetails(entityManager, orderId);
        } finally {
            entityManager.close();
        }
        return orderDetails;
    }

    ListReturnDTO<EntityOrderDetails> getOrderDetails(EntityManager entityManager, String orderId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - orderId:" + orderId);
        }
        ListReturnDTO<EntityOrderDetails> returnDTO = new ListReturnDTO<>();
        try {
            Query query = entityManager.createNamedQuery("EntityOrderDetails.getOrderDetails", EntityOrderDetails.class);
            query.setParameter("id", orderId);
            List<EntityOrderDetails> itemList = query.getResultList();
            if (itemList.isEmpty()) {
                returnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
            } else {
                returnDTO.setList(itemList);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getOrderDetails() : {orderId:" + orderId + "}", e);
            returnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return returnDTO;
    }

    int updateItemStatusByOrderIds(EntityManager entityManager, Collection<String> orderIds, Byte status) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - orderIds:" + orderIds + ",status:" + status);
        }

        int reasonCode = ReasonCode.OPERATION_FAILED;
        String orderIdsString = Utility.stringListToString(orderIds);
        String sql = "UPDATE order_details SET item_status = " + status + " WHERE order_id in (" + orderIdsString + ");";
        Query query = entityManager.createNativeQuery(sql);
        int executeUpdate = query.executeUpdate();
        if (executeUpdate > 0) {
            DBConnection.getInstance().getQueryCache().evictAll(EntityOrderDetails.class);
            reasonCode = ReasonCode.NONE;
        }
        return reasonCode;
    }

    private EntityOrderDetails findById(EntityManager entityManager, OrderProductId orderProductId) {
        return entityManager.find(EntityOrderDetails.class, orderProductId);
    }

    private int updateOrderDetailStatus(EntityManager entityManager, Long activistId, String orderId, Collection<String> productIds, byte itemStatus) {
        int reasonCode = ReasonCode.NONE;
        EntityManager stockManager = DBConnection.getInstance().getEntityManager();

        try {
            List<EntityOrderDetails> orderDetailses = new LinkedList<>();

            for (String productId : productIds) {
                EntityOrderDetails findById = findById(entityManager, new OrderProductId(orderId, productId));
                if (findById == null) {
                    reasonCode = ReasonCode.INVALID_INFORMATION;
                    break;
                }
                orderDetailses.add(findById);
            }
            if (reasonCode == ReasonCode.NONE) {
                entityManager.getTransaction().begin();
                List<EntityHistoryOrderStatus> historyOrderStatuses = new LinkedList<>();
                Long time = System.currentTimeMillis();

                for (EntityOrderDetails orderDetails : orderDetailses) {
                    orderDetails.setItemStatus(itemStatus);
                    entityManager.merge(orderDetails);
                    EntityHistoryOrderStatus ehos = new EntityHistoryOrderStatus(TimeUUID.timeBased().toString(), orderId, orderDetails.getProductId(), activistId, itemStatus, time);
                    historyOrderStatuses.add(ehos);
                }

                if (MarketOrderStatus.ON_SHIPMENT.getValue() == itemStatus) {
                    Map<String, Integer> productQuantityMap = new HashMap<>();

                    for (EntityOrderDetails orderDetail : orderDetailses) {
                        productQuantityMap.put(orderDetail.getProductId(), orderDetail.getQuantity());
                    }
                    ProductStockRepositoryImpl stockRepositoryImpl = (ProductStockRepositoryImpl) MarketRepository.INSTANCE.getProductStockRepository();
                    int stockUpdate = stockRepositoryImpl.decreaseStock(stockManager, productQuantityMap);
                    if (stockUpdate != ReasonCode.NONE) {
                        throw new RuntimeException("Stock decrease failed with reason code = " + stockUpdate);
                    }
                }

                ListReturnDTO<EntityOrderDetails> orderDetails = getOrderDetails(entityManager, orderId);
                int sameStatusCount = 0;
                for (EntityOrderDetails entityOrderDetails : orderDetails.getList()) {
                    if (entityOrderDetails.getItemStatus() == itemStatus) {
                        sameStatusCount++;
                    }
                }
                if (sameStatusCount == orderDetails.getList().size()) {
                    OrderRepositoryImpl orderRepositoryImpl = new OrderRepositoryImpl();
                    orderRepositoryImpl.updateStatusByOrderId(entityManager, orderId, itemStatus);
                }
                entityManager.getTransaction().commit();

                Thread thread = new Thread(() -> {
                    MarketRepository.INSTANCE.getHistoryOrderStatusRepository().addHistory(historyOrderStatuses);
                });
                DBConnection.getInstance().submitAsyncJob(thread);
            } else {
                reasonCode = ReasonCode.NO_DATA_FOUND;
            }
        } catch (Exception e) {
            LOGGER.error("Exception in updateOrderDetailStatus() : {orderId:" + orderId + ", productIds:" + productIds + ", itemStatus:" + itemStatus + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            if (stockManager.getTransaction().isActive()) {
                stockManager.getTransaction().rollback();
            }
        }
        stockManager.close();
        return reasonCode;
    }

    @Override
    public ObjectReturnDTO<EntityOrderDetails> getOrderDetails(String orderId, String productId) {
        ObjectReturnDTO<EntityOrderDetails> returnDTO = new ObjectReturnDTO<>();
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                EntityOrderDetails findById = findById(entityManager, new OrderProductId(orderId, productId));
                if (findById != null) {
                    returnDTO.setValue(findById);
                } else {
                    returnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                }
            } catch (Exception e) {
                LOGGER.error("Exception in getOrderDetails() : {orderId:" + orderId + ", productId:" + productId + "}", e);
                returnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
            }
        } finally {
            entityManager.close();
        }
        return returnDTO;
    }

    @Override
    public int updateOrderDetailStatus(Long activistId, String orderId, Collection<String> productIds, byte itemStatus) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode;
        try {
            reasonCode = updateOrderDetailStatus(entityManager, activistId, orderId, productIds, itemStatus);
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    protected int deleteOrderDetailsByOrderIdFromDatabase(EntityManager entityManager, String orderId) {
        int reasonCode = ReasonCode.NONE;

        Query query = entityManager.createNamedQuery("EntityOrderDetails.deleteDetailsByOrderId");
        try {
            entityManager.getTransaction().begin();
            query.setParameter("id", orderId);
            int executeUpdate = query.executeUpdate();
            if (executeUpdate > 0) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        } catch (Exception e) {
            LOGGER.error("Exception in deleteOrderDetailsByOrderIdFromDatabase() : {orderId:" + orderId + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
        return reasonCode;
    }

    public int updateProductImageUrl(EntityManager entityManager, String productId, String imageUrl) {
        Query query = entityManager.createNamedQuery("EntityOrderDetails.updateProductImage");
        query.setParameter("url", imageUrl);
        query.setParameter("productId", productId);       
        return query.executeUpdate();
    }

}
