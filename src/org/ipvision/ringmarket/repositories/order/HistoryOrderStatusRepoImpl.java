/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.order.EntityHistoryOrderStatus;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class HistoryOrderStatusRepoImpl implements HistoryOrderStatusRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(HistoryOrderStatusRepoImpl.class);

    @Override
    public int addHistory(String orderId, String productId, byte status, long activistId, long time) {
        int reasonCode = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                entityManager.getTransaction().begin();
                add(entityManager, orderId, productId, status, activistId, time);
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                LOGGER.error("Exception in addHistory() ", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public int addHistory(Collection<EntityHistoryOrderStatus> orderStatusHistory) {

        int reasonCode = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            entityTransaction.begin();
            try {
                int i = 0;
                for (EntityHistoryOrderStatus entityHistoryOrderStatus : orderStatusHistory) {
                    if (i > 0 && i % DBConnection.getInstance().BATCH_SIZE == 0) {
                        entityTransaction.commit();
                        entityManager.clear();
                        entityTransaction.begin();
                    }
                    entityManager.persist(entityHistoryOrderStatus);
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("R - id:" + entityHistoryOrderStatus.getId() + ", orderId:" + entityHistoryOrderStatus.getOrderId() + ", productId:" + entityHistoryOrderStatus.getProductId());
                    }
                    i++;
                }
                entityTransaction.commit();
            } catch (Exception e) {
                LOGGER.error("Exception in addHistory() ", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
                if (entityTransaction.isActive()) {
                    entityTransaction.rollback();
                }
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    public void add(EntityManager entityManager, String orderId, String productId, byte status, long activistId, long time) {
        String id = TimeUUID.timeBased().toString();
        EntityHistoryOrderStatus historyOrderStatus = new EntityHistoryOrderStatus(id, orderId, productId, activistId, status, time);
        entityManager.persist(historyOrderStatus);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - id:" + historyOrderStatus.getId() + ", orderId:" + historyOrderStatus.getOrderId() + ", productId:" + historyOrderStatus.getProductId());
        }
    }
}
