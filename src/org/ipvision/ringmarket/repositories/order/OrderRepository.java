/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import javafx.util.Pair;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.dtos.orders.InvoiceDTO;
import org.ipvision.ringmarket.entities.order.EntityOrder;
import org.ipvision.ringmarket.entities.order.EntityOrderDetails;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;

/**
 *
 * @author saikat
 */
public interface OrderRepository {

    ListReturnDTO<EntityOrder> getOrderListByInvoiceId(String invoiceId);

    ObjectReturnDTO<String> placeOrder(Long activistId, Long userId, Integer quantity, EntityProduct product, Shipment shipmentMethod, String addressId, byte paymentMethod);

    int completePayment(Long activistId, String invoiceId, Byte paymentMode);

    PivotedListReturnDTO<EntityOrder, Pair<String, Long>> getFilteredOrders(Pair<String, Long> pivot, Long sellerId, Byte shipmentMethod,
            Collection<Byte> statusList, Byte paymentMethod, Scroll scroll, int limit);

    ObjectReturnDTO<String> orderItemsAndGetInvoiceId(Long activistId, Long buyerId, List<EntityCartItem> orders, Shipment shipment, String addressId, Byte paymentType);

    int updateOrderStatus(Long activistId, String orderId, Byte status);

    PivotedListReturnDTO<EntityOrder, Pair<String, Long>> getBuyerOrders(Long userId, Pair<String, Long> pivot, Scroll scroll, int limit);

    ObjectReturnDTO<EntityOrder> findOrder(String orderId);

    ListReturnDTO<InvoiceDTO> getInvoiceList(int start, int limit);

    int updateOrderStatus(Long activistId, Collection<String> orderIds, Byte status);

    ObjectReturnDTO<String> updateAndGetInvoiceId(String orderId, Long activistId);

}
