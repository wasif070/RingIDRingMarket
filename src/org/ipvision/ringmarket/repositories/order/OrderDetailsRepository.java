/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import java.util.Collection;
import org.ipvision.ringmarket.entities.order.EntityOrderDetails;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;

/**
 *
 * @author saikat
 */
public interface OrderDetailsRepository {

    ListReturnDTO<EntityOrderDetails> getOrderDetails(String orderId) ;
    int updateOrderDetailStatus(Long activistId, String orderId, Collection<String> productIds, byte itemStatus);
    ObjectReturnDTO<EntityOrderDetails> getOrderDetails(String orderId, String productId);
}
