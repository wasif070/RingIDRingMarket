/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import com.google.gson.Gson;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.DiscountUnit;
import org.ipvision.ringmarket.constants.MarketOrderStatus;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.dtos.orders.InvoiceDTO;
import org.ipvision.ringmarket.entities.order.EntityHistoryOrderStatus;
import org.ipvision.ringmarket.entities.order.EntityOrder;
import org.ipvision.ringmarket.entities.order.EntityOrderDetails;
import org.ipvision.ringmarket.entities.order.EntityShippingAddress;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.product.ProductStockRepositoryImpl;
import org.ipvision.ringmarket.repositories.shop.ShopRepositoryImpl;
import org.ipvision.ringmarket.repositories.user.cart.CartItemRepositoryImpl;
import org.ipvision.ringmarket.uniqueid.GeneratorException;
import org.ipvision.ringmarket.uniqueid.UniqueNumberGenerator;
import org.ipvision.ringmarket.utils.JpaConverterJson;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.ipvision.ringmarket.utils.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class OrderRepositoryImpl implements OrderRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderRepositoryImpl.class);
    private final ProductStockRepositoryImpl stockRepositoryImpl = new ProductStockRepositoryImpl();

    protected void addOrder(EntityManager entityManager, EntityOrder entityOrder) {
        entityManager.persist(entityOrder);
    }

    private ListReturnDTO<EntityOrder> getOrderListByInvoiceId(EntityManager entityManager, String invoiceId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("invoice id : " + invoiceId);
        }
        ListReturnDTO<EntityOrder> listReturnDTO = new ListReturnDTO<>();
        try {
            List<EntityOrder> entityOrders;
            Query query = entityManager.createNamedQuery("EntityOrder.getOrdersByInvoice", EntityOrder.class);
            query.setParameter("invoiceId", invoiceId);
            entityOrders = query.getResultList();
            if (entityOrders.isEmpty()) {
                listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
            } else {
                listReturnDTO.setList(entityOrders);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getOrderListByInvoiceId() : {invoiceId:" + invoiceId + "}", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    @Override
    public ListReturnDTO<EntityOrder> getOrderListByInvoiceId(String invoiceId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ListReturnDTO<EntityOrder> orderListByInvoiceId;
        try {
            orderListByInvoiceId = getOrderListByInvoiceId(entityManager, invoiceId);
        } finally {
            entityManager.close();
        }
        return orderListByInvoiceId;
    }

    public String orderCartItemsAndGetInvoiceId(EntityManager entityManager, Long activistId, List<EntityCartItem> cartItems, Shipment shipment, String addressId, Byte paymentType) {

        Map<Long, EntityOrder> ordersByShopMap = new HashMap<>();
        List<EntityOrderDetails> entityOrderDetailses = new ArrayList<>();
        Integer invoiceIdValue = InvoiceIdGenerartor.getInvoiceId();
        String invoiceId = null;
        if (invoiceIdValue != null) {
            invoiceId = String.valueOf(invoiceIdValue);
            EntityShippingAddress findAddressById = MarketRepository.INSTANCE.getShippingAddressRepository().findAddressById(addressId);
            LinkedHashMap address;
            if (findAddressById != null) {
//                JSONObject addr = new JSONObject(findAddressById.getAddress());
//                address = addr.toMap();
                address = JpaConverterJson.GSON.fromJson(JpaConverterJson.GSON.toJsonTree(findAddressById.getAddress()), JpaConverterJson.MAP_TYPE);
            } else {
                return null;
            }

            ShopRepositoryImpl shopRepositoryImpl = new ShopRepositoryImpl();

            List<EntityHistoryOrderStatus> historyOrderSatusList = new LinkedList<>();

            for (EntityCartItem cartItem : cartItems) {
                Long shopId = cartItem.getShopId();
                double accumulatedItemPrice;
                double perProductPrice;
                if (cartItem.getDiscountUnit() != null) {
                    if (cartItem.getDiscountUnit() == DiscountUnit.PERCENTAGE.getValue()) {
                        perProductPrice = cartItem.getPrice() - (cartItem.getPrice() * cartItem.getDiscount() / 100);
                    } else {
                        perProductPrice = cartItem.getPrice() - cartItem.getDiscount();
                    }
                } else {
                    perProductPrice = cartItem.getPrice();
                }
                accumulatedItemPrice = perProductPrice * cartItem.getQuantity();
                if (shipment == Shipment.HOME_DELIVERY) {
                    accumulatedItemPrice += cartItem.getShipmentPrice();
                }
                BigDecimal price = new BigDecimal(accumulatedItemPrice, MathContext.DECIMAL64);

                EntityOrderDetails details = new EntityOrderDetails();
                details.setCurrecyCode(cartItem.getCurrecyCode());
                details.setPrice(cartItem.getPrice());
                details.setProductId(cartItem.getProductId());
                details.setProductImage(cartItem.getProductImage());
                details.setProductName(cartItem.getProductName());
                details.setQuantity(cartItem.getQuantity());
                details.setShipmentPrice(cartItem.getShipmentPrice());
//                details.setShopId(cartItem.getShopId());
//                details.setShopName(cartItem.getShopName());
//                details.setUserId(cartItem.getUserId());
//                details.setTime(System.currentTimeMillis());
                details.setItemStatus(MarketOrderStatus.PAYMENT_INCOMPLETE.getValue());
//                details.setAddress(address);

                if (ordersByShopMap.containsKey(shopId)) {
                    ordersByShopMap.get(shopId).getPrice().add(price);
                } else {

                    try {
                        EntityShop shopById = shopRepositoryImpl.findShop(entityManager, cartItem.getShopId());
                        EntityOrder entityOrder = new EntityOrder();
                        entityOrder.setId(UniqueNumberGenerator.get());
                        entityOrder.setPrice(price);
                        entityOrder.setShopId(cartItem.getShopId());
                        entityOrder.setShipmentMethod(shipment.getValue());
//                    entityOrder.setAddressId(addressId);

                        entityOrder.setUserId(cartItem.getUserId());
                        entityOrder.setStatus(MarketOrderStatus.PAYMENT_INCOMPLETE.getValue());
                        entityOrder.setInvoiceId(invoiceId);
                        entityOrder.setPaymentMethod(paymentType);
                        entityOrder.setProtectionDuration(shopById.getProtectionDuration());
                        entityOrder.setCancelDuration(shopById.getCancelDuration());
                        entityOrder.setFeedbackDuration(shopById.getProtectionDuration());
                        entityOrder.setAddedTime(System.currentTimeMillis());
                        entityOrder.setAddress(address);
                        entityOrder.setShopName(cartItem.getShopName());

                        ordersByShopMap.put(shopId, entityOrder);
                    } catch (GeneratorException ex) {
                        LOGGER.error(ex.getLocalizedMessage(), ex);
                        return null;
                    }
                }

                details.setOrderId(ordersByShopMap.get(shopId).getId());
                entityOrderDetailses.add(details);
                String orderId = details.getOrderId();
                String productId = details.getProductId();
                Byte status = details.getItemStatus();
                EntityHistoryOrderStatus ehos = new EntityHistoryOrderStatus(TimeUUID.timeBased().toString(), orderId, productId, activistId, status, System.currentTimeMillis());
                historyOrderSatusList.add(ehos);

            }

            placeOrders(entityManager, ordersByShopMap.values());

            OrderDetailsRepositoryImpl detailsRepositoryImpl = new OrderDetailsRepositoryImpl();
            detailsRepositoryImpl.addOrderDetails(entityManager, entityOrderDetailses);

            Thread thread = new Thread(() -> {
                MarketRepository.INSTANCE.getHistoryOrderStatusRepository().addHistory(historyOrderSatusList);
            });
            DBConnection.getInstance().submitAsyncJob(thread);

        }

        return invoiceId;
    }

    public int itemStockCheck(EntityManager entityManager, Collection<EntityCartItem> items) {
        int reasonCode = ReasonCode.NONE;
        for (EntityCartItem item : items) {
            boolean available = stockRepositoryImpl.isAvailable(entityManager, item.getProductId());
            if (available) {
                reasonCode = stockRepositoryImpl.stockCheck(entityManager, item.getProductId(), item.getQuantity());
                if (reasonCode != ReasonCode.NONE) {
                    break;
                }
            } else {
                reasonCode = ReasonCode.RingMarket.UNAVAILABLE;
                break;
            }
        }
        return reasonCode;
    }

    @Override
    public ObjectReturnDTO<String> orderItemsAndGetInvoiceId(Long activistId, Long buyerId, List<EntityCartItem> orders, Shipment shipment, String addressId, Byte paymentType) {
        ObjectReturnDTO<String> objectReturnDTO = new ObjectReturnDTO<>();
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                int reasonCode = itemStockCheck(entityManager, orders);
                if (reasonCode == ReasonCode.NONE) {
                    entityManager.getTransaction().begin();
                    String invoiceId = orderItemsAndGetInvoiceId(entityManager, activistId, buyerId, orders, shipment, addressId, paymentType);
                    if (invoiceId != null) {
                        entityManager.getTransaction().commit();
                        objectReturnDTO.setValue(invoiceId);
                    } else {
                        entityManager.getTransaction().rollback();
                        objectReturnDTO.setReasonCode(ReasonCode.OPERATION_FAILED);
                    }
                } else {
                    objectReturnDTO.setReasonCode(reasonCode);
                }
            } catch (Exception e) {
                LOGGER.error("Exception in orderItemsAndGetInvoiceId() : {buyerId:" + buyerId + ", shipment:" + shipment + ", addressId:" + addressId + ", paymentType:" + paymentType + "}", e);
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
                objectReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
            }

        } finally {
            entityManager.close();
        }
        return objectReturnDTO;
    }

    private String orderItemsAndGetInvoiceId(EntityManager entityManager, Long activistId, Long buyerId, List<EntityCartItem> items, Shipment shipment, String addressId, Byte paymentType) {
        CartItemRepositoryImpl cartItemRepositoryImpl = new CartItemRepositoryImpl();
        for (EntityCartItem item : items) {
            item.setUserId(buyerId);
            cartItemRepositoryImpl.fillCartItemProperty(entityManager, item);
        }
        String invoiceId = orderCartItemsAndGetInvoiceId(entityManager, activistId, items, shipment, addressId, paymentType);

        return invoiceId;
    }

    protected void placeOrders(EntityManager entityManager, Collection<EntityOrder> entityOrders) {
        entityOrders.forEach((entityOrder) -> {
            entityManager.persist(entityOrder);
        });
    }

    @Override
    public ObjectReturnDTO<String> placeOrder(Long activistId, Long userId, Integer quantity, EntityProduct product, Shipment shipmentMethod, String addressId, byte paymentMethod) {
        ObjectReturnDTO<String> returnDTO = new ObjectReturnDTO<>();
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {

            int reasonCode = stockRepositoryImpl.stockCheck(entityManager, product.getId(), quantity);

            if (reasonCode == ReasonCode.NONE) {
                Integer invoiceId = InvoiceIdGenerartor.getInvoiceId();
                if (invoiceId != null) {

                    try {
                        String invoice = String.valueOf(invoiceId);

                        String orderId = UniqueNumberGenerator.get();
                        EntityOrderDetails details = new EntityOrderDetails();
                        details.setCurrecyCode(product.getCurrencyCode());
//                        details.setPrice(product.getPrice());
                        double currentPrice;
                        if (product.getDiscountUnit() != null) {
                            if (product.getDiscountUnit() == DiscountUnit.PERCENTAGE.getValue()) {
                                currentPrice = product.getPrice() - (product.getPrice() * product.getDiscount() / 100);
                            } else {
                                currentPrice = product.getPrice() - product.getDiscount();
                            }
                        } else {
                            currentPrice = product.getPrice();
                        }
                        details.setPrice(currentPrice);

                        details.setProductId(product.getId());
                        details.setProductImage(product.getImageUrl());
                        details.setProductName(product.getName());
                        details.setQuantity(quantity);
//                details.setShopId(product.getShopId());
//                details.setShopName(product.getShopName());
//                details.setUserId(userId);
                        details.setShipmentPrice(0d);
                        details.setOrderId(orderId);
//                details.setTime(System.currentTimeMillis());
                        details.setItemStatus(MarketOrderStatus.PAYMENT_INCOMPLETE.getValue());
                        EntityShippingAddress findAddressById = MarketRepository.INSTANCE.getShippingAddressRepository().findAddressById(addressId);
//                    details.setAddress(findAddressById.getAddress());

                        EntityOrder entityOrder = new EntityOrder();
                        entityOrder.setId(orderId);
                        entityOrder.setShopId(product.getShopId());
                        entityOrder.setUserId(userId);
                        entityOrder.setInvoiceId(invoice);
                        entityOrder.setShipmentMethod(shipmentMethod.getValue());
                        entityOrder.setPaymentMethod(paymentMethod);

                        ShopRepositoryImpl shopRepositoryImpl = new ShopRepositoryImpl();
                        EntityShop shopById = shopRepositoryImpl.findShop(entityManager, product.getShopId());
                        entityOrder.setProtectionDuration(shopById.getProtectionDuration());
                        entityOrder.setCancelDuration(shopById.getCancelDuration());
                        entityOrder.setFeedbackDuration(shopById.getProtectionDuration());
                        entityOrder.setAddedTime(System.currentTimeMillis());
//                    JSONObject address = new JSONObject(findAddressById.getAddress());
//                    entityOrder.setAddress(address.toMap());
                        entityOrder.setAddress(JpaConverterJson.GSON.fromJson(JpaConverterJson.GSON.toJsonTree(findAddressById.getAddress()), JpaConverterJson.MAP_TYPE));
                        entityOrder.setShopName(product.getShopName());

                        double price = details.getPrice() * details.getQuantity();
                        if (shipmentMethod == Shipment.HOME_DELIVERY) {
                            entityOrder.setPrice(new BigDecimal(price + details.getShipmentPrice(), MathContext.DECIMAL64));
//                    entityOrder.setAddressId(addressId);
                        } else {
                            entityOrder.setPrice(new BigDecimal(price, MathContext.DECIMAL64));
                        }
                        entityOrder.setStatus(MarketOrderStatus.PAYMENT_INCOMPLETE.getValue());

                        entityManager.getTransaction().begin();

                        placeOrder(entityManager, entityOrder);
                        OrderDetailsRepositoryImpl detailsRepositoryImpl = new OrderDetailsRepositoryImpl();
                        detailsRepositoryImpl.addOrderDetails(entityManager, details);

                        entityManager.getTransaction().commit();
                        returnDTO.setValue(invoice);

                        Byte status = entityOrder.getStatus();
                        EntityHistoryOrderStatus ehos = new EntityHistoryOrderStatus(TimeUUID.timeBased().toString(), orderId, product.getId(), activistId, status, System.currentTimeMillis());
                        List<EntityHistoryOrderStatus> historyOrderSatusList = new LinkedList<>();
                        historyOrderSatusList.add(ehos);
                        Thread thead = new Thread(() -> {
                            MarketRepository.INSTANCE.getHistoryOrderStatusRepository().addHistory(historyOrderSatusList);
                        });
                        DBConnection.getInstance().submitAsyncJob(thead);
                    } catch (Exception e) {
                        LOGGER.error("Exception in placeOrder() : {userId:" + userId + ", quantity:" + quantity + ", product:" + new Gson().toJson(product) + "}", e);
                        returnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
                    }
                } else {
                    returnDTO.setReasonCode(ReasonCode.OPERATION_FAILED);
                }
            } else {
                returnDTO.setReasonCode(reasonCode);
            }

        } finally {
            entityManager.close();
        }
        return returnDTO;
    }

    private void placeOrder(EntityManager entityManager, EntityOrder entityOrder) {
        entityManager.persist(entityOrder);
    }

    @Override
    public int completePayment(Long activistId, String invoiceId, Byte paymentMethod) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode;
        try {
            entityManager.getTransaction().begin();
            reasonCode = paymentComplete(entityManager, invoiceId, MarketOrderStatus.PAYMENT_COMPLETE.getValue(), paymentMethod);
            List<EntityOrderDetails> orderDetails;
            OrderDetailsRepositoryImpl detailsRepositoryImpl = null;
            List<String> orderIds = null;
            if (reasonCode == ReasonCode.NONE) {
                detailsRepositoryImpl = new OrderDetailsRepositoryImpl();
                ListReturnDTO<EntityOrder> orderListByInvoiceId = getOrderListByInvoiceId(entityManager, invoiceId);
                List<EntityOrder> orders = orderListByInvoiceId.getList();
                orderIds = new LinkedList<>();

                for (EntityOrder order : orders) {
                    orderIds.add(order.getId());
                }
                if (!orderIds.isEmpty()) {
                    reasonCode = detailsRepositoryImpl.updateItemStatusByOrderIds(entityManager, orderIds, MarketOrderStatus.PAYMENT_COMPLETE.getValue());

                } else {
                    reasonCode = ReasonCode.OPERATION_FAILED;
                }

            }
            if (reasonCode == ReasonCode.NONE) {

                entityManager.getTransaction().commit();

                orderDetails = detailsRepositoryImpl.getOrderDetails(entityManager, orderIds);
                Thread thread = new Thread(() -> {
                    List<EntityHistoryOrderStatus> historyOrderStatuses = new LinkedList<>();
                    for (EntityOrderDetails orderDetail : orderDetails) {
                        EntityHistoryOrderStatus ehos = new EntityHistoryOrderStatus(TimeUUID.timeBased().toString(), orderDetail.getOrderId(), orderDetail.getProductId(), activistId, MarketOrderStatus.PAYMENT_COMPLETE.getValue(), System.currentTimeMillis());
                        historyOrderStatuses.add(ehos);
                    }
                    MarketRepository.INSTANCE.getHistoryOrderStatusRepository().addHistory(historyOrderStatuses);
                });

                DBConnection.getInstance().submitAsyncJob(thread);

            } else {
                entityManager.getTransaction().rollback();
            }

        } catch (Exception e) {
            LOGGER.error("Exception in completePayment() : {invoiceId:" + invoiceId + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    private int paymentComplete(EntityManager entityManager, String invoiceId, Byte status, Byte paymentMode) {
        Query query = entityManager.createNamedQuery("EntityOrder.paymentCompleteByInvoice");
        query.setParameter("status", status);
        query.setParameter("invoiceId", invoiceId);
        query.setParameter("paymentTime", System.currentTimeMillis());
        query.setParameter("paymentMethod", paymentMode);

        int executeUpdate = query.executeUpdate();
        return executeUpdate > 0 ? ReasonCode.NONE : ReasonCode.OPERATION_FAILED;
    }

    @Override
    public PivotedListReturnDTO<EntityOrder, Pair<String, Long>> getFilteredOrders(Pair<String, Long> pivot, Long sellerId, Byte shipmentMethod, Collection<Byte> statusList,
            Byte paymentMethod, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        PivotedListReturnDTO<EntityOrder, Pair<String, Long>> filteredSellerOrders;
        try {
            filteredSellerOrders = getFilteredOrders(entityManager, pivot, sellerId, shipmentMethod,
                    statusList, paymentMethod, scroll, limit);
        } finally {
            entityManager.close();
        }
        return filteredSellerOrders;
    }

    protected PivotedListReturnDTO<EntityOrder, Pair<String, Long>> getFilteredOrders(EntityManager entityManager, Pair<String, Long> pivot,
            Long sellerId, Byte shipmentMethod,
            Collection<Byte> statusList, Byte paymentMethod, Scroll scroll, int limit) {
        PivotedListReturnDTO<EntityOrder, Pair<String, Long>> pivotedListReturnDTO = new PivotedListReturnDTO<>();
//        if (pivotId == null) {
//            pivotId = " '' ";
//        }
//        String sql = "SELECT * FROM order_list where";
//        if (scroll == Scroll.DOWN) {
//            sql += " id >" + pivotId;
//        } else {
//            sql += " id <" + pivotId;
//        }
//        
//        if (sellerId != null) {
//            sql += " AND shop_id = " + sellerId;
//        }
//        
//        if (shipmentMethod != null) {
//            sql += " AND shipment_method = " + shipmentMethod;
//        }
//        
//        if (paymentMethod != null) {
//            sql += " AND payment_method = " + paymentMethod;
//        }
//        
//        if (statusList != null && !statusList.isEmpty()) {
//            String ByteListToString = Utility.ByteListToString(statusList);
//            sql += " AND status IN  (" + ByteListToString + ")";
//        }
//        
//        if (scroll == Scroll.DOWN) {
//            sql += " ORDER BY id";
//        } else {
//            sql += " ORDER BY id desc";
//        }
        String pivotId = pivot.getKey();
        Long pivotTime = pivot.getValue();
        boolean isEmptyPivot = false;
        if (pivotId == null && pivotTime <= 0) {
            pivotId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }
//        String sql = "SELECT o.id FROM order_list o where";
        String sql;
        StringBuilder sqlBuilder = new StringBuilder("SELECT o.id FROM order_list o where");

        if (scroll == Scroll.DOWN) {
            if (isEmptyPivot) {
//                sql += " (o.added_time,o.id) >(?1 , ?2)";
                sqlBuilder.append(" (o.added_time,o.id) >(?1 , ?2)");
            } else {
//                sql += " (o.added_time,o.id) <(?1 , ?2)";
                sqlBuilder.append(" (o.added_time,o.id) <(?1 , ?2)");
            }
        } else {
//            sql += " (o.added_time,o.id) >(?1 , ?2)";
            sqlBuilder.append(" (o.added_time,o.id) >(?1 , ?2)");
        }

        if (sellerId != null) {
//            sql += " AND o.shop_id = " + sellerId;
            sqlBuilder.append(" AND o.shop_id = ").append(sellerId);
        }

        if (shipmentMethod != null) {
//            sql += " AND o.shipment_method = " + shipmentMethod;
            sqlBuilder.append(" AND o.shipment_method = ").append(shipmentMethod);
        }

        if (paymentMethod != null) {
//            sql += " AND o.payment_method = " + paymentMethod;
            sqlBuilder.append(" AND o.payment_method = ").append(paymentMethod);
        }

        if (statusList != null && !statusList.isEmpty()) {
//            String ByteListToString = Utility.ByteListToString(statusList);
//            sql += " AND o.status IN  (" + ByteListToString + ")";
            sqlBuilder.append(" AND o.status IN  (").append(Utility.ByteListToString(statusList)).append(")");
        }

        if (scroll == Scroll.DOWN) {
//            sql += " ORDER BY o.added_time desc , o.id desc";
            sqlBuilder.append(" ORDER BY o.added_time desc , o.id desc");
        } else {
//            sql += " ORDER BY o.added_time asc, o.id asc";
            sqlBuilder.append(" ORDER BY o.added_time asc , o.id asc");
        }
        sql = sqlBuilder.toString();
        try {
//            Query query = entityManager.createNativeQuery(sql, EntityOrder.class);
            Query query = entityManager.createNativeQuery(sql);
            query.setParameter(2, pivotId);
            query.setParameter(1, pivotTime);

//            List<EntityOrder> orders = query.setMaxResults(limit).getResultList();
            List<String> orderIds = query.setMaxResults(limit).getResultList();
            if (orderIds.isEmpty()) {
                if (isEmptyPivot) {
                    pivotId = null;
                    pivotedListReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    pivotedListReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }
            } else {
                List<EntityOrder> orders = new ArrayList<>();
                for (String orderId : orderIds) {
                    orders.add(findById(entityManager, orderId));
                }
                pivotedListReturnDTO.setList(orders);
                pivotId = orders.get(orders.size() - 1).getId();
                pivotTime = orders.get(orders.size() - 1).getAddedTime();
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getFilteredSellerOrders() : ", e);
            pivotedListReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        pivot = new Pair<>(pivotId, pivotTime);
        pivotedListReturnDTO.setPivot(pivot);
        return pivotedListReturnDTO;
    }

//    protected ListReturnDTO<EntityOrder> getInvoiceList(EntityManager entityManager, String invoiceId, Long userId, Long sellerId, Byte shipmentMethod,
//            Byte status, Byte paymentMethod) {
//        ListReturnDTO<EntityOrder> pivotedListReturnDTO = new ListReturnDTO<>();
//
//        String sql = "SELECT * FROM order_list where";
//
//        if (invoiceId != null) {
//            sql += " AND invoice_id = " + invoiceId;
//        }
//        
//        if (sellerId != null) {
//            sql += " AND shop_id = " + sellerId;
//        }
//
//        if (userId != null) {
//            sql += " AND user_id = " + userId;
//        }
//
//        if (shipmentMethod != null) {
//            sql += " AND shipment_method = " + shipmentMethod;
//        }
//
//        if (paymentMethod != null) {
//            sql += " AND payment_method = " + paymentMethod;
//        }
//
//        if (status != null) {
//            sql += " AND status =" + status;
//        }
//
//            sql += " GROUP BY invoice_id";
//
//        try {
//            Query query = entityManager.createNativeQuery(sql, EntityOrder.class);
//            List<EntityOrder> orders = query.getResultList();
//            if (orders.isEmpty()) {
//                    pivotedListReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
//            } else {
//                pivotedListReturnDTO.setList(orders);
//            }
//        } catch (Exception e) {
//            LOGGER.error("Exception in getFilteredSellerOrders() : ", e);
//            pivotedListReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
//        }
//        return pivotedListReturnDTO;
//    }
    @Override
    public int updateOrderStatus(Long activistId, String orderId, Byte status) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode;
        try {
            reasonCode = updateOrderStatus(entityManager, activistId, orderId, status);
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    @Override
    public int updateOrderStatus(Long activistId, Collection<String> orderIds, Byte status) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        int reasonCode;
        try {
            reasonCode = updateOrderStatus(entityManager, activistId, orderIds, status);
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    private EntityOrder findById(EntityManager entityManager, String id) {
        return entityManager.find(EntityOrder.class, id);
    }

    protected void updateStatusByOrderId(EntityManager entityManager, String orderId, Byte status) {
        EntityOrder findById = findById(entityManager, orderId);
        findById.setStatus(status);
        entityManager.merge(findById);
    }

    private int updateOrderStatus(EntityManager entityManager, Long activistId, Collection<String> orderIds, Byte status) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("R - activistId:" + activistId + ", orderIds:" + orderIds + ",status:" + status);
        }
        int reasonCode = ReasonCode.NONE;
        EntityManager stockManager = DBConnection.getInstance().getEntityManager();

        try {
            entityManager.getTransaction().begin();
            for (String orderId : orderIds) {
                EntityOrder order = findById(entityManager, orderId);
                if (order != null) {
                    order.setStatus(status);
                    entityManager.merge(order);
                    OrderDetailsRepositoryImpl detailsRepositoryImpl = new OrderDetailsRepositoryImpl();
                    List<String> idList = new LinkedList<>();
                    idList.add(orderId);
                    reasonCode = detailsRepositoryImpl.updateItemStatusByOrderIds(entityManager, idList, status);

                    if (reasonCode == ReasonCode.NONE) {

//                        List<EntityOrderDetails> orderDetails = detailsRepositoryImpl.getOrderDetails(entityManager, orderId).getList();
                        List<EntityOrderDetails> orderDetails = detailsRepositoryImpl.getOrderDetails(orderId).getList();

                        if (MarketOrderStatus.ON_SHIPMENT.getValue() == status) {
                            Map<String, Integer> productQuantityMap = new HashMap<>();

                            for (EntityOrderDetails orderDetail : orderDetails) {
                                productQuantityMap.put(orderDetail.getProductId(), orderDetail.getQuantity());
                            }
                            int stockUpdate = stockRepositoryImpl.decreaseStock(stockManager, productQuantityMap);
                            if (stockUpdate != ReasonCode.NONE) {
                                throw new Exception("Stock decrease failed with reason code = " + stockUpdate);
                            }
                        }

                        Thread thread = new Thread(() -> {
                            List<EntityHistoryOrderStatus> historyOrderStatuses = new LinkedList<>();
                            Long time = System.currentTimeMillis();
                            for (EntityOrderDetails orderDetail : orderDetails) {
                                EntityHistoryOrderStatus ehos = new EntityHistoryOrderStatus(TimeUUID.timeBased().toString(), orderDetail.getOrderId(), orderDetail.getProductId(), activistId, status, time);
                                historyOrderStatuses.add(ehos);
                            }
                            MarketRepository.INSTANCE.getHistoryOrderStatusRepository().addHistory(historyOrderStatuses);
                        });

                        DBConnection.getInstance().submitAsyncJob(thread);

                    } else {
                        entityManager.getTransaction().rollback();
                    }

                } else {
                    reasonCode = ReasonCode.NO_DATA_FOUND;
                    if (entityManager.getTransaction().isActive()) {
                        entityManager.getTransaction().rollback();
                    }
                }
            }
            entityManager.getTransaction().commit();

        } catch (Exception e) {
            LOGGER.error("Exception in updateOrderStatus() : {orderIds:" + orderIds.toString() + ", status:" + status + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            if (stockManager.getTransaction().isActive()) {
                stockManager.getTransaction().rollback();
            }
        }
        stockManager.close();

        return reasonCode;
    }

    private int updateOrderStatus(EntityManager entityManager, Long activistId, String orderId, Byte status) {

        int reasonCode;
        EntityManager stockManager = DBConnection.getInstance().getEntityManager();
        try {
            EntityOrder order = findById(entityManager, orderId);
            if (order != null) {
                entityManager.getTransaction().begin();
                order.setStatus(status);
                entityManager.merge(order);
                OrderDetailsRepositoryImpl detailsRepositoryImpl = new OrderDetailsRepositoryImpl();
                List<String> idList = new LinkedList<>();
                idList.add(orderId);
                reasonCode = detailsRepositoryImpl.updateItemStatusByOrderIds(entityManager, idList, status);
                if (reasonCode == ReasonCode.NONE) {
                    entityManager.getTransaction().commit();

                    List<EntityOrderDetails> orderDetails = detailsRepositoryImpl.getOrderDetails(entityManager, idList);

                    if (MarketOrderStatus.ON_SHIPMENT.getValue() == status) {
                        Map<String, Integer> productQuantityMap = new HashMap<>();

                        for (EntityOrderDetails orderDetail : orderDetails) {
                            productQuantityMap.put(orderDetail.getProductId(), orderDetail.getQuantity());
                        }
                        int stockUpdate = stockRepositoryImpl.decreaseStock(stockManager, productQuantityMap);
                        if (stockUpdate != ReasonCode.NONE) {
                            throw new Exception("Stock decrease failed with reason code = " + stockUpdate);
                        }
                    }

                    Thread thread = new Thread(() -> {
                        List<EntityHistoryOrderStatus> historyOrderStatuses = new LinkedList<>();
                        Long time = System.currentTimeMillis();
                        for (EntityOrderDetails orderDetail : orderDetails) {
                            EntityHistoryOrderStatus ehos = new EntityHistoryOrderStatus(TimeUUID.timeBased().toString(), orderDetail.getOrderId(), orderDetail.getProductId(), activistId, status, time);
                            historyOrderStatuses.add(ehos);
                        }
                        MarketRepository.INSTANCE.getHistoryOrderStatusRepository().addHistory(historyOrderStatuses);
                    });
                    DBConnection.getInstance().submitAsyncJob(thread);

                } else {
                    entityManager.getTransaction().rollback();
                }

            } else {
                reasonCode = ReasonCode.NO_DATA_FOUND;
            }
        } catch (Exception e) {
            LOGGER.error("Exception in updateOrderStatus() : {orderId:" + orderId + ", status:" + status + "}", e);
            reasonCode = ReasonCode.NONE;
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            if (stockManager.getTransaction().isActive()) {
                stockManager.getTransaction().rollback();
            }
        }
        stockManager.close();
        return reasonCode;
    }

    protected int deleteOrderFromDatabase(EntityManager entityManager, String orderId) {
        int reasonCode = ReasonCode.NONE;
        try {
            EntityOrder findById = findById(entityManager, orderId);
            if (findById == null) {
                reasonCode = ReasonCode.NO_DATA_FOUND;
            } else {
                entityManager.getTransaction().begin();
                entityManager.remove(findById);
                entityManager.getTransaction().commit();
            }
        } catch (Exception e) {
            LOGGER.error("Exception in deleteOrderFromDatabase() : {orderId:" + orderId + "}", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    @Override
    public PivotedListReturnDTO<EntityOrder, Pair<String, Long>> getBuyerOrders(Long userId, Pair<String, Long> pivot, Scroll scroll, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        PivotedListReturnDTO<EntityOrder, Pair<String, Long>> buyerOrders;
        try {
            buyerOrders = getBuyerOrders(entityManager, userId, pivot, scroll, limit);
        } finally {
            entityManager.close();
        }
        return buyerOrders;
    }

    protected PivotedListReturnDTO<EntityOrder, Pair<String, Long>> getBuyerOrders(EntityManager entityManager, Long userId, Pair<String, Long> pivot, Scroll scroll, int limit) {
        PivotedListReturnDTO<EntityOrder, Pair<String, Long>> returnDTO = new PivotedListReturnDTO<>();
        boolean isEmptyPivot = false;
        String pivotId = pivot.getKey();
        Long pivotTime = pivot.getValue();
        if (pivotId == null || pivotId.isEmpty()) {
            pivotId = Utility.EMPTY_DB_STRING;
            isEmptyPivot = true;
        }
        List<EntityOrder> list;
        try {
            Query query;
            if (scroll == Scroll.UP) {
                query = entityManager.createNamedQuery("EntityOrder.getOrdersByScrollUp");
            } else if (isEmptyPivot) {
                query = entityManager.createNamedQuery("EntityOrder.getOrdersByPivotEmptyAndScrollDown");
            } else {
                query = entityManager.createNamedQuery("EntityOrder.getOrdersByScrollDown");
            }
            query.setParameter(1, pivotTime);
            query.setParameter(2, pivotId);
            query.setParameter(3, userId);
            List<String> orderIds = query.setMaxResults(limit).getResultList();
            if (orderIds.isEmpty()) {
                if (isEmptyPivot) {
                    returnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    returnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
                }
            } else {

                list = new ArrayList<>();
                for (String orderId : orderIds) {
                    list.add(findById(entityManager, orderId));
                }
                pivotId = list.get(list.size() - 1).getId();
                pivotTime = list.get(list.size() - 1).getAddedTime();

                OrderDetailsRepositoryImpl detailsRepositoryImpl = new OrderDetailsRepositoryImpl();

                for (EntityOrder entityOrder : list) {
                    ListReturnDTO<EntityOrderDetails> orderDetails = detailsRepositoryImpl.getOrderDetails(entityManager, entityOrder.getId());
                    if (orderDetails.getList() != null) {
                        entityOrder.setDetails(orderDetails.getList());
                    }
                }
                returnDTO.setList(list);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getOrderList(): {userId:" + userId + " , pivotId:" + pivotId + " , pivotTime:" + pivotTime + ", scroll:" + scroll + " , limit:" + limit + "}", e);
            returnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }

        pivot = new Pair<>(pivotId, pivotTime);
        returnDTO.setPivot(pivot);
        return returnDTO;
    }

    @Override
    public ObjectReturnDTO<EntityOrder> findOrder(String orderId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ObjectReturnDTO<EntityOrder> orderInfo;
        try {
            orderInfo = new ObjectReturnDTO<>();
            try {
                EntityOrder order = findById(entityManager, orderId);

                if (order == null) {
                    orderInfo.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    orderInfo.setValue(order);
                }
            } catch (Exception e) {
                orderInfo.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
                LOGGER.error("Exception in findOrder() : {orderId:" + orderId + "}", e);
            }
        } finally {
            entityManager.close();
        }
        return orderInfo;
    }

    @Override
    public ListReturnDTO<InvoiceDTO> getInvoiceList(int start, int limit) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ListReturnDTO<InvoiceDTO> invoiceList;
        try {
            invoiceList = getInvoiceList(entityManager, start, limit);
        } finally {
            entityManager.close();
        }
        return invoiceList;
    }

    private ListReturnDTO<InvoiceDTO> getInvoiceList(EntityManager entityManager, int start, int limit) {
        ListReturnDTO<InvoiceDTO> listReturnDTO = new ListReturnDTO<>();
        try {

            Query query = entityManager.createNamedQuery("EntityOrder.getInvoiceList");
            List<Object[]> resultList = query.setFirstResult(start).setMaxResults(limit).getResultList();
            if (resultList.isEmpty()) {
                listReturnDTO.setReasonCode(ReasonCode.NO_MORE_DATA);
            } else {
                List<InvoiceDTO> invoiceList = new LinkedList<>();

                for (Object[] o : resultList) {
                    InvoiceDTO invoiceDTO = new InvoiceDTO((String) o[0], (long) o[1], (long) o[2], (BigDecimal) o[3], (long) o[4]);
                    invoiceList.add(invoiceDTO);
                }
                listReturnDTO.setList(invoiceList);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getInvoiceList() ", e);
            listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return listReturnDTO;
    }

    @Override
    public ObjectReturnDTO<String> updateAndGetInvoiceId(String orderId, Long activistId) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        ObjectReturnDTO<String> updateAndGetInvoiceId;
        try {
            updateAndGetInvoiceId = updateAndGetInvoiceId(entityManager, orderId, activistId);
        } finally {
            entityManager.close();
        }
        return updateAndGetInvoiceId;
    }

    private ObjectReturnDTO<String> updateAndGetInvoiceId(EntityManager entityManager, String orderId, Long activistId) {
        ObjectReturnDTO<String> objectReturnDTO = new ObjectReturnDTO<>();

        try {
            EntityOrder findById = findById(entityManager, orderId);
            if (findById == null) {
                objectReturnDTO.setReasonCode(ReasonCode.INVALID_INFORMATION);
            } else if (findById.getStatus() != MarketOrderStatus.PAYMENT_INCOMPLETE.getValue()) {
                objectReturnDTO.setReasonCode(ReasonCode.PERMISSION_DENIED);
            } else {
                Integer invoiceId = InvoiceIdGenerartor.getInvoiceId();
                if (invoiceId == null) {
                    objectReturnDTO.setReasonCode(ReasonCode.OPERATION_FAILED);
                } else {
                    String invoiceStr = String.valueOf(invoiceId);
                    entityManager.getTransaction().begin();
                    findById.setInvoiceId(invoiceStr);
                    entityManager.merge(findById);
                    entityManager.getTransaction().commit();
                    objectReturnDTO.setValue(invoiceStr);
                    LOGGER.info("updateAndGetInvoiceId() : {orderId:" + orderId + ", activistId:" + activistId + "}");
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception in updateAndGetInvoiceId() : {orderId:" + orderId + ", activistId:" + activistId + "}", e);
            objectReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
        }
        return objectReturnDTO;
    }

    public void evictOrdersOfShop(EntityManager entityManager, long shopId) {
        Query query = entityManager.createNamedQuery("EntityOrder.getOrdersOfShop");
        query.setParameter("shopId", shopId);
        List<String> orderIds = query.getResultList();
        DBConnection.getInstance().getCache().evictAll(EntityOrder.class, orderIds);
    }

    public int updateShopName(EntityManager entityManager, long shopId, String shopName) {
        Query query = entityManager.createNamedQuery("EntityOrder.updateShopName");
        query.setParameter("name", shopName);
        query.setParameter("shopId", shopId);
        return query.executeUpdate();
    }

}
