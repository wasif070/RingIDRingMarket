/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import java.util.Collection;
import org.ipvision.ringmarket.entities.order.EntityHistoryOrderStatus;

/**
 *
 * @author saikat
 */
public interface HistoryOrderStatusRepository {

    int addHistory(String orderId, String productId, byte status, long activistId, long time);

    int addHistory(Collection<EntityHistoryOrderStatus> orderStatusHistory);
    
}
