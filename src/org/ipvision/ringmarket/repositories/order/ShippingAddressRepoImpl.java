/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.order.Address;
import org.ipvision.ringmarket.entities.order.EntityShippingAddress;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class ShippingAddressRepoImpl implements ShippingAddressRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShippingAddressRepoImpl.class);

    @Override
    public ObjectReturnDTO<String> addShippingAddress(Long userId, Address address, boolean isDefault, byte type) {
        ObjectReturnDTO<String> objectReturnDTO = new ObjectReturnDTO<>();
        EntityShippingAddress entityShippingAddress = new EntityShippingAddress();
        String id = TimeUUID.timeBased().toString();
        entityShippingAddress.setId(id);
        entityShippingAddress.setAddress(address);
        entityShippingAddress.setUserId(userId);
        entityShippingAddress.setIsDefault(isDefault);
        entityShippingAddress.setType(type);
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                entityManager.getTransaction().begin();
                addShippingAddress(entityManager, entityShippingAddress);
                if (isDefault) {
                    setOthersAddressDefaultFalse(entityManager, userId, id);
                }
                entityManager.getTransaction().commit();
                objectReturnDTO.setValue(id);
            } catch (Exception e) {
                LOGGER.error("Exception in addShippingAddress() : {userId:" + userId + ", address:" + address + "}", e);
                objectReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        } finally {
            entityManager.close();
        }
        return objectReturnDTO;
    }

    private EntityShippingAddress findAddressById(EntityManager entityManager, String id) {
        return entityManager.find(EntityShippingAddress.class, id);
    }

    @Override
    public EntityShippingAddress findAddressById(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        EntityShippingAddress findAddressById;
        try {
            findAddressById = findAddressById(entityManager, id);
        } finally {
            entityManager.close();
        }
        return findAddressById;
    }

    private void setOthersAddressDefaultFalse(EntityManager entityManager, Long userId, String addressId) {
        Query query = entityManager.createNamedQuery("EntityShippingAddress.setDefaultFalse");
        query.setParameter("userId", userId);
        query.setParameter("id", addressId);
        query.executeUpdate();
    }

    @Override
    public int updateShippingAddress(String id, Address address) {
        int reasonCode = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                EntityShippingAddress shippingAddress = findAddressById(entityManager, id);
                if (shippingAddress != null) {
                    entityManager.getTransaction().begin();
                    shippingAddress.setAddress(address);
                    entityManager.merge(shippingAddress);
                    entityManager.getTransaction().commit();
                } else {
                    reasonCode = ReasonCode.INVALID_INFORMATION;
                }
            } catch (Exception e) {
                LOGGER.error("Exception in updateShippingAddress() : {id:" + id + ", address:" + address + "}", e);
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    private void addShippingAddress(EntityManager entityManager, EntityShippingAddress address) {
        entityManager.persist(address);
    }

    @Override
    public ListReturnDTO<EntityShippingAddress> getShippingAddresses(long userId) {
        ListReturnDTO<EntityShippingAddress> listReturnDTO = new ListReturnDTO<>();
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            Query query = entityManager.createNamedQuery("EntityShippingAddress.getAddressesOfUser", EntityShippingAddress.class);
            query.setParameter("userId", userId);
            try {
                List<EntityShippingAddress> addresses = query.getResultList();
                if (addresses.isEmpty()) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setList(addresses);
                }
            } catch (Exception e) {
                LOGGER.error("Exception in getShippingAddresses() : {userId: " + userId + "}", e);
                listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
            }
        } finally {
            entityManager.close();
        }
        return listReturnDTO;
    }

    @Override
    public ListReturnDTO<EntityShippingAddress> getNearestRingStores(float lat, float lon, byte type) {
        ListReturnDTO<EntityShippingAddress> listReturnDTO = new ListReturnDTO<>();
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            Query query = entityManager.createNamedQuery("EntityShippingAddress.getNearestRingStores", EntityShippingAddress.class);
            query.setParameter("type", type);
            try {
                List<EntityShippingAddress> addresses = query.getResultList();
                if (addresses.isEmpty()) {
                    listReturnDTO.setReasonCode(ReasonCode.NO_DATA_FOUND);
                } else {
                    listReturnDTO.setList(addresses);
                }
            } catch (Exception e) {
                LOGGER.error("Exception in getNearestRingStores() : {type: " + type + "}", e);
                listReturnDTO.setReasonCode(ReasonCode.EXCEPTION_OCCURED);
            }
        } finally {
            entityManager.close();
        }
        return listReturnDTO;
    }

    @Override
    public int removeShippingAddress(String id) {
        int reasonCode = ReasonCode.NONE;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                EntityShippingAddress findAddressById = findAddressById(entityManager, id);
                if (findAddressById == null) {
                    reasonCode = ReasonCode.INVALID_INFORMATION;
                } else if (!findAddressById.isIsDefault()) {
                    entityManager.getTransaction().begin();
                    entityManager.remove(findAddressById);
                    entityManager.getTransaction().commit();
                } else {
                    reasonCode = ReasonCode.PERMISSION_DENIED;
                }
            } catch (Exception e) {
                LOGGER.error("Exception in removeShippingAddress() : {id:" + id + "}", e);
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    public void remove(String id) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                EntityShippingAddress findAddressById = findAddressById(entityManager, id);
                if (findAddressById == null) {
                } else {
                    entityManager.getTransaction().begin();
                    entityManager.remove(findAddressById);
                    entityManager.getTransaction().commit();
                }
            } catch (Exception e) {
                LOGGER.error("Exception in removeShippingAddress() : {id:" + id + "}", e);
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int setDefault(String id) {
        int reasonCode = ReasonCode.INVALID_INFORMATION;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            try {
                EntityShippingAddress findAddressById = findAddressById(entityManager, id);
                if (findAddressById != null) {
                    entityManager.getTransaction().begin();
                    findAddressById.setIsDefault(true);
                    setOthersAddressDefaultFalse(entityManager, findAddressById.getUserId(), id);
                    entityManager.getTransaction().commit();
                    reasonCode = ReasonCode.NONE;
                }

            } catch (Exception e) {
                LOGGER.error("Exception in setDefault(): {id:" + id + "}", e);
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
                reasonCode = ReasonCode.EXCEPTION_OCCURED;
            }
        } finally {
            entityManager.close();
        }
        return reasonCode;
    }

    public List<EntityShippingAddress> getAllAddresses() {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        List<EntityShippingAddress> shippingAddresses;
        try {

            Query query = entityManager.createQuery("SELECT s FROM  EntityShippingAddress s", EntityShippingAddress.class);
            shippingAddresses = new ArrayList<>(query.getResultList());

        } finally {
            entityManager.close();
        }
        return shippingAddresses;
    }
}
