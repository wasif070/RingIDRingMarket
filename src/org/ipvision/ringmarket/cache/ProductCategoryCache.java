/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.category.CategoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class ProductCategoryCache {

    private final ConcurrentHashMap<Integer, EntityCategory> categoryMap;
    private final ConcurrentHashMap<Integer, List<Integer>> subCategoryListMap;

    private List<Integer> rootCategoryIds;
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductCategoryCache.class);
    private static ProductCategoryCache INSTANCE = null;

    ProductCategoryCache() {
        categoryMap = new ConcurrentHashMap<>();
        subCategoryListMap = new ConcurrentHashMap<>();
        reload();
    }

    public static ProductCategoryCache getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ProductCategoryCache();
        }
        return INSTANCE;
    }

    private void buildSubCategoryInfo(int id) {
        try {
            Thread.sleep(100);
            CategoryRepository categoryRepository = MarketRepository.INSTANCE.getCategoryRepository();
            List<EntityCategory> subCategories = categoryRepository.getSubCategories(id);

            if (subCategories == null || subCategories.isEmpty()) {
                return;
            }
            List<Integer> subCategoryIdList = new ArrayList<>();

            for (EntityCategory subCategory : subCategories) {
                categoryMap.put(subCategory.getId(), subCategory);
                if (subCategory.getHasNext()) {
                    buildSubCategoryInfo(subCategory.getId());
                }
                subCategoryIdList.add(subCategory.getId());
            }
            subCategoryListMap.put(id, subCategoryIdList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void reload() {
        CategoryRepository category = MarketRepository.INSTANCE.getCategoryRepository();
        List<EntityCategory> rootCategories = category.getRootCategories();
        rootCategoryIds = new ArrayList<>();
        for (EntityCategory rootCategory : rootCategories) {
            categoryMap.put(rootCategory.getId(), rootCategory);
            if (rootCategory.getHasNext() != null && rootCategory.getHasNext()) {
                buildSubCategoryInfo(rootCategory.getId());
            }
            rootCategoryIds.add(rootCategory.getId());
        }
    }

    public void forceReload() {
        reload();
    }

    /**
     * clear current cache
     */
    public void clear() {
        categoryMap.clear();
    }

    public String getName(int categoryId) {
        if (categoryMap.containsKey(categoryId)) {
            return categoryMap.get(categoryId).getName();
        }
        return null;
    }

    public int getParentId(int categoryId) {
        if (categoryMap.containsKey(categoryId)) {
            return categoryMap.get(categoryId).getParentId();
        }
        return -1;
    }

    public EntityCategory getCategoryInfo(int categoryId) {
        if (categoryMap.containsKey(categoryId)) {
            return categoryMap.get(categoryId);
        }
        return null;
    }

    public List<Integer> getRootCategoryIds() {
        return rootCategoryIds;
    }

    public List<EntityCategory> getCategories(Integer categoryId, int start, int limit) {
        LOGGER.debug("getCategories() : {categoryId:" + categoryId + ",start:" + start + ",limit:" + limit + "}");
        List<EntityCategory> categories = null;
        if (start >= 0) {
            if (categoryId == null) {
                categories = getRootCategoryInfos(start, limit);
            } else {
                categories = getSubCatoryInfos(categoryId, start, limit);
            }
        }
        return categories;
    }

    private int getEnd(int expectedEnd, int size) {
        if (expectedEnd > size) {
            expectedEnd = size;
        }
        return expectedEnd;
    }

    private List<EntityCategory> getRootCategoryInfos(int start, int limit) {
        return getCategoriesWithLimit(start, limit, rootCategoryIds);
    }

    private List<EntityCategory> getSubCatoryInfos(int categoryId, int start, int limit) {
        List<EntityCategory> categories = null;

        if (subCategoryListMap.containsKey(categoryId)) {
            categories = getCategoriesWithLimit(start, limit, subCategoryListMap.get(categoryId));
        }
        return categories;
    }

    private List<EntityCategory> getCategoriesWithLimit(int start, int limit, List<Integer> categoryIds) {
        List<EntityCategory> categories = null;

        int size = categoryIds.size();
        int end = getEnd(start + limit, size);
        if (end > start) {
            categories = new ArrayList<>();
            for (int i = start; i < end; i++) {
                categories.add(categoryMap.get(categoryIds.get(i)));
            }
        }
        return categories;
    }

    public Map<Integer, List<Integer>> getSubCategoryListMap() {
        return subCategoryListMap;
    }

    public void addCategoryToCache(EntityCategory category) {
        int parentId = category.getParentId();
        if (parentId > 0) {
            if (this.subCategoryListMap.containsKey(parentId)) {
                this.subCategoryListMap.get(parentId).add(category.getId());
            } else {
                List<Integer> subcategoryIds = new ArrayList<>();
                subcategoryIds.add(category.getId());
                this.subCategoryListMap.put(parentId, subcategoryIds);
            }
        } else {
            rootCategoryIds.add(category.getId());
        }
        this.categoryMap.put(category.getId(), category);
    }

    public List<Integer> getSubCategoryIds(int categoryId) {
        return subCategoryListMap.get(categoryId);
    }

    public void update(EntityCategory category) {
        this.categoryMap.put(category.getId(), category);
    }
}
