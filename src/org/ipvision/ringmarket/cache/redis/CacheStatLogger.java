/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache.redis;

/**
 *
 * @author alamgir
 */
public class CacheStatLogger extends Thread {

    private static CacheStatLogger instance;
    private static RedisClusterQueryCache queryCache;
    private static RedisClusterDataCache dataCache;
    private static boolean running = false;
    private static int PRINT_INTERVAL = 5 * 60 * 1000;
//    private static int PRINT_INTERVAL = 1000;

    private CacheStatLogger() {
    }

    public static CacheStatLogger getInstance() {
        if (instance == null) {
            running = true;
            instance = new CacheStatLogger();
        }
        return instance;
    }
    
    public void setQueryCacheController(RedisClusterQueryCache queryCache){
        CacheStatLogger.queryCache = queryCache;
    }
    public void setDataCacheController(RedisClusterDataCache dataCache){
        CacheStatLogger.dataCache = dataCache;
    }

    @Override
    public void run() {
        while (running) {
            try {
                if (queryCache != null) {
                    queryCache.printQueryCacheStat();
                }
                if (dataCache != null) {
                    dataCache.printDataCacheStat();
                }
                Thread.sleep(PRINT_INTERVAL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void stopService() {
        running = false;
    }

}
