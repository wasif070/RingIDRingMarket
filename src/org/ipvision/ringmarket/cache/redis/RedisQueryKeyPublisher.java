/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache.redis;

import redis.clients.jedis.JedisCluster;

/**
 *
 * @author alamgir
 */
public class RedisQueryKeyPublisher {

    private final JedisCluster publisherJedis;

    private final String channel;

    public RedisQueryKeyPublisher(JedisCluster publisherJedis, String channel) {
        this.publisherJedis = publisherJedis;
        this.channel = channel;
    }

    public void publish(String msg) {
        publisherJedis.publish(channel, msg);
    }
}
