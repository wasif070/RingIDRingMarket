/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache.redis;

/**
 *
 * @author alamgir
 */
import org.apache.openjpa.datacache.DataCachePCData;
import org.apache.openjpa.util.OpenJPAId;

import java.util.Set;
import org.apache.openjpa.datacache.AbstractDataCache;
import org.apache.openjpa.datacache.CacheStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisCluster;

public class RedisClusterDataCache extends AbstractDataCache {

    Logger logger = LoggerFactory.getLogger(RedisClusterDataCache.class);

    protected String _dataCachePrefix = "";
    protected String _classCachePrefix = "";
    protected JedisCluster _jedisPool = null;
    protected boolean _lru = false;
    private int _cacheSize = Integer.MIN_VALUE;
    private int _softRefs = Integer.MIN_VALUE;

    public RedisClusterDataCache() {
        RedisClusterHelper cacheHelper = RedisClusterHelper.getInstance();
        _jedisPool = cacheHelper.getJedisPool();
        _dataCachePrefix = cacheHelper.getCachePrefix() + "data:";
        _classCachePrefix = cacheHelper.getCachePrefix() + "class:";
        CacheStatLogger.getInstance().setDataCacheController(this);

    }

    public int getCacheSize() {
        return _cacheSize;
    }

    public void setCacheSize(int size) {
        _cacheSize = size;
    }

    public int getSoftReferenceSize() {
        return _softRefs;
    }

    public void setSoftReferenceSize(int size) {
        _softRefs = size;
    }

    @Override
    public void writeLock() {
    }

    @Override
    public void writeUnlock() {
    }

    @Override
    public boolean pinInternal(Object o) {
        return false;
    }

    @Override
    public boolean unpinInternal(Object o) {
        return false;
    }

    @Override
    public DataCachePCData removeInternal(Object o) {
        OpenJPAId oId = (OpenJPAId) o;
        String objectKey = _dataCachePrefix + oId.getType().getName() + ":" + o.toString();

        DataCachePCData data = getInternalJedis(objectKey);
        if (data == null) {
            return null;
        }
        String classKey = _classCachePrefix + data.getType().getName();

        _jedisPool.del(objectKey);
        _jedisPool.srem(classKey, objectKey);

        logger.info("Remove : " + oId.getType().getName());
        return data;
    }

    @Override
    public void removeAllInternal(Class<?> aClass, boolean b) {
        String classKey = _classCachePrefix + aClass.getName();

        Set<String> classObjects = _jedisPool.smembers(classKey);

        for (String entry : classObjects) {
            _jedisPool.srem(classKey, entry);
            _jedisPool.del(entry);
        }

        logger.info("Remove all: " + aClass.getCanonicalName());
    }

    @Override
    public DataCachePCData getInternal(Object o) {

        OpenJPAId oId = (OpenJPAId) o;
        String objectKey = _dataCachePrefix + oId.getType().getName() + ":" + o.toString();

        try {
            DataCachePCData data = getInternalJedis(objectKey);
            return data;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public DataCachePCData putInternal(Object o, DataCachePCData dataCachePCData) {
        String classKey = _classCachePrefix + dataCachePCData.getType().getName();

        OpenJPAId oId = (OpenJPAId) o;
        String objectKey = _dataCachePrefix + oId.getType().getName() + ":" + o.toString();

        try {
            _jedisPool.set(objectKey, RedisClusterHelper.serialize(dataCachePCData));
            _jedisPool.sadd(classKey, objectKey);
        } catch (Exception ex) {
            logger.error(String.format("Cache Enabled but not connected. Caching data failed (id : %s )", o), ex);
        }
        return dataCachePCData;
    }

    @Override
    public void clearInternal() {
        // TODO there's a race here, obviously.
//        logger.info("RedisClusterDataCache: clearInternal");
//        RedisClusterHelper.getInstance().clearAll(_classCachePrefix + "*");
//        RedisClusterHelper.getInstance().clearAll(_dataCachePrefix + "*");
    }

    private DataCachePCData getInternalJedis(String objectKey) {
        try {
            String fromCache = _jedisPool.get(objectKey);
            if (fromCache == null) {
                return null;
            }
            return (DataCachePCData) RedisClusterHelper.deserialize(fromCache);
        } catch (Exception ex) {
            logger.error(String.format("Cache Enabled but not connected. Caching data failed (id : %s )", objectKey), ex);
            return null;
        }
    }

    public void printDataCacheStat() {
        CacheStatistics stat = getStatistics();
        if (stat != null) {
            float cached = 0;
            if (stat.getTotalReadCount() > 0) {
                cached = (stat.getTotalHitCount() * 100) / stat.getTotalReadCount();
            }

            logger.debug(
                    "Data from cache (" + cached + "%), "
                    + "Data fetched(" + stat.getTotalReadCount() + "), "
                    + "from cache(" + stat.getTotalHitCount() + ")"
            );
        }
    }
}
