/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache.redis;

/**
 *
 * @author alamgir
 */
import java.util.ArrayList;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.apache.openjpa.datacache.AbstractQueryCache;
import org.apache.openjpa.datacache.QueryKey;
import org.apache.openjpa.datacache.QueryResult;
import org.apache.openjpa.datacache.TypesChangedEvent;
import org.apache.openjpa.event.RemoteCommitListener;
import org.ipvision.ringmarket.cache.IQueryCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisCluster;

public class RedisClusterQueryCache1 extends AbstractQueryCache implements RemoteCommitListener, IQueryCache {

    protected String _queryCachePrefix = "";
    protected String _queryKeyCachePrefix = "";
    protected JedisCluster _jedisPool = null;
    Logger logger = LoggerFactory.getLogger(RedisClusterQueryCache1.class);

    public RedisClusterQueryCache1() {
//        conf.getRemoteCommitEventManager().addInternalListener(this);
        RedisClusterHelper cacheHelper = RedisClusterHelper.getInstance();
        _jedisPool = cacheHelper.getJedisPool();
        _queryCachePrefix = cacheHelper.getCachePrefix() + "query:";
        _queryKeyCachePrefix = cacheHelper.getCachePrefix() + "queryKey:";
    }

    @Override
    public void writeLock() {
    }

    @Override
    public void writeUnlock() {
    }

    @Override
    public boolean pinInternal(QueryKey queryKey) {
        return true;
    }

    @Override
    public boolean unpinInternal(QueryKey queryKey) {
        return true;
    }

    @Override
    public Collection keySet() {
        return RedisClusterHelper.getInstance().getAllKeys(_queryCachePrefix + "*");
    }

    protected Collection queryKeySet() {
        return RedisClusterHelper.getInstance().getAllKeys(_queryKeyCachePrefix + "*");
    }

    @Override
    public void onTypesChanged(TypesChangedEvent ev) {
        if (evictPolicy == EvictPolicy.DEFAULT) {
            writeLock();
            Collection keys = null;
            try {
                if (hasListeners()) {
                    fireEvent(ev);
                }
                keys = queryKeySet();
            } finally {
                writeUnlock();
            }

            QueryKey qk;
            List<QueryKey> removes = null;

            
            try {
                for (Object o : keys) {
                    QueryKey result = getInternalJedisQueryKey( (String) o);
                    qk = (QueryKey) result;
                    if (qk.changeInvalidatesQuery(ev.getTypes())) {
                        if (removes == null) {
                            removes = new ArrayList<>();
                        }
                        removes.add(qk);
                    }

                }
            } catch (Exception ex) {
            } 
            if (removes != null) {
                removeAllInternal(removes);
            }
        } else {
            Collection changedTypes = ev.getTypes();
            HashMap<String, Long> changedClasses
                    = new HashMap<>();
            Long tstamp = System.currentTimeMillis();
            for (Object o : changedTypes) {
                String name = ((Class) o).getName();
                if (!changedClasses.containsKey(name)) {
                    changedClasses.put(name, tstamp);
                }
            }
            // Now update entity timestamp map
            updateEntityTimestamp(changedClasses);
        }
    }

    @Override
    public QueryResult putInternal(QueryKey queryKey, QueryResult result) {
        String queryCacheKey = _queryKeyCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        String cacheKey = _queryCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();

        if (result == null || result.isEmpty()) {
            return result;
        }

        try {
            _jedisPool.set(queryCacheKey, RedisClusterHelper.serialize(queryKey));
            _jedisPool.set(cacheKey, RedisClusterHelper.serialize(result));
            return result;
        } catch (Exception ex) {
            logger.error("Cache enable but connection not established while query caching.", ex);
        } 
        return null;
    }

    @Override
    public QueryResult getInternal(QueryKey queryKey) {
        String cacheKey = _queryCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        try {
            QueryResult result = getInternalJedis( cacheKey);
            return result;
        } catch (Exception ex) {
            logger.error("get query from cache error!", ex);
        } 
        return null;
    }

    @Override
    public QueryResult removeInternal(QueryKey queryKey) {
        String cacheKey = _queryCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        String queryCacheKey = _queryKeyCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        try {
            QueryResult data = getInternalJedis( cacheKey);
            _jedisPool.del(cacheKey);
            _jedisPool.del(queryCacheKey);
            return data;
        } catch (Exception ex) {
            logger.error("Remove query cache error!", ex);
        } 
        return null;
    }

    @Override
    public void clearInternal() {
        RedisClusterHelper.getInstance().clearAll(_queryCachePrefix + "*");
        RedisClusterHelper.getInstance().clearAll(_queryKeyCachePrefix + "*");
    }

    private QueryResult getInternalJedis(String cacheKey) {
        String fromCache = null;
        try {
            fromCache = _jedisPool.get(cacheKey);
            return (QueryResult) RedisClusterHelper.deserialize(fromCache);
        } catch (Exception ex) {
            logger.error("Cache enable but connection not established while query caching.", ex);
        }
        return null;
    }

    private QueryKey getInternalJedisQueryKey(String queryCacheKey) {
        String fromCache = null;
        try {
            fromCache = _jedisPool.get(queryCacheKey);
            return (QueryKey) RedisClusterHelper.deserialize(fromCache);
        } catch (Exception ex) {
            logger.error("Cache enable but connection not established while query caching.", ex);
        }
        return null;
    }

}
