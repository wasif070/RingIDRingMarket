/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache.redis;

/**
 *
 * @author alamgir
 */
import java.util.ArrayList;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.apache.openjpa.datacache.AbstractQueryCache;
import org.apache.openjpa.datacache.DataCacheManager;
import org.apache.openjpa.datacache.QueryKey;
import org.apache.openjpa.datacache.QueryResult;
import org.apache.openjpa.datacache.TypesChangedEvent;
import org.apache.openjpa.event.RemoteCommitListener;
import org.ipvision.ringmarket.cache.IQueryCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedisShardQueryCache extends AbstractQueryCache implements RemoteCommitListener, IQueryCache {

    protected String _queryCachePrefix = "";
    protected String _queryKeyCachePrefix = "";
    protected ShardedJedisPool _jedisPool = null;
    Logger logger = LoggerFactory.getLogger(RedisShardQueryCache.class);

    public RedisShardQueryCache() {
//        conf.getRemoteCommitEventManager().addInternalListener(this);
        RedisShardHelper cacheHelper = RedisShardHelper.getInstance();
        _jedisPool = cacheHelper.getJedisPool();
        _queryCachePrefix = cacheHelper.getCachePrefix() + "query:";
        _queryKeyCachePrefix = cacheHelper.getCachePrefix() + "queryKey:";
    }

    @Override
    public void writeLock() {
    }

    @Override
    public void writeUnlock() {
    }

    @Override
    public boolean pinInternal(QueryKey queryKey) {
        return true;
    }

    @Override
    public boolean unpinInternal(QueryKey queryKey) {
        return true;
    }

    @Override
    public Collection keySet() {
        return RedisShardHelper.getInstance().getAllKeys(_queryCachePrefix + "*");
    }

    protected Collection queryKeySet() {
        return RedisShardHelper.getInstance().getAllKeys(_queryKeyCachePrefix + "*");
    }

    @Override
    public void onTypesChanged(TypesChangedEvent ev) {
        if (evictPolicy == EvictPolicy.DEFAULT) {
            writeLock();
            Collection keys = null;
            try {
                if (hasListeners()) {
                    fireEvent(ev);
                }
                keys = queryKeySet();
            } finally {
                writeUnlock();
            }

            QueryKey qk;
            List<QueryKey> removes = null;

            ShardedJedis jedis = _jedisPool.getResource();

            try {
                for (Object o : keys) {
                    QueryKey result = getInternalJedisQueryKey(jedis, (String) o);
                    qk = (QueryKey) result;
                    if (qk.changeInvalidatesQuery(ev.getTypes())) {
                        if (removes == null) {
                            removes = new ArrayList<>();
                        }
                        removes.add(qk);
                    }

                }
            } catch (Exception ex) {
            } finally {
                _jedisPool.returnResource(jedis);
            }
            if (removes != null) {
                removeAllInternal(removes);
            }
        } else {
            Collection changedTypes = ev.getTypes();
            HashMap<String, Long> changedClasses
                    = new HashMap<>();
            Long tstamp = System.currentTimeMillis();
            for (Object o : changedTypes) {
                String name = ((Class) o).getName();
                if (!changedClasses.containsKey(name)) {
                    changedClasses.put(name, tstamp);
                }
            }
            // Now update entity timestamp map
            updateEntityTimestamp(changedClasses);
        }
    }

    @Override
    public QueryResult putInternal(QueryKey queryKey, QueryResult objects) {
        String cacheKey = _queryCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        String queryCacheKey = _queryKeyCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();

        if (objects == null || objects.isEmpty()) {
            return objects;
        }

        ShardedJedis jedis = _jedisPool.getResource();
        try {
            jedis.set(cacheKey, RedisShardHelper.serialize(objects));
            jedis.set(queryCacheKey, RedisShardHelper.serialize(queryKey));
            return objects;
        } catch (Exception ex) {
            logger.error("Cache enable but connection not established while query caching.");
            return null;
        } finally {
            _jedisPool.returnResource(jedis);
        }
    }

    @Override
    public QueryResult getInternal(QueryKey queryKey) {
        String cacheKey = _queryCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();

        ShardedJedis jedis = _jedisPool.getResource();
        try {
            QueryResult result = getInternalJedis(jedis, cacheKey);
            return result;
        } catch (Exception ex) {
            return null;
        } finally {
            _jedisPool.returnResource(jedis);
        }
    }

    @Override
    public QueryResult removeInternal(QueryKey queryKey) {
        String cacheKey = _queryCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        String queryCacheKey = _queryKeyCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();

        ShardedJedis jedis = _jedisPool.getResource();
        try {
            QueryResult data = getInternalJedis(jedis, cacheKey);
            jedis.del(cacheKey);
            jedis.del(queryCacheKey);
            return data;
        } catch (Exception ex) {
            return null;
        } finally {
            _jedisPool.returnResource(jedis);
        }
    }

    @Override
    public void clearInternal() {
        RedisShardHelper.getInstance().clearAll(_queryCachePrefix + "*");
        RedisShardHelper.getInstance().clearAll(_queryKeyCachePrefix + "*");
    }

    private QueryResult getInternalJedis(ShardedJedis jedis, String cacheKey) {
        String fromCache = null;
        try {
            fromCache = jedis.get(cacheKey);
            return (QueryResult) RedisShardHelper.deserialize(fromCache);
        } catch (Exception ex) {
            logger.error("Cache enable but connection not established while query caching.");
        }
        return null;
    }

    private QueryKey getInternalJedisQueryKey(ShardedJedis jedis, String cacheKey) {
        String fromCache = null;
        try {
            fromCache = jedis.get(cacheKey);
            return (QueryKey) RedisShardHelper.deserialize(fromCache);
        } catch (Exception ex) {
            logger.error("Cache enable but connection not established while query caching.");
        }
        return null;
    }

}
