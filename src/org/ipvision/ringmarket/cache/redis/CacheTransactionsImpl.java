package org.ipvision.ringmarket.cache.redis;

import java.util.List;
import java.util.Map;
import redis.clients.jedis.JedisCluster;

public class CacheTransactionsImpl implements CacheTransactions {

    private final JedisCluster CACHE = RedisClusterHelper.getInstance().getJedisPool();

    @Override
    public String getCacheString(String key) {

        return CACHE.get(key);
    }

    @Override
    public List<String> getCacheList(String key) {

        return CACHE.lrange(key, 0, -1);
    }

    @Override
    public List<String> getCacheList(String key, int start, int end) {

        return CACHE.lrange(key, start, end);
    }

    @Override
    public Map<String, String> getCacheMap(String key) {

        return CACHE.hgetAll(key);
    }

    @Override
    public void setCacheString(String key, String value) {

        CACHE.set(key, value);
    }

    @Override
    public void setCacheList(String key, List<String> values) {

        for (String value : values) {
            CACHE.lpush(key, value);
        }
    }

    @Override
    public void setCacheMap(String key, Map<String, String> value) {
        CACHE.hmset(key, value);
    }

    @Override
    public void deleteCache(String... keys) {

        CACHE.del(keys);
    }

    @Override
    public void deleteFieldFromCacheMap(String key, String... fields) {

        CACHE.hdel(key, fields);
    }

    @Override
    public boolean isCacheEnable() {
        return (CACHE != null);
    }

    @Override
    public void addToCacheList(String key, String... value) {
        CACHE.lpush(key, value);
    }

}
