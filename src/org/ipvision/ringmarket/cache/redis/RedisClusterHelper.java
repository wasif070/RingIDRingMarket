/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache.redis;

/**
 *
 * @author alamgir
 */
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;

import java.io.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import org.apache.mina.util.ConcurrentHashSet;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

public class RedisClusterHelper {

    private static Logger logger = LoggerFactory.getLogger(RedisClusterHelper.class);

    private static final int DEFAULT_REDIS_PORT = 6379;
    private static final String CONFIG_FILE_NAME = "./conf/openjpa_redis.xml";
    private String _cachePrefix = "";

    private static RedisClusterHelper _instance;

    private Set<HostAndPort> _shards = new HashSet<>();
    private JedisCluster _jedisPool = null;

    public static synchronized RedisClusterHelper getInstance() {
        if (_instance == null) {
            _instance = new RedisClusterHelper();
        }
        return _instance;
    }

    public static String serialize(Serializable o) {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            ObjectOutputStream oStream = new ObjectOutputStream(stream);
            oStream.writeObject(o);
            return Base64.encodeBase64String(stream.toByteArray());
        } catch (IOException ex) {
            logger.error("Serialization failed! ", ex);
        }
        return null;
    }

    static Object deserialize(String s) {
        if (s == null || s.isEmpty()) {
            return null;
        }
        try {
            ByteArrayInputStream stream = new ByteArrayInputStream(Base64.decodeBase64(s));
            ObjectInputStream iStream = new ObjectInputStream(stream);
            return iStream.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            return null;
        }
    }

    public static byte[] serialize(Object obj) throws IOException {
        try (ByteArrayOutputStream b = new ByteArrayOutputStream()) {
            try (ObjectOutputStream o = new ObjectOutputStream(b)) {
                o.writeObject(obj);
            }
            return b.toByteArray();
        }
    }

    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream b = new ByteArrayInputStream(bytes)) {
            try (ObjectInputStream o = new ObjectInputStream(b)) {
                return o.readObject();
            }
        }
    }

    JedisCluster getJedisPool() {
        return _jedisPool;
    }

    public String getCachePrefix() {
        return _cachePrefix;
    }

    public Set<String> getAllKeys(String pattern) {
        Set<String> keys = new ConcurrentHashSet<>();
        Iterator<JedisPool> poolIterator = _jedisPool.getClusterNodes().values().iterator();
        while (poolIterator.hasNext()) {
            JedisPool pool = poolIterator.next();
            Jedis jedis = pool.getResource();
            try {
                keys.addAll(jedis.keys(pattern));
            } catch (Exception ex) {
                logger.error("Exception in cache service: {} ", ex.getMessage());
            }
        }
        return keys;
    }

    void clearAll(String pattern) {

        Iterator<JedisPool> poolIterator = _jedisPool.getClusterNodes().values().iterator();
        while (poolIterator.hasNext()) {
            JedisPool pool = poolIterator.next();
            Jedis jedis = pool.getResource();
            try {
                Set<String> keys = jedis.keys(pattern);
                for (String key : keys) {
                    jedis.del(key);
                }
            } catch (Exception ex) {
                logger.error("Exception in cache service: {} ", ex.getMessage());
            }
        }

    }

    private RedisClusterHelper() {
        try {
            XMLConfiguration config = new XMLConfiguration(CONFIG_FILE_NAME);
            Set<HostAndPort> nodes = new HashSet<>();

            List<HierarchicalConfiguration> serverProps = config.configurationsAt("servers.server");
            for (HierarchicalConfiguration serverProp : serverProps) {
                String host = serverProp.getString("host");
                Integer port = serverProp.getInt("port", DEFAULT_REDIS_PORT);

                _shards.add(new HostAndPort(host, port));
            }
            _cachePrefix = config.getString("prefix") + ":";

            int poolSize = 100;
            boolean blockWhenExhausted = false;

            JedisPoolConfig poolConfig = new JedisPoolConfig();
            poolConfig.setMaxWaitMillis(5000);

            poolConfig.setLifo(true);
            poolConfig.setTestOnBorrow(true);
            poolConfig.setTestOnReturn(false);
            poolConfig.setBlockWhenExhausted(blockWhenExhausted);
            poolConfig.setMinIdle(Math.max(1, (int) (poolSize / 10))); // keep 10% hot always
            poolConfig.setMaxTotal(poolSize);
            poolConfig.setTestWhileIdle(false);
            poolConfig.setSoftMinEvictableIdleTimeMillis(3000L);
            poolConfig.setNumTestsPerEvictionRun(5);
            poolConfig.setTimeBetweenEvictionRunsMillis(5000L);
            poolConfig.setJmxEnabled(true);

            _jedisPool = new JedisCluster(_shards, poolConfig);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MarketRepository.INSTANCE.getProductViewRepository().syncProductView();
        System.out.println("Clearing Redis entry. Please wait...");
        System.out.println("Total cache entry: " + RedisClusterHelper.getInstance().getAllKeys(RedisClusterHelper.getInstance().getCachePrefix() + "*").size());
        System.out.println("Removing all cache...");
        RedisClusterHelper.getInstance().clearAll(RedisClusterHelper.getInstance().getCachePrefix() + "*");
        System.out.println("All entry removed.");
        System.exit(0);
    }
}
