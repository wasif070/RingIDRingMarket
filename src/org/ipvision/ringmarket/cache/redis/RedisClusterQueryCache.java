/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache.redis;

/**
 *
 * @author alamgir
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import org.apache.openjpa.datacache.AbstractQueryCache;
import org.apache.openjpa.datacache.QueryKey;
import org.apache.openjpa.datacache.QueryResult;
import org.apache.openjpa.datacache.TypesChangedEvent;
import org.apache.openjpa.kernel.QueryStatistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisCluster;

public final class RedisClusterQueryCache
        extends AbstractQueryCache {

//    private CacheMap _cache;
    protected String _queryCachePrefix = "";
    protected String _queryCacheChannelPrefix = "";
    protected String _queryKeyCachePrefix = "";
//    protected String _queryKeyCachePrefix = "";
    protected JedisCluster _jedisPool = null;
    private Logger logger = LoggerFactory.getLogger(RedisClusterQueryCache.class);

    protected boolean _lru = false;
    private int _cacheSize = Integer.MIN_VALUE;
    private int _softRefs = Integer.MIN_VALUE;

    private Collection<String> keyset = new HashSet();
    private final RedisQueryKeyPublisher publisher;

//    public CacheMap getCacheMap() {
//        return _cache;
//    }
//
    public int getCacheSize() {
        return _cacheSize;
    }

    public void setCacheSize(int size) {
        _cacheSize = size;
    }

    public int getSoftReferenceSize() {
        return _softRefs;
    }

    public void setSoftReferenceSize(int size) {
        _softRefs = size;
    }
    public RedisClusterQueryCache() {
        RedisClusterHelper cacheHelper = RedisClusterHelper.getInstance();
        _jedisPool = cacheHelper.getJedisPool();
        _queryCachePrefix = cacheHelper.getCachePrefix() + "query:";
        _queryCacheChannelPrefix = cacheHelper.getCachePrefix() + "queryChannel:";
        _queryKeyCachePrefix = cacheHelper.getCachePrefix() + "queryKey:";
//        evictPolicy = EvictPolicy.TIMESTAMP;
        keyset = RedisClusterHelper.getInstance().getAllKeys(_queryKeyCachePrefix + "*");

        RedisQueryKeySubscriber subscriber = new RedisQueryKeySubscriber(keyset, _queryCacheChannelPrefix);
        CacheStatLogger.getInstance().setQueryCacheController(this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    _jedisPool.subscribe(subscriber, _queryCacheChannelPrefix);
                } catch (Exception e) {
                    e.printStackTrace();
                    // e.printStackTrace();
                }

            }
        }, "subscriberThread").start();
        publisher = new RedisQueryKeyPublisher(_jedisPool, _queryCacheChannelPrefix);

    }

    @Override
    public void onTypesChanged(TypesChangedEvent ev) {
        try {
            if (evictPolicy == EvictPolicy.DEFAULT) {
                writeLock();
                Collection<String> keys = null;
                try {
                    if (hasListeners()) {
                        fireEvent(ev);
                    }
                    keys = keySet();
                } finally {
                    writeUnlock();
                }

                QueryKey qk;
                List<QueryKey> removes = null;
                for (String o : keys) {
                    QueryKey result = getInternalJedisQueryKey((String) o);
                    qk = (QueryKey) result;
                    if (qk.changeInvalidatesQuery(ev.getTypes())) {
                        if (removes == null) {
                            removes = new ArrayList<>();
                        }
                        removes.add(qk);
                    }
                }
                if (removes != null) {
                    removeAllInternal(removes);
                }
            } else {
                Collection changedTypes = ev.getTypes();
                HashMap<String, Long> changedClasses
                        = new HashMap<>();
                Long tstamp = System.currentTimeMillis();
                for (Object o : changedTypes) {
                    String name = ((Class) o).getName();
                    if (!changedClasses.containsKey(name)) {
                        changedClasses.put(name, tstamp);
                    }
                }
                // Now update entity timestamp map
                updateEntityTimestamp(changedClasses);
            }
        } catch (Exception ex) {
            logger.error("Types change error!", ex);
        }
    }

    public void writeLock() {
//        _cache.writeLock();
    }

    public void writeUnlock() {
//        _cache.writeUnlock();
    }

//    protected CacheMap newCacheMap() {
//        CacheMap res = new CacheMap(_lru, 100000000);
//        return res;
//    }
    @Override
    public QueryResult getInternal(QueryKey queryKey) {
        String cacheKey = _queryCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        try {
            QueryResult result = getInternalJedis(cacheKey);
            return result;
        } catch (Exception ex) {
            logger.error("get query from cache error!", ex);
        }
        return null;
    }

    private QueryResult getInternalJedis(String cacheKey) {
        String fromCache = null;
        try {
            fromCache = _jedisPool.get(cacheKey);
            return (QueryResult) RedisClusterHelper.deserialize(fromCache);
        } catch (Exception ex) {
            logger.error("Cache enable but connection not established while query caching.", ex);
        }
        return null;
    }

    private QueryKey getInternalJedisQueryKey(String cacheKey) {
        String fromCache = null;
        try {
            fromCache = _jedisPool.get(cacheKey);
            return (QueryKey) RedisClusterHelper.deserialize(fromCache);
        } catch (Exception ex) {
            logger.error("Cache enable but connection not established while query caching.", ex);
        }
        return null;
    }

    @Override
    public QueryResult putInternal(QueryKey queryKey, QueryResult result) {
        String cacheKey = _queryCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        String queryCacheKey = _queryKeyCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();

//        if (result == null || result.isEmpty()) {
//            return result;
//        }
        try {
            _jedisPool.set(queryCacheKey, RedisClusterHelper.serialize(queryKey));
            _jedisPool.set(cacheKey, RedisClusterHelper.serialize(result));
            publisher.publish("ADD" + queryCacheKey);
            return result;
        } catch (Exception ex) {
            logger.error("Cache enable but connection not established while query caching.", ex);
        }
        return null;
    }

    @Override
    public QueryResult removeInternal(QueryKey queryKey) {
        String cacheKey = _queryCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        String queryCacheKey = _queryKeyCachePrefix + queryKey.getCandidateTypeName() + ":" + queryKey.toString();
        try {
            QueryResult data = getInternalJedis(cacheKey);
            _jedisPool.del(cacheKey);
            _jedisPool.del(queryCacheKey);
            publisher.publish("REMOVE" + queryCacheKey);
            return data;
        } catch (Exception ex) {
            logger.error("Remove query cache error!", ex);
        }
        return null;
    }

    @Override
    public void clearInternal() {
//        logger.info("RedisClusterQueryCache: clearInternal");
//        RedisClusterHelper.getInstance().clearAll(_queryCachePrefix + "*");
    }

    @Override
    public boolean pinInternal(QueryKey qk) {
        return false;
    }

    @Override
    public boolean unpinInternal(QueryKey qk) {
        return false;
    }

    @Override
    public Collection<String> keySet() {
        return keyset;
    }

    protected Collection queryKeySet() {
        return RedisClusterHelper.getInstance().getAllKeys(_queryKeyCachePrefix + "*");
    }

    @Override
    public AbstractQueryCache.EvictPolicy getEvictPolicy() {
        return super.evictPolicy;
    }

    public void setLru(boolean l) {
        _lru = l;
    }

    public boolean getLru() {
        return _lru;
    }

    public void printQueryCacheStat() {
        QueryStatistics stat = getStatistics();
        if (stat != null) {

            float cached = 0;
            if (stat.getTotalExecutionCount() > 0) {
                cached = (stat.getTotalHitCount() * 100) / stat.getTotalExecutionCount();
            }
            logger.debug(
                    "Query from cache (" + cached + "%),"
                    + "fetched(" + stat.getTotalExecutionCount() + "), "
                    + "from cache(" + stat.getTotalHitCount() + ")"
            );
        }
    }
}
