package org.ipvision.ringmarket.cache.redis;

import java.util.List;
import java.util.Map;

public interface CacheTransactions {

    // GET Cache String type object
    public String getCacheString(String key);

    // GET Cache List type object
    public List<String> getCacheList(String key);

    // GET Cache List type object with range
    public List<String> getCacheList(String key, int start, int end);

    // GET Cache Map type object
    public Map<String, String> getCacheMap(String key);

    // SET Cache String type object
    public void setCacheString(String key, String value);

    // SET Cache List type object
    public void setCacheList(String key, List<String> value);
    
    public void addToCacheList(String key, String... value);

    // SET Cache map type object
    public void setCacheMap(String key, Map<String, String> value);

    /*
	 * Delete Cache entries of given keys
     */
    public void deleteCache(String... keys);

    /*
	 * Delete Cache fields of given key
     */
    public void deleteFieldFromCacheMap(String key, String... fields);

    public boolean isCacheEnable();

}
