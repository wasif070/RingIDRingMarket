/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache.redis;

/**
 *
 * @author alamgir
 */
import org.apache.openjpa.datacache.DataCachePCData;
import org.apache.openjpa.util.OpenJPAId;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.Set;
import org.apache.openjpa.datacache.AbstractDataCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedisShardDataCache extends AbstractDataCache {

    Logger logger = LoggerFactory.getLogger(RedisShardDataCache.class);
    
    protected String _dataCachePrefix = "";
    protected String _classCachePrefix = "";
    protected ShardedJedisPool _jedisPool = null;

    
    public RedisShardDataCache(){
        RedisShardHelper cacheHelper = RedisShardHelper.getInstance();
        _jedisPool = cacheHelper.getJedisPool();
        _dataCachePrefix = cacheHelper.getCachePrefix() + "data:";
        _classCachePrefix = cacheHelper.getCachePrefix() + "class:";
    }

    @Override
    public void writeLock() {
    }

    @Override
    public void writeUnlock() {
    }

    @Override
    public boolean pinInternal(Object o) {
        return false;
    }

    @Override
    public boolean unpinInternal(Object o) {
        return false;
    }

    @Override
    public DataCachePCData removeInternal(Object o) {
        OpenJPAId oId = (OpenJPAId) o;
        String objectKey = _dataCachePrefix + oId.getType().getName() + ":" + o.toString();

        try (ShardedJedis jedis = _jedisPool.getResource()) {
            DataCachePCData data = getInternalJedis(jedis, objectKey);
            if (data == null) {
                return null;
            }
            String classKey = _classCachePrefix + data.getType().getName();

            jedis.del(objectKey);
            jedis.srem(classKey, objectKey);

            return data;
        }
    }

    @Override
    public void removeAllInternal(Class<?> aClass, boolean b) {
        String classKey = _classCachePrefix + aClass.getName();

        try (ShardedJedis jedis = _jedisPool.getResource()) {
            Set<String> classObjects = jedis.smembers(classKey);

            for (String entry : classObjects) {
                jedis.srem(classKey, entry);
                jedis.del(entry);
            }
        }

    }

    @Override
    public DataCachePCData getInternal(Object o) {
        
        OpenJPAId oId = (OpenJPAId) o;
        String objectKey = _dataCachePrefix + oId.getType().getName() + ":" + o.toString();

        ShardedJedis jedis = _jedisPool.getResource();
        try {
            DataCachePCData data = getInternalJedis(jedis, objectKey);
            return data;
        } catch(Exception ex){
            ex.printStackTrace();
            return null;
        } finally {
            _jedisPool.returnResource(jedis);
        }
    }

    @Override
    public DataCachePCData putInternal(Object o, DataCachePCData dataCachePCData) {
        String classKey = _classCachePrefix + dataCachePCData.getType().getName();

        OpenJPAId oId = (OpenJPAId) o;
        String objectKey = _dataCachePrefix + oId.getType().getName() + ":" + o.toString();

        ShardedJedis jedis = _jedisPool.getResource();
        try {
            jedis.set(objectKey, RedisShardHelper.serialize(dataCachePCData));
            jedis.sadd(classKey, objectKey);
        }
        catch(Exception ex){
            logger.error(String.format("Cache Enabled but not connected. Caching data failed (id : %s )" , o) );
        }
        finally{
            _jedisPool.returnResource(jedis);
        }
        return dataCachePCData;
    }

    @Override
    public void clearInternal() {
        // TODO there's a race here, obviously.
        RedisShardHelper.getInstance().clearAll(_classCachePrefix + "*");
        RedisShardHelper.getInstance().clearAll(_dataCachePrefix + "*");
    }

    private DataCachePCData getInternalJedis(ShardedJedis jedis, String objectKey) {
        try {
            String fromCache = jedis.get(objectKey);
            if (fromCache == null) {
                return null;
            }
            return (DataCachePCData) RedisShardHelper.deserialize(fromCache);
        } catch (Exception ex) {
            return null;
        }
    }

}
