/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache.redis;

import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisPubSub;

/**
 *
 * @author alamgir
 */
public class RedisQueryKeySubscriber extends JedisPubSub {

    private final Logger logger = LoggerFactory.getLogger(RedisQueryKeySubscriber.class);
    private final Collection<String> keySet;
    private final String _queryCacheChannelPrefix;

    public RedisQueryKeySubscriber(Collection<String> keySet, String _queryCacheChannelPrefix) {
        this.keySet = keySet;
        this._queryCacheChannelPrefix = _queryCacheChannelPrefix;
        
    }

    @Override
    public void onMessage(String channel, String message) {
        if (channel.equals(_queryCacheChannelPrefix)) {
            if (message.startsWith("ADD")) {
                message = message.replace("ADD", "");
                this.keySet.add(message);
                logger.info("Adding new key: " + message);
            } else if (message.startsWith("REMOVE")) {
                message = message.replace("REMOVE", "");
                this.keySet.remove(message);
                logger.info("Removing key: " + message);
            }
        }
    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        logger.info("Subscribe channel :" + channel);
    }

}
