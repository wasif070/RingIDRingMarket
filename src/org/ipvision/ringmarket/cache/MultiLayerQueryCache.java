/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache;

/**
 *
 * @author alamgir
 */

import org.ipvision.ringmarket.cache.redis.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Cacheable;
import org.apache.openjpa.datacache.AbstractQueryCache;
import org.apache.openjpa.datacache.DataCacheManager;
import org.apache.openjpa.datacache.QueryKey;
import org.apache.openjpa.datacache.QueryResult;
import org.apache.openjpa.datacache.TypesChangedEvent;

import org.apache.openjpa.event.RemoteCommitListener;
import org.apache.openjpa.util.CacheMap;
import org.apache.openjpa.util.OpenJPAId;
import org.ipvision.ringmarket.cache.annotation.Cache;

/**
 * A {@link QueryCache} implementation that is optimized for concurrent
 * access. When the cache fill up, values to remove from the cache are chosen
 * randomly. Due to race conditions, it is possible that a get call might not
 * retur a cached instance if that instance is being transferred between
 * internal datastructures.
 *
 * @since 0.4.1
 */
public class MultiLayerQueryCache extends AbstractQueryCache implements RemoteCommitListener {

    private JVMQueryCache jvmQueryCache = null;
    private RedisShardQueryCache redisQueryCache = null;
    private int _cacheSize = 1000000;
    private int _softReferenceSize = 1000000;
    
    /**
     * Sets the maximum number of unpinned objects to keep hard references to.
     * If the map contains more unpinned objects than <code>size</code>, then
     * this method will result in the cache flushing old values.
     * @param size
     */
    public void setCacheSize(int size) {
        _cacheSize = size;
    }

    /**
     * Returns the maximum number of unpinned objects to keep hard references
     * to.
     * @return 
     */
    public int getCacheSize() {
        return _cacheSize;
    }

    /**
     * Sets the maximum number of unpinned objects to keep soft references to.
     * If the map contains more soft references than <code>size</code>, then
     * this method will result in the cache flushing values.
     * @param size
     */
    public void setSoftReferenceSize(int size) {
        _softReferenceSize = size;
    }

    /**
     * Returns the maximum number of unpinned objects to keep soft references
     * to. Defaults to <code>-1</code>.
     * @return 
     */
    public int getSoftReferenceSize() {
        return _softReferenceSize;
    }
    
    private IQueryCache getCacheProvider(String className){

        IQueryCache cacheProvider = null;
        try {
            Class cls = Class.forName(className);
            Cacheable cacheable = (Cacheable)cls.getAnnotation(Cacheable.class);
            Cache cache = (Cache)cls.getAnnotation(Cache.class);
            
            if(cacheable == null){
                return null;
            }
            
            if(cache == null){
                return jvmQueryCache;
            }
            
            switch(cache.type()){
                case JVM:
                    cacheProvider = jvmQueryCache;
                    break;
                case REDIS:
                    cacheProvider = redisQueryCache;
                    break;
                default:
                    cacheProvider = jvmQueryCache;
                    break;
            }
            
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return cacheProvider;
    }
    
    @Override
    public void initialize(DataCacheManager mgr) {
        super.initialize(mgr);
        conf.getRemoteCommitEventManager().addInternalListener(this);
        jvmQueryCache = new JVMQueryCache();
        redisQueryCache = new RedisShardQueryCache();
        
        jvmQueryCache.setCacheSize(getCacheSize());
        jvmQueryCache.setSoftReferenceSize(getSoftReferenceSize());

    }
@Override
    public void onTypesChanged(TypesChangedEvent ev) {
        if (evictPolicy == EvictPolicy.DEFAULT) {
            writeLock();
            Collection keys = null;
            try {
                if (hasListeners())
                    fireEvent(ev);
                keys = keySet();
            } finally {
                writeUnlock();
            }
    
            if(keys == null){
                return;
            }
            QueryKey qk;
                List<QueryKey> removes = null;
                for (Object o: keys) {
                    qk = (QueryKey) o;
                if (qk.changeInvalidatesQuery(ev.getTypes())) {
                    if (removes == null)
                        removes = new ArrayList<QueryKey>();
                    removes.add(qk);
                }
            }
            if (removes != null)
                removeAllInternal(removes);
        } else {
            Collection changedTypes = ev.getTypes();
            HashMap<String,Long> changedClasses = 
                new HashMap<String,Long>();
            Long tstamp = Long.valueOf(System.currentTimeMillis());
            for (Object o: changedTypes) {
                String name = ((Class) o).getName();
                if(!changedClasses.containsKey(name)) {
                    changedClasses.put(name, tstamp );
                }
            }           
            // Now update entity timestamp map
            updateEntityTimestamp(changedClasses);
        }
        
    }
    
    @Override
    public void writeLock() {
        
    }

    @Override
    public void writeUnlock() {
    }

    @Override
    protected QueryResult getInternal(QueryKey qk) {
        IQueryCache queryCache = getCacheProvider(qk.getCandidateTypeName());
        if(queryCache != null){
            return (QueryResult) queryCache.getInternal(qk);
        }
        return null;
    }

    @Override
    protected QueryResult putInternal(QueryKey qk, QueryResult result) {
        if(result == null || result.size() <= 0 ){
            return null;
        }
        
        IQueryCache queryCache = getCacheProvider(qk.getCandidateTypeName());
        if(queryCache != null){
            return (QueryResult) queryCache.putInternal(qk, result);
        }
        return null;
    }

    @Override
    protected QueryResult removeInternal(QueryKey qk) {
        IQueryCache queryCache = getCacheProvider(qk.getCandidateTypeName());
        if(queryCache != null){
            return (QueryResult) queryCache.removeInternal(qk);
        }
        return null;
    }

    @Override
    protected void clearInternal() {
        if(jvmQueryCache != null){
            jvmQueryCache.clearInternal();
        }
        if(redisQueryCache != null){
            redisQueryCache.clearInternal();
        }
    }

    @Override
    protected boolean pinInternal(QueryKey qk) {
        IQueryCache queryCache = getCacheProvider(qk.getCandidateTypeName());
        if(queryCache != null){
            return queryCache.pinInternal(qk);
        }
        return false;
    }

    @Override
    protected boolean unpinInternal(QueryKey qk) {
        IQueryCache queryCache = getCacheProvider(qk.getCandidateTypeName());
        if(queryCache != null){
            return queryCache.unpinInternal(qk);
        }
        return false;
    }


    /**
     * Returns the eviction policy of the query cache
     * @return 
     */
    @Override
    public AbstractQueryCache.EvictPolicy getEvictPolicy() {
        return EvictPolicy.DEFAULT;
    }

//    @Override
    protected Collection keySet() {
        return null;
    }

}

