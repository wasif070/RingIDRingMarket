/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache;

import java.util.concurrent.TimeUnit;
import org.ipvision.ringmarket.db.DBConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author saikat
 */
public class ProductCategoryCacheReloader extends Thread {

    private static ProductCategoryCacheReloader instance;
    private Logger logger = LoggerFactory.getLogger(ProductCategoryCacheReloader.class);
    private volatile boolean running = false;

    private ProductCategoryCacheReloader() {
        this.running = true;
        setName("ProductCategoryCacheReloader");
    }

    public static ProductCategoryCacheReloader getInstance() {
        if (instance == null) {
            instance = new ProductCategoryCacheReloader();
        }
        return instance;
    }

    @Override
    public void run() {
        while (running) {
            try {
                ProductCategoryCache.getInstance().forceReload();
                if(DBConnection.CACHEABLE){
                    TimeUnit.MINUTES.sleep(5);
                }
                else{
                    TimeUnit.MINUTES.sleep(30);
                }
                logger.info("Loading product category cache.");
            } catch (InterruptedException ex) {
                logger.error(ex.getLocalizedMessage());
            }
        }
    }

    public void stopReloader(){
        this.running = false;
    }
}
