/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache;

import org.apache.openjpa.datacache.DataCachePCData;

/**
 *
 * @author alamgir
 */
public interface IDataCache{

    
    public DataCachePCData getInternal(Object oid);

    
    public DataCachePCData putInternal(Object oid, DataCachePCData pc) ;

    
    public DataCachePCData removeInternal(Object oid);

    
    public void removeAllInternal(Class<?> cls, boolean subclasses);

    
    public void clearInternal();

    
    public boolean pinInternal(Object oid);

    
    public boolean unpinInternal(Object oid);

    
    public void writeLock() ;

    
    public void writeUnlock() ;
    
}
