///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.ipvision.ringmarket.cache;
//
//import javax.persistence.Cacheable;
//import org.apache.openjpa.datacache.AbstractDataCache;
//import org.apache.openjpa.datacache.DataCacheManager;
//import org.apache.openjpa.datacache.DataCachePCData;
//import org.apache.openjpa.event.RemoteCommitListener;
//import org.apache.openjpa.util.OpenJPAId;
//import org.ipvision.ringmarket.cache.annotation.Cache;
//import org.ipvision.ringmarket.cache.redis.RedisDataCache;
//
///**
// *
// * @author alamgir
// */
//public class MultiLayerDataCache extends AbstractDataCache implements RemoteCommitListener{
//
//    private JVMDataCache jvmDataCache = null;
//    private RedisDataCache redisDataCache = null;
//    private int _cacheSize = 1000000;
//    private int _softReferenceSize = 0;
//    
//    /**
//     * Sets the maximum number of unpinned objects to keep hard references to.
//     * If the map contains more unpinned objects than <code>size</code>, then
//     * this method will result in the cache flushing old values.
//     * @param size
//     */
//    public void setCacheSize(int size) {
//        _cacheSize = size;
//    }
//
//    /**
//     * Returns the maximum number of unpinned objects to keep hard references
//     * to.
//     * @return 
//     */
//    public int getCacheSize() {
//        return _cacheSize;
//    }
//
//    /**
//     * Sets the maximum number of unpinned objects to keep soft references to.
//     * If the map contains more soft references than <code>size</code>, then
//     * this method will result in the cache flushing values.
//     * @param size
//     */
//    public void setSoftReferenceSize(int size) {
//        _softReferenceSize = size;
//    }
//
//    /**
//     * Returns the maximum number of unpinned objects to keep soft references
//     * to. Defaults to <code>-1</code>.
//     * @return 
//     */
//    public int getSoftReferenceSize() {
//        return _softReferenceSize;
//    }
//    
//    
//    private IDataCache getCacheProvider(OpenJPAId oid){
//
//        Class cls = oid.getType();
//        Cache cache = (Cache)cls.getAnnotation(Cache.class);
//        Cacheable cacheable = (Cacheable)cls.getAnnotation(Cacheable.class);
//        
//        IDataCache cacheProvider = null;
//        if(cacheable == null){
//            return null;
//        }
//        
//        if(cache == null){
//            return jvmDataCache;
//        }
//
//        switch(cache.type()){
//            case JVM: 
//                cacheProvider = jvmDataCache;
//                break;
//            case REDIS:
//                cacheProvider = redisDataCache;
//                break;
//            default:
//                cacheProvider = jvmDataCache;
//                break;
//        }
//        return cacheProvider;
//    }
//     
//    @Override
//    public synchronized void  initialize(DataCacheManager mgr) {
//        super.initialize(mgr);
//        conf.getRemoteCommitEventManager().addInternalListener(this);
//        jvmDataCache = new JVMDataCache(this);
//        redisDataCache = new RedisDataCache();
//        
//        jvmDataCache.setSoftReferenceSize(getSoftReferenceSize());
//        jvmDataCache.setCacheSize(getCacheSize());
//        
//        jvmDataCache.setLru(true);
//        
//    }
//    
//    @Override
//    protected DataCachePCData getInternal(Object oid) {
//        IDataCache cacheProvider = getCacheProvider((OpenJPAId)oid);
//        if(cacheProvider != null){
//            return cacheProvider.getInternal(oid);
//        }
//        return null;
//    }
//
//    @Override
//    protected DataCachePCData putInternal(Object oid, DataCachePCData pc) {
//        IDataCache cacheProvider = getCacheProvider((OpenJPAId)oid);
//        if(cacheProvider != null){
//            return cacheProvider.putInternal(oid, pc);
//        }
//        return null;
//    }
//
//    @Override
//    protected DataCachePCData removeInternal(Object oid) {
//        IDataCache cacheProvider = getCacheProvider((OpenJPAId)oid);
//        if(cacheProvider != null){
//            return cacheProvider.removeInternal(oid);
//        }
//        return null;
//    }
//
//    @Override
//    protected void removeAllInternal(Class<?> cls, boolean subclasses) {}
//
//    @Override
//    protected void clearInternal() {
//        if(jvmDataCache != null){
//            jvmDataCache.clearInternal();
//        }
//        if(redisDataCache != null){
//            redisDataCache.clearInternal();
//        }
//    }
//
//    @Override
//    protected boolean pinInternal(Object oid) {
//        IDataCache cacheProvider = getCacheProvider((OpenJPAId)oid);
//        if(cacheProvider != null){
//            return cacheProvider.pinInternal(oid);
//        }
//        return false;
//    }
//
//    @Override
//    protected boolean unpinInternal(Object oid) {
//        IDataCache cacheProvider = getCacheProvider((OpenJPAId)oid);
//        if(cacheProvider != null){
//            return cacheProvider.unpinInternal(oid);
//        }
//        return false;
//    }
//
//    @Override
//    public void writeLock() {
//        
//    }
//
//    @Override
//    public void writeUnlock() {
//        
//    }
//    
//}
