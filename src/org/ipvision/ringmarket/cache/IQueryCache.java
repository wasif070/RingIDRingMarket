/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache;

import java.util.Collection;
import org.apache.openjpa.datacache.QueryKey;
import org.apache.openjpa.datacache.QueryResult;

/**
 *
 * @author alamgir
 */
public interface IQueryCache {
 
    public void writeLock() ;

    public void writeUnlock() ;

    public QueryResult getInternal(QueryKey qk);

    public QueryResult putInternal(QueryKey qk, QueryResult result) ;

    public QueryResult removeInternal(QueryKey qk);

    public void clearInternal() ;

    public boolean pinInternal(QueryKey qk) ;

    public boolean unpinInternal(QueryKey qk) ;

    public Collection keySet() ;

}
