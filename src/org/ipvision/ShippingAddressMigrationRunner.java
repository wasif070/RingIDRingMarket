///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.ipvision;
//
//import com.google.gson.Gson;
//import java.util.List;
//import org.ipvision.ringmarket.entities.order.Address;
//import org.ipvision.ringmarket.entities.order.EntityShippingAddress;
//import org.ipvision.ringmarket.repositories.order.ShippingAddressRepoImpl;
//import org.junit.runner.JUnitCore;
//import org.junit.runner.Result;
//import org.junit.runner.notification.Failure;
//
///**
// *
// * @author saikat
// */
//public class ShippingAddressMigrationRunner {
//
//    public static void main(String[] args) {
//        Gson gson = new Gson();
//        ShippingAddressRepoImpl addressRepoImpl = new ShippingAddressRepoImpl();
//        List<EntityShippingAddress> allAddresses = addressRepoImpl.getAllAddresses();
//        if (!allAddresses.isEmpty()) {
//            for (EntityShippingAddress entityAddress : allAddresses) {
//                Address address = gson.fromJson(entityAddress.getAddressJson(), Address.class);
//
//                if (address.getEmail() == null || address.getDialingCode() == null || address.getContactName() == null
//                        || address.getPostCode() == null || address.getMobile() == null || address.getCity() == null
//                        || address.getCountry() == null) {
//                    addressRepoImpl.remove(entityAddress.getId());
//                } else {
//                    addressRepoImpl.updateShippingAddress(entityAddress.getId(), address);
//                }
//
//            }
//        }
//    }
//}
