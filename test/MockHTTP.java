
import org.ipvision.ringmarket.controller.MarketController;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class MockHTTP {

    public MockMvc mockMvc;

    @InjectMocks
    private MarketController requestController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(requestController)
                .build();
    }

    public String getResponse(String request) {
        try {

            MvcResult result = mockMvc.perform(post("/market").param("request", request)).andReturn();

            String resultContent = result.getResponse().getContentAsString();
            return resultContent;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
