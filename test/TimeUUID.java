/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author saikat
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
//import com.google.common.base.Charsets;

/**
 * Utility methods to work with UUID and most specifically with time-based ones
 * (version 1).
 */
public final class TimeUUID {

    private TimeUUID() {
    }

    private TimeUUID(UUID uuid) {
        this.mostSigBits = uuid.getMostSignificantBits();
        this.leastSigBits = uuid.getLeastSignificantBits();
    }

    public TimeUUID(long mostSigBits, long leastSigBits) {
        this.mostSigBits = mostSigBits;
        this.leastSigBits = leastSigBits;
    }

    // http://www.ietf.org/rfc/rfc4122.txt
    private static final long START_EPOCH = makeEpoch();
    private static final long CLOCK_SEQ_AND_NODE = makeClockSeqAndNode();

    public static final TimeUUID EMPTY_UUID = new TimeUUID(0l, 0l);
    private static long mostSigBits;
    private static long leastSigBits;

    /*
     * The min and max possible lsb for a UUID.
     * Note that his is not 0 and all 1's because Cassandra TimeUUIDType
     * compares the lsb parts as a signed byte array comparison. So the min
     * value is 8 times -128 and the max is 8 times +127.
     *
     * Note that we ignore the uuid variant (namely, MIN_CLOCK_SEQ_AND_NODE
     * have variant 2 as it should, but MAX_CLOCK_SEQ_AND_NODE have variant 0)
     * because I don't trust all uuid implementation to have correctly set
     * those (pycassa don't always for instance).
     */
    private static final long MIN_CLOCK_SEQ_AND_NODE = 0x8080808080808080L;
    private static final long MAX_CLOCK_SEQ_AND_NODE = 0x7f7f7f7f7f7f7f7fL;

    private static final AtomicLong lastTimeStamp = new AtomicLong(0L);

    private static long makeEpoch() {
        // UUID v1 timestamp must be in 100-nanoseconds interval since 00:00:00.000 15 Oct 1582.
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT-0"));
        c.set(Calendar.YEAR, 1582);
        c.set(Calendar.MONTH, Calendar.OCTOBER);
        c.set(Calendar.DAY_OF_MONTH, 15);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();
    }

    private static long makeNode() {

        /*
         * We don't have access to the MAC address (in pure JAVA at least) but
         * need to generate a node part that identify this host as uniquely as
         * possible.
         * The spec says that one option is to take as many source that
         * identify this node as possible and hash them together. That's what
         * we do here by gathering all the ip of this host as well as a few
         * other sources.
         */
        try {

            MessageDigest digest = MessageDigest.getInstance("MD5");
            for (String address : getAllLocalAddresses()) {
                update(digest, address);
            }

            Properties props = System.getProperties();
            update(digest, props.getProperty("java.vendor"));
            update(digest, props.getProperty("java.vendor.url"));
            update(digest, props.getProperty("java.version"));
            update(digest, props.getProperty("os.arch"));
            update(digest, props.getProperty("os.name"));
            update(digest, props.getProperty("os.version"));

            byte[] hash = digest.digest();

            long node = 0;
            for (int i = 0; i < 6; i++) {
                node |= (0x00000000000000ffL & (long) hash[i]) << (i * 8);
            }
            // Since we don't use the mac address, the spec says that multicast
            // bit (least significant bit of the first byte of the node ID) must be 1.
            return node | 0x0000010000000000L;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private static void update(MessageDigest digest, String value) {
        if (value != null) {
            try {
                digest.update(value.getBytes("UTF-8"));
            }
            catch (UnsupportedEncodingException ex) {

            }
        }
    }

    private static long makeClockSeqAndNode() {
        long clock = new Random(System.currentTimeMillis()).nextLong();
        long node = makeNode();

        long lsb = 0;
        lsb |= (clock & 0x0000000000003FFFL) << 48;
        lsb |= 0x8000000000000000L;
        lsb |= node;
        leastSigBits = lsb;
        return lsb;
    }

    /**
     * Creates a new random (version 4) UUID.
     * <p>
     * This method is just a convenience for {@code UUID.randomUUID()}.
     *
     * @return a newly generated, pseudo random, version 4 UUID.
     */
    public static TimeUUID random() {
        return new TimeUUID(UUID.randomUUID());
    }

    /**
     * Creates a new time-based (version 1) UUID.
     * <p>
     * UUID generated by this method are suitable for use with the
     * {@code timeuuid} Cassandra type. In particular the generated UUID
     * includes the timestamp of its generation.
     *
     * @return a new time-based UUID.
     */
    public static TimeUUID timeBased() {
        return new TimeUUID(makeMSB(getCurrentTimestamp()), CLOCK_SEQ_AND_NODE);
    }

    /**
     * Creates a "fake" time-based UUID that sorts as the smallest possible
     * version 1 UUID generated at the provided timestamp.
     * <p>
     * Such created UUID are useful in queries to select a time range of a
     * {@code timeuuid} column.
     * <p>
     * The UUID created by this method <b>are not unique</b> and as such are
     * <b>not</b> suitable for anything else than querying a specific time
     * range. In particular, you should not insert such UUID.
     * <p>
     * Also, the timestamp to provide as parameter must be a unix timestamp (as
     * returned by {@link System#currentTimeMillis} or
     * {@link java.util.Date#getTime}), not a UUID 100-nanoseconds intervals
     * since 15 October 1582. In other words, given a UUID {@code uuid}, you
     * should never do {@code startOf(uuid.timestamp())} but rather
     * {@code startOf(unixTimestamp(uuid.timestamp()))}.
     * <p>
     * Lastly, please note that Cassandra's timeuuid sorting is not compatible
     * with {@link UUID#compareTo} and hence the UUID created by this method are
     * not necessarily lower bound for that latter method.
     *
     * @param timestamp the unix timestamp for which the created UUID must be a
     * lower bound.
     * @return the smallest (for Cassandra timeuuid sorting) UUID of
     * {@code timestamp}.
     */
    public static TimeUUID startOf(long timestamp) {
        return new TimeUUID(makeMSB(fromUnixTimestamp(timestamp)), MIN_CLOCK_SEQ_AND_NODE);
    }

    /**
     * Creates a "fake" time-based UUID that sorts as the biggest possible
     * version 1 UUID generated at the provided timestamp.
     * <p>
     * Such created UUID are useful in queries to select a time range of a
     * {@code timeuuid} column.
     * <p>
     * The UUID created by this method <b>are not unique</b> and as such are
     * <b>not</b> suitable for anything else than querying a specific time
     * range. In particular, you should not insert such UUID.
     * <p>
     * Also, the timestamp to provide as parameter must be a unix timestamp (as
     * returned by {@link System#currentTimeMillis} or
     * {@link java.util.Date#getTime}), not a UUID 100-nanoseconds intervals
     * since 15 October 1582. In other words, given a UUID {@code uuid}, you
     * should never do {@code startOf(uuid.timestamp())} but rather
     * {@code startOf(unixTimestamp(uuid.timestamp()))}.
     * <p>
     * Lastly, please note that Cassandra's timeuuid sorting is not compatible
     * with {@link UUID#compareTo} and hence the UUID created by this method are
     * not necessarily upper bound for that latter method.
     *
     * @param timestamp the unix timestamp for which the created UUID must be an
     * upper bound.
     * @return the biggest (for Cassandra timeuuid sorting) UUID of
     * {@code timestamp}.
     */
    public static TimeUUID endOf(long timestamp) {
        long uuidTstamp = fromUnixTimestamp(timestamp + 1) - 1;
        return new TimeUUID(makeMSB(uuidTstamp), MAX_CLOCK_SEQ_AND_NODE);
    }

    /**
     * Return the unix timestamp contained by the provided time-based UUID.
     * <p>
     * This method is not equivalent to {@code uuid.timestamp()}. More
     * precisely, a version 1 UUID stores a timestamp that represents the number
     * of 100-nanoseconds intervals since midnight, 15 October 1582 and that is
     * what {@code uuid.timestamp()} returns. This method however converts that
     * timestamp to the equivalent unix timestamp in milliseconds, i.e. a
     * timestamp representing a number of milliseconds since midnight, January
     * 1, 1970 UTC. In particular the timestamps returned by this method are
     * comparable to the timestamp returned by
     * {@link System#currentTimeMillis}, {@link java.util.Date#getTime}, etc.
     *
     * @param uuid the UUID to return the timestamp of.
     * @return the unix timestamp of {@code uuid}.
     *
     * @throws IllegalArgumentException if {@code uuid} is not a version 1 UUID.
     */
    public static long unixTimestamp(TimeUUID uuid) {
        if (uuid.version() != 1) {
            throw new IllegalArgumentException(String.format("Can only retrieve the unix timestamp for version 1 uuid (provided version %d)", uuid.version()));
        }

        long timestamp = uuid.timestamp();
        return (timestamp / 10000) + START_EPOCH;
    }

    /**
     * The version number associated with this {@code UUID}. The version
     * number describes how this {@code UUID} was generated.
     *
     * The version number has the following meaning:
     * <ul>
     * <li>1 Time-based UUID
     * <li>2 DCE security UUID
     * <li>3 Name-based UUID
     * <li>4 Randomly generated UUID
     * </ul>
     *
     * @return The version number of this {@code UUID}
     */
    public int version() {
        // Version is bits masked by 0x000000000000F000 in MS long
        return (int) ((mostSigBits >> 12) & 0x0f);
    }

    /**
     * The timestamp value associated with this UUID.
     *
     * <p>
     * The 60 bit timestamp value is constructed from the time_low,
     * time_mid, and time_hi fields of this {@code UUID}. The resulting
     * timestamp is measured in 100-nanosecond units since midnight,
     * October 15, 1582 UTC.
     *
     * <p>
     * The timestamp value is only meaningful in a time-based UUID, which
     * has version type 1. If this {@code UUID} is not a time-based UUID then
     * this method throws UnsupportedOperationException.
     *
     * @throws UnsupportedOperationException
     * If this UUID is not a version 1 UUID
     * @return The timestamp of this {@code UUID}.
     */
    public long timestamp() {
        if (version() != 1) {
            throw new UnsupportedOperationException("Not a time-based UUID");
        }

        return (mostSigBits & 0x0FFFL) << 48
                | ((mostSigBits >> 16) & 0x0FFFFL) << 32
                | mostSigBits >>> 32;
    }

    /*
     * Note that currently we use System.currentTimeMillis() for a base time in
     * milliseconds, and then if we are in the same milliseconds that the
     * previous generation, we increment the number of nanoseconds.
     * However, since the precision is 100-nanoseconds intervals, we can only
     * generate 10K UUID within a millisecond safely. If we detect we have
     * already generated that much UUID within a millisecond (which, while
     * admittedly unlikely in a real application, is very achievable on even
     * modest machines), then we stall the generator (busy spin) until the next
     * millisecond as required by the RFC.
     */
    public static long getCurrentTimestamp() {
        while (true) {
            long now = fromUnixTimestamp(System.currentTimeMillis());
            long last = lastTimeStamp.get();
            if (now > last) {
                if (lastTimeStamp.compareAndSet(last, now)) {
                    return now;
                }
            }
            else {
                long lastMillis = millisOf(last);
                // If the clock went back in time, bail out
                if (millisOf(now) < millisOf(last)) {
                    return lastTimeStamp.incrementAndGet();
                }

                long candidate = last + 1;
                // If we've generated more than 10k uuid in that millisecond,
                // we restart the whole process until we get to the next millis.
                // Otherwise, we try use our candidate ... unless we've been
                // beaten by another thread in which case we try again.
                if (millisOf(candidate) == lastMillis && lastTimeStamp.compareAndSet(last, candidate)) {
                    return candidate;
                }
            }
        }
    }

    // Package visible for testing
    static long fromUnixTimestamp(long tstamp) {
        return (tstamp - START_EPOCH) * 10000;
    }

    private static long millisOf(long timestamp) {
        return timestamp / 10000;
    }

    // Package visible for testing
    static long makeMSB(long timestamp) {
        long msb = 0L;
        msb |= (0x00000000ffffffffL & timestamp) << 32;
        msb |= (0x0000ffff00000000L & timestamp) >>> 16;
        msb |= (0x0fff000000000000L & timestamp) >>> 48;
        msb |= 0x0000000000001000L; // sets the version to 1.
        mostSigBits = msb;
        return msb;
    }

    private static Set<String> getAllLocalAddresses() {
        Set<String> allIps = new HashSet<>();
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            allIps.add(localhost.toString());
            // Also return the hostname if available, it won't hurt (this does a dns lookup, it's only done once at startup)
            allIps.add(localhost.getCanonicalHostName());
            InetAddress[] allMyIps = InetAddress.getAllByName(localhost.getCanonicalHostName());
            if (allMyIps != null) {
                for (InetAddress allMyIp : allMyIps) {
                    allIps.add(allMyIp.toString());
                }
            }
        }
        catch (UnknownHostException e) {
            // Ignore, we'll try the network interfaces anyway
        }

        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            if (en != null) {
                while (en.hasMoreElements()) {
                    Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                    while (enumIpAddr.hasMoreElements()) {
                        allIps.add(enumIpAddr.nextElement().toString());
                    }
                }
            }
        }
        catch (SocketException e) {
            // Ignore, if we've really got nothing so far, we'll throw an exception
        }

        return allIps;
    }

    public static TimeUUID fromString(String name) {
        String[] components = name.split("-");
        if (components.length != 5) {
            throw new IllegalArgumentException("Invalid UUID string: " + name);
        }
        for (int i = 0; i < 5; i++) {
            components[i] = "0x" + components[i];
        }

        long mostSigBits = Long.decode(components[2]);
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(components[1]);
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(components[0]);

        long leastSigBits = Long.decode(components[3]);
        leastSigBits <<= 48;
        leastSigBits |= Long.decode(components[4]);

        return new TimeUUID(mostSigBits, leastSigBits);
    }

    private static String digits(long val, int digits) {
        long hi = 1L << (digits * 4);
        return Long.toHexString(hi | (val & (hi - 1))).substring(1);
    }

    @Override
    public String toString() {
        return (digits(mostSigBits, 4) + "-"
                + digits(mostSigBits >> 16, 4) + "-"
                + digits(mostSigBits >> 32, 8) + "-"
                + digits(leastSigBits >> 48, 4) + "-"
                + digits(leastSigBits, 12));
    }
    
    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis());
    }
}
