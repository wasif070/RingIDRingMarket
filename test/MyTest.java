/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import ExRunner.ExtendedRunner;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.product.EntityProductImage;
import org.ipvision.ringmarket.entities.EntitySavedProduct;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductImageRepositoryImpl;
import org.ipvision.ringmarket.product.buyer.SavedProduct;
import org.ipvision.ringmarket.product.seller.SellerProduct;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Imran
 */
@RunWith(ExtendedRunner.class)
public class MyTest {
    static float[] lat = new float[10];
    static float[] lon = new float[10];
    static String[] name = new String[10];
    static int cnt = 0;
    @BeforeClass
    public static void setUpClass() {
        lat[0] = (float) 23.71; lon[0] = (float) 90.35; name[0] = "watch ";
        lat[1] = (float) 23.72; lon[1] = (float) 90.36; name[1] = "guitar ";
        lat[2] = (float) 23.73; lon[2] = (float) 90.37; name[2] = "iphone ";
        lat[3] = (float) 23.74; lon[3] = (float) 90.38; name[3] = "android ";
        lat[4] = (float) 23.75; lon[4] = (float) 90.39; name[4] = "novel ";
        lat[5] = (float) 23.76; lon[5] = (float) 90.40; name[5] = "shoe ";
        lat[6] = (float) 23.77; lon[6] = (float) 90.39; name[6] = "t-shirt ";
        lat[7] = (float) 23.78; lon[7] = (float) 90.38; name[7] = "soap ";
        lat[8] = (float) 23.79; lon[8] = (float) 90.37; name[8] = "polo ";
        lat[9] = (float) 23.80; lon[9] = (float) 90.36; name[9] = "cake ";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Ignore
    @Test
    @ExtendedRunner.Repeat(10)
    public void Test1() {
        EntityProduct iProduct = new EntityProduct();

        iProduct.setId(UUID.randomUUID().toString());
        iProduct.setName(name[cnt] + UUID.randomUUID().toString().substring(15, 25));
//        iProduct.setIsFixedPrice(false);
        iProduct.setIsExclusive(Boolean.FALSE);
        if(cnt == 5){
            iProduct.setIsExclusive(Boolean.TRUE);
        }
        double price = new Random().nextInt(100);
        iProduct.setPrice(new Double(price));
        iProduct.setLat(lat[cnt]);
        iProduct.setLon(lon[cnt]);
        iProduct.setImageUrl(UUID.randomUUID().toString().substring(15, 30));
//        iProduct.setCategoryId(1);
//        iProduct.setViewCount(new Random().nextInt(100));
        iProduct.setStatus(ProductStatus.AVAILABLE.getValue());
        ProductRepositoryImpl product = new ProductRepositoryImpl();
        product.addProduct(iProduct);
        
        ++cnt;
        cnt = cnt % 10;
    }
    
    @Ignore
    @Test
    @ExtendedRunner.Repeat(10)
    public void Test2() {
        SavedProduct product = new SavedProduct();
        product.addToSavedList(1, UUID.randomUUID().toString());
    }

    @Ignore
    @Test
    public void Test3() {
        ProductRepositoryImpl product = new ProductRepositoryImpl();
        product.getMostViewedProducts("", Scroll.DOWN, 5);
    }

    
    @Ignore
    @Test
    @ExtendedRunner.Repeat(6)
    public void Test4() {
        ProductImageRepositoryImpl product = new ProductImageRepositoryImpl();
        product.addImage(UUID.randomUUID().toString().substring(11, 20), 5, 8, "aewaer");
    }
    
    @Ignore
    @Test
    public void Test5() {
        ProductImageRepositoryImpl product = new ProductImageRepositoryImpl();
        List<EntityProductImage> images = product.getImagesOfProduct("aewaer");
        for (EntityProductImage image : images) {
            System.out.println(image.getImageUrl());
        }
    }

    @Ignore
    @Test
    public void Test6() {
        SavedProduct product = new SavedProduct();
        List<EntitySavedProduct> items = product.getSavedProductList((long) 1, "", Scroll.DOWN, 5);
        for (EntitySavedProduct image : items) {
            System.out.println(image.getProductId());
        }
    }

    @Ignore
    @Test
    public void Test7() {
        SellerProduct sellerProduct = new SellerProduct();
        sellerProduct.addNewProduct(4l, UUID.randomUUID().toString(), System.currentTimeMillis(), "prod name", "prod image");
    }

    @Ignore
    @Test
    public void Test8() {
//        AgentOrder agentProduct = new AgentOrder();
//        agentProduct.addProduct(8, "d8f20972-f757-4766-83ab-7250a5ae459e", 10, 100 + new Random().nextInt(300));
//        agentProduct.addProduct(8, "398bbc55-1f91-4842-a429-8f91c2de0714", 15, 100 + new Random().nextInt(300));
//        agentProduct.addProduct(8, "54353389-2570-4940-aa36-36a2e1fd4054", 10, 100 + new Random().nextInt(300));
    }


}
