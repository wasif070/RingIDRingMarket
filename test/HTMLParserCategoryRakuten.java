
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.cache.ProductCategoryCache;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.repositories.category.CategoryRepositoryImpl;

public class HTMLParserCategoryRakuten {

    static CategoryRepositoryImpl productCategory = new CategoryRepositoryImpl();

    public static void getCats(String linkString, int id) throws IOException {
        Document doc2 = Jsoup.connect(linkString).get();
        // get all links
        Elements links2 = doc2.select("li[class=b-open]").select("a[href][class=b-list-item]");
        for (Element element : links2) {
            if (!element.text().isEmpty()) {
                String linkString1 = element.attr("href");
                String temp[] = linkString1.split("/");
                int id1 = Integer.parseInt(temp[5]);

                if (productCategory.getCategoryById(id1) == null && checkCatExists(id, element.text()) == null) {
                    addCategory(id1, id, element.text(), 0);
                    EntityCategory ipc = productCategory.getCategoryById(id1);
                    System.out.println("--text : " + element.text() + " || " + ipc.getId() + " || " + ipc.getParentId());
                    getCats(element.attr("href"), ipc.getId());
                }
            }
        }
    }

    public static void main(String[] args) {
        migrationCategoryPosition();
    }

    public static void parseRakutenData() throws Exception {
        Document doc;
        try {

            // need http protocol
            doc = Jsoup.connect("https://global.rakuten.com/en/").get();

            // get page title
            String title = doc.title();
            System.out.println("title : " + title);

            // get all links
//            Elements links = doc.select("a[href]");
//            
//            for (Element link : links) {
//
//                // get the value from href attribute
//                String linkString = link.attr("href");
//                if(linkString.contains("category") && !link.text().isEmpty()){
//                    System.out.println("text : " + link.text());
//                    
//                    Document doc2 = Jsoup.connect(linkString).get();
//                    // get all links
//                    Elements links2 = doc2.select("li[class=b-open]").select("a[href][class=b-list-item]");
//                    for (Element element : links2) {
//                        if(!element.text().isEmpty()){
//                            System.out.println("--text : " + element.text());
//                        }
//                    }
//                }
//            }
            Elements links = doc.select("li").select("dl").select("dt").select("a[href]");

            ArrayList<String> linkStrings = new ArrayList<>();
            ArrayList<Integer> rootCatIds = new ArrayList<>();

            for (Element link : links) {
                String linkString = link.attr("href");
                if (linkString.contains("category") && !link.text().isEmpty()) {
                    String temp[] = linkString.split("/");
                    int id = Integer.parseInt(temp[5]);

                    if (productCategory.getCategoryById(id) == null) {
                        addCategory(id, 0, link.text(), 0);
                    }
                    EntityCategory ipc = productCategory.getRootCategory(link.text());
                    System.out.println("text : " + link.text() + " || " + ipc.getId() + " || " + ipc.getParentId());
                    linkStrings.add(linkString);
                    rootCatIds.add(ipc.getId());

//                    getCats(linkString, ipc.getId(), 0);
//                    Document doc2 = Jsoup.connect(linkString).get();
//                    // get all links
//                    Elements links2 = doc2.select("li[class=b-open]").select("a[href][class=b-list-item]");
//                    for (Element element : links2) {
//                        if (!element.text().isEmpty()) {
//                            System.out.println("--text : " + element.text());
//                            if(productCategory.checkCatExists(id, element.text().toLowerCase()) == null){
//                                productCategory.addSubCategory(id, element.text().toLowerCase());
//                            }
//                        }
//                    }
                }
            }

            for (int i = 0; i < linkStrings.size(); i++) {
                getCats(linkStrings.get(i), rootCatIds.get(i));
            }

//            links = doc.select("div[class]").select("ul[class]").select("li[class]");
//            links = doc.select("dd[class]").select("div[class]").select("ul[class]").select("li[class]");
//            ProductCategory productCategory = new ProductCategory();
//            ArrayList<Map<String, Integer>> subCategories = new ArrayList<>();
//            links = doc.select("dd[class]");
//            for (Element link : links) {
//                int id = 0;
//                Map<String, Integer> subcat = new HashMap<>();
//                Elements mainCat = link.select("h4[class]");
//                for (Element element : mainCat) {
//                    System.out.println(element.text());
//                    productCategory.addRootCategory(element.text());
//                    id = productCategory.getRootCategory(element.text()).getId();
//                }
//                System.out.println("===");
//                Elements subCat = link.select("div[class]").select("ul[class]").select("li[class]");
//                for (Element element : subCat) {
//                    if(!element.text().isEmpty()){
//                            System.out.println(element.text() + " --> " + id);
//                            subcat.put(element.text().toLowerCase(), id);
//                    }
//                }
//                subCategories.add(subcat);
//                System.out.println("==========================");
//                System.out.println("==========================");
//            }
//
//            
//            for (Map<String, Integer> subCategory : subCategories) {
//                for (Map.Entry<String, Integer> entry : subCategory.entrySet()) {
//                    String key = entry.getKey();
//                    Integer value = entry.getValue();
//                    productCategory.addSubCategory(value, key);
//                    
//                }
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static EntityCategory checkCatExists(int parentId, String name) {
        CategoryRepositoryImpl categoryHierarchy = new CategoryRepositoryImpl();
        List<EntityCategory> myList = categoryHierarchy.getSubCategories(parentId);
        for (EntityCategory object : myList) {
            if (object.getName().equalsIgnoreCase(name)) {
                return object;
            }
        }
        return null;
    }

    public static void addCategory(int id, int parentId, String name, int pos) {
        EntityManager session = DBConnection.getInstance().getEntityManager();
        EntityCategory productCategory = new EntityCategory();
        productCategory.setName(name);
        productCategory.setParentId(parentId);
        productCategory.setPosition(pos);
        session.getTransaction().begin();
        session.persist(productCategory);
        session.getTransaction().commit();
        session.close();
    }
//    public static void updateId(int id){
//        EntityManager session = DBConnection.getInstance().getEntityManager();
//        EntityCategory productCategory = session.newInstance(EntityCategory.class, id);
//        session.updatePersistent(productCategory);
//        session.close();
//    }

    public static void updateParentId(int id, int parentId) {
        EntityManager session = DBConnection.getInstance().getEntityManager();
        EntityCategory productCategory = session.find(EntityCategory.class, id);
        productCategory.setParentId(parentId);
        session.merge(productCategory);
        session.close();
    }

    public static void migrationCategoryPosition() {
        List<Integer> rootCategoryIds = ProductCategoryCache.getInstance().getRootCategoryIds();

        for (Integer categoryId : rootCategoryIds) {
            setPosition(categoryId, 1);
//            List<EntityCategory> subCategories = MarketRepository.INSTANCE.getCategoryRepository().getSubCategories(categoryId);
//            boolean hasNext = true;
//            if (subCategories == null || subCategories.isEmpty()) {
//                hasNext = false;
//            }
//
//            EntityCategory categoryById = MarketRepository.INSTANCE.getCategoryRepository().getCategoryById(categoryId);
//            categoryById.setPosition(1);
//            categoryById.setHasNext(hasNext);
//            EntityManager entityManager = DBConnection.getInstance().getEntityManager();

        }
    }

    private static void setPosition(int categoryId, int position) {
        List<EntityCategory> subCategories = MarketRepository.INSTANCE.getCategoryRepository().getSubCategories(categoryId);
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        if (subCategories == null || subCategories.isEmpty()) {

            try {

                try {
                    entityManager.getTransaction().begin();

                    EntityCategory categoryById = entityManager.find(EntityCategory.class, categoryId);
                    categoryById.setHasNext(false);
                    entityManager.merge(categoryById);
                    entityManager.getTransaction().commit();
                } finally {
                    entityManager.close();
                }
                System.err.println("category ID leaf :" + categoryId);
                Thread.sleep(10);

                return;
            } catch (InterruptedException ex) {
                Logger.getLogger(HTMLParserCategoryRakuten.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {

            entityManager.getTransaction().begin();

            EntityCategory categoryById = entityManager.find(EntityCategory.class, categoryId);
            categoryById.setHasNext(true);
            entityManager.merge(categoryById);
            entityManager.getTransaction().commit();
        }

        try {
            int batchSize = 200;

            entityManager.getTransaction().begin();
            for (int i = 0; i < subCategories.size(); i++) {
                EntityCategory subCategoryById = subCategories.get(i);
                subCategoryById.setPosition(position + 1);
                subCategoryById.setHasNext(Boolean.TRUE);
                subCategoryById.setParentId(categoryId);
//                entityManager.merge(categoryById);

                if (i % batchSize == 0 && i > 0) {
                    entityManager.flush();
                    entityManager.clear();
                }
            }
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }

        for (EntityCategory subCategory : subCategories) {
            setPosition(subCategory.getId(), position + 1);
//                EntityCategory categoryById = MarketRepository.INSTANCE.getCategoryRepository().getCategoryById(subCategory.getId());
//                categoryById.setPosition(position);
//                categoryById.setHasNext(Boolean.TRUE);
//                DBConnection.getInstance().getEntityManager().merge(categoryById);
//                DBConnection.getInstance().getEntityManager().close();
        }
    }
}
