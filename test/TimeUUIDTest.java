
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.db.DBConnection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author saikat
 */
public class TimeUUIDTest {

    public static void main(String[] args) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
        entityManager.getTransaction().begin();
        try {
            Query createNativeQuery = entityManager.createNativeQuery("insert into timeuuidtest (id,num) values(?,?)");
            for (int i = 0; i < 150; i++) {
                createNativeQuery.setParameter(1, TimeUUID.timeBased().toString());
                createNativeQuery.setParameter(2, System.currentTimeMillis());
                createNativeQuery.executeUpdate();
            }


            entityManager.getTransaction().commit();

            String id = "";
            int count =0;
            while (true) {
                Query select = entityManager.createNativeQuery("select * from timeuuidtest where id>? order by id");
                select.setParameter(1, id);
                List<Object[]> resultList = select.setMaxResults(10).getResultList();
                if (resultList.isEmpty()) {
                    break;
                }
                for (Object[] object : resultList) {
                    System.out.println("id=" + object[0] + " , num to date=" + new Date((long) object[1]));
                }
                id = (String) resultList.get(resultList.size()-1)[0];
                count +=resultList.size();
            }
            
            System.out.println("total row : " + count);
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        }
        }
finally{
entityManager.close();
}
    }
}
