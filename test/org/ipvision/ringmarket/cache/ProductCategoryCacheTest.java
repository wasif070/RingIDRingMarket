/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.cache;

import java.util.List;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class ProductCategoryCacheTest {

    ProductCategoryCache instance = null;

    public ProductCategoryCacheTest() {
        instance = ProductCategoryCache.getInstance();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    /**
//     * Test of values method, of class ProductCategoryCache.
//     */
//    @Test
//    public void testValues() {
//        System.out.println("values");
//        ProductCategoryCache[] expResult = null;
//        ProductCategoryCache[] result = ProductCategoryCache.values();
//        assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of valueOf method, of class ProductCategoryCache.
//     */
//    @Test
//    public void testValueOf() {
//        System.out.println("valueOf");
//        String name = "";
//        ProductCategoryCache expResult = null;
//        ProductCategoryCache result = ProductCategoryCache.valueOf(name);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of forceReload method, of class ProductCategoryCache.
//     */
//    @Test
//    public void testForceReload() {
//        System.out.println("forceReload");
//        ProductCategoryCache instance = null;
//        instance.forceReload();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of clear method, of class ProductCategoryCache.
//     */
//    @Test
//    public void testClear() {
//        System.out.println("clear");
//        ProductCategoryCache instance = null;
//        instance.clear();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getName method, of class ProductCategoryCache.
//     */
//    @Test
//    public void testGetName() {
//        System.out.println("getName");
//        int categoryId = 0;
//        ProductCategoryCache instance = null;
//        String expResult = "";
//        String result = instance.getName(categoryId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getParentId method, of class ProductCategoryCache.
//     */
//    @Test
//    public void testGetParentId() {
//        System.out.println("getParentId");
//        int categoryId = 0;
//        ProductCategoryCache instance = null;
//        int expResult = 0;
//        int result = instance.getParentId(categoryId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getCategoryInfo method, of class ProductCategoryCache.
//     */
//    @Test
//    public void testGetCategoryInfo() {
//        System.out.println("getCategoryInfo");
//        int categoryId = 0;
//        ProductCategoryCache instance = null;
//        EntityCategory expResult = null;
//        EntityCategory result = instance.getCategoryInfo(categoryId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getRootCategoryIds method, of class ProductCategoryCache.
//     */
//    @Test
//    public void testGetRootCategoryIds() {
//        System.out.println("getRootCategoryIds");
//        ProductCategoryCache instance = null;
//        List<Integer> expResult = null;
//        List<Integer> result = instance.getRootCategoryIds();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getCategories method, of class ProductCategoryCache.
     */
    @Test
    public void testRootCategoriesInfo() {
        System.out.println("getCategories");
        Integer categoryId = null;
        int totatRoots = instance.getRootCategoryIds().size();

        int start = 0;
        int limit = 10;

        List<EntityCategory> expResult = null;
        List<EntityCategory> result = instance.getCategories(categoryId, start, limit);
//        assertNotNull(result);
//        if (totatRoots > limit) {
//            limit = 100;
//            result = instance.getCategories(categoryId, result.size(), limit);
//            assertNotNull(result);
//        }
//
//        result = instance.getCategories(categoryId, totatRoots, limit);
//        assertNull(result);
    }

//    @Test
    public void testSubCategoriesInfo() {
        System.out.println("getSubCategories");

        List<Integer> rootCategoryIds = instance.getRootCategoryIds();
        for (Integer categoryId : rootCategoryIds) {
            int start = 0;
            int limit = 10;
            int totatSubs = 0;
            if (instance.getSubCategoryListMap().containsKey(categoryId)) {
                totatSubs = instance.getSubCategoryListMap().get(categoryId).size();
            }
            List<EntityCategory> result = instance.getCategories(categoryId, start, limit);
            if (totatSubs > 0) {
                if (totatSubs > limit) {
                    assertNotNull(result);
                    limit = 100;
                    result = instance.getCategories(categoryId, result.size(), limit);
                    assertNotNull(result);
                    assertNotEquals(limit, result.size());
                }

                result = instance.getCategories(categoryId, totatSubs, limit);
                assertNull(result);
            } else {
                assertNull(result);
            }
        }

    }

}
