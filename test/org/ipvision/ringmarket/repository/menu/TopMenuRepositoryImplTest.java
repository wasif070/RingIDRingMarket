/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repository.menu;

import org.ipvision.ringmarket.repositories.menu.TopMenuRepositoryImpl;
import java.util.List;
import org.ipvision.ringmarket.constants.PageType;
import org.ipvision.ringmarket.constants.TopNavItem;
import org.ipvision.ringmarket.entities.menu.EntityTopMenu;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class TopMenuRepositoryImplTest {

    public TopMenuRepositoryImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    @Test
    public void testAddMenus() {
        TopNavItem[] values = TopNavItem.values();
        for (TopNavItem value : values) {
            EntityTopMenu item = new EntityTopMenu();
            item.setId(value.getId());
            item.setName(value.getName());
            item.setIsEnable(true);
            item.setOrdinal(values.length - value.getId() - 1);

            switch (value) {
                case ALL:
                    item.setIcon("cloud/ipvstore-165/2112373810/1889951520844779013_all.png");
                    break;
                case BRANDS:
                    item.setIcon("cloud/ipvstore-165/2112373810/9976881520844799974_brands.png");
                    item.setType(PageType.SHOP);
                    break;
                case TRENDS:
                    item.setIcon("cloud/ipvstore-165/2112373810/6120921520844833994_trends.png");
                    item.setType(PageType.PRODUCT);
                    break;
                case EXCLUSIVE:
                    item.setIcon("cloud/ipvstore-165/2112373810/3497141520844812973_exclusive.png");
                    item.setType(PageType.PRODUCT);
                    break;
                case SALE:
                    item.setIcon("cloud/ipvstore-165/2112373810/5536721520844823750_sale.png");
                    item.setType(PageType.PRODUCT);
                    break;
            }

            testAddMenu_EntityTopMenu(item);
        }
    }

    /**
     * Test of addMenu method, of class TopMenuRepositoryImpl.
     *
     * @param menu
     */
    public void testAddMenu_EntityTopMenu(EntityTopMenu menu) {
        System.out.println("addMenu");
        TopMenuRepositoryImpl instance = new TopMenuRepositoryImpl();
        boolean expResult = true;
        boolean result = instance.addMenu(menu);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTopMenus method, of class TopMenuRepositoryImpl.
     */
//    @Test
    public void testGetTopMenus() {
        System.out.println("getTopMenus");
        TopMenuRepositoryImpl instance = new TopMenuRepositoryImpl();
        int expResult = 5;
        List<EntityTopMenu> result = instance.getTopMenus();
        assertEquals(expResult, result.size());
    }
    
    @Test
    public void testAddMenu(){
        EntityTopMenu entityTopMenu = new EntityTopMenu();
        entityTopMenu.setIcon("adasd");
        entityTopMenu.setName("a");
        entityTopMenu.setOrdinal(5);
        entityTopMenu.setIsEnable(Boolean.FALSE);
        testAddMenu_EntityTopMenu(entityTopMenu);
    }

}
