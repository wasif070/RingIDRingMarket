/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 *
 * @author saikat
 */
public class RequestSender {

    private static final int READ_TIME_OUT = 5000*20;
    private static final int BUFFER_SIZE = 5000;
    private static final String USER_AGENT = "Mozilla/5.0";
    private static final String URL = "http://localhost:9090/market";
    public static void main(String args[]) {
        String request = "{\"actn\":3027,\"productId\":\"00d4bd60-72a2-11e7-8ac4-693083f55de7\",\"lmt\":20,\"tm\":1502261950431}";
        System.out.println("Request --> " + request);
        String response = getResponse(URL, request, BUFFER_SIZE);
        System.out.println("Response --> " + response);
    }

    private static String getResponse(String url, String request, int bufferSize) {
        StringBuilder response = new StringBuilder(bufferSize);
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add request header
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            // Send post request
            con.setReadTimeout(READ_TIME_OUT);
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            String urlParameters = "request=" + URLEncoder.encode(request, "UTF-8");
            wr.writeBytes(urlParameters);
            wr.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            if (inputLine != null) {

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return response.toString();
    }
}
