/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.user;

import java.util.Random;
import org.ipvision.ringmarket.constants.RingmarketRole;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author saikat
 */
public class UserTest {

    public UserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addUsersWithRole() {
        RingmarketUser ringmarketUser = new RingmarketUser();
        UserRole role = new UserRole();
        long LOWER_RANGE = 0; //assign lower range value
        long UPPER_RANGE = 10000; //assign upper range value
        Random random = new Random();

        for (RingmarketRole Rolevalue : RingmarketRole.values()) {
            for (int i = 0; i < 5; i++) {
                long userID = LOWER_RANGE
                        + (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
                ringmarketUser.addUser(userID, 90.04f, 23.05f, "+880", "1711" + userID, "Dhaka", Rolevalue + "-" + userID, "", "");
                role.addUser(userID, Rolevalue);
            }
        }

    }
}
