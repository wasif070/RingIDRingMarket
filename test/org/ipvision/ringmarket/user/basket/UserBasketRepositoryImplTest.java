/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.user.basket;

import org.ipvision.ringmarket.repositories.user.basket.UserBasketRepositoryImpl;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.UserTypeConstants;
import org.ipvision.ringmarket.entities.user.basket.EntityUserBasket;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author saikat
 */
public class UserBasketRepositoryImplTest {

    private String basketId = TimeUUID.timeBased().toString();
    private long userId = 420;

    public UserBasketRepositoryImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testUserBasketRepository() {
        testAddBasket(userId);
        testFindBasket();
        testGetCelebrityBaskets();
        testUpdateUserBasket();
//        testRemoveBasket();
    }

    /**
     * Test of addBasket method, of class UserBasketRepositoryImpl.
     */
    public EntityUserBasket testAddBasket(long userId) {
        System.out.println("addBasket");
        EntityUserBasket basket = new EntityUserBasket();
        basket.setId(basketId);
        basket.setName("Test basket");
        basket.setCreationTime(System.currentTimeMillis());
        basket.setImageUrl("Test image");
        basket.setImageHeight(100);
        basket.setImageWidth(100);
        basket.setOwnerType(UserTypeConstants.DEFAULT_USER);
        basket.setUserId(userId);
        UserBasketRepositoryImpl instance = new UserBasketRepositoryImpl();
        int expResult = 0;
        int result = instance.addBasket(basket);
        assertEquals(expResult, result);
        return basket;
    }

    /**
     * Test of getCelebrityBaskets method, of class UserBasketRepositoryImpl.
     */
    public void testGetCelebrityBaskets() {
        System.out.println("getCelebrityBaskets");
        String pivotId = null;
        int limit = 10;
        Scroll scroll = Scroll.DOWN;
        UserBasketRepositoryImpl instance = new UserBasketRepositoryImpl();
        ListReturnDTO<EntityUserBasket> result = instance.getCelebrityBaskets(pivotId, limit, scroll);
        assertEquals(result.getReasonCode(), ReasonCode.NONE);
    }

    /**
     * Test of findBasket method, of class UserBasketRepositoryImpl.
     */
    public void testFindBasket() {
        System.out.println("findBasket");
        UserBasketRepositoryImpl instance = new UserBasketRepositoryImpl();
        EntityUserBasket result = instance.findBasket(basketId);
        assertNotNull(result);
        assertEquals(new Long(userId), result.getUserId());
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of updateUserBasket method, of class UserBasketRepositoryImpl.
     */
    public void testUpdateUserBasket() {
        System.out.println("updateUserBasket");
        String basketName = "Test basket Updated";
        String image = "";
        Integer imageHeight = null;
        Integer imageWith = null;
        Integer ownerType = null;
        UserBasketRepositoryImpl instance = new UserBasketRepositoryImpl();
        boolean expResult = true;
        boolean result = instance.updateUserBasket(basketId, basketName, image, imageHeight, imageWith, ownerType);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeBasket method, of class UserBasketRepositoryImpl.
     */
    public void testRemoveBasket() {
        System.out.println("removeBasket");
        UserBasketRepositoryImpl instance = new UserBasketRepositoryImpl();
        boolean expResult = true;
        boolean result = instance.removeBasket(basketId);
        assertEquals(expResult, result);
    }

}
