/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.user.basket;

import org.ipvision.ringmarket.repositories.user.basket.BasketProductCategoryRepositoryImpl;
import java.util.List;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.user.basket.EntityUserBasket;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class BasketProductCategoryRepositoryImplTest {

    private final Long userId = 420l;
    private static String basketId;
    private static String productId;

    public BasketProductCategoryRepositoryImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
        if (basketId != null && productId != null) {
            testDeleteProductFromBasket(basketId, productId);
        }
    }

    @Test
    public void testBasketProductCategoryRepository() {
        ListReturnDTO<EntityUserBasket> userBaskets = MarketRepository.INSTANCE.getBasketRepository().getUserBaskets(userId);
        if (userBaskets.getReasonCode() == ReasonCode.NONE) {
            basketId = userBaskets.getList().get(0).getId();
        } else {
            UserBasketRepositoryImplTest implTest = new UserBasketRepositoryImplTest();
            EntityUserBasket testAddBasket = implTest.testAddBasket(userId);
            basketId = testAddBasket.getId();
        }
        ListReturnDTO<EntityProduct> exclusiveProducts = MarketRepository.INSTANCE.getProductRepository().getExclusiveProducts(null, Scroll.DOWN, 1);
        List<EntityProduct> list = exclusiveProducts.getList();
        productId = list.get(0).getId();


        testAddBasketProductCategory(basketId, productId);
        List<EntityCategory> categoriesByProduct = testGetBasketCategoriesById(basketId, null);
        testGetBasketCategoriesById(basketId, categoriesByProduct.get(0).getId());
        testGetBasketIdByUserIdAndProductId(userId, productId);
        testGetProductsOfBasket(basketId, null);

    }

    /**
     * Test of addBasketProductCategory method, of class
     * BasketProductCategoryRepositoryImpl.
     *
     * @param basketId
     * @param productId
     */
    public void testAddBasketProductCategory(String basketId, String productId) {
        System.out.println("addBasketProductCategory");
        BasketProductCategoryRepositoryImpl instance = new BasketProductCategoryRepositoryImpl();
        int expResult = 0;
        int result = instance.addBasketProductCategory(basketId, productId, userId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getBasketCategoriesById method, of class
     * BasketProductCategoryRepositoryImpl.
     */
    public List<EntityCategory> testGetBasketCategoriesById(String basketId, Integer categoryId) {
        System.out.println("getBasketCategoriesById");
        int start = 0;
        int limit = 10;
        BasketProductCategoryRepositoryImpl instance = new BasketProductCategoryRepositoryImpl();
        List<EntityCategory> result = instance.getBasketCategoriesById(basketId, start, limit, categoryId);
        assertNotNull(result);
        result.forEach((entityCategory) -> {
            testGetProductsOfBasket(basketId, entityCategory.getId());
        });
        return result;
    }

    /**
     * Test of getBasketIdByUserIdAndProductId method, of class
     * BasketProductCategoryRepositoryImpl.
     */
    public void testGetBasketIdByUserIdAndProductId(Long userId, String productId) {
        System.out.println("getBasketIdByUserIdAndProductId");
        BasketProductCategoryRepositoryImpl instance = new BasketProductCategoryRepositoryImpl();
        String result = instance.getBasketIdByUserIdAndProductId(userId, productId);
        assertNotNull(result);
    }

    /**
     * Test of getProductsOfBasket method, of class
     * BasketProductCategoryRepositoryImpl.
     *
     * @param basketId
     * @param categoryId
     */
    public void testGetProductsOfBasket(String basketId, Integer categoryId) {
        System.out.println("getProductsOfBasket");
        String pivotProductId = null;
        Scroll scroll = Scroll.DOWN;
        int limit = 10;
        BasketProductCategoryRepositoryImpl instance = new BasketProductCategoryRepositoryImpl();
        ListReturnDTO<EntityProduct> result = instance.getProductsOfBasket(basketId, categoryId, pivotProductId, scroll, limit);
        assertNotEquals(0, result.getList().size());
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of deleteProductFromBasket method, of class
     * BasketProductCategoryRepositoryImpl.
     */
    public void testDeleteProductFromBasket(String basketId, String productId) {
        System.out.println("deleteProductFromBasket");
        BasketProductCategoryRepositoryImpl instance = new BasketProductCategoryRepositoryImpl();
        int expResult = ReasonCode.NONE;
        int result = instance.deleteProductFromBasket(basketId, productId, userId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

}
