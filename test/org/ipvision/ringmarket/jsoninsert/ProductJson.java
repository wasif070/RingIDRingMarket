/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.jsoninsert;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;

/**
 *
 * @author Imran
 */
public class ProductJson {

    public static void main(String[] args) throws FileNotFoundException {
        String JSON_PATH = "conf" + File.separator + "data" + File.separator + "product.json";
        Gson gson = new Gson();
        BufferedReader br = new BufferedReader(new FileReader(JSON_PATH));
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(br).getAsJsonArray();

        System.out.println(array.size());

        for (JsonElement jsonElement : array) {
            System.out.println(jsonElement.toString());
            System.out.println(addProduct(jsonElement.getAsJsonObject()));
        }
    }


    public static String addProduct(JsonObject jsonObject) {
        ProductRepositoryImpl product = new ProductRepositoryImpl();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        EntityProduct entityProduct = gson.fromJson(jsonObject.toString(), EntityProduct.class);
        System.out.println("IS FEATURED: " + entityProduct.isExclusive());
        System.out.println("IS FEATURED: " + jsonObject.get("isFtrd"));
        boolean susc = product.addProduct(entityProduct);
        String message = null;
        if (susc) {
            message = "Successfully added a new product";
        }
        else {
            message = "Couldn't add this product";
        }
        return message;
    }

}