/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.mock;

import org.ipvision.ringmarket.entities.order.Address;

/**
 *
 * @author saikat
 */
public class MockAddress {

    public static Address mockAddress() {
        Address address = new Address();
        address.setApartment("House 5");
        address.setCity("Dhaka");
        address.setContactName("Mr. X");
        address.setCountry("Bangladesh");
        address.setDialingCode("+880");
        address.setMobile("1711123456");
        address.setEmail("a@yopmail.com");
        address.setPostCode(Short.valueOf("1100"));
        return address;
    }
}
