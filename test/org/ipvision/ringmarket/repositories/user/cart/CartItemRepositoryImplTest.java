/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user.cart;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.MarketOrderStatus;
import org.ipvision.ringmarket.constants.PaymentMode;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.order.Address;
import org.ipvision.ringmarket.entities.order.EntityOrder;
import org.ipvision.ringmarket.entities.order.EntityShippingAddress;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.product.EntityProductStock;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.mock.MockAddress;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.order.OrderRepositoryImplTest;
import org.ipvision.ringmarket.repositories.order.ShippingAddressRepoImplTest;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author saikat
 */
public class CartItemRepositoryImplTest {

    private Long userId = 420l;
    List<EntityProduct> exclusiveProducts;
    String invoiceId;
    static List<String> cartItemIds = new ArrayList<>();
    CartItemRepositoryImpl instance;

    public CartItemRepositoryImplTest() {
        instance = new CartItemRepositoryImpl();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {

        if (cartItemIds != null && !cartItemIds.isEmpty()) {
            new CartItemRepositoryImplTest().
                    testDeleteCartItems(cartItemIds);
        }
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testCartItemRepositort() {
        exclusiveProducts = MarketRepository.INSTANCE.getProductRepository().getExclusiveProducts(null, Scroll.DOWN, 2).getList();
        if (exclusiveProducts != null && !exclusiveProducts.isEmpty()) {
            int quantity = 1;
            for (EntityProduct exclusiveProduct : exclusiveProducts) {
                EntityProductStock stock = MarketRepository.INSTANCE.getProductStockRepository().getStock(exclusiveProduct.getId());
                testAddCartItem(userId, exclusiveProduct.getId(), quantity, ReasonCode.NONE);
            }

            List<EntityCartItem> testGetCartItems = testGetCartItems();
            quantity = 2;
            for (EntityCartItem cartItem : testGetCartItems) {
                cartItemIds.add(cartItem.getId());
                EntityProductStock stock = MarketRepository.INSTANCE.getProductStockRepository().getStock(cartItem.getProductId());
                if (stock.getQuantity() >= quantity) {
                    testUpdateCartItemQuantity(cartItem.getId(), quantity);
                }
            }

            testCartItemsCount(userId);
//            testCartItems(cartItemIds);
//            testUserCount();
//            testGetCartItems(userId, null, null, Scroll.DOWN, 10);

//            testCheckOutCart();
            testDeleteCartItems(cartItemIds);
            testCartItemsCount(userId);
        }
    }

    @Test
    @Ignore
    public void testCartItemRepositortFaliedCase() {
        exclusiveProducts = MarketRepository.INSTANCE.getProductRepository().getExclusiveProducts(null, Scroll.DOWN, 1).getList();
        if (exclusiveProducts != null && !exclusiveProducts.isEmpty()) {
            String productId = exclusiveProducts.get(0).getId();
            EntityProductStock stock = MarketRepository.INSTANCE.getProductStockRepository().getStock(productId);
            if (stock != null) {
                int expectedResult;
                if (stock.getQuantity() == 0) {
                    expectedResult = ReasonCode.RingMarket.STOCK_OUT;
                } else {
                    expectedResult = ReasonCode.RingMarket.STOCK_LIMIT_OVER;
                }
                testAddCartItem(userId, productId, stock.getQuantity() + 1, expectedResult);
            }
        }
    }

    public void testCheckOutCart() {
        ListReturnDTO<EntityShippingAddress> shippingAddresses = MarketRepository.INSTANCE.getShippingAddressRepository().getShippingAddresses(userId);
        ShippingAddressRepoImplTest repoImplTest = new ShippingAddressRepoImplTest();

        String addressId;
        if (shippingAddresses.getReasonCode() == ReasonCode.NONE) {
            addressId = shippingAddresses.getList().get(0).getId();
        } else {
            Address mockAddress = MockAddress.mockAddress();
            addressId = repoImplTest.testAddShippingAddress(userId, mockAddress, Boolean.FALSE, (byte) 0);
        }

        invoiceId = testOrderProductsFromCartWithSuccess(cartItemIds, userId, userId, Shipment.HOME_DELIVERY, addressId, PaymentMode.SSL_WIRELESS_BD);

        OrderRepositoryImplTest implTest = new OrderRepositoryImplTest();
        implTest.testGetBuyerOrders();
        List<EntityOrder> testGetOrderListByInvoiceId = implTest.testGetOrderListByInvoiceId(invoiceId);

        List<String> orderIds = new LinkedList<>();

        for (EntityOrder entityOrder : testGetOrderListByInvoiceId) {
            orderIds.add(entityOrder.getId());
        }

        int reasonCode = MarketRepository.INSTANCE.getOrderRepository().updateOrderStatus(userId, orderIds, MarketOrderStatus.PAYMENT_COMPLETE.getValue());
        assertEquals(reasonCode, ReasonCode.NONE);
//            repoImplTest.testRemoveShippingAddress(addressId);
    }

    /**
     * Test of getUserCartItems method, of class CartItemRepositoryImpl.
     *
     * @param cartItemIds
     */
    public void testCartItems(List<String> cartItemIds) {
        int expResult = 0;
        int result = instance.getCartItems(cartItemIds).getReasonCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of addCartItem method, of class CartItemRepositoryImpl.
     *
     * @param userId
     * @param productId
     * @param quantity
     * @param expResult
     */
    public void testAddCartItem(Long userId, String productId, int quantity, int expResult) {
//        System.out.println("addCartItem");
        int result = instance.addCartItem(productId, userId, quantity);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getUserCartItems method, of class CartItemRepositoryImpl.
     *
     * @return
     */
    public List<EntityCartItem> testGetCartItems() {
        System.out.println("getCartItems");
        String pivotId = "";
        Scroll scroll = Scroll.DOWN;
        int limit = 100;
        List<EntityCartItem> result = instance.getUserCartItems(userId, pivotId, scroll, limit).getList();
//        assertNotEquals(result.size(), 0);
        return result;
    }

    /**
     * Test of findCartById method, of class CartItemRepositoryImpl.
     *
     * @param id
     */
    public void testFindCartById(String id) {
        System.out.println("findCartById");
        EntityManager entityManager = null;
        EntityCartItem result = instance.findCartById(entityManager, id);
        assertNotNull(result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of updateCartItemQuantity method, of class CartItemRepositoryImpl.
     *
     * @param cartItemId
     * @param quantity
     */
    public void testUpdateCartItemQuantity(String cartItemId, int quantity) {
//        System.out.println("updateCartItemQuantity");
        int expResult = 0;
        int result = instance.updateCartItemQuantity(cartItemId, quantity);
        assertEquals(expResult, result);
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
        EntityCartItem findCartById = instance.findCartById(entityManager, cartItemId);
        assertEquals(quantity, findCartById.getQuantity().intValue());
        }
finally{
entityManager.close();
}
    }

    /**
     * Test of deleteCartItems method, of class CartItemRepositoryImpl.
     *
     * @param cartItemIds
     */
    public void testDeleteCartItems(List<String> cartItemIds) {
        System.out.println("deleteCartItems");
        boolean expResult = true;
        boolean result = instance.deleteCartItems(userId, cartItemIds);
        assertEquals(expResult, result);
        this.cartItemIds.removeAll(cartItemIds);
    }

    /**
     * Test of orderProductsFromCart method, of class CartItemRepositoryImpl.
     *
     * @param cartItemIds
     * @param activistId
     * @param userId
     * @param shipment
     * @param addressId
     * @param paymentMode
     * @return
     */
    public String testOrderProductsFromCartWithSuccess(List<String> cartItemIds, Long activistId, Long userId, Shipment shipment,
            String addressId, Byte paymentMode) {
        System.out.println("orderProductsFromCart");
        ObjectReturnDTO<String> orderProductsFromCart = instance.orderProductsFromCart(activistId, userId, cartItemIds, shipment, addressId, paymentMode);
        assertEquals(orderProductsFromCart.getReasonCode(), ReasonCode.NONE);
        return orderProductsFromCart.getValue();
    }

    /**
     * Test of cartItemsCount method, of class CartItemRepositoryImpl.
     *
     * @param userId
     */
    public void testCartItemsCount(long userId) {
        System.out.println("CartItemsCount");
        int expected = ReasonCode.NONE;
        ObjectReturnDTO<Long> cartItemsCount = instance.cartItemsCount(userId);
        assertEquals(expected, cartItemsCount.getReasonCode());
    }

    /**
     * Test of userCount method, of class CartItemRepositoryImpl.
     */
    public void testUserCount() {
        System.out.println("userCount");
        Long expected = 0l;
        ObjectReturnDTO<Long> cartItemsCount = instance.userCount();
        assertNotEquals(expected, cartItemsCount.getValue());
    }

    /**
     * Test of getCartItems method, of class CartItemRepositoryImpl.
     *
     * @param userId
     * @param shopId
     * @param pivot
     * @param scroll
     * @param limit
     */
    public void testGetCartItems(Long userId, Long shopId, String pivot, Scroll scroll, int limit) {
        System.out.println("getCartItems");
        PivotedListReturnDTO<EntityCartItem, String> cartItems = instance.getCartItems(userId, shopId, pivot, scroll, limit);
        assertNotNull(cartItems.getList());
    }

}
