/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.user.cart;

import com.google.gson.Gson;
import java.util.List;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.junit.Test;

/**
 *
 * @author alamgir
 */
public class CartItem {
    @Test
    public void getUserCartItems(){
        CartItemRepositoryImpl cri = new CartItemRepositoryImpl();
        ListReturnDTO<EntityCartItem>  items = cri.getUserCartItems(137438953703L, null, Scroll.DOWN, 10);
        Gson gson = new Gson();
        System.out.println(gson.toJson(items));
    }
}
