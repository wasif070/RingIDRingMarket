/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import com.google.gson.JsonObject;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.entities.order.Address;
import org.ipvision.ringmarket.entities.order.EntityShippingAddress;
import org.ipvision.ringmarket.mock.MockAddress;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class ShippingAddressRepoImplTest {

    public ShippingAddressRepoImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testShippingAddress() {
        Long userId = 420l;
        Address address = MockAddress.mockAddress();
        byte type = 0;
        String id = testAddShippingAddress(userId, address, Boolean.TRUE, type);
        testGetShippingAddresses(userId);
        testUpdateShippingAddress(id, address);
        testSetDefault(id);
        testRemoveShippingAddress(id);
    }

    /**
     * Test of addShippingAddress method, of class ShippingAddressRepoImpl.
     *
     * @param userId
     * @param address
     * @param isDefault
     * @return
     */
    public String testAddShippingAddress(Long userId, Address address, Boolean isDefault, byte type) {
        System.out.println("addShippingAddress");
        ShippingAddressRepoImpl instance = new ShippingAddressRepoImpl();
        ObjectReturnDTO<String> result = instance.addShippingAddress(userId, address, isDefault, type);
        assertNotNull(result.getValue());
        return result.getValue();
    }

    /**
     * Test of updateShippingAddress method, of class ShippingAddressRepoImpl.
     *
     * @param id
     * @param address
     */
    public void testUpdateShippingAddress(String id, Address address) {
        System.out.println("updateShippingAddress");
        ShippingAddressRepoImpl instance = new ShippingAddressRepoImpl();
        int expResult = ReasonCode.NONE;
        int result = instance.updateShippingAddress(id, address);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDefault method, of class ShippingAddressRepoImpl.
     *
     * @param id
     */
    public void testSetDefault(String id) {
        System.out.println("setDefault");
        ShippingAddressRepoImpl instance = new ShippingAddressRepoImpl();
        int expResult = ReasonCode.NONE;
        int result = instance.setDefault(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getShippingAddresses method, of class ShippingAddressRepoImpl.
     *
     * @param userId
     */
    public void testGetShippingAddresses(Long userId) {
        System.out.println("getShippingAddresses");
        ShippingAddressRepoImpl instance = new ShippingAddressRepoImpl();
        ListReturnDTO<EntityShippingAddress> result = instance.getShippingAddresses(userId);
        assertEquals(false, result.getList().isEmpty());
    }

    /**
     * Test of removeShippingAddress method, of class ShippingAddressRepoImpl.
     *
     * @param id
     */
    public void testRemoveShippingAddress(String id) {
        System.out.println("removeShippingAddress");
        ShippingAddressRepoImpl instance = new ShippingAddressRepoImpl();
        int expResult = ReasonCode.NONE;
        int result = instance.removeShippingAddress(id);
        assertEquals(expResult, result);
    }

}
