/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import java.util.Collection;
import java.util.List;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.order.EntityOrderDetails;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class OrderDetailsRepositoryImplTest {

    public OrderDetailsRepositoryImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    

    /**
     * Test of getOrderDetails method, of class OrderDetailsRepositoryImpl.
     *
     * @param orderId
     * @return
     */
    public List<EntityOrderDetails> testGetOrderDetails(String orderId) {
        System.out.println("getOrderDetails");
        OrderDetailsRepositoryImpl instance = new OrderDetailsRepositoryImpl();

        ListReturnDTO<EntityOrderDetails> result = instance.getOrderDetails(orderId);

        assertEquals(ReasonCode.NONE, result.getReasonCode());
        return result.getList();
    }

    /**
     * Test of updateOrderDetailStatus method, of class
     * OrderDetailsRepositoryImpl.
     *
     * @param orderId
     * @param productIds
     * @param status
     */
    public void testUpdateOrderDetailStatus(Long userId,String orderId, Collection<String> productIds, Byte status) {
        System.out.println("updateOrderDetailStatus");
        OrderDetailsRepositoryImpl instance = new OrderDetailsRepositoryImpl();

        int result = instance.updateOrderDetailStatus(userId, orderId, productIds, status);

        assertEquals(ReasonCode.NONE, result);
    }

}
