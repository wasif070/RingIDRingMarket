/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.order;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javafx.util.Pair;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.MarketOrderStatus;
import org.ipvision.ringmarket.constants.PaymentMode;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.dtos.orders.InvoiceDTO;
import org.ipvision.ringmarket.entities.order.EntityOrder;
import org.ipvision.ringmarket.entities.order.EntityOrderDetails;
import org.ipvision.ringmarket.entities.order.EntityShippingAddress;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author saikat
 */
public class OrderRepositoryImplTest {

    private static Set<String> orderList = new HashSet<>();
    OrderRepositoryImpl instance = new OrderRepositoryImpl();

    public OrderRepositoryImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            OrderRepositoryImpl orderRepositoryImpl = new OrderRepositoryImpl();
            OrderDetailsRepositoryImpl detailsRepositoryImpl = new OrderDetailsRepositoryImpl();
            for (String id : orderList) {
                orderRepositoryImpl.deleteOrderFromDatabase(entityManager, id);
                detailsRepositoryImpl.deleteOrderDetailsByOrderIdFromDatabase(entityManager, id);
            }
        } finally {
            entityManager.close();
        }
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    public void testDeleteOrderFromDatabase(Collection<String> orderIds) {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
            OrderDetailsRepositoryImpl detailsRepositoryImpl = new OrderDetailsRepositoryImpl();
            for (String id : orderIds) {
                instance.deleteOrderFromDatabase(entityManager, id);
                detailsRepositoryImpl.deleteOrderDetailsByOrderIdFromDatabase(entityManager, id);
            }
        } finally {
            entityManager.close();
        }
    }

    /**
     * Test of getOrderListByInvoiceId method, of class OrderRepositoryImpl.
     */
    public List<EntityOrder> testGetOrderListByInvoiceId(String invoiceId) {
        System.out.println("getOrderListByInvoiceId");

        ListReturnDTO<EntityOrder> result = instance.getOrderListByInvoiceId(invoiceId);
        assertNotEquals(0, result.getList().size());
        return result.getList();
    }

    /**
     * Test of completePayment method, of class OrderRepositoryImpl.
     *
     * @param userId
     */
    public void testCompletePayment(Long userId, String invoiceId, Byte paymentMode) {
        System.out.println("completePayment");
        OrderRepositoryImpl instance = new OrderRepositoryImpl();
        int result = instance.completePayment(userId, invoiceId, paymentMode);
        assertEquals(ReasonCode.NONE, result);
    }

    /**
     * Test of placeOrder method, of class OrderRepositoryImpl.
     *
     * @param activistId
     * @param userId
     * @param quantity
     * @param product
     * @param shipment
     * @param addressId
     * @param paymentType
     * @return
     */
    public String testPlaceOrder(Long activistId, Long userId, Integer quantity, EntityProduct product, Shipment shipment, String addressId, byte paymentType) {
        System.out.println("placeOrder");
        OrderRepositoryImpl instance = new OrderRepositoryImpl();
        int expResult = ReasonCode.NONE;
        ObjectReturnDTO<String> result = instance.placeOrder(activistId, userId, quantity, product, shipment, addressId, paymentType);
        assertEquals(expResult, result.getReasonCode());
        return result.getValue();
    }

    /**
     * Test of getFilteredOrders method, of class OrderRepositoryImpl.
     *
     * @param pivotId
     * @param sellerId
     * @param shipmentMethod
     * @param status
     * @param paymentMethod
     * @param scroll
     * @param limit
     */
    public void testGetFilteredSellerOrders(Pair<String, Long> pivot, Long sellerId, Byte shipmentMethod, Collection<Byte> status,
            Byte paymentMethod, Scroll scroll, int limit) {
        System.out.println("getFilteredSellerOrders");

        OrderRepositoryImpl orderRepositoryImpl = new OrderRepositoryImpl();
        PivotedListReturnDTO<EntityOrder, Pair<String, Long>> filteredSellerOrders = orderRepositoryImpl.getFilteredOrders(pivot, sellerId, shipmentMethod, status,
                paymentMethod, scroll, limit);
        assertEquals(ReasonCode.NONE, filteredSellerOrders.getReasonCode());
    }

    /**
     * Test of orderItemsAndGetInvoiceId method, of class OrderRepositoryImpl.
     *
     * @param activistId
     * @param buyerId
     */
    public String testOrderItemsAndGetInvoiceId(Long activistId, Long buyerId, List<EntityCartItem> orders, Shipment shipment, String addressId, Byte paymentType) {
        System.out.println("orderItemsAndGetInvoiceId");

        OrderRepositoryImpl orderRepositoryImpl = new OrderRepositoryImpl();
        ObjectReturnDTO<String> orderItemsAndGetInvoiceId = orderRepositoryImpl.orderItemsAndGetInvoiceId(activistId, buyerId, orders,
                shipment, addressId, paymentType);
        assertEquals(ReasonCode.NONE, orderItemsAndGetInvoiceId.getReasonCode());
        return orderItemsAndGetInvoiceId.getValue();
    }

    @Test
//    @Ignore
    public void testOrderRepository() {
        ListReturnDTO<EntityProduct> nonExclusiveProducts = MarketRepository.INSTANCE.getProductRepository().getNonExclusiveProducts(null, Scroll.DOWN, 1);
        Long userId = 420l;
        Integer quantity = 9;
        ListReturnDTO<EntityShippingAddress> shippingAddresses = MarketRepository.INSTANCE.getShippingAddressRepository().getShippingAddresses(userId);
        Shipment shipment = Shipment.HOME_DELIVERY;
        String addressId = null;
        if (shippingAddresses.getReasonCode() == ReasonCode.NONE) {
            shipment = Shipment.HOME_DELIVERY;
            addressId = shippingAddresses.getList().get(0).getId();
        }

        String invoiceId = testPlaceOrder(userId, userId, quantity, nonExclusiveProducts.getList().get(0), shipment, addressId, PaymentMode.SSL_WIRELESS_BD);

        List<EntityOrder> testGetOrderListByInvoiceId = testGetOrderListByInvoiceId(invoiceId);

        testCompletePayment(userId, invoiceId, PaymentMode.CASH_WALLET);

        OrderDetailsRepositoryImplTest detailsRepositoryImplTest = new OrderDetailsRepositoryImplTest();

        for (EntityOrder entityOrder : testGetOrderListByInvoiceId) {
            List<EntityOrderDetails> testGetOrderDetails = detailsRepositoryImplTest.testGetOrderDetails(entityOrder.getId());
            orderList.add(entityOrder.getId());

            List<String> productIds = new ArrayList<>();
            for (EntityOrderDetails testGetOrderDetail : testGetOrderDetails) {
                productIds.add(testGetOrderDetail.getProductId());
            }
            detailsRepositoryImplTest.testUpdateOrderDetailStatus(userId, entityOrder.getId(), productIds, MarketOrderStatus.ON_SHIPMENT.getValue());
            detailsRepositoryImplTest.testUpdateOrderDetailStatus(userId, entityOrder.getId(), productIds, MarketOrderStatus.SHIPMENT_COMPLETED.getValue());

//            MarketRepository.INSTANCE.getProductStockRepository().getStock(nonExclusiveProducts.getList().get(0).getId());
        }

        Long shopId = testGetOrderListByInvoiceId.get(0).getShopId();
        Set<Byte> statusList = new HashSet<>();
        statusList.add(MarketOrderStatus.SHIPMENT_COMPLETED.getValue());
        testGetFilteredSellerOrders(new Pair<>(null, 0L), shopId, shipment.getValue(), statusList,
                null, Scroll.DOWN, 10);
        testGetBuyerOrders(userId, new Pair<String, Long>(null, 0l), Scroll.DOWN, 10);
    }

    @Test
    public void testStoreOrders() {
        ListReturnDTO<InvoiceDTO> invoiceList = MarketRepository.INSTANCE.getOrderRepository().getInvoiceList(0, 1);
        if (invoiceList.getReasonCode() == ReasonCode.NONE) {
            String invoiceID = invoiceList.getList().get(0).getInvoiceId();
            ListReturnDTO<EntityOrder> orderListByInvoiceId = MarketRepository.INSTANCE.getOrderRepository().getOrderListByInvoiceId(invoiceID);
            Long shopId = orderListByInvoiceId.getList().get(0).getShopId();
            Set<Byte> statusList = new HashSet<>();
            statusList.add(MarketOrderStatus.SHIPMENT_COMPLETED.getValue());
            testGetFilteredSellerOrders(new Pair<>(null, 0L), shopId, null, statusList,
                    null, Scroll.DOWN, 10);
        }
    }

    @Test
    @Ignore
    public void testInStoreOrders() {
        Long buyerId = 420l;
        Shipment shipment = Shipment.PICKUP_FROM_STORE;
        Byte paymentType = PaymentMode.HAND_CASH;
        ListReturnDTO<EntityShippingAddress> nearestRingStores = MarketRepository.INSTANCE.getShippingAddressRepository().getShippingAddresses(buyerId);
        String addressId = nearestRingStores.getList().get(0).getId();
        ListReturnDTO<EntityProduct> nonExclusiveProducts = MarketRepository.INSTANCE.getProductRepository().getNonExclusiveProducts(null, Scroll.DOWN, 10);
        List<EntityProduct> products = nonExclusiveProducts.getList();
        if (nonExclusiveProducts.getReasonCode() == ReasonCode.NONE) {
            List<EntityCartItem> items = new LinkedList<>();
            for (EntityProduct product : products) {
                EntityCartItem details = new EntityCartItem();
                details.setPrice(product.getPrice());
                details.setCurrecyCode(product.getCurrencyCode());
                details.setProductId(product.getId());
                details.setProductImage(product.getImageUrl());
                details.setProductName(product.getName());
                details.setQuantity(1);
                details.setShipmentPrice(0d);
                details.setShopId(product.getShopId());
                details.setShopName(product.getShopName());
                items.add(details);
            }
            String invoice = testOrderItemsAndGetInvoiceId(buyerId, buyerId, items, shipment, addressId, paymentType);
            List<EntityOrder> testGetOrderListByInvoiceId = testGetOrderListByInvoiceId(invoice);
            for (EntityOrder entityOrder : testGetOrderListByInvoiceId) {
                orderList.add(entityOrder.getId());
            }

        }
    }

    /**
     * Test of getBuyerOrders method, of class OrderDetailsRepositoryImpl.
     */
    @Test
    @Ignore
    public void testGetBuyerOrders() {
        System.out.println("getBuyerOrders");
        Long userId = 420l;
        Pair<String, Long> pivot = new Pair<>(null, 0l);
        Scroll scroll = Scroll.DOWN;
        int limit = 5;
        OrderRepositoryImpl instance = new OrderRepositoryImpl();

        int scrollDownItemCount = 0;
        while (true) {

            PivotedListReturnDTO<EntityOrder, Pair<String, Long>> result = instance.getBuyerOrders(userId, pivot, scroll, limit);
            if (result.getReasonCode() != ReasonCode.NONE) {
                break;
            }
            List<EntityOrder> list = result.getList();
            System.out.println("Scroll Down");
            for (EntityOrder entityOrder : list) {
                System.out.println(new Date(entityOrder.getAddedTime()));
            }
            pivot = result.getPivot();
            scrollDownItemCount += list.size();
        }

        scroll = Scroll.UP;
        int scrollUpItemCount = 0;
        while (true) {
            PivotedListReturnDTO<EntityOrder, Pair<String, Long>> result = instance.getBuyerOrders(userId, pivot, scroll, limit);
            if (result.getReasonCode() != ReasonCode.NONE) {
                break;
            }
            List<EntityOrder> list = result.getList();
            System.out.println("Scroll Up");
            for (EntityOrder entityOrder : list) {
                System.out.println(new Date(entityOrder.getAddedTime()));
            }
            pivot = result.getPivot();
            scrollUpItemCount += list.size();
        }

        assertEquals(scrollUpItemCount, scrollDownItemCount - 1);
    }

    /**
     * Test of getBuyerOrders method, of class OrderDetailsRepositoryImpl.
     *
     * @param userId
     * @param pivotId
     * @param scroll
     * @param limit
     * @return
     */
    public PivotedListReturnDTO<EntityOrder, Pair<String, Long>> testGetBuyerOrders(Long userId, Pair<String, Long> pivot, Scroll scroll, int limit) {
        System.out.println("getBuyerOrders");
        OrderRepositoryImpl instance = new OrderRepositoryImpl();

        PivotedListReturnDTO<EntityOrder, Pair<String, Long>> result = instance.getBuyerOrders(userId, pivot, scroll, limit);

        assertEquals(ReasonCode.NONE, result.getReasonCode());
        return result;
    }

}
