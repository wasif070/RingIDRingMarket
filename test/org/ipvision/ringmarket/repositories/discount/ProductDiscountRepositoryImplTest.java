/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.discount;

import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.discount.EntityProductDiscount;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class ProductDiscountRepositoryImplTest {

    private static String productId;
    private static ProductDiscountRepositoryImpl instance = new ProductDiscountRepositoryImpl();

    public ProductDiscountRepositoryImplTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testProductDiscountRepository() {
        ListReturnDTO<EntityProduct> exclusiveProducts = MarketRepository.INSTANCE.getProductRepository().getExclusiveProducts(null, Scroll.DOWN, 1);
        if (exclusiveProducts.getReasonCode() == ReasonCode.NONE) {
            productId = exclusiveProducts.getList().get(0).getId();
            EntityProductDiscount discount = new EntityProductDiscount();
            discount.setCreationTime(System.currentTimeMillis());
            discount.setDiscountUnit((byte) 1);
            discount.setDiscountValue(5f);
            discount.setMaximumDiscountAmount(0d);
            discount.setMinimumOrderValue(0);
            discount.setProductId(productId);
            discount.setValidityTime(0l);
            testAdd(discount, ReasonCode.NONE);
            testGetValidDiscount(productId, System.currentTimeMillis(), ReasonCode.NONE);
        }
    }

    /**
     * Test of add method, of class ProductDiscountRepositoryImpl.
     */
    public void testAdd(EntityProductDiscount discount, int expResult) {
        System.out.println("add");
        int result = instance.add(discount);
        assertEquals(expResult, result);
    }

    /**
     * Test of getValidDiscount method, of class ProductDiscountRepositoryImpl.
     */
    public void testGetValidDiscount(String productId, Long currentTime, int expResult) {
        System.out.println("getValidDiscount");
        ObjectReturnDTO<EntityProductDiscount> result = instance.getValidDiscount(productId, currentTime);
        assertEquals(expResult, result.getReasonCode());
    }

}
