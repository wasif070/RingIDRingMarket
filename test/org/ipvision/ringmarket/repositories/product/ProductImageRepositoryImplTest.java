/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.util.List;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.entities.product.EntityProductImage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author saikat
 */
public class ProductImageRepositoryImplTest {

    static ProductImageRepositoryImpl instance;
    static EntityProductImage image1;
    static EntityProductImage image2;
    static String productId = "11e7-8b1a-1cf5c990-ac78-07759a0bcd66";

    public ProductImageRepositoryImplTest() {
        instance = new ProductImageRepositoryImpl();
    }

    @BeforeClass
    public static void setUpClass() {
        image1 = new EntityProductImage("img1", 100, 100, productId, true, System.currentTimeMillis());
        image2 = new EntityProductImage("img2", 100, 100, productId, false, System.currentTimeMillis());
    }

    @AfterClass
    public static void tearDownClass() {
        instance.deleteImageFromDatabase(productId, image1.getImageUrl());
        instance.deleteImageFromDatabase(productId, image2.getImageUrl());

    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testProductImageRepository() {
        testAddImage(image1);
        testAddImage(image2);
        String previousUrl = image1.getImageUrl();
        image1.setImageUrl("upimg1");
        testUpdateImage(previousUrl, image1);
        previousUrl = image2.getImageUrl();
        image2.setImageUrl("upimg2");
        testUpdateImage(previousUrl, image2);
        testChangeDefaultImage(image1.getImageUrl(), image2.getImageUrl(), productId);
    }

    /**
     * Test of addImage method, of class ProductImageRepositoryImpl.
     */
    public void testAddImage(String url, int hgt, int wdt, String productId) {
        System.out.println("addImage");
        boolean expResult = true;
        boolean result = instance.addImage(url, hgt, wdt, productId);
        assertEquals(expResult, result);
    }

    /**
     * Test of addImage method, of class ProductImageRepositoryImpl.
     */
    public void testAddImage(EntityProductImage entityProductImage) {
        System.out.println("addImage");
        boolean result = instance.addImage(entityProductImage);
        assertTrue(result);
    }

    /**
     * Test of addImageList method, of class ProductImageRepositoryImpl.
     */
    public void testAddImageList(String productId, List<EntityProductImage> entityProductImages) {
        System.out.println("addImageList");
        ProductImageRepositoryImpl instance = new ProductImageRepositoryImpl();
        int expResult = ReasonCode.NONE;
        int result = instance.addImageList(productId, entityProductImages);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteImage method, of class ProductImageRepositoryImpl.
     *
     * @param productId
     * @param imageUrl
     */
    public void testDeleteImage(String productId, String imageUrl, int expResult) {
        System.out.println("deleteImage");
        int result = instance.deleteImage(productId, imageUrl);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteAllImageProduct method, of class
     * ProductImageRepositoryImpl.
     */
    public void testDeleteAllImageProduct(String productId) {
        System.out.println("deleteAllImageProduct");
        ProductImageRepositoryImpl instance = new ProductImageRepositoryImpl();
        boolean expResult = true;
        boolean result = instance.deleteAllImageProduct(productId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getImagesOfProduct method, of class ProductImageRepositoryImpl.
     */
    public void testGetImagesOfProduct(String productId) {
        System.out.println("getImagesOfProduct");
        List<EntityProductImage> expResult = null;
        List<EntityProductImage> result = instance.getImagesOfProduct(productId);
        assertNotEquals(expResult, result);
    }

    /**
     * Test of updateImage method, of class ProductImageRepositoryImpl.
     *
     * @param imageUrl
     * @param updatedImageProperty
     */
    public void testUpdateImage(String imageUrl, EntityProductImage updatedImageProperty) {
        System.out.println("updateImage");
        int expResult = ReasonCode.NONE;
        int result = instance.updateImage(imageUrl, updatedImageProperty);
        assertEquals(expResult, result);
    }

    public void testChangeDefaultImage(String previousImageUrl, String newImageUrl, String productId) {
        System.out.println("updateImage");
        int expResult = ReasonCode.NONE;
        int result = instance.changeDefaultImage(previousImageUrl, newImageUrl, productId);
        assertEquals(expResult, result);
    }

}
