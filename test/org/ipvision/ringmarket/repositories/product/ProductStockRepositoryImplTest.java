/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.entities.product.EntityProductStock;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class ProductStockRepositoryImplTest {

    private ProductStockRepositoryImpl instance;

    public ProductStockRepositoryImplTest() {
        instance = (ProductStockRepositoryImpl) MarketRepository.INSTANCE.getProductStockRepository();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addStock method, of class ProductStockRepositoryImpl.
     */
    public void testAddStock_String_Integer() {
        System.out.println("addStock");
        String productId = "";
        Integer quantity = null;
        ProductStockRepositoryImpl instance = new ProductStockRepositoryImpl();
        int expResult = 0;
        int result = instance.addStock(productId, quantity);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addStock method, of class ProductStockRepositoryImpl.
     */
    public void testAddStock_EntityManager_EntityProductStock() {
        System.out.println("addStock");
        EntityManager entityManager = null;
        EntityProductStock productStock = null;
        ProductStockRepositoryImpl instance = new ProductStockRepositoryImpl();
        instance.addStock(entityManager, productStock);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addStock method, of class ProductStockRepositoryImpl.
     */
    public void testAddStock_3args() {
        System.out.println("addStock");
        EntityManager entityManager = null;
        String productId = "";
        Integer quantity = null;
        ProductStockRepositoryImpl instance = new ProductStockRepositoryImpl();
        int expResult = 0;
        int result = instance.addStock(entityManager, productId, quantity);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStock method, of class ProductStockRepositoryImpl.
     */
    public EntityProductStock testGetStock(String id) {
        System.out.println("getStock");
        ProductStockRepositoryImpl instance = new ProductStockRepositoryImpl();
        EntityProductStock result = instance.getStock(id);
        assertNotNull( result);
        return result;
    }

    /**
     * Test of increaseStock method, of class ProductStockRepositoryImpl.
     */
    public void testIncreaseStock(String productId , Integer quantity) {
        System.out.println("increaseStock");

        ProductStockRepositoryImpl instance = new ProductStockRepositoryImpl();
        int expResult = 0;
        int result = instance.increaseStock(productId, quantity);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateStock method, of class ProductStockRepositoryImpl.
     */
    public void testUpdateStock() {
        System.out.println("updateStock");
        EntityManager entityManager = null;
        EntityProductStock productStock = null;
        ProductStockRepositoryImpl instance = new ProductStockRepositoryImpl();
        int expResult = 0;
        int result = instance.updateStock(entityManager, productStock);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of decreaseStock method, of class ProductStockRepositoryImpl.
     */
    public void testDecreaseStock() {
        System.out.println("decreaseStock");
        String productId = "";
        Integer quantity = null;
        ProductStockRepositoryImpl instance = new ProductStockRepositoryImpl();
        int expResult = 0;
        int result = instance.decreaseStock(productId, quantity);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of deleteStockFromDB method, of class ProductStockRepositoryImpl.
     */
    public void testDeleteStockFromDB(String productId) {
        System.out.println("deleteStockFromDB");
        int expResult = ReasonCode.NONE;
        int result = instance.deleteStockFromDB(productId);
        assertEquals(expResult, result);
    }

}
