/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.product;

import java.security.cert.CertPathValidatorException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.cache.ProductCategoryCache;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.product.EntityProductImage;
import org.ipvision.ringmarket.entities.product.EntityProductStock;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.uniqueid.UniqueNumberGenerator;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author saikat
 */
public class ProductRepositoryImplTest {

    private static String productId;
    private static EntityProduct product;
    private static ProductStockRepositoryImplTest productStockRepositoryImplTest = new ProductStockRepositoryImplTest();

    public ProductRepositoryImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        product = new EntityProduct();
        product.setName("Test name");
        product.setCurrencyCode("$");
        product.setImageHeight(10);
        product.setImageWidth(10);
        product.setIsExclusive(false);
        product.setImageUrl("Test image");
        product.setLat(90f);
        product.setLon(23f);
        product.setPrice(100D);
        product.setShopId(100l);
        product.setShopName("Test shop");
        product.setCreationTime(System.currentTimeMillis());
        product.setStatus(ProductStatus.AVAILABLE);
        product.setCode(UniqueNumberGenerator.generateBarcodeId().toString());
    }

    @AfterClass
    public static void tearDownClass() {
        if (productId != null) {
            ProductRepositoryImpl impl = new ProductRepositoryImpl();
            impl.deleteProductFromDatabase(productId);
            ProductDetailRepositoryImpl detailRepositoryImpl = new ProductDetailRepositoryImpl();
            detailRepositoryImpl.deleteProductDetail(productId);
            ProductImageRepositoryImpl imageRepositoryImpl = new ProductImageRepositoryImpl();
            imageRepositoryImpl.deleteAllImageProduct(productId);

            ProductStockRepositoryImplTest implTest = new ProductStockRepositoryImplTest();
            implTest.testDeleteStockFromDB(productId);

            CategoryProductRepository categoryProductRepository = new CategoryProductRepositoryImpl();
            categoryProductRepository.removeProduct(productId);
        }
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {

    }

    @Test
    @Ignore
    public void productRepositoryTest() {
        Set<Integer> categoryList = new HashSet<>();
        List<EntityCategory> categories = ProductCategoryCache.getInstance().getCategories(null, 0, 1);
        categoryList.add(categories.get(0).getId());
        Integer quantity = 10;

        productId = testAddProduct(product, categoryList, quantity);
        EntityProduct testGetProductById = testGetProductById(productId);
        testGetProductById.setName("Test updated name");
        testUpdateProduct(product);

        EntityProductImage entityProductImage = new EntityProductImage("Updated Image", Integer.MAX_VALUE, Integer.MAX_VALUE, productId, true, System.currentTimeMillis());
        ProductImageRepositoryImplTest imageRepositoryImplTest = new ProductImageRepositoryImplTest();
        imageRepositoryImplTest.testUpdateImage(product.getImageUrl(), entityProductImage);

        productStockRepositoryImplTest.testIncreaseStock(productId, 10);
        EntityProductStock testGetStock = productStockRepositoryImplTest.testGetStock(productId);

        assertEquals(testGetStock.getQuantity().intValue(), (quantity + 10));
    }

    /**
     * Test of addProduct method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testAddProduct_EntityProduct_int() {
        System.out.println("addProduct");
        EntityProduct entityProduct = null;
        int categoryId = 0;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        EntityProduct expResult = product;
        EntityProduct result = instance.addProduct(product, categoryId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addProduct method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testAddProduct_EntityProduct() {
        System.out.println("addProduct");
        EntityProduct entityProduct = null;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        boolean expResult = false;
        boolean result = instance.addProduct(entityProduct);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getProductById method, of class ProductRepositoryImpl.
     */
    public EntityProduct testGetProductById(String productId) {
        System.out.println("getProductById");
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        EntityProduct result = instance.getProductById(productId);
        assertNotNull(result);
        return result;
    }

    /**
     * Test of deleteProduct method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testDeleteProduct_String() {
        System.out.println("deleteProduct");
        String id = "";
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        boolean expResult = false;
        boolean result = instance.deleteProduct(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addProduct method, of class ProductRepositoryImpl.
     */
    public String testAddProduct(EntityProduct entityProduct, Set<Integer> cateogoryList, Integer quantity) {
        System.out.println("addProduct");
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        ObjectReturnDTO<String> expResult = null;
        ObjectReturnDTO<String> result = instance.addProduct(entityProduct, cateogoryList, quantity);
        assertNotNull(result.getValue());
        return result.getValue();
    }

    /**
     * Test of updateProductStatus method, of class ProductRepositoryImpl.
     */
    public void testUpdateProductStatus(String id, ProductStatus status) {
        System.out.println("updateProductStatus");
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        boolean expResult = true;
        int reasonCode = instance.updateProductStatus(id, status);
        assertEquals(expResult, reasonCode);
    }

    /**
     * Test of updateViewCount method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testUpdateViewCount_String() {
        System.out.println("updateViewCount");
        String id = "";
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        boolean expResult = false;
        boolean result = instance.updateViewCount(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExclusiveProducts method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testGetExclusiveProducts() {
        System.out.println("getExclusiveProducts");
        String pivotProductId = "";
        Scroll scroll = null;
        int limit = 0;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        ListReturnDTO<EntityProduct> expResult = null;
        ListReturnDTO<EntityProduct> result = instance.getExclusiveProducts(pivotProductId, scroll, limit);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRecommendedProducts method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testGetRecommendedProducts() {
        System.out.println("getRecommendedProducts");
        String productId = "";
        String pivotId = "";
        Scroll scroll = null;
        int limit = 0;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        PivotedListReturnDTO<EntityProduct, String> expResult = null;
        PivotedListReturnDTO<EntityProduct, String> result = instance.getRecommendedProducts(productId, pivotId, scroll, limit);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNonExclusiveProducts method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testGetNonExclusiveProducts() {
        System.out.println("getNonExclusiveProducts");
        String pivotProductId = "";
        Scroll scroll = null;
        int limit = 0;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        ListReturnDTO<EntityProduct> expResult = null;
        ListReturnDTO<EntityProduct> result = instance.getNonExclusiveProducts(pivotProductId, scroll, limit);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getProductsByShopId method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testGetProductsByShopId() {
        System.out.println("getProductsByShopId");
        String pivotProductId = "";
        long shopId = 0L;
        Scroll scroll = null;
        int limit = 0;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        ListReturnDTO<EntityProduct> expResult = null;
        ListReturnDTO<EntityProduct> result = instance.getProductsByShopId(pivotProductId, shopId, scroll, limit, ProductStatus.AVAILABLE);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNearestLocaitonProducts method, of class
     * ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testGetNearestLocaitonProducts() {
        System.out.println("getNearestLocaitonProducts");
        float lat = 0.0F;
        float lon = 0.0F;
        int radius = 0;
        String pivotProductId = "";
        Scroll scroll = null;
        int limit = 0;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        List<EntityProduct> expResult = null;
        List<EntityProduct> result = instance.getNearestLocaitonProducts(lat, lon, radius, pivotProductId, scroll, limit);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMostViewedProducts method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testGetMostViewedProducts_3args() {
        System.out.println("getMostViewedProducts");
        String pivotProductId = "";
        Scroll scroll = null;
        int limit = 0;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        List<EntityProduct> expResult = null;
        List<EntityProduct> result = instance.getMostViewedProducts(pivotProductId, scroll, limit);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getProductsInAPriceRange method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testGetProductsInAPriceRange() {
        System.out.println("getProductsInAPriceRange");
        double leastPrice = 0.0;
        double mostPrice = 0.0;
        String pivotProductId = "";
        Scroll scroll = null;
        int limit = 0;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        List<EntityProduct> expResult = null;
        List<EntityProduct> result = instance.getProductsInAPriceRange(leastPrice, mostPrice, pivotProductId, scroll, limit);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of deleteProductFromDatabase method, of class ProductRepositoryImpl.
     */
    public void testDeleteProductFromDatabase_String(String id) {
        System.out.println("deleteProductFromDatabase");
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        boolean expResult = true;
        boolean result = instance.deleteProductFromDatabase(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateProduct method, of class ProductRepositoryImpl.
     */
    public void testUpdateProduct(EntityProduct ip) {
        System.out.println("updateProduct");
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        int expResult = 0;
        int result = instance.updateProduct(ip);
        assertEquals(expResult, result);
    }

    /**
     * Test of searchProductByName method, of class ProductRepositoryImpl.
     */
    public String testSearchProductByName(String pivot, String params, Scroll scroll, int limit) {
        System.out.println("searchProductByName");
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        int expResult = 0;
        PivotedListReturnDTO<EntityProduct, String> searchProductByName = instance.searchProductByName(pivot, scroll, limit, params);
        assertEquals(expResult, searchProductByName.getReasonCode());
        return searchProductByName.getPivot();
    }

    /**
     * Test of getAllProducts method, of class ProductRepositoryImpl.
     */
    @Test
    @Ignore
    public void testGetAllProducts() {
        System.out.println("getAllProducts");
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        List<EntityProduct> expResult = null;
        List<EntityProduct> result = instance.getAllProducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFiltererProducts method, of class ProductRepositoryImpl.
     */
    @Test
//    @Ignore
    public void testGetFiltererProducts() {
        System.out.println("getFiltererProducts");
        Integer categoryId = null;
        Long shopId = 68719476842l;
        String pivotProductId = "";
        Scroll scroll = Scroll.DOWN;
        int limit = 10;
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        int itemCount = 0;
        while (true) {
            ListReturnDTO<EntityProduct> result = instance.getFiltererProducts(categoryId, shopId, pivotProductId, scroll, limit, ProductStatus.AVAILABLE);
            if (result.getReasonCode() != ReasonCode.NONE) {
                break;
            }
            pivotProductId = result.getList().get(result.getList().size() - 1).getId();
            itemCount += result.getList().size();
        }
        System.out.println("Item Count : " + itemCount);
    }

    /**
     * Test of getProductByIds method, of class ProductRepositoryImpl.
     */
    public void testGetProductByIds(Collection<String> productIds) {
        System.out.println("getProductByIds");
        ProductRepositoryImpl instance = new ProductRepositoryImpl();
        List<EntityProduct> result = instance.getProductByIds(productIds);
        assertEquals(productIds.size(), result.size());
    }

    @Test
//    @Ignore
    public void testSearch() {
        String pivot = null;
        String param = "chums shirt";
        pivot = testSearchProductByName(pivot, param, Scroll.DOWN, 10);
        testSearchProductByName(pivot, param, Scroll.DOWN, 10);
    }
}
