/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.repositories.shop;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.cache.ProductCategoryCache;
import org.ipvision.ringmarket.constants.PaymentMode;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.constants.ShopType;
import org.ipvision.ringmarket.constants.UserTypeConstants;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.entities.order.Address;
import org.ipvision.ringmarket.entities.order.EntityOrder;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.entities.user.cart.EntityCartItem;
import org.ipvision.ringmarket.mock.MockAddress;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.order.OrderDetailsRepositoryImpl;
import org.ipvision.ringmarket.repositories.order.OrderRepositoryImpl;
import org.ipvision.ringmarket.repositories.order.OrderRepositoryImplTest;
import org.ipvision.ringmarket.repositories.product.CategoryProductRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.CategoryProductRepository;
import org.ipvision.ringmarket.repositories.product.ProductDetailRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductImageRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImplTest;
import org.ipvision.ringmarket.repositories.product.ProductStockRepositoryImplTest;
import org.ipvision.ringmarket.repositories.user.cart.CartItemRepositoryImpl;
import org.ipvision.ringmarket.repositories.user.cart.CartItemRepositoryImplTest;
import org.ipvision.ringmarket.uniqueid.UniqueNumberGenerator;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.ipvision.ringmarket.utils.PivotedListReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author saikat
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ShopRepositoryImplTest {

    static ShopRepositoryImpl instance;
    static Long shopId = System.currentTimeMillis();
    static Long userId = 420l;
    static EntityProduct product;
    static String productId;
    static List<String> cartItemIds;
    static String addressId;
    private static Set<String> orderList;
    static ProductRepositoryImpl productRepositoryImpl = new ProductRepositoryImpl();

    public ShopRepositoryImplTest() {
        instance = new ShopRepositoryImpl();
    }

    @BeforeClass
    public static void setUpClass() {
        product = new EntityProduct();
        product.setName("Test name");
        product.setCurrencyCode("$");
        product.setImageHeight(10);
        product.setImageWidth(10);
        product.setIsExclusive(false);
        product.setImageUrl("Test image");
        product.setLat(90f);
        product.setLon(23f);
        product.setPrice(100D);
        product.setCreationTime(System.currentTimeMillis());
        product.setStatus(ProductStatus.AVAILABLE);
        product.setCode(UniqueNumberGenerator.generateBarcodeId().toString());
    }

    @AfterClass
    public static void tearDownClass() {
        testDeleteShop(shopId);

        if (productId != null) {

            productRepositoryImpl.deleteProductFromDatabase(productId);

            ProductStockRepositoryImplTest implTest = new ProductStockRepositoryImplTest();
            implTest.testDeleteStockFromDB(productId);

            CategoryProductRepository categoryProductRepository = new CategoryProductRepositoryImpl();
            categoryProductRepository.removeProduct(productId);
        }

        if (cartItemIds != null && !cartItemIds.isEmpty()) {
            CartItemRepositoryImpl cartItemRepositoryImpl = new CartItemRepositoryImpl();
            cartItemRepositoryImpl.deleteCartItems(userId, cartItemIds);
        }

        if (orderList != null) {
            OrderRepositoryImplTest orderRepositoryImplTest = new OrderRepositoryImplTest();
            orderRepositoryImplTest.testDeleteOrderFromDatabase(cartItemIds);
        }

    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {

    }

    @Test
//    @Ignore
    public void testShopRepository() {
        EntityShop entityShop = new EntityShop();
        entityShop.setCreationTime(System.currentTimeMillis());
        entityShop.setId(shopId);
        entityShop.setImageHeight(100);
        entityShop.setImageUrl("Test image");
        entityShop.setImageWidth(100);
        entityShop.setName("Test store");
        entityShop.setLat(90f);
        entityShop.setLon(23f);
        entityShop.setCancelDuration(TimeUnit.DAYS.toMillis(15));
        entityShop.setFeedbackDuration(TimeUnit.DAYS.toMillis(15));
        entityShop.setProtectionDuration(TimeUnit.DAYS.toMillis(15));
        entityShop.setOwnerId(userId);

        int userType = UserTypeConstants.CELEBRITY;
        entityShop.setType(ShopType.CELEBRITY_STORE);

        testAddShop(entityShop, userType);
        testToggleShopStatus(shopId, true, shopId);
        testGetShopById(entityShop.getId());

        if (userType == UserTypeConstants.CELEBRITY) {
//            CelebrirtyStoreRepositoryImplTest celebrirtyStoreRepositoryImplTest = new CelebrirtyStoreRepositoryImplTest();
            testGetCelebrityStore(0, 10, Scroll.DOWN);
        }

        testToggleCelebrityStore(entityShop.getId(), userId, Boolean.FALSE);

        Integer type = ShopType.BRANDED_STORE;

        testToggleStoreType(shopId, type, shopId);

        EntityShop testGetShopById = testGetShopById(entityShop.getId());

        assertEquals(type, testGetShopById.getType());

        type = ShopType.GENERAL_STORE;

        testToggleStoreType(shopId, type, shopId);

        testToggleCelebrityStore(entityShop.getId(), userId, Boolean.TRUE);

    }

    @Test
    public void testShopRepositoryUpdate() {
        EntityShop testGetShopById = testGetShopById(shopId);
        product.setShopId(shopId);
        product.setShopName(testGetShopById.getName());
        Set<Integer> categoryList = new HashSet<>();
        List<EntityCategory> categories = ProductCategoryCache.getInstance().getCategories(null, 0, 1);
        categoryList.add(categories.get(0).getId());

        productId = new ProductRepositoryImplTest().testAddProduct(product, categoryList, 10);
        new ProductRepositoryImplTest().testUpdateProductStatus(productId, ProductStatus.AVAILABLE);
        testUpdateShopName(shopId, "update 1", ReasonCode.NONE);
        
         CartItemRepositoryImplTest cartItemRepositoryImplTest = new CartItemRepositoryImplTest();
        cartItemRepositoryImplTest.testAddCartItem(userId, productId, 1, ReasonCode.NONE);

        testUpdateShopName(shopId, "update 2 ", ReasonCode.NONE);

        PivotedListReturnDTO<EntityCartItem, String> cartItems = MarketRepository.INSTANCE.getCartRepository().getCartItems(userId, shopId, null, Scroll.DOWN, Integer.MAX_VALUE);

        cartItemIds = new LinkedList<>();

        for (EntityCartItem entityCartItem : cartItems.getList()) {
            cartItemIds.add(entityCartItem.getId());
        }
        Address address = MockAddress.mockAddress();

        ObjectReturnDTO<String> addShippingAddress = MarketRepository.INSTANCE.getShippingAddressRepository().addShippingAddress(userId, address, true, (byte) 0);
        addressId = addShippingAddress.getValue();

        String invoiceId = cartItemRepositoryImplTest.testOrderProductsFromCartWithSuccess(cartItemIds, userId, userId, Shipment.HOME_DELIVERY, addressId, PaymentMode.HAND_CASH);

        ListReturnDTO<EntityOrder> orderListByInvoiceId = MarketRepository.INSTANCE.getOrderRepository().getOrderListByInvoiceId(invoiceId);

        orderList = new HashSet<>();
        for (EntityOrder entityOrder : orderListByInvoiceId.getList()) {
            orderList.add(entityOrder.getId());
        }
        testUpdateShopName(shopId, "update 3 ", ReasonCode.NONE);
    }

    /**
     * Test of addShop method, of class ShopRepositoryImpl.
     */
    public void testAddShop(EntityShop shop, int userType) {
        System.out.println("addShop");
        int expResult = ReasonCode.NONE;
        int result = instance.addShop(shop, userType);
        assertEquals(expResult, result);
    }

    /**
     * Test of getShopById method, of class ShopRepositoryImpl.
     */
    public EntityShop testGetShopById(long id) {
        System.out.println("getShopById");
        EntityShop result = instance.getShopById(id);
        assertNotNull(result);
        return result;
    }

    /**
     * Test of updateShop method, of class ShopRepositoryImpl.
     */
    public void testUpdateShop(EntityShop shop) {
        System.out.println("updateShop");
        boolean expResult = true;
        boolean result = instance.updateShop(shop);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteShop method, of class ShopRepositoryImpl.
     */
    public static void testDeleteShop(long id) {
        System.out.println("deleteShop");
        int expResult = ReasonCode.NONE;
        int result = instance.deleteShop(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getBrands method, of class ShopRepositoryImpl.
     */
    public void testGetBrands(long pivotShopId, int limit, Scroll scroll) {
        System.out.println("getBrands");
        int reasonCode = ReasonCode.NONE;
        ListReturnDTO<EntityShop> result = instance.getBrands(pivotShopId, limit, scroll, true);
        assertEquals(reasonCode, result.getReasonCode());
    }

    /**
     * Test of getStores method, of class ShopRepositoryImpl.
     *
     * @param pivotShopId
     * @param limit
     * @param scroll
     */
    public void testGetStores(long pivotShopId, int limit, Scroll scroll) {
        System.out.println("getStores");
        int reasonCode = ReasonCode.NONE;
        ListReturnDTO<EntityShop> result = instance.getStores(pivotShopId, limit, scroll, true);
        assertEquals(reasonCode, result.getReasonCode());
    }

    /**
     * Test of toggleShopStatus method, of class ShopRepositoryImpl.
     *
     * @param creationTime
     * @param shopId
     * @param enable
     * @param activistId
     */
    public void testToggleShopStatus(long shopId, boolean enable, Long activistId) {
        System.out.println("getStores");
        int reasonCode = ReasonCode.NONE;
        int result = instance.toggleShopStatus(shopId, enable, activistId);
        assertEquals(reasonCode, result);
    }

    /**
     * Test of toggleStoreType method, of class ShopRepositoryImpl.
     *
     * @param creationTime
     * @param shopId
     * @param type
     * @param activistId
     */
    public void testToggleStoreType(long shopId, int type, Long activistId) {
        System.out.println("toggleStoreType");
        int reasonCode = ReasonCode.NONE;
        int result = instance.toggleStoreType(shopId, type, activistId);
        assertEquals(reasonCode, result);
    }

    /**
     * Test of toggleCelebrityStore method, of class ShopRepositoryImpl.
     *
     * @param shopId
     * @param enable
     * @param activistId
     */
    public void testToggleCelebrityStore(long shopId, long activistId, Boolean enable) {
        System.out.println("toggleCelebrityStore");
        int reasonCode = ReasonCode.NONE;
        int result = instance.toggleCelebrityStore(shopId, activistId, enable);
        assertEquals(reasonCode, result);
    }

    /**
     * Test of getCelebrityStore method, of class ShopRepositoryImpl.
     */
    public void testGetCelebrityStore(long pivotShopId, int limit, Scroll scroll) {
        System.out.println("getCelebrityStore");
        ListReturnDTO<EntityShop> result = instance.getCelebrityStore(pivotShopId, limit, scroll);
        assertNotEquals(0, result.getList().size());
    }

    public void testUpdateShopName(long shopId, String name, int expected) {
        System.out.println("updateShopName");
        int updateShopName = instance.updateShopName(shopId, name);
        EntityShop testGetShopById = testGetShopById(shopId);
        System.out.println(testGetShopById.getName());
        assertEquals(updateShopName, expected);
    }
}
