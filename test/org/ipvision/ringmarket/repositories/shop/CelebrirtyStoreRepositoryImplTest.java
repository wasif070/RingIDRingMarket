///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.ipvision.ringmarket.repositories.shop;
//
//import javax.persistence.EntityManager;
//import org.ipvision.ringmarket.constants.ReasonCode;
//import org.ipvision.ringmarket.constants.Scroll;
//import org.ipvision.ringmarket.entities.shop.EntityCelebrityStore;
//import org.ipvision.ringmarket.entities.shop.EntityShop;
//import org.ipvision.ringmarket.utils.ListReturnDTO;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author saikat
// */
//public class CelebrirtyStoreRepositoryImplTest {
//    
//    public CelebrirtyStoreRepositoryImplTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of addStore method, of class CelebrirtyStoreRepositoryImpl.
//     */
//    public void testAddStore(EntityCelebrityStore shop) {
//        System.out.println("addStore");
//        CelebrirtyStoreRepositoryImpl instance = new CelebrirtyStoreRepositoryImpl();
//        int expResult = ReasonCode.NONE;
//        int result = instance.addStore(shop);
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of deleteCelebrityStore method, of class CelebrirtyStoreRepositoryImpl.
//     */
//    public void testDeleteCelebrityStore(long shopId) {
//        System.out.println("deleteCelebrityStore");
//        CelebrirtyStoreRepositoryImpl instance = new CelebrirtyStoreRepositoryImpl();
//        int expResult = ReasonCode.NONE;
//        int result = instance.deleteCelebrityStore(shopId);
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of getCelebrityStore method, of class CelebrirtyStoreRepositoryImpl.
//     */
//    public void testGetCelebrityStore(long pivotShopId , int limit , Scroll scroll) {
//        System.out.println("getCelebrityStore");
//        CelebrirtyStoreRepositoryImpl instance = new CelebrirtyStoreRepositoryImpl();
//        ListReturnDTO<EntityShop> result = instance.getCelebrityStore(pivotShopId, limit, scroll);
//        assertNotEquals(0, result.getList().size());
//    }
//    
//}
