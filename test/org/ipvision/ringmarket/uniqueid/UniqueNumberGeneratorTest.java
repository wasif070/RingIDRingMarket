/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.uniqueid;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class UniqueNumberGeneratorTest {

    private Set<Long> barcodeSet;
    private static int iteration;

    public UniqueNumberGeneratorTest() {
        barcodeSet = new HashSet<>();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("Total iteration : " + iteration);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGenerateBarcodeId() {
        System.out.println("generateBarcodeId");
        while (true) {
            Long result = UniqueNumberGenerator.generateBarcodeId();
            boolean contains = barcodeSet.contains(result);
            iteration++;
            assertFalse(contains);
            barcodeSet.add(result);
            System.out.println("size : "+ barcodeSet.size());
        }
    }

}
