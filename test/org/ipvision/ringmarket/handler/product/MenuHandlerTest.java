/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.handler.product;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.ipvision.ringmarket.constants.MarketAttribute;
import org.ipvision.ringmarket.sender.ISender;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ipvision.ringmarket.handler.category.MenuHandler;

/**
 *
 * @author saikat
 */
public class MenuHandlerTest {

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static JsonObject jsonObject = new JsonObject();

    private static ISender sender = new ISender() {
        @Override
        public void sendToClient(JsonObject response) {
             String responseStr = response.toString();

            JsonObject responseJson = gson.fromJson(responseStr, JsonObject.class);

            System.out.println("response------>" + gson.toJson(responseJson));
        }
    };

    private static void addMockMenus() {
//        MarketMenu marketMenu = new MarketMenu();
//        marketMenu.addMenu("Brands", MenuType.BRANDS, "icon", true);
//        marketMenu.addMenu("Shops", MenuType.SHOP, "icon", true);
//        marketMenu.addMenu("Hot Deals", MenuType.HOT_DEALS, "icon", true);
    }

    public MenuHandlerTest() {
        jsonObject.addProperty("pckId", "sadasd");
    }

    @BeforeClass
    public static void setUpClass() {
        addMockMenus();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getMarketMenus method, of class MenuHandler.
     */
    @Test
    public void testTopNavMenus() {
        System.out.println("getTopNavigationItems");

        MenuHandler.getTopNavigationItems(jsonObject, sender);
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test()
    public void testMenuItems() {
        System.out.println("getFeaturePages");

        MenuHandler.getFeaturePages(jsonObject, sender);
    }

    @Test()
    public void testGetBrands() {
        System.out.println("getBrands");
        JsonObject requestObject = new JsonObject();
        requestObject.addProperty(MarketAttribute.PACKET_ID, "abc");
        requestObject.addProperty(MarketAttribute.SHOP_ID, 0);
        requestObject.addProperty(MarketAttribute.SCROLL, 1);

        MenuHandler.getBrands(requestObject, sender);
    }

    @Test()
    public void testGetExclusive() {
        System.out.println("getExclusives");
        JsonObject requestObject = new JsonObject();
        requestObject.addProperty(MarketAttribute.PACKET_ID, "abc");
        requestObject.addProperty(MarketAttribute.SCROLL, 1);
        requestObject.add(MarketAttribute.PRODUCT_ID, null);
        MenuHandler.getExclusives(requestObject, sender);
    }

}
