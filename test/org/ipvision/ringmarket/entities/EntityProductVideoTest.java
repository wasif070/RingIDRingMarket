/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.entities;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.db.DBConnection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author saikat
 */
public class EntityProductVideoTest {
    
    public EntityProductVideoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class EntityProductVideo.
     */
    @Test
    public void testGetId() {
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
        entityManager.getTransaction().begin();
        Query query = entityManager.createNativeQuery("insert into product_video (video_url, thumb_image_url, product_id)"
                + " values('a', 'b','c')");
        query.executeUpdate();
        
        EntityProductVideo epv = new EntityProductVideo();
        epv.setProductId("a");
        epv.setThumbImageHeight(10);
        epv.setThumbImageWidth(10);
        epv.setThumbImageUrl("b");
        epv.setVideoUrl("c");
        
        entityManager.persist(epv);
        entityManager.getTransaction().commit();
        }
finally{
entityManager.close();
}
    }
}
