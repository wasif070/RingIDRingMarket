/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.user;

import org.ipvision.ringmarket.user.UserRole;
import java.util.List;
import java.util.Random;
import org.ipvision.ringmarket.constants.RingmarketRole;
import org.ipvision.ringmarket.entities.EntityUserRole;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Imran
 */
public class UserRoleTest {
    
    public UserRoleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testUserRole() {
        UserRole userRole = new UserRole();
        int newUser = new Random().nextInt(4588);
        for (int i = 0; i < 10; i++) {
            userRole.addUser(newUser + i, RingmarketRole.BUYER);
        }
        System.out.println(userRole.getUsersByRole(RingmarketRole.BUYER).size());
        for (EntityUserRole iUserRole : userRole.getUsersByRole(RingmarketRole.BUYER)) {
            assertEquals(iUserRole.getRole(), RingmarketRole.BUYER.ordinal());
        }
        for (int i = 0; i < 10; i++) {
            userRole.removeUser(newUser + i, RingmarketRole.BUYER);
        }
   }
    
}
