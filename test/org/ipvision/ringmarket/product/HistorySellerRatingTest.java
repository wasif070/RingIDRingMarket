/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product;

import javax.persistence.EntityManager;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.product.seller.SellerRatingLogRepositoryImpl;
import org.ipvision.ringmarket.product.seller.SellerRating;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ipvision.ringmarket.entities.EntitySellerRatingLog;
import org.ipvision.ringmarket.entities.EntitySellerRating;

/**
 *
 * @author Imran
 */
public class HistorySellerRatingTest {

    static long sellerId = 10;
    static long buyerId = 20;
    static byte rating = 4;
    
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addNewRatingTest() {
        SellerRating sellerRating = new SellerRating();
        sellerRating.updateSellerRating(sellerId, buyerId, rating);

        EntitySellerRating rater = sellerRating.getRating(sellerId);
        assertEquals(sellerId, rater.getSellerId());

        sellerRating.removeSellerRating(sellerId, buyerId);
    }

    @Test
    public void updateRatingTest() {
        SellerRating sellerRating = new SellerRating();
        sellerRating.updateSellerRating(sellerId, buyerId, rating);

        rating = 3;
        sellerRating.updateSellerRating(sellerId, buyerId, rating);
        EntitySellerRating rater = sellerRating.getRating(sellerId);
        assertEquals(sellerId, rater.getSellerId());

        sellerRating.removeSellerRating(sellerId, buyerId);
    }
}
