/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product;

import org.ipvision.ringmarket.repositories.product.HistoryProduct;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import org.ipvision.ringmarket.constants.OfferStatus;
import org.ipvision.ringmarket.constants.OrderStatus;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.EntityOfferProduct;
import org.ipvision.ringmarket.product.order.AgentOrder;
import org.ipvision.ringmarket.product.buyer.OfferProduct;
import org.ipvision.ringmarket.product.order.HistoryOrder;
import org.ipvision.ringmarket.product.order.OrderProduct;
import org.ipvision.ringmarket.product.seller.SellerProduct;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Imran
 */
/**
 * Steps:
 * 1. add new product by seller
 * 2. buyer notices the product
 * 3. buyer makes an offer
 * 4. seller accepts/rejects the offer
 * 5. if accepts, seller and agent notified, a new order is placed
 * 6. shipment completes and order status is changed to completed
 * 7. related data removed from db
 */
public class OfferAcceptTest {

    public OfferAcceptTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAcceptOffer() {
        //adding a product to product table
        EntityProduct iProduct = addProduct();
        long sellerId = 115;
        //adding the item to seller_product table
        SellerProduct sellerProduct = new SellerProduct();
        sellerProduct.addNewProduct(sellerId, iProduct.getId(), System.currentTimeMillis(), iProduct.getName(), iProduct.getImageUrl());
        
        //buyer noticing the product
        ProductRepositoryImpl product = new ProductRepositoryImpl();
        List<EntityProduct> nearestProducts = product.getNearestLocaitonProducts(24.55f, 76.77f, 1, "", Scroll.DOWN, 10);
        for (EntityProduct nearestProduct : nearestProducts) {
            if(nearestProduct.getId().equals(iProduct.getId())){
                break;
            }
        }
        //buyer makes an offer
        OfferProduct offerProduct = new OfferProduct();
        String offerId = TimeUUID.timeBased().toString();
        long buyerId = 120;
        offerProduct.addNewOffer(offerId, buyerId,"buyer", iProduct.getId(), iProduct.getName(), iProduct.getImageUrl(), 60.0, System.currentTimeMillis()+8454L,OfferStatus.PROCESSING);
        //seller accepts the offer
        offerProduct.updateOfferStatus(offerId, OfferStatus.ACCEPTED);
        EntityOfferProduct iOfferProduct = offerProduct.getOfferInfo(offerId);
        //new order is formed
        OrderProduct orderProduct = new OrderProduct();
        long agentId = 121;
        AgentOrder agentOrder = new AgentOrder();
        boolean addAgentOrder = agentOrder.addAgentOrder(agentId, iOfferProduct.getId(), System.currentTimeMillis());
        Assert.assertEquals(true,addAgentOrder);
        
        // removing datas from db
        sellerProduct.deleteProduct(iProduct.getId());
        HistoryProduct historyProduct = new HistoryProduct();
        historyProduct.removeProductFromHistory(iProduct.getId());
        HistoryOrder historyOrder = new HistoryOrder();
        historyOrder.removeHistory(offerId);
        offerProduct.removeOffer(offerId);
        agentOrder.deleteAgentOrder(offerId, OrderStatus.CANCELLED);
    }

    public EntityProduct addProduct() {
        EntityProduct iProduct = new EntityProduct();
        iProduct.setId(TimeUUID.timeBased().toString());
        iProduct.setName("Offer Accept Test: " + new Random().nextInt(100));
//        iProduct.setIsFixedPrice(false);
        iProduct.setIsExclusive(false);
        iProduct.setPrice(75.00);
        iProduct.setLat(24.55f);
        iProduct.setLon(76.77f);
        iProduct.setImageUrl(UUID.randomUUID().toString().substring(15, 30));
//        iProduct.setCategoryId(1);
//        iProduct.setViewCount(0);
        iProduct.setStatus(ProductStatus.AVAILABLE.getValue());
        ProductRepositoryImpl product = new ProductRepositoryImpl();
        product.addProduct(iProduct);
        return iProduct;
    }

}
