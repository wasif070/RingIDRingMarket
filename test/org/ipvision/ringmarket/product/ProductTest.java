/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product;

import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.product.EntityProductDetail;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Imran
 */
//@RunWith(ExtendedRunner.class)
public class ProductTest {

    static float[] lat = new float[10];
    static float[] lon = new float[10];
    static String[] name = new String[10];
    static int cnt = 0;

    public ProductTest() {
        lat[0] = (float) 23.71;
        lon[0] = (float) 90.35;
        name[0] = "watch ";
        lat[1] = (float) 23.72;
        lon[1] = (float) 90.36;
        name[1] = "guitar ";
        lat[2] = (float) 23.73;
        lon[2] = (float) 90.37;
        name[2] = "iphone ";
        lat[3] = (float) 23.74;
        lon[3] = (float) 90.38;
        name[3] = "android ";
        lat[4] = (float) 23.75;
        lon[4] = (float) 90.39;
        name[4] = "novel ";
        lat[5] = (float) 23.76;
        lon[5] = (float) 90.40;
        name[5] = "shoe ";
        lat[6] = (float) 23.77;
        lon[6] = (float) 90.39;
        name[6] = "t-shirt ";
        lat[7] = (float) 23.78;
        lon[7] = (float) 90.38;
        name[7] = "soap ";
        lat[8] = (float) 23.79;
        lon[8] = (float) 90.37;
        name[8] = "polo ";
        lat[9] = (float) 23.80;
        lon[9] = (float) 90.36;
        name[9] = "cake ";
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    @Ignore
//    @Test
//    //getting products
//    public void testViewProduct() {
//        ProductRepositoryImpl product = new ProductRepositoryImpl();
//        //getting featured products
//        List<IProduct> featuredProducts = product.getExclusiveProducts("", Scroll.DOWN, 10);
//        System.out.println("Number of Featured Products: " + featuredProducts.size());
// 
//        //getting nearest products
//        List<IProduct> nearestProducts = product.getNearestLocaitonProducts(23.73f, 90.40f, 6, "", Scroll.DOWN, 10);
//        System.out.println("Number of Nearest Products: " + nearestProducts.size());
//        
//        //adding a product and updating to featured so feaured product will be increase by 1
//        IProduct newProduct = this.addProduct();
//        newProduct.setIsExclusive(Boolean.TRUE);
//        product.updateProduct(newProduct);
//        List<IProduct> ftrdProdNew = product.getExclusiveProducts("", Scroll.DOWN, 10);
//        Assert.assertEquals(featuredProducts.size() + 1, ftrdProdNew.size());
//        //deleteing the product;
//        product.deleteProductFromDatabase(newProduct.getId());
//
//        //adding a product and updating it's lat, lon to 0 km from search location, so nearest productwill increase by 1
//        newProduct = this.addProduct();
//        newProduct.setLat(23.73f);
//        newProduct.setLon(90.40f);
//        product.updateProduct(newProduct);
//        List<IProduct> nrstPrdtNew = product.getNearestLocaitonProducts(23.73f, 90.40f, 6, "", Scroll.DOWN, 10);
//        Assert.assertEquals(nearestProducts.size() + 1, nrstPrdtNew.size());
//        //deleteing the product;
//        product.deleteProductFromDatabase(newProduct.getId());
//    }
//    
//    @Ignore
//    @Test
//    public void testSearchingWithFilters() {
//        ProductRepositoryImpl product = new ProductRepositoryImpl();
//        IProduct newProduct;
//
//        /** Price Range Filter **/
//        List<IProduct> oldList = product.getProductsInAPriceRange(30.0, 80.0, "", Scroll.DOWN, 10);
//        System.out.println("Number of Products In A Price Range: " + oldList.size());
//        //adding a new product and updating its price to the query range
//        newProduct = this.addProduct();
//        newProduct.setBasePrice(70.0);
//        product.updateProduct(newProduct);
//        List<IProduct> newList = product.getProductsInAPriceRange(30.0, 80.0, "", Scroll.DOWN, 10);
//        Assert.assertEquals(oldList.size() + 1, newList.size());       
//        //deleteing the product;
//        product.deleteProductFromDatabase(newProduct.getId());
//        
//        
//        
////        /** CategoryWise Filter and CategoryWise Price Range Filter **/
////        //Categorywise Filter
////        List<IProduct> categoryWiseProducts = product.getCategoryWiseProduct(1, "", Scroll.DOWN, 10);
////        System.out.println("Number of Category Wise Products: " + categoryWiseProducts.size());
////        //Categorywise Filter In A Price Range
////        List<IProduct> catProductsInPrcRange = product.getCategoryIProductsInAPriceRange(1, 40.0, 100.0, "", Scroll.DOWN, 10);
////        System.out.println("Number of Category Wise Products In A Range: " + catProductsInPrcRange.size());
////        //adding a product and updating its category id 1 & price 50 
////        //so both categoryWiseProducts and catProductsInPrcRange will be increase by 1
////        newProduct = addProduct();
//////        newProduct.setCategoryId(1);
////        newProduct.setBasePrice(50.0);
////        product.updateProduct(newProduct);
////        List<IProduct> catWiseProdNew = product.getCategoryWiseProduct(1, "", Scroll.DOWN, 10);
////        Assert.assertEquals(categoryWiseProducts.size() + 1, catWiseProdNew.size());
////        List<IProduct> catProductsInPrcRangeNew = product.getCategoryIProductsInAPriceRange(1, 40.0, 100.0, "", Scroll.DOWN, 10);
////        Assert.assertEquals(catProductsInPrcRange.size() + 1, catProductsInPrcRangeNew.size());       
////        //deleteing the product;
////        product.deleteProductFromDatabase(newProduct.getId());
//        
//        
//        
//        
//        /** Most Viewed ProductRepositoryImpl Search **/
//        oldList = product.getMostViewedProducts("", Scroll.DOWN, 10);
//        System.out.println("Most Viewed Products' view Count: " + oldList.get(0).getViewCount());
//        //adding a new product and updating its view count to maximum
//        newProduct = this.addProduct();
//        newProduct.setViewCount(oldList.get(0).getViewCount() + 10);
//        product.updateProduct(newProduct);
//        newList = product.getMostViewedProducts("", Scroll.DOWN, 10);
//        Assert.assertEquals(newProduct.getId(), newList.get(0).getId());
//        //deleteing the product;
//        product.deleteProductFromDatabase(newProduct.getId());
//
//        
//        /** Most Viewed ProductRepositoryImpl In A Range Filter **/
//        oldList = product.getMostViewedProductsInAPriceRange(0, 50, "", Scroll.DOWN, 10);
//        System.out.println("Most Viewed Products'(In a Price Range) view Count: " + oldList.get(0).getViewCount());
//        //adding a new product and updating its price view count to maximum
//        newProduct = this.addProduct();
//        newProduct.setViewCount(oldList.get(0).getViewCount() + 10);
//        newProduct.setBasePrice(40.0);
//        product.updateProduct(newProduct);
//        newList = product.getMostViewedProductsInAPriceRange(0, 50.0, "", Scroll.DOWN, 10);
//        Assert.assertEquals(newProduct.getId(), newList.get(0).getId());
//        //deleteing the product;
//        product.deleteProductFromDatabase(newProduct.getId());
//
//
//        /** Similar Named Filter **/
//        oldList = product.getSimilarNamedProducts("watch", "", Scroll.DOWN, 10);
//        System.out.println("Number of Products with Similar Name: " + oldList.size());
//        //adding a new product and updating its name similar to query name
//        newProduct = this.addProduct();
//        newProduct.setName("watch-54412");
//        product.updateProduct(newProduct);
//        newList = product.getSimilarNamedProducts("watch", "", Scroll.DOWN, 10);
//        Assert.assertEquals(oldList.size() + 1, newList.size());       
//        //deleteing the product;
//        product.deleteProductFromDatabase(newProduct.getId());
//        
//    }
    
    
    @Test
    public EntityProduct addProduct() {
        EntityProduct iProduct = new EntityProduct();
        iProduct.setId(UUID.randomUUID().toString());
        iProduct.setName(name[cnt] + UUID.randomUUID().toString().substring(15, 25));
//        iProduct.setIsFixedPrice(false);
        iProduct.setIsExclusive(Boolean.TRUE);
        iProduct.setPrice(new Random().nextInt(100));
        iProduct.setLat(lat[cnt]);
        iProduct.setLon(lon[cnt]);
        iProduct.setImageUrl(UUID.randomUUID().toString().substring(15, 30));
//        iProduct.setCategoryId(1);
//        iProduct.setViewCount(new Random().nextInt(100));
        iProduct.setStatus(ProductStatus.AVAILABLE);
        ProductRepositoryImpl product = new ProductRepositoryImpl();
        product.addProduct(iProduct);
        ++cnt;
        cnt = cnt % 10;
        return iProduct;
    }

//
//    @Ignore
//    @Test
//    public void testDeleteProduct() {
//
//    }
//
//    @Ignore
//    @Test
//    public void testUpdateProductStatus() {
//
//    }
//
//    @Ignore
//    @Test
//    public void testUpdateProduct() {
//        
//    }
//
//    @Ignore
//    @Test
//    public void testGetFeaturedProudcts() {
//        ProductRepositoryImpl instance = new ProductRepositoryImpl();
//        List<IProduct> result = instance.getExclusiveProducts("", Scroll.DOWN, 10);
//        System.out.println(result.size());
//        for (IProduct iProduct : result) {
//            assertEquals(Boolean.TRUE, iProduct.isExclusive());
//        }
//    }
//
////    @Ignore
//    @Test
//    public void testGetNearestLocaitonProducts() {
//        ProductRepositoryImpl instance = new ProductRepositoryImpl();
//        List<IProduct> result = instance.getNearestLocaitonProducts(lat[0], lon[3], 50, "", Scroll.DOWN, 10);
//        for (IProduct iProduct : result) {
//            System.out.println(iProduct.getName());
//        }
//    }
//
//    @Ignore
//    @Test
//    public void testGetSimilarNamedProducts() {
//        ProductRepositoryImpl instance = new ProductRepositoryImpl();
//        List<IProduct> result = instance.getSimilarNamedProducts("Clock", "", Scroll.DOWN, 10);
//        for (IProduct iProduct : result) {
//            System.out.println(iProduct.getName());
//            System.out.println(iProduct.getIsFixedPrice());
//        }
//    }
//
//    @Ignore
//    @Test
//    public void testGetProductsInAPriceRange() {
//        ProductRepositoryImpl instance = new ProductRepositoryImpl();
//        List<IProduct> result = instance.getProductsInAPriceRange(10, 70, "", Scroll.DOWN, 10);
//        for (IProduct iProduct : result) {
//            System.out.println(iProduct.getName() + " --> " + iProduct.getBasePrice());
//        }
//    }
//
//    @Ignore
//    @Test
//    public void testGetMostViewedProductsInAPriceRange() {
//        ProductRepositoryImpl instance = new ProductRepositoryImpl();
//        List<IProduct> result = instance.getMostViewedProductsInAPriceRange(10, 70, "", Scroll.DOWN, 10);
//        for (IProduct iProduct : result) {
//            System.out.println(iProduct.getName() + " --> " + iProduct.getBasePrice() + " --> " + iProduct.getViewCount() );
//        }
//    }
//
//    @Ignore
//    @Test
//    public void testGetMostViewedProducts() {
//        ProductRepositoryImpl instance = new ProductRepositoryImpl();
//        List<IProduct> result = instance.getMostViewedProducts("", Scroll.DOWN, 10);
//    }
//    
//    @Test
//    public void test1(){
//        ProductRepositoryImpl product = new ProductRepositoryImpl();
//        List<IProduct> iProductList = product.getExclusiveProducts(null, Scroll.DOWN, 10);
//        System.out.println(iProductList.size());
//        GsonBuilder builder = new GsonBuilder();
//        builder.registerTypeAdapter(IProduct.class, new ProductAdapter());
//        Gson gson = builder.create();
//        for (IProduct iProduct1 : iProductList) {
////            System.out.println(iProduct1.getId() + " --> " + iProduct1.isExclusive() + " --> " + iProduct1.getStatus());
//            System.out.println(gson.toJsonTree(iProduct1, IProduct.class));
//        }
//    }
//    
    @Test
    public void productDetailsTest() {
        EntityProductDetail entityProductDetail = new EntityProductDetail();
        entityProductDetail.setId("test");
        LinkedHashMap jo = new LinkedHashMap();
        jo.put("color", "black");
        entityProductDetail.setAttributes(jo);
        entityProductDetail.setDescription("Good");
        boolean addProductDetail = MarketRepository.INSTANCE.getProductDetailsRepository().addProductDetail(entityProductDetail);
        Assert.assertTrue(addProductDetail);
        EntityProductDetail productDetail = MarketRepository.INSTANCE.getProductDetailsRepository().getProductDetail(entityProductDetail.getId());
        Assert.assertEquals(productDetail.getAttributes().toString(), jo.toString());

        boolean deleteProductDetail = MarketRepository.INSTANCE.getProductDetailsRepository().deleteProductDetail(entityProductDetail.getId());
        Assert.assertTrue(deleteProductDetail);

    }
}
