/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product;

import org.ipvision.ringmarket.repositories.product.ProductRatingLogRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductRatingRepositoryImpl;
import java.util.Map;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.product.EntityProductRating;
import org.ipvision.ringmarket.entities.product.EntityProductRatingLog;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class ProductRatingRepositoryImplTest {

    public ProductRatingRepositoryImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testProductRating() {
        Long userId = 420l;
        ListReturnDTO<EntityProduct> exclusiveProducts = MarketRepository.INSTANCE.getProductRepository().getExclusiveProducts(null, Scroll.DOWN, 1);
        String productId = exclusiveProducts.getList().get(0).getId();
        testUpdateProductRating(userId, productId, (byte) 3);
        testGetRating(productId);
        testGetRatingDetails(productId);
        testFindRatingByUser(userId, productId, (byte)3);
        testRemoveProductRating(userId, productId);
    }

    /**
     * Test of updateProductRating method, of class ProductRatingRepositoryImpl.
     * @param userId
     * @param productId
     * @param rating
     */
    public void testUpdateProductRating(Long userId, String productId, Byte rating) {
        System.out.println("updateProductRating");
        ProductRatingRepositoryImpl instance = new ProductRatingRepositoryImpl();
        boolean expResult = true;
        boolean result = instance.updateProductRating(productId, userId, rating);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeProductRating method, of class ProductRatingRepositoryImpl.
     *
     * @param userId
     * @param productId
     */
    public void testRemoveProductRating(Long userId, String productId) {
        System.out.println("removeProductRating");
        ProductRatingRepositoryImpl instance = new ProductRatingRepositoryImpl();
        boolean expResult = true;
        boolean result = instance.removeProductRating(productId, userId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRating method, of class ProductRatingRepositoryImpl.
     *
     * @param productId
     */
    public void testGetRating(String productId) {
        System.out.println("getRating");
        ProductRatingRepositoryImpl instance = new ProductRatingRepositoryImpl();
        EntityProductRating result = instance.getRating(productId);
        assertNotNull(result);
    }

    /**
     * Test of findRatingByUser method, of class ProductRatingRepositoryImpl.
     *
     * @param userId
     * @param productId
     * @param expResult
     */
    public void testFindRatingByUser(Long userId, String productId, byte expResult) {
        System.out.println("findRatingByUser");
        ProductRatingRepositoryImpl instance = new ProductRatingRepositoryImpl();
        byte result = instance.findRatingByUser(productId, userId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRatingDetails method, of class ProductRatingLogRepositoryImpl.
     *
     * @param productId
     */
    public void testGetRatingDetails(String productId) {
        System.out.println("findRatingByUser");
        ProductRatingLogRepositoryImpl instance = new ProductRatingLogRepositoryImpl();
        Map<Byte, Long> ratingDetails = instance.getRatingDetails(productId);
        assertEquals(ratingDetails.isEmpty(), false);
    }

}
