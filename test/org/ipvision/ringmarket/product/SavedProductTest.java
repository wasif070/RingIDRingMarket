/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product;

import org.ipvision.ringmarket.repositories.product.ProductVideo;
import org.ipvision.ringmarket.repositories.product.ProductDetailRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductImageRepositoryImpl;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.product.buyer.SavedProduct;
import ExRunner.ExtendedRunner;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.EntitySavedProduct;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Imran
 */
@RunWith(ExtendedRunner.class)
public class SavedProductTest {
    
    public SavedProductTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Ignore
    @Test
//    @ExtendedRunner.Repeat(10)
    public void testSaveProducts() {
        SavedProduct savedProduct = new SavedProduct();
        ProductImageRepositoryImpl productImage = new ProductImageRepositoryImpl();
        ProductVideo productVideo = new ProductVideo();
        ProductDetailRepositoryImpl productDetail = new ProductDetailRepositoryImpl();
        
        //adding a new product
        EntityProduct iProduct = new ProductTest().addProduct();
        
        //adding details
        productDetail.addProductDetail(iProduct.getId(), "A New Product Is Added For Testing. Time: " + new Date().toString());
        
        //adding new images
        productImage.addImage(UUID.randomUUID().toString().substring(15,22), 15, 25, iProduct.getId());
        productImage.addImage(UUID.randomUUID().toString().substring(15,22), 15, 25, iProduct.getId());
        productImage.addImage(UUID.randomUUID().toString().substring(15,22), 15, 25, iProduct.getId());
        Assert.assertEquals(3, productImage.getImagesOfProduct(iProduct.getId()).size());
        
        
        //adding new videos
        productVideo.addVideo(iProduct.getId(), UUID.randomUUID().toString().substring(15,22), UUID.randomUUID().toString().substring(15,22), 15, 25);
        productVideo.addVideo(iProduct.getId(), UUID.randomUUID().toString().substring(15,22), UUID.randomUUID().toString().substring(15,22), 15, 25);
        Assert.assertEquals(2, productVideo.getVideosOfProduct(iProduct.getId()).size());
        
        
        
        //getting saved product list by a buyer
        List<EntitySavedProduct> oldList = savedProduct.getSavedProductList(1, "", Scroll.DOWN, 10);
        
        //saving a new product for the user
        savedProduct.addToSavedList(1, iProduct.getId());

        //getting saved product list by the buyer after saving a new product
        List<EntitySavedProduct> newList = savedProduct.getSavedProductList(1, "", Scroll.DOWN, 10);
        //checking if saved
        boolean check = false;
        for (EntitySavedProduct iSavedProduct : newList) {
            if(iSavedProduct.getProductId().equals(iProduct.getId())){
                check = true;
            }
        }
        Assert.assertEquals(true, check);
        
        //deleting the product from saved list
        savedProduct.deleteSavedProduct(1, iProduct.getId());
        //checking if deleted
        newList = savedProduct.getSavedProductList(1, "", Scroll.DOWN, 10);
        check = false;
        for (EntitySavedProduct iSavedProduct : newList) {
            if(iSavedProduct.getProductId().equals(iProduct.getId())){
                check = true;
            }
        }
        Assert.assertEquals(false, check);
        
        
//        //update the products's likecount
//        productDetail.updateProductLikeCount(iProduct.getId());
//        Assert.assertEquals(1, productDetail.getProductDetail(iProduct.getId()).getLikeCount());
//        
//        //update the products's commentcount
//        productDetail.updateProductCommentCount(iProduct.getId());
//        Assert.assertEquals(1, productDetail.getProductDetail(iProduct.getId()).getCommentCount());
        
        //deleting the product from product table
        new ProductRepositoryImpl().deleteProductFromDatabase(iProduct.getId());
        //deleting the product from product detail table
        productDetail.deleteProductDetail(iProduct.getId());
        //deleting the images
        productImage.deleteAllImageProduct(iProduct.getId());
        //deleting the videos
        productVideo.deleteAllVideosOfProduct(iProduct.getId());
    }
    
    @Test
    public void scrollTest() {
        SavedProduct savedProduct = new SavedProduct();
        for (int i = 0; i < 30; i++) {
            savedProduct.addToSavedList(1064, "TestSavedProduct " + i);
        }
        
        List<EntitySavedProduct> iSavedProducts = savedProduct.getSavedProductList(1064, "", Scroll.DOWN, 5);
        for (EntitySavedProduct iSavedProduct : iSavedProducts) {
            System.out.println(iSavedProduct.getProductId());
        }
        System.out.println("====================================================");
        iSavedProducts = savedProduct.getSavedProductList(1064, iSavedProducts.get(iSavedProducts.size() - 1).getProductId(), Scroll.DOWN, 5);
        for (EntitySavedProduct iSavedProduct : iSavedProducts) {
            System.out.println(iSavedProduct.getProductId());
        }
        System.out.println("====================================================");
        iSavedProducts = savedProduct.getSavedProductList(1064, iSavedProducts.get(iSavedProducts.size() - 1).getProductId(), Scroll.DOWN, 5);
        for (EntitySavedProduct iSavedProduct : iSavedProducts) {
            System.out.println(iSavedProduct.getProductId());
        }
        System.out.println("====================================================");
        iSavedProducts = savedProduct.getSavedProductList(1064, iSavedProducts.get(iSavedProducts.size() - 1).getProductId(), Scroll.DOWN, 5);
        for (EntitySavedProduct iSavedProduct : iSavedProducts) {
            System.out.println(iSavedProduct.getProductId());
        }
        System.out.println("====================================================");
        System.out.println("====================================================");
        iSavedProducts = savedProduct.getSavedProductList(1064, iSavedProducts.get(0).getProductId(), Scroll.UP, 5);
        for (EntitySavedProduct iSavedProduct : iSavedProducts) {
            System.out.println(iSavedProduct.getProductId());
        }
        System.out.println("====================================================");
        iSavedProducts = savedProduct.getSavedProductList(1064, iSavedProducts.get(0).getProductId(), Scroll.UP, 5);
        for (EntitySavedProduct iSavedProduct : iSavedProducts) {
            System.out.println(iSavedProduct.getProductId());
        }
        System.out.println("====================================================");
        
        iSavedProducts = savedProduct.getSavedProductList(1064, "", Scroll.DOWN, 100);
        for (EntitySavedProduct iSavedProduct : iSavedProducts) {
            savedProduct.deleteSavedProduct(1064, iSavedProduct.getProductId());
        }
    }
}
