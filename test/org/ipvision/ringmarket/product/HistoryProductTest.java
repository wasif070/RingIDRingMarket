/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product;

import org.ipvision.ringmarket.repositories.product.HistoryProduct;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import java.util.Random;
import java.util.UUID;
import org.ipvision.ringmarket.constants.OrderStatus;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.constants.Shipment;
import org.ipvision.ringmarket.entities.EntityHistoryOrder;
import org.ipvision.ringmarket.entities.EntityOrderProduct;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.product.order.HistoryOrder;
import org.ipvision.ringmarket.product.order.OrderProduct;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Imran
 */
public class HistoryProductTest {
    
    public HistoryProductTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testHistory() {
        HistoryOrder historyOrder = new HistoryOrder();
        HistoryProduct historyProduct = new HistoryProduct();

        String id = UUID.randomUUID().toString();
        long buyerId = new Random().nextInt(1000);
        long sellerId = new Random().nextInt(1000);
        long agentId = new Random().nextInt(1000);
        String productId = UUID.randomUUID().toString();
        Shipment shipment = Shipment.HOME_DELIVERY;
        double offeredPrice = 40.0;
        double shipmentCost = 5.0;
        long orderDate = 454555;
        long expDelDate = 488777;
        OrderStatus orderStatus = OrderStatus.PROCESSING;
        long recvDayLimit = 48788888;
        
        int currentNumOfOrdersByBuyer = historyOrder.getCompletedOrderByBuyer(buyerId).size();
        int currentNumOfOrdersBySeller = historyOrder.getCompletedOrderBySeller(sellerId).size();
        
        /*****adding a new product********/
        ProductRepositoryImpl product = new ProductRepositoryImpl();
        addProduct(productId);
        
        OrderProduct instance = new OrderProduct();
        /**adding a new order**/
        instance.addNewOrder(id, buyerId, sellerId, agentId, productId, shipment, offeredPrice, shipmentCost, orderDate, expDelDate, orderStatus);
        //getting the info about the order
        EntityOrderProduct iOrderProduct = instance.getOrderInfo(id);
        //checking..
        assertEquals(iOrderProduct.getId(), id);
        assertEquals(iOrderProduct.getProductId(), productId);
        assertEquals(iOrderProduct.getShipmentMethod(), shipment.ordinal());
        assertEquals(iOrderProduct.getStatus(), orderStatus.ordinal());
        
        //updating status
        orderStatus = OrderStatus.PROCESSING;
        instance.updateOrderStatus(id, orderStatus,System.currentTimeMillis());
        //getting the info about the order
        iOrderProduct = instance.getOrderInfo(id);
        //checking
        assertEquals(iOrderProduct.getStatus(), orderStatus.ordinal());

        //updating status to complete
        orderStatus = OrderStatus.COMPLETED;
        instance.updateOrderStatus(id, orderStatus,System.currentTimeMillis());
        //getting the info about the order
        iOrderProduct = instance.getOrderInfo(id);
        //checking as order completed, deleted from order table
        assertEquals(iOrderProduct, null);
        
        //deleteing the product
        //product should be already deleted from updateOrderStatus method
        assertEquals(product.getProductById(productId), null);
        
        //order should be deleted and updated to history
        //finding it from history
        EntityHistoryOrder iHistoryOrder = historyOrder.getHistory(id);
        assertEquals(iHistoryOrder.getAgentId(), agentId);
        assertEquals(iHistoryOrder.getBuyerId(), buyerId);
        assertEquals(iHistoryOrder.getId(), id);
        assertEquals(iHistoryOrder.getProductId(), productId);
        assertEquals(iHistoryOrder.getSellerId(), sellerId);


        int newNumOfOrdersByBuyer = historyOrder.getCompletedOrderByBuyer(buyerId).size();
        int newNumOfOrdersBySeller = historyOrder.getCompletedOrderBySeller(sellerId).size();
        
        assertEquals(currentNumOfOrdersByBuyer + 1, newNumOfOrdersByBuyer);
        assertEquals(currentNumOfOrdersBySeller + 1, newNumOfOrdersBySeller);
               
        //removing the product from product history
        historyProduct.removeProductFromHistory(productId);
        assertEquals(historyProduct.getProductFromHistory(productId), null);


        //removing from history order as well
        historyOrder.removeHistory(id);
    }
    
    
    
    public EntityProduct addProduct(String id) {
        EntityProduct iProduct = new EntityProduct();
        iProduct.setId(id);
        iProduct.setName("Order Product Test: " + System.currentTimeMillis());
//        iProduct.setIsFixedPrice(false);
        iProduct.setIsExclusive(Boolean.TRUE);
        iProduct.setPrice(new Random().nextInt(100));
        iProduct.setLat((float) 55.47);
        iProduct.setLon((float) 23.69);
        iProduct.setImageUrl(UUID.randomUUID().toString().substring(15, 30));
//        iProduct.setCategoryId(1);
//        iProduct.setViewCount(new Random().nextInt(100));
        iProduct.setStatus(ProductStatus.AVAILABLE.getValue());
        ProductRepositoryImpl product = new ProductRepositoryImpl();
        product.addProduct(iProduct);
        return iProduct;
    }
}