/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product;

import org.ipvision.ringmarket.product.order.AgentOrder;
import java.util.List;
import java.util.UUID;
import org.ipvision.ringmarket.constants.OrderStatus;
import org.ipvision.ringmarket.constants.Scroll;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ipvision.ringmarket.entities.EntityAgentOrder;

/**
 *
 * @author Imran
 */
public class AgentOrderTest {

    public AgentOrderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAgentProductN() {
        AgentOrder agentOrder = new AgentOrder();
        for (int i = 0; i < 30; i++) {
            agentOrder.addAgentOrder(1076, UUID.randomUUID().toString(), System.currentTimeMillis());
//            agentOrder.addOrder(1076, UUID.randomUUID().toString(), (long) new Random().nextInt(100), OrderStatus.PROCESSING);
        }

        agentOrder.getAgentOrderList(1076, "", Scroll.DOWN, 5);
        List<EntityAgentOrder> iAgentProducts = agentOrder.getAgentOrderList(1076, "", Scroll.DOWN, 5);

        for (EntityAgentOrder iAgentProduct : iAgentProducts) {
            System.out.println(iAgentProduct.getOrderId() + " --> " + iAgentProduct.getExpDeliveryDate());
        }
        System.out.println("====================================================");

        iAgentProducts = agentOrder.getAgentOrderList(1076, iAgentProducts.get(iAgentProducts.size() - 1).getOrderId(), Scroll.DOWN, 5);
        for (EntityAgentOrder iAgentProduct : iAgentProducts) {
            System.out.println(iAgentProduct.getOrderId() + " --> " + iAgentProduct.getExpDeliveryDate());
        }
        System.out.println("====================================================");
        iAgentProducts = agentOrder.getAgentOrderList(1076, iAgentProducts.get(iAgentProducts.size() - 1).getOrderId(), Scroll.DOWN, 5);
        for (EntityAgentOrder iAgentProduct : iAgentProducts) {
            System.out.println(iAgentProduct.getOrderId() + " --> " + iAgentProduct.getExpDeliveryDate());
        }
        System.out.println("====================================================");
        iAgentProducts = agentOrder.getAgentOrderList(1076, iAgentProducts.get(iAgentProducts.size() - 1).getOrderId(), Scroll.DOWN, 5);
        for (EntityAgentOrder iAgentProduct : iAgentProducts) {
            System.out.println(iAgentProduct.getOrderId() + " --> " + iAgentProduct.getExpDeliveryDate());
        }
        System.out.println("====================================================");
        System.out.println("====================================================");

        iAgentProducts = agentOrder.getAgentOrderList(1076, iAgentProducts.get(0).getOrderId(), Scroll.UP, 5);
        for (EntityAgentOrder iAgentProduct : iAgentProducts) {
            System.out.println(iAgentProduct.getOrderId() + " --> " + iAgentProduct.getExpDeliveryDate());
        }
        System.out.println("====================================================");
        iAgentProducts = agentOrder.getAgentOrderList(1076, iAgentProducts.get(0).getOrderId(), Scroll.UP, 5);
        for (EntityAgentOrder iAgentProduct : iAgentProducts) {
            System.out.println(iAgentProduct.getOrderId() + " --> " + iAgentProduct.getExpDeliveryDate());
        }
        System.out.println("====================================================");

        iAgentProducts = agentOrder.getAgentOrderList(1076, "", Scroll.DOWN, 500);
        for (EntityAgentOrder iAgentProduct : iAgentProducts) {
            agentOrder.deleteAgentOrder(iAgentProduct.getOrderId(),OrderStatus.CANCELLED);
        }
    }
}
