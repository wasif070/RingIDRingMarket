/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.categories;

import ExRunner.ExtendedRunner;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ipvision.ringmarket.constants.ReasonCode;
import org.ipvision.ringmarket.constants.Scroll;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.ipvision.ringmarket.entities.category.EntityCategory;
import org.ipvision.ringmarket.repositories.category.CategoryRepositoryImpl;
import org.ipvision.ringmarket.utils.ObjectReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 *
 * @author alamgir
 */
//@RunWith(ExtendedRunner.class)
public class CategoryRepositoryTest {

    static String name = "SportAndOutdoor";
    static String updatedName = "SportsAndOutdoor";
    static String img = null;
    private static CategoryRepositoryImpl instance;

    static int rootCategoryId;
    static int subCategoryId;

    public CategoryRepositoryTest() {
        instance = new CategoryRepositoryImpl();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
        testDeleteCategory(rootCategoryId, true);
        testDeleteCategory(subCategoryId, false);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {

    }

    @Before
    public void initi() {
        Random r = new Random();
        name = UUID.randomUUID().toString().substring(15, 30) + r.nextInt(100000);
        updatedName = UUID.randomUUID().toString().substring(15, 30) + r.nextInt(100000);
    }

//    @Ignore
//    @Test
    public static void getCats(int parent, String spc) {
        CategoryRepositoryImpl pc = new CategoryRepositoryImpl();
        List<EntityCategory> categories = pc.getSubCategories(parent);
        for (EntityCategory cat : categories) {
            System.out.println(spc + cat.getId() + " | " + cat.getName() + " | " + cat.getParentId() + " || " + pc.getSubCategories(cat.getId()).size());
//            String tmp = spc+"-->";
//            getCats(cat.getId(), spc + "-->");
//            for (IProductCategory subCat : pc.getSubCategories(cat.getId())) {
//                System.out.println("-->" + subCat.getId() + " | " + subCat.getName());
//            }
        }
    }

    @Ignore
    @Test
    @ExtendedRunner.Repeat(1)
    public void Test1AddCategories() throws Exception {
        // set a new category information
        CategoryRepositoryImpl pc = new CategoryRepositoryImpl();
        int parentId = 0;
        int pos = 1;

        //add the root category
        pc.addRootCategory(name, img);

        EntityCategory ipc = pc.getRootCategory(name);

        //checking if informations are correct
        assertEquals(parentId, ipc.getParentId());
        assertEquals(name, ipc.getName());
        assertEquals(pos, ipc.getPosition());

        try {
            pc.addRootCategory(name, img);
            fail("Duplicate Data Inserted!");
        } catch (Exception e) {

        }

        parentId = ipc.getId();
        Random r = new Random();

        //add sub categories
        String subCategoryName = "Sport" + r.nextInt(100000);
        pos = 1;
//        pc.addSubCategory(parentId, subCategoryName, pos);
        EntityCategory category = new EntityCategory();
        category.setParentId(parentId);
        category.setName(subCategoryName);
        category.setOrdinal(0);
        category.setPosition(pos);
        category.setHasNext(Boolean.FALSE);
        pc.addSubCategory(category);

        subCategoryName = "Outdoor" + r.nextInt(100000);
        pos = 2;
//        pc.addSubCategory(parentId, subCategoryName, pos);
        category = new EntityCategory();
        category.setParentId(parentId);
        category.setName(subCategoryName);
        category.setOrdinal(0);
        category.setPosition(pos);
        category.setHasNext(Boolean.FALSE);
        pc.addSubCategory(category);

        List<EntityCategory> subCategorys = pc.getSubCategories(ipc.getId());

        for (EntityCategory subCategory : subCategorys) {
            assertEquals(ipc.getId(), subCategory.getParentId());
        }

        pc.deleteCategory(ipc.getId());
    }

    @Ignore
    @Test
    public void TestIndexing() throws Exception {
        // set a new category information
        CategoryRepositoryImpl pc = new CategoryRepositoryImpl();
        for (int i = 0; i < 100; i++) {
            pc.addRootCategory(name + ": " + i, img);
            EntityCategory ipc = getCategory(name + ": " + i);
            for (int j = 0; j < 1000; j++) {
//                pc.addSubCategory(ipc.getId(), name + ": sub : " + j);
                EntityCategory category = new EntityCategory();
                category.setParentId(ipc.getId());
                category.setName(name + ": sub : " + j);
                category.setOrdinal(0);
                category.setHasNext(Boolean.FALSE);
                pc.addSubCategory(category);
            }
        }

        long curTime = System.currentTimeMillis();
        for (EntityCategory ipc : pc.getRootCategories()) {
            pc.getSubCategories(ipc.getId());
        }
        System.out.println(System.currentTimeMillis() - curTime);
        for (EntityCategory ipc : pc.getRootCategories()) {
            pc.deleteCategory(ipc.getId());
        }
    }

    @Ignore
    @Test
    public void Test2Updates() throws Exception {
        //adding a root category
        name = "KidsAndBabies" + (new Random().nextInt(1000));
        updatedName = "HelloKids" + (new Random().nextInt(1000));

        CategoryRepositoryImpl pc = new CategoryRepositoryImpl();
        int parentId = 0;
        int pos = 0;

        pc.addRootCategory(name, img);

        //checking informations
        EntityCategory ipc = pc.getRootCategory(name);

        assertEquals(parentId, ipc.getParentId());
        assertEquals(name, ipc.getName());
        assertEquals(pos, ipc.getPosition());

        pos = 2;
        parentId = ipc.getId();

        //add subcategories
        String subCategoryName = "Kids" + (new Random().nextInt(10000));
//        pc.addSubCategory(parentId, subCategoryName, pos);
//        pc.addSubCategory(parentId, subCategoryName);
        EntityCategory category = new EntityCategory();
        category.setParentId(ipc.getId());
        category.setName(subCategoryName);
        category.setOrdinal(pos);
        category.setHasNext(Boolean.FALSE);
        pc.addSubCategory(category);

        subCategoryName = "Babies" + (new Random().nextInt(10000));
//        pc.addSubCategory(parentId, subCategoryName, pos);
        category = new EntityCategory();
        category.setParentId(ipc.getId());
        category.setName(subCategoryName);
        category.setOrdinal(pos);
        category.setHasNext(Boolean.FALSE);
        pc.addSubCategory(category);

        List<EntityCategory> subCategorys = pc.getSubCategories(ipc.getId());

        for (EntityCategory subCategory : subCategorys) {
            assertEquals(ipc.getId(), subCategory.getParentId());
        }

        // updating a category name
        pc.updateCategoryName(ipc.getId(), updatedName);
        EntityCategory updatedIPC = pc.getCategoryById(ipc.getId());
        assertEquals(updatedName, updatedIPC.getName());

        // updating a category position
        pc.updateCategoryPosition(ipc.getId(), 3);
        updatedIPC = pc.getCategoryById(updatedIPC.getId());
        assertEquals(3, updatedIPC.getPosition());

        // updating a sub category to root category
        EntityCategory ipc2 = getCategory(subCategoryName);
        pc.updateCategoryToRoot(ipc2.getId());
        updatedIPC = pc.getCategoryById(ipc2.getId());
        assertEquals(updatedIPC.getParentId(), 0);

        pc.deleteCategory(ipc.getId());
        pc.deleteCategory(ipc2.getId());
    }

    @Ignore
    @Test
    public void Test3Deletes() throws Exception {
        //setting up a category
        CategoryRepositoryImpl pc = new CategoryRepositoryImpl();
        int parentId = 0;
        int pos = 0;

//        pc.addSubCategory(parentId, name, pos);
        EntityCategory category = new EntityCategory();
        category.setParentId(parentId);
        category.setName(name);
        category.setOrdinal(pos);
        category.setHasNext(Boolean.FALSE);
        pc.addSubCategory(category);

        EntityCategory ipc = pc.getRootCategory(name);

        assertEquals(parentId, ipc.getParentId());
        assertEquals(name, ipc.getName());
        assertEquals(pos, ipc.getPosition());

        int id = ipc.getId();

        // deleting the category
        pc.deleteCategory(ipc.getId());

        try {
            ipc = pc.getRootCategory(updatedName);
            fail("Delete Isn't Successful!");
        } catch (Exception e) {
        }
    }

    @Ignore
//    @Test
    public void Test4AddAList() throws Exception {
        // set a new category information
        CategoryRepositoryImpl pc = new CategoryRepositoryImpl();

        name = "Category " + new Random().nextInt();
        pc.addRootCategory(name, img);
        EntityCategory ipc = pc.getRootCategory(name);

        final int parent = ipc.getId();
        int pos = 0;
        final String cat = "Category ";
        final Random rn = new Random();
        ArrayList<EntityCategory> iProductCategorys = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            EntityCategory e = new EntityCategory();
            e.setName(cat + rn.nextInt(100000));

            iProductCategorys.add(e);
        }

        //add the list
        pc.addCateogoryByList(iProductCategorys);

        pc.deleteCategory(ipc.getId());
    }

    /**
     * a method for getting category/subcategory information
     */
    public EntityCategory getCategory(String name) {

        try {
            CategoryRepositoryImpl categoryHierarchy = new CategoryRepositoryImpl();
            return categoryHierarchy.getRootCategory(name);
        } catch (Exception ex) {
            Logger.getLogger(CategoryRepositoryTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Ignore
    @Test
    public void testScrollCategories() throws Exception {
        CategoryRepositoryImpl pc = new CategoryRepositoryImpl();

        pc.addRootCategory("TestScrollCat", img);
        EntityCategory iProductCategory = pc.getRootCategory("TestScrollCat");
        for (int i = 0; i < 30; i++) {
//            pc.addSubCategory(iProductCategory.getId(), "TestScrollCat " + i);
            EntityCategory category = new EntityCategory();
            category.setParentId(iProductCategory.getId());
            category.setName("TestScrollCat " + i);
            category.setOrdinal(0);
            category.setHasNext(Boolean.FALSE);
            pc.addSubCategory(category);
        }
        System.out.println(pc.getSubCategories(iProductCategory.getId()).size());
        List<EntityCategory> categorys = pc.getCategoriesByScroll(iProductCategory.getId(), 0, 10, Scroll.DOWN);
        for (EntityCategory category : categorys) {
//            System.out.println(category.getName() + " --> " + category.getId());
        }
        System.out.println("====================================================");

        categorys = pc.getCategoriesByScroll(iProductCategory.getId(), categorys.get(categorys.size() - 1).getId(), 10, Scroll.DOWN);
        for (EntityCategory category : categorys) {
            System.out.println(category.getName() + " --> " + category.getId());
        }
        System.out.println("====================================================");
        categorys = pc.getCategoriesByScroll(iProductCategory.getId(), categorys.get(categorys.size() - 1).getId(), 10, Scroll.DOWN);
        for (EntityCategory category : categorys) {
            System.out.println(category.getName() + " --> " + category.getId());
        }
        System.out.println("====================================================");
        categorys = pc.getCategoriesByScroll(iProductCategory.getId(), categorys.get(0).getId(), 10, Scroll.UP);
        for (EntityCategory category : categorys) {
            System.out.println(category.getName() + " --> " + category.getId());
        }
        System.out.println("====================================================");

        pc.deleteCategory(iProductCategory.getId());
    }

    public int testAddRootCategory(String name, String image, int expectedResult) {
        System.out.println("addRootCategory");
        ObjectReturnDTO<Integer> addRootCategory = instance.addRootCategory(name, image);
        assertEquals(expectedResult, addRootCategory.getReasonCode());
        return addRootCategory.getValue();
    }

    public int testAddSubCategory(EntityCategory category, int expectedResult) {
        System.out.println("addSubCategory");
        ObjectReturnDTO<Integer> addSubCategory = instance.addSubCategory(category);
        assertEquals(expectedResult, addSubCategory.getReasonCode());
        return addSubCategory.getValue();
    }

    public void testUpdateCategoryName(int id, String newName, int expectedResult) {
        System.out.println("updateCategoryName");
        int result = instance.updateCategoryName(id, newName);
        assertEquals(expectedResult, result);
    }

    public void testUpdateCategoryImage(int id, String newImage, int expectedResult) {
        System.out.println("updateCategoryImage");
        int result = instance.updateCategoryImage(id, newImage);
        assertEquals(expectedResult, result);
    }

    public EntityCategory testGetCategoryById(int id) {
        System.out.println("getCategoryById");
        EntityCategory categoryById = instance.getCategoryById(id);
        assertNotNull(categoryById);
        return categoryById;
    }

    public static void testDeleteCategory(int id, boolean expResult) {
        System.out.println("deleteCategory");
        boolean result = instance.deleteCategory(id);
        assertEquals(expResult, result);
    }

    @Test
    public void testCategoryRepository() {
        String rooCategoryName = "root - " + System.currentTimeMillis();
        String rooCategoryImage = "img - " + System.currentTimeMillis();
        rootCategoryId = testAddRootCategory(rooCategoryName, rooCategoryImage, ReasonCode.NONE);
        testGetCategoryById(rootCategoryId);
        EntityCategory category = new EntityCategory();
        category.setName("sub - " + System.currentTimeMillis());
        category.setParentId(rootCategoryId);
        category.setImageUrl("img1");
        subCategoryId = testAddSubCategory(category, ReasonCode.NONE);
        String updateName = "upsub - " + System.currentTimeMillis();
        String updateImage = "upimg1";
        testUpdateCategoryName(subCategoryId, updateName, ReasonCode.NONE);
        testUpdateCategoryImage(subCategoryId, updateImage, ReasonCode.NONE);
        category = testGetCategoryById(subCategoryId);
        assertEquals(category.getName(), updateName);
        assertEquals(category.getImageUrl(), updateImage);
    }
}
