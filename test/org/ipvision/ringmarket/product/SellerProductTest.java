/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product;

import org.ipvision.ringmarket.product.seller.SellerProduct;
import java.util.List;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.EntitySellerProduct;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Imran
 */
public class SellerProductTest {
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testSellerProduct(){
        SellerProduct sellerProduct = new SellerProduct();
        for (int i = 0; i < 20; i++) {
            sellerProduct.addNewProduct( 1052l, "TestProduct " + i, System.currentTimeMillis(),"Prod Name", "Prod Image");            
        }
        
                
        List<EntitySellerProduct> iSellerProducts = sellerProduct.getProductList(1052l, "", Scroll.DOWN, 5);
        
        for (EntitySellerProduct iSellerProduct : iSellerProducts) {
            System.out.println(iSellerProduct.getProductId() + " --> " + iSellerProduct.getAddedDate());
        }
        System.out.println("====================================================");
        
        iSellerProducts = sellerProduct.getProductList(1052l, iSellerProducts.get(iSellerProducts.size() - 1).getProductId(), Scroll.DOWN, 5);
        for (EntitySellerProduct iSellerProduct : iSellerProducts) {
            System.out.println(iSellerProduct.getProductId() + " --> " + iSellerProduct.getAddedDate());
        }
        System.out.println("====================================================");
        iSellerProducts = sellerProduct.getProductList(1052l, iSellerProducts.get(iSellerProducts.size() - 1).getProductId(), Scroll.DOWN, 5);
        for (EntitySellerProduct iSellerProduct : iSellerProducts) {
            System.out.println(iSellerProduct.getProductId() + " --> " + iSellerProduct.getAddedDate());
        }
        System.out.println("====================================================");
        iSellerProducts = sellerProduct.getProductList(1052l, iSellerProducts.get(iSellerProducts.size() - 1).getProductId(), Scroll.DOWN, 5);
        for (EntitySellerProduct iSellerProduct : iSellerProducts) {
            System.out.println(iSellerProduct.getProductId() + " --> " + iSellerProduct.getAddedDate());
        }
        System.out.println("====================================================");

        iSellerProducts = sellerProduct.getProductList(1052l, iSellerProducts.get(0).getProductId(), Scroll.UP, 5);
        for (EntitySellerProduct iSellerProduct : iSellerProducts) {
            System.out.println(iSellerProduct.getProductId() + " --> " + iSellerProduct.getAddedDate());
        }
        System.out.println("====================================================");
        
        iSellerProducts = sellerProduct.getProductList(1052l, "", Scroll.DOWN, 1000);
        for (EntitySellerProduct iSellerProduct : iSellerProducts) {
            sellerProduct.deleteProduct(iSellerProduct.getProductId());
        }
    }

}
