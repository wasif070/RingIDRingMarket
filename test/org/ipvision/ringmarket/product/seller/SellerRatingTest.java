/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.ringmarket.product.seller;

import java.util.Map;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.entities.EntitySellerRating;
import org.ipvision.ringmarket.entities.EntitySellerRatingLog;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.repositories.seller.SellerRatingLogRepository;
import org.ipvision.ringmarket.utils.ListReturnDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author saikat
 */
public class SellerRatingTest {

    public SellerRatingTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSellerRating() {
        long buyerId = 840;
        ListReturnDTO<EntityShop> stores = MarketRepository.INSTANCE.getShopRepository().getStores(0, 1, Scroll.DOWN , true);
        long id = stores.getList().get(0).getId();
        testUpdateSellerRating(id, buyerId, (byte) 2);
        testGetRating(id);
        testGetRatingDetails(id);
        testFindRatingByBuyer(id, buyerId, (byte) 2);
        testRemoveSellerRating(id, buyerId);
    }

    /**
     * Test of updateSellerRating method, of class SellerRating.
     */
    public void testUpdateSellerRating(long sellerId, long buyerId, byte rating) {
        System.out.println("updateSellerRating");
        SellerRating instance = new SellerRating();
        boolean expResult = true;
        boolean result = instance.updateSellerRating(sellerId, buyerId, rating);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeSellerRating method, of class SellerRating.
     */
    public void testRemoveSellerRating(long sellerId, long buyerId) {
        System.out.println("removeSellerRating");
        SellerRating instance = new SellerRating();
        boolean expResult = true;
        boolean result = instance.removeSellerRating(sellerId, buyerId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRating method, of class SellerRating.
     */
    public void testGetRating(long sellerId) {
        System.out.println("getRating");
        SellerRating instance = new SellerRating();
        EntitySellerRating result = instance.getRating(sellerId);
        assertNotNull(result);
    }

    /**
     * Test of findRatingByBuyer method, of class SellerRating.
     */
    public void testFindRatingByBuyer(long sellerId, long buyerId, byte expResult) {
        System.out.println("findRatingByBuyer");
        SellerRating instance = new SellerRating();
        byte result = instance.findRatingByBuyer(sellerId, buyerId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRatingDetails method, of class SellerRatingLogRepositoryImpl.
     */
    public void testGetRatingDetails(long sellerId) {
        System.out.println("getRatingDetails");
        SellerRatingLogRepository instance = new SellerRatingLogRepositoryImpl();
        Map<Byte, Long> ratingDetails = instance.getRatingDetails(sellerId);
        assertEquals(false, ratingDetails.isEmpty());
    }

}
