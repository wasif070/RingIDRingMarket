
import java.util.Random;
import java.util.UUID;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.entities.product.EntityProduct;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Imran
 */
public class AddProduct {
    public static void main(String[] args) {
        EntityProduct iProduct = new EntityProduct();
        iProduct.setId(UUID.randomUUID().toString());
        iProduct.setName("TEEKJHKjkrjWER");
//        iProduct.setIsFixedPrice(false);
        iProduct.setIsExclusive(Boolean.TRUE);
        iProduct.setPrice(new Random().nextInt(100));
        iProduct.setLat(23.56f);
        iProduct.setLon(90.85f);
        iProduct.setImageUrl(UUID.randomUUID().toString().substring(15, 30));
        iProduct.setCreationTime(System.currentTimeMillis());
//        iProduct.setCategoryId(1);
//        iProduct.setViewCount(new Random().nextInt(100));
        iProduct.setStatus(ProductStatus.SOLD_OUT);
        org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl product = new org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl();
        product.addProduct(iProduct);
    }
}
