
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.ipvision.ringmarket.constants.Scroll;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.repositories.MarketRepository;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.entities.shop.EntityShop;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.repositories.shop.ShopRepositoryImpl;
import org.ipvision.ringmarket.utils.TimeUUID;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bose
 */
public class ProductDataMigration {

    public static void main(String[] args) {
        ShopRepositoryImpl repositoryImpl = new ShopRepositoryImpl();
        List<EntityShop> brands = repositoryImpl.getBrands(0, 10, Scroll.DOWN , true).getList();
        List<EntityShop> stores = repositoryImpl.getStores(0, 10, Scroll.DOWN , true).getList();

        List<EntityShop> allStores = brands;
        allStores.addAll(stores);

        System.out.println("no of stores : " + allStores.size());

        ProductRepositoryImpl productRepositoryImpl = new ProductRepositoryImpl();
        List<EntityProduct> allProducts = productRepositoryImpl.getAllProducts();
        int index = 0;

        int batchSize = 150;
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            for (EntityProduct _item : allProducts) {

                if (index > 0 && index % batchSize == 0) {
                    entityManager.flush();
                    entityManager.clear();
                }
                EntityShop shop = allStores.get(index % allStores.size());
                _item.setShopId(shop.getId());
                _item.setShopName(shop.getName());
                _item.setCreationTime(System.currentTimeMillis());
                entityManager.merge(_item);
                index++;
            }

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction.isActive()) {
                transaction.rollback();
            }
        }
        }
finally{
entityManager.close();
}
    }
}
