
import java.io.IOException;
import java.util.Random;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.utils.TimeUUID;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HTMLParserShop {

    static int curIdx = 0;
    static int isFeatured = 0;
    static float[] lat = new float[10];
    static float[] lon = new float[10];

    public static void setLatLon() {
        lat[0] = (float) 23.51;
        lon[0] = (float) 90.35;
        lat[1] = (float) 23.62;
        lon[1] = (float) 90.36;
        lat[2] = (float) 23.73;
        lon[2] = (float) 90.37;
        lat[3] = (float) 23.84;
        lon[3] = (float) 90.38;
        lat[4] = (float) 23.95;
        lon[4] = (float) 90.39;
        lat[5] = (float) 23.46;
        lon[5] = (float) 90.40;
        lat[6] = (float) 23.37;
        lon[6] = (float) 90.39;
        lat[7] = (float) 23.28;
        lon[7] = (float) 90.38;
        lat[8] = (float) 23.19;
        lon[8] = (float) 90.37;
        lat[9] = (float) 23.08;
        lon[9] = (float) 90.36;
    }

    public static void getDetails(String linkString) throws IOException {
        Document doc1 = Jsoup.connect(linkString).get();
//        ProductImage productImage = new ProductImage();
//        Product product = new Product();
//        CategoryProduct categoryProduct = new CategoryProduct();
//        SellerProduct sellerProduct = new SellerProduct();
//        UserRole userRole = new UserRole();
        
        System.out.println("Title: " + doc1.title());

        String parseName = "";
        //name
        Elements links1 = doc1.select("span[class=bwcProductTitle]");
        for (Element link : links1) {
            String name = link.text();
            for (int i = 0; i < name.length(); i++) {
                if ((int) name.charAt(i) < 256) {
                    parseName += name.charAt(i);
                }
            }
            System.out.println("Name: " + parseName);
        }
        EntityProduct iProduct = addProduct(parseName);
        //images
        links1 = doc1.select("img[src]");
        for (Element link : links1) {
            System.out.println(link.attributes().get("src"));
//            productImage.addImage(url, 330, 330, iProduct.getId());
//            iProduct.setImageUrl(url);
//            iProduct.setImageHeight(330);
//            iProduct.setImageWidth(330);
        }
//        //adding product to product table
////        product.addProduct(iProduct);
//        //categories
//        links1 = doc1.select("a[href]");
//        for (Element link : links1) {
//            String linkStr = link.attr("href");
//            if (linkStr.contains("https://global.rakuten.com/en/category/")) {
//                String temp[] = linkStr.split("/");
//                int id1 = Integer.parseInt(temp[5]);
//                System.out.println(link.text() + " --> " + id1);
////                categoryProduct.addCategoryProduct(id1, iProduct.getId());
//            }
//        }
//
        //adding product to seller_product
//        List<IUserRole> isellers = (List<IUserRole>) userRole.getUsersByRole(RingmarketRole.SELLER);
//        Session session = DBConnection.getInstance().getSession();
//        curIdx = curIdx % isellers.size();
//        sellerProduct.addNewProduct(session, isellers.get(curIdx).getId(), iProduct.getId(), System.currentTimeMillis());
//        curIdx++;
//        session.close();

        //adding product details
//        ProductDetail productDetail = new ProductDetail();
//        productDetail.addProductDetail(iProduct.getId(), parseName, (int) System.currentTimeMillis() % 20, (int) System.currentTimeMillis() % 5);
    }

    public static void main(String[] args) throws Exception {
//        setLatLon();
        Document doc, doc2;
        try {
            doc2 = Jsoup.connect("http://techrabbit.shop.rakuten.com/").get();
            Elements links2 = doc2.select("div[class=productInfo]"). select("a[href]");
            for (Element element : links2) {
                String linkString = element.attr("href");
                System.out.println(linkString);
                getDetails(linkString);
//                doc = Jsoup.connect(linkString).get();
            }
        }
        catch (Exception ex) {

        }
    }

    public static EntityProduct addProduct(String name) {
        ++isFeatured;
        isFeatured %= 5;
        EntityProduct iProduct = new EntityProduct();
        iProduct.setId(TimeUUID.timeBased().toString());
        iProduct.setName(name);
//        iProduct.setIsFixedPrice(false);
        iProduct.setIsExclusive(isFeatured == 0);
        iProduct.setPrice(new Random().nextInt(1000));
        iProduct.setLat(lat[isFeatured]);
        iProduct.setLon(lon[isFeatured]);
//        iProduct.setViewCount(0);
        iProduct.setStatus(ProductStatus.AVAILABLE.getValue());
        return iProduct;
    }

}
