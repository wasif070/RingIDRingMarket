
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.ipvision.ringmarket.db.DBConnection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author saikat
 */
public class ImageCloudParse {

    public static void main(String[] args) {
        try {
//            File file = new File("/home/saikat/NetBeansProjects/RingMarketGroup/ringmarket/imageCloud");
            File file = new File("/home/saikat/converted_image_url.sql");

            FileWriter fileWriter = new FileWriter("/home/saikat/converted_image_url_final.sql");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            Scanner s = new Scanner(file);
            String line = s.nextLine();
//            while (s.hasNextLine()) {
//                String[] split = line.split("set");
//                String updateString = "update product set" + split[1] + " where " + split[0]+";";
//                System.out.println(updateString);
//                line = s.nextLine();
//            }
            int count = 1;
            while (s.hasNextLine()) {
                if (!line.trim().isEmpty()) {
                    EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
                    try {
                        entityManager.getTransaction().begin();
                        Query query = entityManager.createNativeQuery(line);
                        query.executeUpdate();
                        entityManager.getTransaction().commit();
                        if (count % 20 == 0) {
                            Thread.sleep(2000);
                        }
                        printWriter.println(line);
                        count++;
                    } catch (Exception e) {
                        System.err.println("failed -- > " + line);
                        e.printStackTrace();
                        entityManager.getTransaction().rollback();
                    }
                    }
finally{
entityManager.close();
}
                }
                line = s.nextLine();
            }
            printWriter.close();

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
