
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.constants.ProductStatus;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author saikat
 */
public class PrimaryKeyIndexTest {

    private static List<String> productIds = new ArrayList<>();

    @BeforeClass
    public static void setUpClass() {
        EntityManager session = DBConnection.getInstance().getEntityManager();
        EntityProduct newInstance = new  EntityProduct();
        session.getTransaction().begin();

        for (int i = 0; i < 100; i++) {
            newInstance.setId(UUID.randomUUID().toString());
            newInstance.setPrice(i);
//            newInstance.setCategoryId(1);
            newInstance.setImageUrl(newInstance.getId() + "img");
            newInstance.setName("P" + i);
            newInstance.setStatus(ProductStatus.AVAILABLE.getValue());
//            newInstance.setViewCount(i + 1);
            session.persist(newInstance);
            productIds.add(newInstance.getId());
            if (i % 500 == 0) {
                session.getTransaction().commit();
                session.getTransaction().begin();
            }
        }

        session.getTransaction().commit();
        session.close();

    }

    @AfterClass
    public static void tearDownClass() {
        productIds.clear();
    }

//    @Test
//    public void selectByQuery() {
//        System.out.println("");
//        Session session = DBConnection.getInstance().getSession();
//        
//        QueryBuilder builder = session.getQueryBuilder();
//        QueryDomainType<IProduct> domain = builder.createQueryDefinition(IProduct.class);
//        domain.where(domain.get("id").equal(domain.param("id")));
//        Query<IProduct> query = session.createQuery(domain);
//        
//        long startTime = System.currentTimeMillis();
//        for (String productId : productIds) {
//
//            query.setParameter("id", productId);
//            List<IProduct> resultList = query.getResultList();
////            System.out.println(resultList.get(0).getId());
//        }
//        System.out.println("PrimaryKeyIndexTest.selectByQuery() Running time : " + (System.currentTimeMillis() - startTime));
//
//        session.close();
//    }
//
//    @Test
//    public void selectByFind() {
//        System.out.println("");
//        Session session = DBConnection.getInstance().getSession();
//        long startTime = System.currentTimeMillis();
//        for (String productId : productIds) {
//            IProduct find = session.find(IProduct.class, productId);
////            System.out.println(find.getId());
//        }
//        System.out.println("PrimaryKeyIndexTest.selectByFind() Running time : " + (System.currentTimeMillis() - startTime));
//        session.close();
//    }

}
