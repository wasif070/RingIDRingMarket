
import java.util.List;
import javax.persistence.EntityManager;
import org.ipvision.ringmarket.db.DBConnection;
import org.ipvision.ringmarket.entities.product.EntityProduct;
import org.ipvision.ringmarket.repositories.product.ProductRepository;
import org.ipvision.ringmarket.repositories.product.ProductRepositoryImpl;
import org.ipvision.ringmarket.uniqueid.UniqueNumberGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author saikat
 */
public class ProductCodeAdd {

    public static void main(String[] args) {
        ProductRepositoryImpl impl = new ProductRepositoryImpl();
        List<EntityProduct> allProducts = impl.getAllProducts();
        EntityManager entityManager = DBConnection.getInstance().getEntityManager();
        try {
        entityManager.getTransaction().begin();
        int i = 0;
        for (EntityProduct entityProduct : allProducts) {
            entityProduct.setCode(String.valueOf(UniqueNumberGenerator.generateBarcodeId()));
            if (i > 0 && i % DBConnection.getInstance().BATCH_SIZE == 0) {
                entityManager.getTransaction().commit();
                entityManager.getTransaction().begin();
                entityManager.clear();
            }
            entityManager.merge(entityProduct);
            i++;
        }
        entityManager.getTransaction().commit();
        }
finally{
entityManager.close();
}
    }
}
