use ringmarket;

-- ALTER TABLE shop ADD owner_id bigint(20) NOT NULL DEFAULT 0;
-- alter table shop add index idx_shop_owner (owner_id) using btree;
-- 
-- alter table order_list drop index idx_order_list_added_time;
-- alter table order_list add index `idx_orders_add_tm_id` (`added_time`,`id`);

-- alter table product add trending bit(1) DEFAULT b'0';

--  alter table order_list add index `idx_order_list_shop` (`shop_id`) using BTREE;

--  CREATE TABLE `product_discount` (
--   `product_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
--   `coupon_code` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
--   `creation_time` bigint(20) NOT NULL DEFAULT '0',
--   `discount_unit` tinyint(1) NOT NULL,
--   `discount_value` float NOT NULL DEFAULT '0',
--   `is_redeem_allowed` bit(1) NOT NULL DEFAULT b'0',
--   `maximum_discount_amount` double NOT NULL DEFAULT '0',
--   `minimum_order_value` int(11) DEFAULT '0',
--   `validity_time` bigint(20) NOT NULL DEFAULT '0',
--   PRIMARY KEY (`product_id`)
-- ) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ;
-- 
-- ALTER TABLE `product` add `is_discounted` bit(1) DEFAULT b'0';
-- 
-- ALTER TABLE `product_discount` add `active_time` bigint(20) NOT NULL DEFAULT '0';
-- 
-- 
-- alter table user_cart_item drop column product_price ;
-- alter table user_cart_item drop column product_name ;
-- alter table user_cart_item drop column shop_name;
-- alter table user_cart_item drop column currency_code ;


-- CREATE FULLTEXT INDEX f_idx_product_name ON product (name);
-- alter table category_product add column prod_en bit(1) DEFAULT b'1';
-- alter table basket_category_product add column prod_en bit(1) DEFAULT b'1';
-- alter table user_cart_item add column prod_en bit(1) DEFAULT b'1';
-- 
-- alter table category modify column imageUrl varchar(200) NOT NULL;
-- alter table shop modify column imageUrl varchar(200) NOT NULL;
alter table basket_category_product add index `idx_basket_category_product_id` (`product_id`);