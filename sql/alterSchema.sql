/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  saikat
 * Created: Jul 23, 2017
 */
use ringmarket;

ALTER TABLE saved_product
ADD COLUMN product_name varchar(40) NOT NULL AFTER buyer_id, 
ADD COLUMN product_img_url varchar(256) DEFAULT NULL AFTER product_name, 
ADD COLUMN product_img_height int(5) NOT NULL DEFAULT 0 AFTER product_img_url, 
ADD COLUMN product_img_width int(5) NOT NULL DEFAULT 0 AFTER product_img_height; 
